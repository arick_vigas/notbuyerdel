package com.notibuyer.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.stetho.Stetho;
import com.notibuyer.app.dagger.AppComponent;
import com.notibuyer.app.dagger.AppModule;
import com.notibuyer.app.dagger.DaggerAppComponent;
import com.notibuyer.app.service.ShakeService;

import net.danlew.android.joda.JodaTimeAndroid;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends Application {

    @Inject
    protected Prefs prefs;
    private AppComponent appComponent;

    public static AppComponent getAppComponent(Context context) {
        return ((App) context.getApplicationContext()).appComponent;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        appComponent.inject(this);
        FacebookSdk.sdkInitialize(this);

        if (prefs.isShakeEnabled()) ShakeService.startService(this);
//        SpeechActivationService.startService(this, "Noti");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/HelveticaNeueCyr-Medium_0.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        Stetho.initializeWithDefaults(this);
        JodaTimeAndroid.init(this);

        getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
                .putBoolean("isFirstRun", true).apply();
    }

}