package com.notibuyer.app;

public class Constants {
    public static final String SERVER_CLIENT_ID = "209070845607-1has7elsbgnrirhdrv9534k8s4tmv47e.apps.googleusercontent.com";
    public static final String BASE_URL = "http://notibuyer.pafnuty.net/";

    public static final float DEFAULT_LINE_MARGIN = 5;
    public static final float DEFAULT_TAG_MARGIN = 2;
    public static final float DEFAULT_TAG_TEXT_PADDING_LEFT = 8;
    public static final float DEFAULT_TAG_TEXT_PADDING_TOP = 5;
    public static final float DEFAULT_TAG_TEXT_PADDING_RIGHT = 8;
    public static final float DEFAULT_TAG_TEXT_PADDING_BOTTOM = 5;
}