package com.notibuyer.app;

import android.content.Context;
import android.content.SharedPreferences;

public class PersistencePrefs {

    private static final String EMAIL = "email";
    private final SharedPreferences preferences;

    public PersistencePrefs(Context context) {
        preferences = context.getSharedPreferences("OtherPrefs", Context.MODE_PRIVATE);
    }

    public String getLastEmail() {
        return preferences.getString(EMAIL, "");
    }

    public void setLastEmail(String email) {
        preferences.edit().putString(EMAIL, email).apply();
    }

}
