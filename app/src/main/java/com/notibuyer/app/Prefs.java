package com.notibuyer.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

public class Prefs {

    private static final String USER = "user";
    private static final String FIRST_TIME_LAUNCH = "first_time_launch";
    private static final String TOKEN = "token";
    private static final String TASKS_SYNC_TIME = "tasks_sync_time";
    private static final String SHAKE = "shake";
    private static final String VOICE = "voice";
    private static final String RECORD_ON_START = "record_on_start";
    private static final String LAST_VISITED_CIRCLE = "last_visited_circle";
    private static final String SKU = "current_subscribe";
    private static final String PUSH_TOKEN = "push_token";
    private static final String LAST_NOTIFICATIONS_COUNT = "notifications_count";
    private static final String FB_LOGIN = "fb_login";
    private static final String GOOGLE_LOGIN = "google_login";
    private static final String PHONE = "phone";
    private static final String LAST_CIRCLES_COUNT = "last_circles_count";
    private static final String LAST_FRIENDS_COUNT = "last_friends_count";
    private static final String ON_LAUNCH = "on_launch";
    private static final String WAS_LAUNCHED = "was_launched";
    private final SharedPreferences preferences;



    public Prefs(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);

    }

    public boolean isWasLaunched() {
        return preferences.getBoolean(WAS_LAUNCHED, false);
    }

    public void setWasLaunched(boolean wasLaunched) {
        preferences.edit().putBoolean(WAS_LAUNCHED, wasLaunched).apply();
    }

    public boolean isOnLaunch() {
        return preferences.getBoolean(ON_LAUNCH, false);
    }

    public void setOnLaunch(boolean onLaunch) {
        preferences.edit().putBoolean(ON_LAUNCH, onLaunch).apply();
    }

    public boolean isFirstTimeLaunch() {
        return preferences.getBoolean(FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeLaunch(boolean firstTimeLaunch) {
        preferences.edit().putBoolean(FIRST_TIME_LAUNCH, firstTimeLaunch).apply();
    }

    public String getCurrentSubscribe() {
        return preferences.getString(SKU, null);
    }

    public void setCurrentSubscribe(String sku) {

        preferences.edit().putString(SKU, sku).apply();
    }

    @Nullable
    public String getAccessToken() {
        return preferences.getString(TOKEN, null);
    }

    public void setAccessToken(String token) {
        preferences.edit().putString(TOKEN, token).apply();
    }

    public long getLastTimeTasksSynced() {
        return preferences.getLong(TASKS_SYNC_TIME, 0);
    }

    public void setLastTimeTasksSynced(long timestamp) {
        preferences.edit().putLong(TASKS_SYNC_TIME, timestamp).apply();
    }

    public boolean isShakeEnabled() {
        return preferences.getBoolean(SHAKE, false);
    }

    public void setShakeEnabled(boolean enabled) {
        preferences.edit().putBoolean(SHAKE, enabled).apply();
    }

    public boolean isVoiceEnabled() {
        return preferences.getBoolean(VOICE, false);
    }

    public void setVoiceEnabled(boolean enabled) {
        preferences.edit().putBoolean(VOICE, enabled).apply();
    }

    public boolean isAutoRecordEnabled() {
        return preferences.getBoolean(RECORD_ON_START, false);
    }

    public void setAutoRecordEnabled(boolean enabled) {
        preferences.edit().putBoolean(RECORD_ON_START, enabled).apply();
    }

    public int getLastNotificationsCount() {
        return preferences.getInt(LAST_NOTIFICATIONS_COUNT, 0);
    }

    public void setLastNotificationsCount(int count) {
        preferences.edit().putInt(LAST_NOTIFICATIONS_COUNT, count).apply();
    }

    public int getLastFriendsCount() {
        return preferences.getInt(LAST_FRIENDS_COUNT, 0);
    }

    public void setLastFriendsCount(int count) {
        preferences.edit().putInt(LAST_FRIENDS_COUNT, count).apply();
    }

    public String getLastVisitedCircle() {
        return preferences.getString(LAST_VISITED_CIRCLE, "");
    }

    public void setLastVisitedCircle(String id) {
        preferences.edit().putString(LAST_VISITED_CIRCLE, id).apply();
    }

    public String getGcmToken() {
        return preferences.getString(PUSH_TOKEN, "");
    }

    public void saveGcmToken(String gcmToken) {
        preferences.edit().putString(PUSH_TOKEN, gcmToken).apply();
    }

    public boolean isLoginWithFb() {
        return preferences.getBoolean(FB_LOGIN, false);
    }

    public void setLoginWithFb() {
        preferences.edit().putBoolean(FB_LOGIN, true).apply();
    }

    public boolean isLoginWithGoogle() {
        return preferences.getBoolean(GOOGLE_LOGIN, false);
    }

    public void setLoginWithGoogle() {
        preferences.edit().putBoolean(GOOGLE_LOGIN, true).apply();
    }

    public String getTempPhone() {
        return preferences.getString(PHONE, "");
    }

    public void setTempPhone(String phone) {
        preferences.edit().putString(PHONE, phone).apply();
    }

    public void clearAll() {
        preferences.edit().clear().apply();
    }
}