package com.notibuyer.app;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.notibuyer.app.interfaces.LoadMoreListener;

public class ScrollListener extends RecyclerView.OnScrollListener {

    private LoadMoreListener loadMoreListener;
    private boolean loading = false;
    private int previousTotal = 0;
    private int visibleThreshold = 2;
    int firstVisibleItem, visibleItemCount, totalItemCount, currentPage = 1;


    public ScrollListener(LoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = recyclerView.getAdapter().getItemCount();
        firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }
        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
            currentPage++;
            loadMoreListener.loadMore(currentPage);
            loading = true;
        }
    }
}