package com.notibuyer.app.adapter;

import android.widget.CompoundButton;

import com.notibuyer.app.model.Task;

import java.util.List;

public abstract class AbstractExpandableDataProvider {

    public static abstract class BaseData {
        public abstract void setPinned(boolean pinned);
        public abstract boolean isPinned();
    }

    public static abstract class GroupData extends BaseData {
        public abstract boolean isSectionHeader();
        public abstract long getGroupId();
        public abstract String getText();
    }

    public static abstract class ChildData {
        public abstract long getChildId();
        public abstract Task getTask();
        public abstract void setPinnedState(int pinnedState);
        public abstract int getPinnedState();
        public abstract void setChecked(boolean isChecked);
        public abstract void setCheckbox(CompoundButton checkbox);
    }

    public abstract void setUp(List<Task> taskList);

    public abstract void filter(CharSequence charSequence);

    public abstract int getGroupCount();
    public abstract int getChildCount(int groupPosition);

    public abstract GroupData getGroupItem(int groupPosition);
    public abstract ChildData getChildItem(int groupPosition, int childPosition);

    public abstract void moveChildItem(int fromGroupPosition, int fromChildPosition, int toGroupPosition, int toChildPosition);

    public abstract void moveToCompleted(int fromPosition);
    public abstract void moveToCurrent(int fromPosition);

    public abstract void removeChildItem(int groupPosition, int childPosition);
    public abstract void unCheckAll();
}
