package com.notibuyer.app.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemViewHolder;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableSwipeableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultAction;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionDefault;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionMoveToSwipedDirection;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionRemoveItem;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableSwipeableItemViewHolder;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.notibuyer.app.R;
import com.notibuyer.app.helper.TagCircleFactory;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.model.Task;
import com.notibuyer.app.ui.activity.MainActivity;
import com.notibuyer.app.ui.activity.SearchActivity;
import com.notibuyer.app.ui.activity.TaskViewActivity;
import com.notibuyer.app.ui.view.CircleListLayout;
import com.notibuyer.app.ui.view.ExpandableItemIndicator;
import com.notibuyer.app.ui.view.TagCircleView;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.UIUtils;
import com.notibuyer.app.utils.Utils;
import com.notibuyer.app.utils.ViewUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ArchiveTaskAdapter extends RecyclerView.Adapter<ArchiveTaskAdapter.ArchiveTaskHolder>{

    private List<Task> tasks = new ArrayList<>();
    private List<Task> filteredTasks = new ArrayList<>();
    private List<Task> savedTasks = new ArrayList<>();
    private DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private static final String TAG = "archiveTasksAdapter";
    private boolean completed;
    private Context context;


    public ArchiveTaskAdapter(Context context, boolean completed){
        this.context = context;
        this.completed = completed;
    }

    @Override
    public ArchiveTaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ArchiveTaskHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.arch_task_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(ArchiveTaskHolder holder, int position) {
        Task task = tasks.get(position);
        holder.title.setText(task.getText());
        holder.mContainer.setOnClickListener(v3 -> TaskViewActivity.start(context, task.getFbKey()));
        holder.mContainer.setOnLongClickListener(v2 -> {
            DialogFactory.getConfirmationDialog((MainActivity)context, R.string.remove_task, (dialog, which) -> {
                databaseReference.child("users").child(mFirebaseUser.getUid()).child("tasks").child(task.getFbKey()).removeValue();
                databaseReference.child("tasks").child(task.getFbKey()).removeValue();
            }).show();
            return true;
        });
        holder.date.setText(Utils.getFormattedDate(holder.itemView.getContext(), new Date(task.getUpdated())));
        holder.search.setOnClickListener(v -> {
            if(!TextUtils.isEmpty(task.getImageUrl())) {
                DialogFactory.getSomeDialog(context, R.string.choose_action, R.string.text, R.string.image,
                        (dialog, which) -> {
                            SearchActivity.searchText((MainActivity)context, task.getText());
                        }, (dialog, which) -> {
                            SearchActivity.searchImage((MainActivity)context, task.getImageUrl());
                        }).show();
                holder.taskImage.setOnClickListener(v1 -> {
                    SearchActivity.searchImage((MainActivity)context, task.getImageUrl());
                });
            } else {
                SearchActivity.searchText((MainActivity)context, task.getText());
            }
        });

        databaseReference.child("users").child(task.getOwnerId()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                holder.name.setText((String)dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
        if (task.isCompleted()) {
            if(task.getCompletedBy()!=null){
                databaseReference.child("users").child(task.getCompletedBy()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        holder.completedBy.setVisibility(View.VISIBLE);
                        holder.resolverName.setVisibility(View.VISIBLE);
                        holder.resolverName.setText((String)dataSnapshot.getValue());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            }
        } else {
            holder.completedBy.setVisibility(View.GONE);
            holder.resolverName.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(task.getImageUrl())) {
            holder.taskImage.setVisibility(View.VISIBLE);
            Picasso.with(holder.itemView.getContext()).load(task.getImageUrl()) .placeholder( R.drawable.progress_loading_animation).fit().centerCrop().into(holder.taskImage);
        } else {
            holder.taskImage.setVisibility(View.GONE);
            holder.taskImage.setImageBitmap(null);
        }

        databaseReference.child("tasks").child(task.getFbKey()).child("circles").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<String> circles = new ArrayList<>();
                List<Circle> taskCircles = new ArrayList();
                for(DataSnapshot circleId:dataSnapshot.getChildren()){
                    circles.add(circleId.getKey());
                    databaseReference.child("circles").child(circleId.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot data) {
                            Circle circle = new Circle();
                            circle.setFbKey(data.getKey());
                            circle.setName((String) data.child("name").getValue());
                            circle.setColor((String) data.child("color").getValue());
                            if (!circle.getName().equals("Private")){
                                taskCircles.add(circle);
                                Collections.sort(taskCircles, ((lhs, rhs) -> lhs.getName().compareTo(rhs.getName())));
                                TagCircleFactory circleViewFactory = new TagCircleFactory();
                                List<TagCircleView> circleViews = circleViewFactory.createCircles(holder.circleListLayout.getContext(), taskCircles);
                                holder.circleListLayout.addCircles(circleViews);
                                holder.circleListLayout.setUp();
                                holder.circleListLayout.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e("TasksAdapter", databaseError.getMessage());
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG,databaseError.getMessage());
            }
        });
        holder.checkedOverlay.setVisibility(task.isCompleted() ? View.VISIBLE : View.GONE);
        holder.title.setPaintFlags(task.isCompleted() ?
                holder.title.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG :
                holder.title.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        //holder.search.setOnClickListener(v -> getEventListener().onSearchClick(groupPosition, childPosition));
        //holder.taskImage.setOnClickListener(v -> getEventListener().onImageClick(groupPosition, childPosition));

    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public void addItem(String id){
        databaseReference.child("tasks").child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Task task = new Task();
                task.setOwnerId((String)dataSnapshot.child("ownerId").getValue());
                task.setCompleted((Boolean)dataSnapshot.child("completed").getValue());
                task.setArchived((Boolean)dataSnapshot.child("archived").getValue());
                task.setText((String)dataSnapshot.child("text").getValue());
                task.setUpdated((Long)dataSnapshot.child("updated").getValue());
                if(dataSnapshot.hasChild("imageUrl"))
                    task.setImageUrl((String)dataSnapshot.child("imageUrl").getValue());
                if(dataSnapshot.hasChild("completedBy"))
                    task.setCompletedBy((String)dataSnapshot.child("completedBy").getValue());
                if(dataSnapshot.hasChild("resolvedBy"))
                    task.setResolvedBy((String)dataSnapshot.child("resolvedBy").getValue());
                task.setCreated((Long)dataSnapshot.child("created").getValue());
                task.setFbKey(dataSnapshot.getKey());
                if(task.isArchived() && task.isCompleted() == ArchiveTaskAdapter.this.completed) {
                    tasks.add(0, task);
                    notifyItemInserted(0);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    public void removeItem(String id){
        int pos = -1;
        Task removeTask = null;
        for(Task current:tasks){
            if(current.getFbKey().equals(id)){
                pos = tasks.indexOf(current);
                removeTask = current;
                break;
            }
        }
        if(removeTask!=null){
            tasks.remove(removeTask);
            notifyItemRemoved(pos);
        }
    }

    public void filter(String query){
        filteredTasks.clear();
        if(savedTasks.size()==0) {
            savedTasks.addAll(tasks);
        } else {
            tasks.clear();
            tasks.addAll(savedTasks);
        }
        for(Task item:tasks){
            if(item.getText()!=null &&item.getText().toLowerCase().contains(query.toLowerCase())){
                filteredTasks.add(item);
            }
        }
        tasks.clear();
        tasks.addAll(filteredTasks);
        notifyDataSetChanged();
    }

    public static class ArchiveTaskHolder extends RecyclerView.ViewHolder  {

        @Bind(R.id.container) protected RelativeLayout mContainer;
        @Bind(R.id.picture) protected ImageView taskImage;
        @Bind(R.id.title) protected TextView title;
        @Bind(R.id.name) protected TextView name;
        @Bind(R.id.resolver_name) protected TextView resolverName;
        @Bind(R.id.completed_by) protected TextView completedBy;
        @Bind(R.id.date) protected TextView date;
        @Bind(R.id.checked_overlay) protected View checkedOverlay;
        @Bind(R.id.circles_list_layout) protected CircleListLayout circleListLayout;
        @Bind(R.id.search) protected ImageView search;

        public ArchiveTaskHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
