package com.notibuyer.app.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.model.Notification;
import com.notibuyer.app.model.User;
import com.notibuyer.app.network.FirebasePushService;
import com.notibuyer.app.network.firebase.CirclesService;
import com.notibuyer.app.ui.activity.UserProfileActivity;
import com.notibuyer.app.ui.view.CircleTransformation;
import com.notibuyer.app.utils.NotificationFactory;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CircleMembersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_USER = 0;
    private static final int TYPE_DIVIDER = 1;
    private List<User> notMembers = new ArrayList<>();
    private List<User> members = new ArrayList<>();
    private List<User> filter_notMembers = new ArrayList<>();
    private List<User> filter_members = new ArrayList<>();
    private List<User> cached_notMembers = new ArrayList<>();
    private List<User> cached_members = new ArrayList<>();
    private CirclesService circlesService = new CirclesService();
    private String circleId;
    private Context context;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;

    public CircleMembersAdapter(Context context, String circleId){
        this.circleId = circleId;
        this.context = context;


        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        //Set listener on user friends changes
        //on ADD/REMOVE friend


    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        System.out.println(viewType);
        if (viewType == TYPE_DIVIDER) {
            return new AddMoreFriendViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.add_more_friend_item, parent, false));
        } else {
            return new UserViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_in_circles, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        System.out.println("position : " + position);
        System.out.println("members.size() =" +  members.size());
        System.out.println("notMembers.size() = " + notMembers.size());

        if(getItemViewType(position)==TYPE_DIVIDER){
            AddMoreFriendViewHolder divider = (AddMoreFriendViewHolder) holder;
            if(notMembers.size()==0)
                divider.addMoreFriends.setVisibility(View.GONE);
            else
                divider.addMoreFriends.setVisibility(View.VISIBLE);
        }   else {

            UserViewHolder userViewHolder = (UserViewHolder) holder;

            String friendKey;
            if (position < members.size()) {
                friendKey = members.get(position).getFbKey();
                ((UserViewHolder) holder).avatar.setOnClickListener(v1 -> UserProfileActivity.start(context, friendKey, ""));
                ((UserViewHolder) holder).userName.setOnClickListener(v1 -> UserProfileActivity.start(context, friendKey, ""));
                userViewHolder.inCircle.setImageResource(R.drawable.ic_action_tick_active);
                userViewHolder.inCircle.setOnClickListener((v) -> {
                    // TODO: 17.08.2016 refactor - put this code in service
                    //remove from user circles
                    databaseReference.child("users").child(friendKey).child("circles").child(circleId).removeValue();
                    //remove user from circle members
                    databaseReference.child("circles").child(circleId).child("members").child(friendKey).removeValue();
                });
                setOnChangeListener(friendKey, userViewHolder);
            } else if (position > members.size()) {
                friendKey = notMembers.get(position - (members.size()+1)).getFbKey();
                ((UserViewHolder) holder).avatar.setOnClickListener(v1 -> UserProfileActivity.start(context, friendKey, ""));
                ((UserViewHolder) holder).userName.setOnClickListener(v1 -> UserProfileActivity.start(context, friendKey, ""));
                userViewHolder.inCircle.setImageResource(R.drawable.ic_add_circle);
                userViewHolder.inCircle.setOnClickListener((v) -> {
                            circlesService.addCircle(circleId, friendKey);

                            Notification noti = NotificationFactory.shareCircle(circleId);
                            noti.setRead(false);
                            DatabaseReference notiRef = databaseReference.child("notifications").push();
                            notiRef.setValue(noti);
                            databaseReference.child("users").child(friendKey).child("notifications").child(notiRef.getKey()).setValue(true);

                    databaseReference.child("users").child(friendKey).child("notifications_unread").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            long current = 0;
                            if (dataSnapshot.exists()) {
                                current = (long) dataSnapshot.getValue();
                            }
                            databaseReference.child("users").child(friendKey).child("notifications_unread").setValue(current + 1);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d("TAG", databaseError.getMessage());
                        }
                    });

                            databaseReference.child("users").child(friendKey).child("fcm_token").addListenerForSingleValueEvent(new ValueEventListener() {
                                @TargetApi(Build.VERSION_CODES.KITKAT)
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    String[] recepients = {(String)dataSnapshot.getValue()};
                                    try {
                                        JSONArray recepientsJson = new JSONArray(recepients);
                                        FirebasePushService.sendMessage(recepientsJson, Notification.TYPE.CIRCLE.getNumVal(), Notification.ACTION.SHARE.getNumVal(), mFirebaseUser.getUid(), circleId, null, null);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e("Circle adapter", databaseError.getMessage());
                                }
                            });


                });
                setOnChangeListener(friendKey, userViewHolder);
            }

        }
    }

    private void setOnChangeListener(String friendId, UserViewHolder userViewHolder){
        databaseReference.child("users").child(friendId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("name")){
                    //friend's name
                    userViewHolder.userName.setText((String)dataSnapshot.getValue());
                } else if(dataSnapshot.getKey().equals("avatar")){
                    //userpic
                    Picasso.with(context)
                            .load((String)dataSnapshot.getValue())
                            .transform(new CircleTransformation())
                            .error(R.drawable.profile_icon_error)
                            .into(userViewHolder.avatar);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("name")){
                    //friend's name
                    userViewHolder.userName.setText((String)dataSnapshot.getValue());
                    Collections.sort(members,(lhs, rhs)->lhs.getName().compareTo(rhs.getName()));
                    Collections.sort(notMembers,(lhs, rhs)->lhs.getName().compareTo(rhs.getName()));
                    notifyDataSetChanged();
                } else if(dataSnapshot.getKey().equals("avatar")){
                    //userpic
                    Picasso.with(context)
                            .load((String)dataSnapshot.getValue())
                            .transform(new CircleTransformation())
                            .error(R.drawable.profile_icon_error)
                            .into(userViewHolder.avatar);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("friends on change", databaseError.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        //members + non-members + divider
        return members.size() + notMembers.size()+1;
    }

    @Override
    public int getItemViewType(int position) {
        if(position==members.size())
            return TYPE_DIVIDER;
        else
            return TYPE_USER;
    }

    public void filter(String query){
        System.out.println(query);

        restore();

        //save non-filtered data
        if(cached_members.size()==0 && members.size()>0)
            cached_members.addAll(members);
        if(cached_notMembers.size()==0 && notMembers.size()>0)
            cached_notMembers.addAll(notMembers);

        //filter data
        for(User member:members){
            if(member.getName().toLowerCase().contains(query.toLowerCase()))
                filter_members.add(member);

        }
        for(User notMember:notMembers){
            if(notMember.getName().toLowerCase().contains(query.toLowerCase()))
                filter_notMembers.add(notMember);
        }
        members.clear();
        notMembers.clear();
        members.addAll(filter_members);
        notMembers.addAll(filter_notMembers);
        notifyDataSetChanged();
    }

    public void restore(){
        filter_notMembers.clear();
        filter_members.clear();

        if(cached_members.size()>0) {
            members.clear();
            members.addAll(cached_members);
        }
        if(cached_notMembers.size()>0) {
            notMembers.clear();
            notMembers.addAll(cached_notMembers);
        }
    }

    public void addItem(String friendId){
        databaseReference.child("users").child(friendId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot data) {
                //serialize user from database
                User user = data.getValue(User.class);
                user.setFbKey(data.getKey());
                //check in user circles if exists currentCircle
                if(!data.child("circles").hasChild(circleId)) {
                    notMembers.add(user);
                    Collections.sort(notMembers, (lhs, rhs) -> lhs.getName().compareTo(rhs.getName()));
                    notifyDataSetChanged();
                }
                databaseReference.child("users").child(friendId).child("circles").addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        if(dataSnapshot.getKey().equals(circleId)) {
                            members.add(user);
                            Collections.sort(members, (lhs, rhs) -> lhs.getName().compareTo(rhs.getName()));
                            notMembers.remove(user);
                            notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getKey().equals(circleId)) {
                            notMembers.add(user);
                            Collections.sort(notMembers, (lhs, rhs) -> lhs.getName().compareTo(rhs.getName()));
                            members.remove(user);
                            notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e("CircleMembersAdapter", databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("CircleMembersAdapter", databaseError.getMessage());
            }
        });


    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.avatar)
        protected ImageView avatar;
        @Bind(R.id.username)
        protected TextView userName;
        @Bind(R.id.in_circle)
        protected ImageView inCircle;

        public UserViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class AddMoreFriendViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.add_more_friends)
        protected TextView addMoreFriends;

        public AddMoreFriendViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
