package com.notibuyer.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.model.Circle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CirclesTabsAdapter extends RecyclerView.Adapter<CirclesTabsAdapter.CircleHolder> {

    private static final String TAG = "CircleTabsAdapter";
    private List<Circle> circles = new ArrayList<>();
    private Context context;
    private CircleSelectListener circleSelectListener;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private int selectedPosition = 1;
    private ViewPager pager;
    private String selectedCircleId;
    private RecyclerView circlesView;

    public CirclesTabsAdapter(Context context, CircleSelectListener circleSelectListener, ViewPager pager, RecyclerView circlesView, String selectedCircleId) {
        this.context = context;
        this.circleSelectListener = circleSelectListener;
        this.pager = pager;
        this.circlesView = circlesView;
        Circle allCircles = new Circle();
        allCircles.setName("All");
        allCircles.setIndex(-1L);
        allCircles.setFbKey("All");
        allCircles.setColor("#ffffff");
        circles.add(allCircles);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        this.selectedCircleId = selectedCircleId;

    }


    public CirclesTabsAdapter(Context context, CircleSelectListener circleSelectListener, ViewPager pager, RecyclerView circlesView) {


        this.context = context;
        this.circleSelectListener = circleSelectListener;
        this.pager = pager;
        this.circlesView = circlesView;
        Circle allCircles = new Circle();
        allCircles.setName("All");
        allCircles.setIndex(-1L);
        allCircles.setFbKey("All");
        circles.add(allCircles);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        this.selectedCircleId = selectedCircleId;
        databaseReference.child("users").child(mFirebaseUser.getUid()).child("private_circle").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                selectedCircleId = (String) dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    public CircleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CircleHolder(LayoutInflater.from(context).inflate(R.layout.circle_tv, parent, false));
    }


    public void selectCircle(String circleId) {
        selectedCircleId = circleId;
    }

    public int getSelectedCircleIndex() {
        return selectedPosition;
    }

    @Override
    public void onBindViewHolder(CircleHolder holder, int position) {


        //holder.container.setVisibility(View.GONE);
        // holder.countTasks.setVisibility(View.GONE);
        // holder.countTasks.setText("");

        Circle circle = circles.get(position);
        if (circle.getCountTasks() > 0 && position > 0) {
            holder.container.setVisibility(View.VISIBLE);
            holder.countTasks.setVisibility(View.VISIBLE);
            holder.countTasks.setText(String.valueOf(circle.getCountTasks()));
        } else {
            holder.countTasks.setVisibility(View.GONE);
            holder.container.setVisibility(View.GONE);
            holder.countTasks.setText("0");
        }

        holder.circleView.setText(circle.getName());
        holder.circleView.setTextColor(ContextCompat.getColor(context, android.R.color.white));
        GradientDrawable gd = (GradientDrawable) holder.circleView.getBackground().getCurrent();
        if (circle.getFbKey().equals(selectedCircleId)) {
            System.out.println(selectedCircleId);
            gd.setColor(Color.parseColor(circle.getColor()));
            gd.setStroke(0, 0);
            circleSelectListener.onCircleSelected(position, circle.getFbKey());
            selectedPosition = position;
        } else {
            gd.setColor(Color.TRANSPARENT);
            gd.setStroke(2, ContextCompat.getColor(context, android.R.color.white));
        }
        holder.circleView.setOnClickListener(v -> {
            if (selectedPosition == position)
                return;
            int prevPosition = selectedPosition;
            selectedPosition = position;
            selectedCircleId = circle.getFbKey();
            notifyItemChanged(prevPosition);

            if (position == 0) {
                holder.circleView.setTextColor(ContextCompat.getColor(context, android.R.color.black));
                gd.setColor(ContextCompat.getColor(context, android.R.color.white));
                gd.setStroke(0, 0);
            } else {
                gd.setColor(Color.parseColor(circle.getColor()));
                holder.circleView.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                gd.setStroke(0, 0);
            }
            circleSelectListener.onCircleSelected(position, circle.getFbKey());
                }
        );


        if (position > 0) {
            databaseReference.child("circles").child(circle.getFbKey()).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    //if(dataSnapshot.getKey().equals("current_tasks") && holder.container==null) {
                    //   holder.countTasks.setText(String.valueOf(dataSnapshot.getChildrenCount()));
                    // }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    switch (dataSnapshot.getKey()) {
                        case "name":
                            holder.circleView.setText((String) dataSnapshot.getValue());
                            circle.setName((String) dataSnapshot.getValue());
                            break;
                        case "color":
                            circle.setColor((String) dataSnapshot.getValue());
                            if (position == selectedPosition) {
                                GradientDrawable gd = (GradientDrawable) holder.circleView.getBackground().getCurrent();
                                holder.circleView.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                                gd.setColor(Color.parseColor((String) dataSnapshot.getValue()));
                                gd.setStroke(0, 0);
                            }
                            break;
                    }


                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d(TAG, databaseError.getMessage());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return circles.size();
    }

    public void addItem(Circle circle){
        for(Circle curr:circles){
            if(curr.getFbKey().equals(circle.getFbKey()))
               return;
        }
        circles.add(circle);

        //System.out.println(circles.size());
       //Collections.sort(circles, (lhs, rhs)-> lhs.getIndex().compareTo(rhs.getIndex()));
        notifyItemInserted(circles.size());
        databaseReference.child("circles").child(circle.getFbKey()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getKey().equals("current_tasks")) {
                    System.out.println(dataSnapshot.getChildrenCount());
                    onCountChanged(circle.getFbKey(), dataSnapshot.getChildrenCount());
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getKey().equals("current_tasks")) {
                    onCountChanged(circle.getFbKey(), dataSnapshot.getChildrenCount());
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getKey().equals("current_tasks")) {
                    onCountChanged(circle.getFbKey(), 0);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage());
            }
        });
       // notifyDataSetChanged();
    }

    private void onCountChanged(String id, long count) {
        for (Circle circle : circles) {
            if (circle.getFbKey().equals(id)) {
                circle.setCountTasks(count);
                circles.get(circles.indexOf(circle)).setCountTasks(count);
                System.out.println(circle.getCountTasks());
                notifyItemChanged(circles.indexOf(circle));
                System.out.println("INDEX " + circles.indexOf(circle));
            }
        }
    }

    public void removeItem(String key){
        int pos = -1;
        Circle removeCircle = null;
        for(Circle item:circles){
            if(item.getFbKey().equals(key)){
                pos = circles.indexOf(item);
                removeCircle = item;
                break;
            }
        }
        if(removeCircle!=null) {
            circles.remove(removeCircle);
            notifyItemRemoved(pos);
        }
    }

    public void onIndexChanged(int fromPosition, int toPosition) {
        boolean selectedChanged = false;
        String fromId = circles.get(fromPosition).getFbKey();
        String toId = circles.get(toPosition).getFbKey();
        databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles").child(fromId).child("index").setValue(toPosition-1);
        databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles").child(toId).child("index").setValue(fromPosition-1);


        Collections.swap(circles, fromPosition, toPosition);


        if(selectedPosition==fromPosition) {
            selectedPosition=toPosition;
            selectedChanged = true;
        }
        if(selectedPosition==toPosition && !selectedChanged)
            selectedPosition=fromPosition;

       notifyItemMoved(fromPosition, toPosition);
        notifyItemChanged(fromPosition);
        notifyItemChanged(toPosition);
    }

    public interface CircleSelectListener {
        void onCircleSelected(int position, String circleId);
    }

    public class CircleHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.circle_tab_name)
        TextView circleView;
        @Bind(R.id.tab_container)
        RelativeLayout tabContainer;
        @Bind(R.id.count_tasks)
        TextView countTasks;
        @Bind(R.id.count_container)
        RelativeLayout container;
        View v;
        public CircleHolder(View v){
            super(v);
            ButterKnife.bind(this, v);
            this.v = v;
        }

        public View getView() {
            return v;
        }
    }

}
