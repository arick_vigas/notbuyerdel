package com.notibuyer.app.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.ui.view.CircleTransformation;
import com.notibuyer.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;



/**
 * Created by alexander on 18.08.2016.
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentHolder>{

    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private String taskId;
    private List<String> comments = new ArrayList<>();
    private Context context;

    public CommentsAdapter(String taskId, Context context){
        this.taskId = taskId;
        this.context = context;
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
    }

    @Override
    public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CommentHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, parent, false));
    }

    @Override
    public void onBindViewHolder(CommentHolder holder, int position) {
        String commentId = comments.get(position);
        databaseReference.child("comments").child(commentId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "text":
                        holder.commentText.setText((String) dataSnapshot.getValue());
                        break;
                    case "created":
                        holder.created.setText(Utils.getFormattedDate(holder.itemView.getContext(), new Date((Long)dataSnapshot.getValue())));
                        break;
                    case "authorId":
                        databaseReference.child("tasks").child(taskId).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                if(snapshot.getValue().equals(mFirebaseUser.getUid())
                                        && dataSnapshot.getValue().equals(mFirebaseUser.getUid()) ){
                                    holder.editComment.setVisibility(View.VISIBLE);
                                    holder.editComment.setOnClickListener(v -> {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle(context.getResources().getString(R.string.edit_comment));
                                        EditText txt = new EditText(context);
                                        txt.setText(holder.commentText.getText());
                                        builder.setView(txt);
                                        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                databaseReference.child("comments").child(commentId).child("text").setValue(txt.getText().toString());
                                            }
                                        });
                                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                        builder.show();
                                    });
                                    holder.removeComment.setVisibility(View.VISIBLE);
                                    holder.removeComment.setOnClickListener(v -> {
                                        databaseReference.child("tasks").child(taskId).child("comments").child(commentId).removeValue();
                                        databaseReference.child("comments").child(commentId).removeValue();
                                    });

                                } else {
                                    holder.editComment.setVisibility(View.INVISIBLE);
                                    holder.removeComment.setVisibility(View.INVISIBLE);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e("ComAdapter", databaseError.getMessage());
                            }
                        });
                        databaseReference.child("users").child((String) dataSnapshot.getValue()).addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                if(dataSnapshot.getKey().equals("avatar")){
                                    Picasso.with(holder.userPic.getContext())
                                            .load((String) dataSnapshot.getValue())
                                            .transform(new CircleTransformation())
                                            .into(holder.userPic);
                                }
                                if(dataSnapshot.getKey().equals("name")){
                                    holder.userName.setText((String) dataSnapshot.getValue());
                                }
                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                if(dataSnapshot.getKey().equals("avatar")){
                                    Picasso.with(holder.userPic.getContext())
                                            .load((String) dataSnapshot.getValue())
                                            .transform(new CircleTransformation())
                                            .into(holder.userPic);
                                }
                                if(dataSnapshot.getKey().equals("name")){
                                    holder.userName.setText((String) dataSnapshot.getValue());
                                }
                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e("Comments Adapter", databaseError.getMessage());
                            }
                        });
                        break;
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "text":
                        holder.commentText.setText((String) dataSnapshot.getValue());
                        break;
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("Comments Adapter", databaseError.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public void addItem(String id){
        comments.add(id);
        notifyItemInserted(comments.size());
    }

    public void removeItem(String id){
        int pos = comments.indexOf(id);
        comments.remove(id);
        notifyItemRemoved(pos);
    }

    public class CommentHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.username)
        TextView userName;
        @Bind(R.id.text)
        TextView commentText;
        @Bind(R.id.avatar)
        ImageView userPic;
        @Bind(R.id.date)
        TextView created;
        @Bind(R.id.remove)
        ImageView removeComment;
        @Bind(R.id.edit)
        ImageView editComment;

        public CommentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
