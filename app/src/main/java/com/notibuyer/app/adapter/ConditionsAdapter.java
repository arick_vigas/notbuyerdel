package com.notibuyer.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.notibuyer.app.R;
import com.notibuyer.app.model.Condition;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by alexander on 26.08.2016.
 */
public class ConditionsAdapter extends RecyclerView.Adapter<ConditionsAdapter.ConditionHolder>{

    private List<Condition> conditions = new ArrayList<>();
    private Context context;
    private String color;

    public ConditionsAdapter(Context context, String color){
        this.context = context;
        this.color = color;
    }

    @Override
    public ConditionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ConditionHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.condition_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(ConditionHolder holder, int position) {
        Condition condition = conditions.get(position);
        holder.title.setText(condition.getTitle());
        holder.description.setText(condition.getTitle());
        Picasso.with(context).load(condition.getIcon()).into(holder.icon, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                //paint icon
                System.out.println(color + " color");
                System.out.println(holder.icon!=null);
                holder.icon.setColorFilter(Color.parseColor(color));
            }

            @Override
            public void onError() {
                Log.e("ADAPTER", "error");
            }
        });
    }

    @Override
    public int getItemCount() {
        return conditions.size();
    }

    public void addItem(Condition condition){
        conditions.add(condition);
        notifyItemInserted(conditions.size());
    }

    public class ConditionHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.description)
        TextView description;
        @Bind(R.id.icon)
        ImageView icon;
        public ConditionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
