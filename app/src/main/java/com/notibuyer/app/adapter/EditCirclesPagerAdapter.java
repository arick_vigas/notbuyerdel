package com.notibuyer.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.notibuyer.app.ui.fragments.CircleMembersFragment;

import com.notibuyer.app.ui.fragments.SearchFriendsFragment;

/**
 * Created by alexander on 09.08.2016.
 */
public class EditCirclesPagerAdapter extends FragmentPagerAdapter {
    private String circleId;
    private Fragment currentFragment;
    public EditCirclesPagerAdapter(FragmentManager fm, String circleId) {
        super(fm);
        this.circleId = circleId;
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0){
            currentFragment = CircleMembersFragment.newInstance(circleId);
        } else {
            currentFragment = SearchFriendsFragment.newInstance(circleId);
        }
        return currentFragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public Fragment getCurrentFragment(){
        return this.currentFragment;
    }
}
