package com.notibuyer.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.helper.TagCircleFactory;
import com.notibuyer.app.interfaces.FriendProfileClickListener;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.model.User;
import com.notibuyer.app.ui.activity.UserProfileActivity;
import com.notibuyer.app.ui.view.CircleListLayout;
import com.notibuyer.app.ui.view.CircleTransformation;
import com.notibuyer.app.ui.view.TagCircleView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import lombok.Setter;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.UserHolder> {

    private List<User> friends = new ArrayList<>();
    private List<User> savedFriends = new ArrayList<>();
    private List<User> filteredFriends = new ArrayList<>();
    private Context context;
    @Setter
    private FriendProfileClickListener friendProfileClickListener;

    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;

    public FriendsAdapter(Context context) {
        this.context = context;
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    public List<User> getFriends(){
        return friends;
    }



    @Override
    public FriendsAdapter.UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_item, parent, false));
    }

    @Override
    public void onBindViewHolder(FriendsAdapter.UserHolder holder, int position) {
        holder.circleListLayout.setVisibility(View.GONE);
        holder.circleListLayout.removeAllViews();
        User friend = friends.get(position);

        /* logic is when user removes from friend another user :
            1. remove friend from user's friendlist
            2. remove user from firend's friendlist
            3. remove friend from user's circles
                3.1 remove friend from circle members
                3.2 remove circle from friend circles
            4. remove user from friend's circles
                4.1 remove user from circle members
                4.2 remove circle from user circles
         */
        holder.avatar.setOnClickListener(v-> UserProfileActivity.start(context, friend.getFbKey(), ""));
        holder.username.setOnClickListener(v-> UserProfileActivity.start(context, friend.getFbKey(), ""));
        holder.removeFromFriends.setOnClickListener(v->{
            //remove user from friend's friendlist
            databaseReference.child("users").child(friend.getFbKey()).child("friends").child(mFirebaseUser.getUid()).removeValue();
            //remove friend from user's friendList
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("friends").child(friend.getFbKey()).removeValue();
            //remove friend from users's circles
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //for each circe id in user circles
                    for(DataSnapshot circleData:dataSnapshot.getChildren()){
                        // select * from "circles" where circle id ='id'
                        databaseReference.child("circles").child(circleData.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot data) {
                                //if user is circle owner and friend is just a member
                                if(data.child("ownerId").getValue().equals(mFirebaseUser.getUid()) && data.child("members").hasChild(friend.getFbKey())){
                                    //remove friend from circle members
                                    databaseReference.child("circles").child(circleData.getKey()).child("members").child(friend.getFbKey()).removeValue();
                                    //remove circle from friend circles
                                    databaseReference.child("users").child(friend.getFbKey()).child("circles").child(circleData.getKey()).removeValue();
                                //if friend is circle owner and user is just a member
                                } else if(data.child("ownerId").getValue().equals(friend.getFbKey()) && data.child("members").hasChild(mFirebaseUser.getUid())){
                                    //remove user from circle members
                                    databaseReference.child("circles").child(circleData.getKey()).child("members").child(mFirebaseUser.getUid()).removeValue();
                                    //remove circle from user circles
                                    databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles").child(circleData.getKey()).removeValue();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e("FriendsAdapter", databaseError.getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        });
        databaseReference.child("users").child(friend.getFbKey()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "name":
                        String name = (String)dataSnapshot.getValue();
                        holder.username.setText(name.length()<54?name:name.substring(0,51) + "...");
                        break;
                    case "avatar":
                        Picasso.with(holder.avatar.getContext())
                                .load((String)dataSnapshot.getValue())
                                .transform(new CircleTransformation())
                                .into(holder.avatar);
                        break;
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()) {
                    case "name":
                        String name = (String) dataSnapshot.getValue();
                        holder.username.setText(name.length() < 54 ? name : name.substring(0, 51) + "...");
                        break;
                    case "avatar":
                        Picasso.with(holder.avatar.getContext())
                                .load((String) dataSnapshot.getValue())
                                .transform(new CircleTransformation())
                                .into(holder.avatar);
                        break;
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("FriendsAdapter", databaseError.getMessage());
            }
        });

        TagCircleFactory circleViewFactory = new TagCircleFactory();

        //listen circle changes only shared with current users!!!
       List<Circle> sharedCircles = new ArrayList<Circle>();
        databaseReference.child("users").child(friend.getFbKey()).child("circles").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                holder.circleListLayout.setVisibility(View.VISIBLE);
                databaseReference.child("circles").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot data) {
                        System.out.println(data);
                        //check if current user is author of this circle and friend is member of
                        if(data.child("ownerId").getValue().equals(mFirebaseUser.getUid()) &&  data.child("members").hasChild(friend.getFbKey())){
                           Circle circle = data.getValue(Circle.class);
                            circle.setFbKey(data.getKey());
                            sharedCircles.add(circle);
                            Collections.sort(sharedCircles, ((lhs, rhs) -> lhs.getName().compareTo(rhs.getName())));
                            System.out.println(sharedCircles.size() + " shared cirlces");
                            List<TagCircleView> circleViews = circleViewFactory.createCircles(holder.circleListLayout.getContext(), sharedCircles);
                            holder.circleListLayout.addCircles(circleViews);
                            holder.circleListLayout.setUp();
                        }
                        //if owner - friend and current user is member of

                        else if(data.child("ownerId").getValue().equals(friend.getFbKey()) && data.child("members").hasChild(mFirebaseUser.getUid())){
                            Circle circle = data.getValue(Circle.class);
                            circle.setFbKey(data.getKey());

                            sharedCircles.add(circle);
                            Collections.sort(sharedCircles, ((lhs, rhs) -> lhs.getName().compareTo(rhs.getName())));
                            System.out.println(sharedCircles.size() + " shared cirlces");
                            List<TagCircleView> circleViews = circleViewFactory.createCircles(holder.circleListLayout.getContext(), sharedCircles);
                            holder.circleListLayout.addCircles(circleViews);
                            holder.circleListLayout.setUp();
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e("FriendsAdapter", databaseError.getMessage());
                    }
                });



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                holder.circleListLayout.setVisibility(View.VISIBLE);
                databaseReference.child("circles").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot data) {
                        System.out.println(data);
                        //check if current user is author of this circle and friend is member of
                        if(data.child("ownerId").getValue().equals(mFirebaseUser.getUid()) &&  data.child("members").hasChild(friend.getFbKey())){
                            Circle circle = data.getValue(Circle.class);
                            circle.setFbKey(data.getKey());
                            sharedCircles.add(circle);
                            Collections.sort(sharedCircles, ((lhs, rhs) -> lhs.getName().compareTo(rhs.getName())));
                            System.out.println(sharedCircles.size() + " shared cirlces");
                            List<TagCircleView> circleViews = circleViewFactory.createCircles(holder.circleListLayout.getContext(), sharedCircles);
                            holder.circleListLayout.addCircles(circleViews);
                            holder.circleListLayout.setUp();
                        }
                        //if owner - friend and current user is member of

                        else if(data.child("ownerId").getValue().equals(friend.getFbKey()) && data.child("members").hasChild(mFirebaseUser.getUid())){
                            Circle circle = data.getValue(Circle.class);
                            circle.setFbKey(data.getKey());

                            sharedCircles.add(circle);
                            Collections.sort(sharedCircles, ((lhs, rhs) -> lhs.getName().compareTo(rhs.getName())));
                            System.out.println(sharedCircles.size() + " shared cirlces");
                            List<TagCircleView> circleViews = circleViewFactory.createCircles(holder.circleListLayout.getContext(), sharedCircles);
                            holder.circleListLayout.addCircles(circleViews);
                            holder.circleListLayout.setUp();
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e("FriendsAdapter", databaseError.getMessage());
                    }
                });

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Circle removeCircle = null;
                for(Circle circle:sharedCircles){
                    if(circle.getFbKey().equals(dataSnapshot.getKey())){
                        removeCircle = circle;
                        break;
                    }
                }
                if(removeCircle != null){
                    sharedCircles.remove(removeCircle);
                    List<TagCircleView> circleViews = circleViewFactory.createCircles(holder.circleListLayout.getContext(), sharedCircles);
                    holder.circleListLayout.addCircles(circleViews);
                    holder.circleListLayout.setUp();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //if (friends.get(position).getCircles() != null) {
          //  holder.circleListLayout.setVisibility(View.VISIBLE);
         //   List<Circle> taskCircles = friends.get(position).getCircles();
          //  List<TagCircleView> circleViews = circleViewFactory.createCircles(holder.circleListLayout.getContext(), taskCircles);
         //   holder.circleListLayout.addCircles(circleViews);
         //   holder.circleListLayout.setUp();
      //  } else {
     //       holder.circleListLayout.setVisibility(View.GONE);
     //   }
    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    public void addItem(String id){
        databaseReference.child("users").child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    int pos = 0;
                    user.setFbKey(dataSnapshot.getKey());
                    for (User friend : friends) {
                        if (user.getName().compareTo(friend.getName()) < 0) {
                            pos = friends.indexOf(friend);
                            break;
                        } else if (user.getName().compareTo(friend.getName()) > 0) {
                            pos++;
                        }
                    }
                    friends.add(pos, user);
                    notifyItemInserted(pos);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("FRIENDS_ADAPTER", databaseError.getMessage());
            }
        });
    }

    public void removeItem(String id){
        int pos = -1;
        User removeItem = null;
        for(User item:friends){
            if(item.getFbKey().equals(id)){
                pos = friends.indexOf(item);
                removeItem = item;
                break;
            }
        }
        if(removeItem!=null){
            friends.remove(removeItem);
            notifyItemRemoved(pos);
        }
    }

    public void filter(String query) {
        filteredFriends.clear();
        if (savedFriends.size() == 0) {
            savedFriends.addAll(friends);
        } else {
            friends.clear();
            friends.addAll(savedFriends);
        }
        for (User item : friends) {
            if (item.getName().toLowerCase().contains(query.toLowerCase())) {
                filteredFriends.add(item);
            }
        }
        friends.clear();
        friends.addAll(filteredFriends);
        notifyDataSetChanged();
    }

    class UserHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.avatar)
        protected ImageView avatar;
        @Bind(R.id.username)
        protected TextView username;
        @Bind(R.id.circles_list_layout)
        protected CircleListLayout circleListLayout;
        @Bind(R.id.remove_from_friends)
        protected ImageView removeFromFriends;

        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
// TODO: 11.08.2016  ADD CLICABLE USERPIC
          //  itemView.setOnClickListener(v -> friendProfileClickListener.onProfileClick(friends.get(getAdapterPosition()).getId()));
        }
    }
}
