package com.notibuyer.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.notibuyer.app.ui.fragments.FriendsListFragment;
import com.notibuyer.app.ui.fragments.SearchFriendsFragment;

/**
 * Created by alexander on 10.08.2016.
 */
public class FriendsPagerAdapter extends FragmentPagerAdapter {
    public FriendsPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0){
            return FriendsListFragment.newInstance();
        } else {
            return SearchFriendsFragment.newInstance("");
        }
    }

    //this is called when notifyDataSetChanged() is called
    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }
}
