package com.notibuyer.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.model.Notification;
import com.notibuyer.app.ui.activity.MainActivity;
import com.notibuyer.app.ui.activity.TaskViewActivity;
import com.notibuyer.app.ui.activity.UserProfileActivity;
import com.notibuyer.app.ui.view.CircleTransformation;
import com.notibuyer.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationHolder> {

    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private List<String> notificationList = new ArrayList<>();
    private Context context;

    public NotificationsAdapter(Context context) {
        this.context = context;
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
    }


    @Override
    public NotificationsAdapter.NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotificationHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_task, parent, false));
    }

    @Override
    public void onBindViewHolder(NotificationHolder holder, int position) {
        //holder.message.setTextColor(context.getResources().getColor(android.R.color.black));
        //holder.username.setTextColor(context.getResources().getColor(android.R.color.black));
        String id = notificationList.get(position);
        holder.avatar.setImageDrawable(null);
        //get notification from FireBase by id
        databaseReference.child("notifications").child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Notification notification = dataSnapshot.getValue(Notification.class);
                if(notification.isRead()) {
                    holder.message.setTextColor(context.getResources().getColor(android.R.color.tertiary_text_light));
                    holder.username.setTextColor(context.getResources().getColor(android.R.color.tertiary_text_light));
                } else {
                    holder.message.setTextColor(context.getResources().getColor(android.R.color.black));
                    holder.username.setTextColor(context.getResources().getColor(android.R.color.black));
                }
                //set DATE
                holder.date.setText(Utils.getFormattedDate(holder.itemView.getContext(), new Date(notification.getDate())));

                //set message
                Notification.TYPE type = Notification.TYPE.getById(notification.getType());
                Notification.ACTION action = Notification.ACTION.getById(notification.getAction());

                switch (type){
                    case CIRCLE:
                        if(action.equals(Notification.ACTION.SHARE)){
                            holder.message.setText(Html.fromHtml("has shared circle <b>TEST</b> with you"));
                        }
                        holder.message.setOnClickListener(v1 -> {
                            MainActivity.start(context, MainActivity.ALL, notification.getCircleId());
                            databaseReference.child("notifications").child(id).child("read").setValue(true);
                            databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        long count = (long) dataSnapshot.getValue();
                                        if (count > 0) {
                                            databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").setValue(count - 1);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.d("TAG", databaseError.getMessage());
                                }
                            });
                        });
                        break;
                    case TASK:
                        databaseReference.child("tasks").child(notification.getTaskId()).child("text").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String taskName = (String) dataSnapshot.getValue();
                                if (taskName == null)
                                    taskName = "";
                                if (taskName.equals("\"\""))
                                    taskName = "";
                                else
                                    taskName = "\"" + taskName + "\"";
                                switch (action){
                                    case CREATE:
                                        holder.message.setText(Html.fromHtml("has created task <b>" + taskName + "</b>"));
                                        break;
                                    case EDIT:
                                        holder.message.setText(Html.fromHtml("has edited task <b>" + taskName + "</b>"));
                                        break;
                                    case COMPLETE:
                                        holder.message.setText(Html.fromHtml("has completed task <b>" + taskName + "</b>"));
                                        break;
                                    case RESTORE:
                                        holder.message.setText(Html.fromHtml("has restored task <b>" + taskName + "</b>"));
                                        break;
                                }
                                holder.message.setOnClickListener(v -> {
                                    TaskViewActivity.start(context, notification.getTaskId());
                                    databaseReference.child("notifications").child(id).child("read").setValue(true);
                                    databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                long count = (long) dataSnapshot.getValue();
                                                if (count > 0) {
                                                    databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").setValue(count - 1);
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            Log.d("TAG", databaseError.getMessage());
                                        }
                                    });
                                });
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e("NotiAdapter", databaseError.getMessage());
                            }
                        });
                        break;
                    case COMMENT:
                        if(action.equals(Notification.ACTION.CREATE)){
                            databaseReference.child("tasks").child(notification.getTaskId()).addChildEventListener(new ChildEventListener() {
                                @Override
                                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                    if(dataSnapshot.getKey().equals("text"))
                                        holder.message.setText(Html.fromHtml("has left a comment in your task <b>\"" + dataSnapshot.getValue() + "\"</b>"));
                                }

                                @Override
                                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                                    if(dataSnapshot.getKey().equals("text"))
                                        holder.message.setText(Html.fromHtml("has left a comment in your task <b>\"" + dataSnapshot.getValue() + "\"</b>"));
                                }

                                @Override
                                public void onChildRemoved(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.getKey().equals("text"))
                                        holder.message.setText(Html.fromHtml("has left a comment in your task"));
                                }

                                @Override
                                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e("NotificationAdapter", databaseError.getMessage());
                                }
                            });
                        }
                        holder.message.setOnClickListener(v -> {
                            TaskViewActivity.start(context, notification.getTaskId());
                            databaseReference.child("notifications").child(id).child("read").setValue(true);
                            databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        long count = (long) dataSnapshot.getValue();
                                        if (count > 0) {
                                            databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").setValue(count - 1);
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.d("TAG", databaseError.getMessage());
                                }
                            });
                        });
                        break;
                    case FRIENDSHIP:
                        if(action.equals(Notification.ACTION.INVITE)) {
                            holder.message.setText(Html.fromHtml("has sent a friendship request to you"));
                            holder.message.setOnClickListener(v -> {
                                MainActivity.start(context, MainActivity.FRIENDS);
                                databaseReference.child("notifications").child(id).child("read").setValue(true);
                                databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            long count = (long) dataSnapshot.getValue();
                                            if (count > 0) {
                                                databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").setValue(count - 1);
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.d("TAG", databaseError.getMessage());
                                    }
                                });
                            });
                        } else if(action.equals(Notification.ACTION.ACCEPT)) {
                            holder.message.setText(Html.fromHtml("accepted your friendship request"));
                            holder.message.setOnClickListener(v -> {
                                UserProfileActivity.start(context, notification.getUserId(), "");
                                databaseReference.child("notifications").child(id).child("read").setValue(true);
                                databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            long count = (long) dataSnapshot.getValue();
                                            if (count > 0) {
                                                databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").setValue(count - 1);
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.d("TAG", databaseError.getMessage());
                                    }
                                });
                            });
                        }

                        break;
                }

                holder.username.setOnClickListener(v -> {
                    UserProfileActivity.start(context, notification.getUserId(), "");
                    databaseReference.child("notifications").child(id).child("read").setValue(true);
                    databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                long count = (long) dataSnapshot.getValue();
                                if (count > 0) {
                                    databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").setValue(count - 1);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d("TAG", databaseError.getMessage());
                        }
                    });
                });
                holder.avatar.setOnClickListener(v -> {
                    UserProfileActivity.start(context, notification.getUserId(), "");
                    databaseReference.child("notifications").child(id).child("read").setValue(true);
                    databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                long count = (long) dataSnapshot.getValue();
                                if (count > 0) {
                                    databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread").setValue(count - 1);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d("TAG", databaseError.getMessage());
                        }
                    });
                });


                //sync userPic and Name
                databaseReference.child("users").child(notification.getUserId()).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        if(dataSnapshot.getKey().equals("name"))
                            holder.username.setText((String)dataSnapshot.getValue());
                        if(dataSnapshot.getKey().equals("avatar"))
                            Picasso.with(holder.avatar.getContext())
                                    .load((String) dataSnapshot.getValue())
                                    .transform(new CircleTransformation())
                                    .into(holder.avatar);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        if(dataSnapshot.getKey().equals("name"))
                            holder.username.setText((String)dataSnapshot.getValue());
                        if(dataSnapshot.getKey().equals("avatar"))
                            Picasso.with(holder.avatar.getContext())
                                    .load((String) dataSnapshot.getValue())
                                    .transform(new CircleTransformation())
                                    .into(holder.avatar);
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e("NotificationsAdapter", databaseError.getMessage());
                    }
                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("NotificationAdapter", databaseError.getMessage());
            }
        });
    }

    public void addItem(String id){
        notificationList.add(0, id);
        notifyItemInserted(0);
    }

    public void removeItem(String id){
        int pos = -1;
        String removeId = null;
        for(String currentId:notificationList){
            if(currentId.equals(id)){
                pos = notificationList.indexOf(currentId);
                removeId = id;
                break;
            }
        }
        notificationList.remove(removeId);
        notifyItemRemoved(pos);
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    class NotificationHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.username)
        protected TextView username;
        @Bind(R.id.avatar)
        protected ImageView avatar;
        @Bind(R.id.message)
        protected TextView message;
        @Bind(R.id.time)
        protected TextView date;

        public NotificationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }



}