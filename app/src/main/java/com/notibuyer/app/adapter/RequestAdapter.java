package com.notibuyer.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.model.FriendshipRequest;
import com.notibuyer.app.model.Notification;
import com.notibuyer.app.network.FirebasePushService;
import com.notibuyer.app.network.firebase.CirclesService;
import com.notibuyer.app.ui.activity.UserProfileActivity;
import com.notibuyer.app.ui.view.CircleTransformation;
import com.notibuyer.app.utils.NotificationFactory;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by alexander on 16.08.2016.
 */
public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.RequestHolder> {

    private static final String TAG = "request adapter";
    private Context context;
    private List<FriendshipRequest> requestList = new ArrayList<>();
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference databaseReference;
    private CirclesService circlesService = new CirclesService();

    public RequestAdapter(Context context){
        this.context = context;
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
    }

    @Override
    public RequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RequestHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_request, parent, false));
    }

    @Override
    public int getItemCount() {
        return requestList.size();
    }

    @Override
    public void onBindViewHolder(RequestHolder holder, int position) {
        FriendshipRequest request = requestList.get(position);

        holder.provider.setText("from " + request.getProvider());
        holder.deleteButton.setOnClickListener(v1 -> {
            //remove request from user requests
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("friendship_requests").child(request.getUserId()).removeValue();
        });
        holder.addToFriendsButton.setOnClickListener(v -> {
            // add to current_user friend list
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("friends").child(request.getUserId()).setValue(true);
            //if request is with circle -> add circle;
            if(request.getCircle()!=null && !TextUtils.isEmpty(request.getCircle())) {
                circlesService.addCircle(request.getCircle(), mFirebaseUser.getUid());
            }
            //add to request_user friend list
            databaseReference.child("users").child(request.getUserId()).child("friends").child(mFirebaseUser.getUid()).setValue(true);
            //remove from user friend_requests
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("friendship_requests").child(request.getUserId()).removeValue();

            Notification notification = NotificationFactory.acceptFriendshipRequest();
            notification.setRead(false);
            DatabaseReference notiRef = databaseReference.child("notifications").push();
            notiRef.setValue(notification);
            databaseReference.child("users").child(request.getUserId()).child("notifications").child(notiRef.getKey()).setValue(true);

            databaseReference.child("users").child(request.getUserId()).child("notifications_unread").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    long current = 0;
                    if (dataSnapshot.exists()) {
                        current = (long) dataSnapshot.getValue();
                    }
                    databaseReference.child("users").child(request.getUserId()).child("notifications_unread").setValue(current + 1);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d("TAG", databaseError.getMessage());
                }
            });

            //push
            databaseReference.child("users").child(request.getUserId()).child("fcm_token").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String[] tokens = {(String)dataSnapshot.getValue()};

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                        try {
                            JSONArray recepients = new JSONArray(tokens);
                            FirebasePushService.sendMessage(recepients, Notification.TYPE.FRIENDSHIP.getNumVal(), Notification.ACTION.ACCEPT.getNumVal(), mFirebaseUser.getUid(), null, null, null);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });

        });
        databaseReference.child("users").child(request.getUserId()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "name":
                        holder.userName.setText((String)dataSnapshot.getValue());
                        holder.userName.setOnClickListener(v1 -> {
                            UserProfileActivity.start(context, request.getUserId(), request.getProvider());
                        });
                        break;
                    case "avatar":
                        Picasso.with(holder.itemView.getContext())
                                .load((String)dataSnapshot.getValue())
                                .transform(new CircleTransformation())
                                .into(holder.userPic);
                        holder.userPic.setOnClickListener(v1 -> {
                            UserProfileActivity.start(context, request.getUserId(), request.getProvider());
                        });
                        break;
                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "name":
                        holder.userName.setText((String)dataSnapshot.getValue());
                        break;
                    case "avatar":
                        Picasso.with(holder.itemView.getContext())
                                .load((String)dataSnapshot.getValue())
                                .transform(new CircleTransformation())
                                .into(holder.userPic);
                        break;
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

    }

    public void addItem(FriendshipRequest request){
        requestList.add(request);
        notifyItemInserted(requestList.size());
    }

    public void removeItem(String id){
        int pos = -1;
        FriendshipRequest removeItem = null;
        for(FriendshipRequest item:requestList){
            if(item.getUserId().equals(id)){
                pos = requestList.indexOf(item);
                removeItem = item;
                break;
            }
        }
        if(removeItem!=null){
            requestList.remove(removeItem);
            notifyItemRemoved(pos);
        }
    }

    class RequestHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.avatar)
        ImageView userPic;
        @Bind(R.id.username)
        TextView userName;
        @Bind(R.id.delete)
        ImageView deleteButton;
        @Bind(R.id.add_to_friends)
        ImageView addToFriendsButton;
        @Bind(R.id.provider)
        TextView provider;

        public RequestHolder(View itemView) {
            super(itemView);
            try {
                ButterKnife.bind(this, itemView);
            } catch (Exception e){e.printStackTrace();}
        }
    }
}
