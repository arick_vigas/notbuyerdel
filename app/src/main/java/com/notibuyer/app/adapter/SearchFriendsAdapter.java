package com.notibuyer.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.model.User;
import com.notibuyer.app.ui.activity.UserProfileActivity;
import com.notibuyer.app.ui.view.CircleTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by alexander on 10.08.2016.
 */

public class SearchFriendsAdapter extends RecyclerView.Adapter {

    private final static String TAG = "Search Friends Adapter";
    private List<User> usersList = new ArrayList();
    private List<User> filteredUserList = new ArrayList<>();
    private List<User> savedUserList =new ArrayList<>();
     private String circleId;
    private UserInvitedListener userInvitedListener;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private Context context;

    public SearchFriendsAdapter(Context context, String circleId, UserInvitedListener userInvitedListener) {
        this.userInvitedListener = userInvitedListener;
        this.circleId = circleId;
        this.context = context;
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SuggestedHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_suggested, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SuggestedHolder userHolder = (SuggestedHolder)holder;
        //userHolder.setIsRecyclable(false);
        User user = usersList.get(position);

        userHolder.provider.setText("from " + user.getProvider());

        ImageView addToFriend = (ImageView) userHolder.itemView.findViewById(R.id.add_to_friends);
        TextView invited = (TextView) userHolder.itemView.findViewById(R.id.invited);

        userHolder.username.setOnClickListener(v1 -> {
            UserProfileActivity.start(context, user.getFbKey(), user.getProvider());
        });
        userHolder.avatar.setOnClickListener(v1 -> {
            UserProfileActivity.start(context, user.getFbKey(), user.getProvider());
        });


        addToFriend.setOnClickListener(v -> {
            userInvitedListener.onUserInvited(user.getFbKey(), circleId, user.getProvider());

        });

        invited.setOnClickListener(v -> {
            userInvitedListener.onUserInviteRemoved(user.getFbKey());
        });
        addToFriend.setVisibility(View.VISIBLE);
        invited.setVisibility(View.GONE);
        databaseReference.child("users").child(user.getFbKey()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("name")){
                    String name = (String)dataSnapshot.getValue();
                    userHolder.username.setText(name.length()<40?name:name.substring(0, 37)+"...");
                }
                if(dataSnapshot.getKey().equals("avatar")){
                    Picasso.with(userHolder.itemView.getContext())
                            .load((String)dataSnapshot.getValue())
                            .transform(new CircleTransformation())
                            .into(userHolder.avatar);
                }
                if(dataSnapshot.getKey().equals("friendship_requests")){
                    if(dataSnapshot.hasChild(mFirebaseUser.getUid())){
                        addToFriend.setVisibility(View.GONE);
                        invited.setVisibility(View.VISIBLE);
                    } else {
                        addToFriend.setVisibility(View.VISIBLE);
                        invited.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("name")){
                    String name = (String)dataSnapshot.getValue();
                    userHolder.username.setText(name.length()<40?name:name.substring(0, 37)+"...");
                }
                if(dataSnapshot.getKey().equals("avatar")){
                    Picasso.with(userHolder.itemView.getContext())
                            .load((String)dataSnapshot.getValue())
                            .transform(new CircleTransformation())
                            .into(userHolder.avatar);
                }
                if(dataSnapshot.getKey().equals("friendship_requests")){
                    if(dataSnapshot.hasChild(mFirebaseUser.getUid())){
                        userHolder.inFriends.setVisibility(View.GONE);
                        addToFriend.setVisibility(View.GONE);
                        invited.setVisibility(View.VISIBLE);
                    } else {
                        addToFriend.setVisibility(View.VISIBLE);
                        invited.setVisibility(View.GONE);
                        databaseReference.child("users").child(mFirebaseUser.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                if(snapshot.hasChild(user.getFbKey())){
                                    removeItem(user.getFbKey());
                                } else {
                                    userHolder.inFriends.setVisibility(View.GONE);
                                    addToFriend.setVisibility(View.VISIBLE);
                                    invited.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e(TAG, databaseError.getMessage());
                            }
                        });
                    }



                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getKey().equals("friendship_requests")) {
                    databaseReference.child("users").child(mFirebaseUser.getUid()).child("friends").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            if(snapshot.hasChild(user.getFbKey())){
                                removeItem(user.getFbKey());
                            } else {
                                userHolder.inFriends.setVisibility(View.GONE);
                                addToFriend.setVisibility(View.VISIBLE);
                                invited.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public void addItem(User item){
        for(User u:usersList) {
            if (u.getFbKey().equals(item.getFbKey())) {
                return;
            }
        }
        databaseReference.child("users").child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.child("friendship_requests").hasChild(item.getFbKey()) && !dataSnapshot.child("friends").hasChild(item.getFbKey())){
                    insertItemAsc(item);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    private void insertItemAsc(User user) {
        int pos = 0;
        for (User current : usersList) {
            //System.out.println(user.getName() + " " + current.getName() + " " + user.getName().compareTo(current.getName()));
            if (user.getName().compareTo(current.getName()) < 0) {
                pos = usersList.indexOf(current);
                break;
            } else if (user.getName().compareTo(current.getName()) > 0) {
                pos++;
            }

        }
        usersList.add(pos, user);
        notifyItemInserted(pos);
    }

    public void removeItem(String id){
        User user = null;
        int pos = -1;
        for(User item:usersList) {
            if (item.getFbKey().equals(id)){
                user = item;
                pos = usersList.indexOf(item);
                break;
             }
        }
        if(user!=null){
            usersList.remove(user);
            notifyItemRemoved(pos);
        }

    }

    public void filter(String query){
        filteredUserList.clear();
        if(savedUserList.size()==0) {
            savedUserList.addAll(usersList);
        } else {
            usersList.clear();
            usersList.addAll(savedUserList);
        }
        for(User item:usersList){
            if(item.getName().toLowerCase().contains(query.toLowerCase())){
                filteredUserList.add(item);
            }
        }
        usersList.clear();
        usersList.addAll(filteredUserList);
        notifyDataSetChanged();
    }

    public interface UserInvitedListener {
        void onUserInvited(String userId, String circleId, String provider);

        void onUserInviteRemoved(String userId);
    }

    class SuggestedHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.in_friends)
        protected ImageView inFriends;

        @Bind(R.id.avatar)
        protected ImageView avatar;
        @Bind(R.id.username)
        protected TextView username;
        @Bind(R.id.provider)
        protected TextView provider;
        //@Bind(R.id.invited)
        //protected  TextView invited;
        //@Bind(R.id.add_to_friends)
        //protected ImageView addToFriend;

        public SuggestedHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
