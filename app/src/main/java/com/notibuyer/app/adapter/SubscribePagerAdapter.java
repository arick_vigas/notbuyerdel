package com.notibuyer.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.widget.Toolbar;

import com.notibuyer.app.model.Subscribe;
import com.notibuyer.app.ui.fragments.SubscribeFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexander on 26.08.2016.
 */
public class SubscribePagerAdapter extends FragmentPagerAdapter {
    private Toolbar toolbar;
    private List<Subscribe> subscribes = new ArrayList<>();
    private Context context;
    public SubscribePagerAdapter(FragmentManager fm, Toolbar toolbar, Context context) {
        super(fm);
        this.toolbar = toolbar;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Subscribe subscribe = subscribes.get(position);
        System.out.println("adapter " + subscribe.getColor());
        return SubscribeFragment.newInstance(subscribe.getId(), subscribe.getColor());

    }

    @Override
    public int getCount() {
        return subscribes.size();
    }

    public void addItem(Subscribe subscribe){
        subscribes.add(subscribe);
        notifyDataSetChanged();
    }

    public void removeItem(String id){
        Subscribe remove = null;
        for(Subscribe subscribe:subscribes){
            if(subscribe.getId().equals(id)) {
                remove = subscribe;
                break;
            }
        }
        if(remove!=null){
            subscribes.remove(remove);
            notifyDataSetChanged();
        }
    }
}
