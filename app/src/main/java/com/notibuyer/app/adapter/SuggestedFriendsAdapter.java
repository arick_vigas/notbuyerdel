package com.notibuyer.app.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.notibuyer.app.R;
import com.notibuyer.app.interfaces.AcceptFriendInviteListener;
import com.notibuyer.app.model.User;
import com.notibuyer.app.ui.view.CircleTransformation;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Getter;
import lombok.Setter;

public class SuggestedFriendsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_INVITE = 1;
    private static final int VIEW_TYPE_CONTACT = 2;
    private static final int VIEW_TYPE_FB = 3;

    private static final int VIEW_TYPE_INVITE_HEADER = 4;
    private static final int VIEW_TYPE_CONTACT_HEADER = 5;
    private static final int VIEW_TYPE_FB_HEADER = 6;


    @Setter
    @Getter
    private List<User> invitationList;
    @Setter
    @Getter
    private List<User> contactsList;
    @Setter
    @Getter
    private List<User> fbList;

    private int invitationCount = 0;
    private int contactsCount = 0;
    private int fbCount = 0;

    private AcceptFriendInviteListener acceptFriendInviteListener;

    public SuggestedFriendsAdapter(AcceptFriendInviteListener acceptFriendInviteListener) {
        this.acceptFriendInviteListener = acceptFriendInviteListener;
    }

    public SuggestedFriendsAdapter(List<User> contactsList, List<User> invitationList, AcceptFriendInviteListener acceptFriendInviteListener) {
        this.contactsList = contactsList;
        this.invitationList = invitationList;
        this.acceptFriendInviteListener = acceptFriendInviteListener;
    }

    @Override
    public int getItemViewType(int position) {
        if (invitationCount != 0 && contactsCount == 0 && fbCount != 0) {
            if (position == 0) return VIEW_TYPE_INVITE_HEADER;
            if (position > 0 && position < invitationCount + 1) return VIEW_TYPE_INVITE;
            if (position == invitationCount + 1) return VIEW_TYPE_FB_HEADER;
            if (position >= invitationCount + 2 && position < invitationCount+fbCount + 2) return VIEW_TYPE_FB;
        }
        if (invitationCount != 0) {
            if (position == 0) return VIEW_TYPE_INVITE_HEADER;
            if (position > 0 && position < invitationCount + 1) return VIEW_TYPE_INVITE;
            if (position == invitationCount + 1) return VIEW_TYPE_CONTACT_HEADER;
            if (position >= invitationCount + 2 && position < invitationCount+contactsCount + 2) return VIEW_TYPE_CONTACT;
            if (position == invitationCount+contactsCount + 2) return VIEW_TYPE_FB_HEADER;
            else return VIEW_TYPE_FB;
        }
        if (contactsCount != 0) {
            if (position == 0) return VIEW_TYPE_CONTACT_HEADER;
            if (position > 0 && position < contactsCount + 1) return VIEW_TYPE_CONTACT;
            if (position == contactsCount + 1) return VIEW_TYPE_FB_HEADER;
            if (position >= contactsCount + 2 && position < contactsCount+fbCount + 2) return VIEW_TYPE_FB;
        }
        if (fbCount != 0) {
            if (position == 0) return VIEW_TYPE_FB_HEADER;
            else return VIEW_TYPE_FB;
        }
        return VIEW_TYPE_INVITE_HEADER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_CONTACT_HEADER || viewType == VIEW_TYPE_FB_HEADER || viewType == VIEW_TYPE_INVITE_HEADER)
            return new HeaderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_header, parent, false));

        if (viewType == VIEW_TYPE_INVITE)
            return new InvitationHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_request, parent, false));

        return new SuggestedHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_suggested, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_TYPE_INVITE_HEADER) {
            ((HeaderHolder) holder).header.setText(((HeaderHolder) holder).header.getContext().getString(R.string.invitations));
        } else if (getItemViewType(position) == VIEW_TYPE_CONTACT_HEADER) {
            ((HeaderHolder) holder).header.setText(((HeaderHolder) holder).header.getContext().getString(R.string.contacts));
        } else if (getItemViewType(position) == VIEW_TYPE_FB_HEADER) {
            ((HeaderHolder) holder).header.setText(((HeaderHolder) holder).header.getContext().getString(R.string.facebook));
        } else if (getItemViewType(position) == VIEW_TYPE_INVITE) {
            Picasso.with(holder.itemView.getContext())
                    .load(invitationList.get(position-1).getAvatar())
                    .transform(new CircleTransformation())
                    .into(((InvitationHolder) holder).avatar);
            ((InvitationHolder) holder).username.setText(invitationList.get(position - 1).getName());
        } else if (getItemViewType(position) == VIEW_TYPE_CONTACT) {
            ((SuggestedHolder) holder).setUp(contactsList.get(position - invitationCount - 1 - (invitationCount > 0 ? 1 : 0)));
        } else {
            ((SuggestedHolder) holder).setUp(fbList.get(position - invitationCount - contactsCount - 1 - (invitationCount > 0 ? 1 : 0) - (contactsCount > 0 ? 1 : 0)));
        }
    }

    @Override
    public int getItemCount() {
        if (contactsList != null) contactsCount = contactsList.size();
        if (fbList != null) fbCount = fbList.size();
        if (invitationList != null) invitationCount = invitationList.size();
        int result = 0;
        if (invitationCount > 0) result+=invitationCount + 1;
        if (contactsCount > 0) result+=contactsCount + 1;
        if (fbCount > 0) result+=fbCount + 1;
        return result;
    }

    abstract class BaseHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.avatar)
        protected ImageView avatar;
        @Bind(R.id.username)
        protected TextView username;

        public BaseHolder(View itemView) {
            super(itemView);
        }
    }

    class SuggestedHolder extends BaseHolder {

        @Bind(R.id.in_friends)
        protected ImageView inFriends;
        @Bind(R.id.add_to_friends)
        protected ImageView addToFriend;
        @Bind(R.id.invited)
        protected TextView invited;

        public SuggestedHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.add_to_friends)
        protected void addToFriend() {
            if (getItemViewType() == VIEW_TYPE_INVITE) {
         //       acceptFriendInviteListener.onAddToFriendClick(invitationList.get(getAdapterPosition() - 1).getId());
            } else if (getItemViewType() == VIEW_TYPE_CONTACT) {
       //         acceptFriendInviteListener.onAddToFriendClick(contactsList.get(getAdapterPosition() - invitationCount - 1 - (invitationCount > 0 ? 1 : 0)).getId());
            } else {
         //       acceptFriendInviteListener.onAddToFriendClick(fbList.get(getAdapterPosition() - invitationCount - contactsCount - 1 - (invitationCount > 0 ? 1 : 0) - (contactsCount > 0 ? 1 : 0)).getId());
            }
        }

        @OnClick(R.id.invited)
        protected void removeInvite() {
            if (getItemViewType() == VIEW_TYPE_CONTACT) {
        //        acceptFriendInviteListener.onInviteRemoved(contactsList.get(getAdapterPosition() - invitationCount - 1 - (invitationCount > 0 ? 1 : 0)).getId());
            } else {
        //        acceptFriendInviteListener.onInviteRemoved(fbList.get(getAdapterPosition() - invitationCount - contactsCount - 1 - (invitationCount > 0 ? 1 : 0) - (contactsCount > 0 ? 1 : 0)).getId());
            }
        }

        public void setUp(User user) {
            if (user.getIsFriend() == 1) {
                inFriends.setVisibility(View.VISIBLE);
                addToFriend.setVisibility(View.GONE);
                invited.setVisibility(View.GONE);
            } else if (user.getInvited() == 1) {
                inFriends.setVisibility(View.GONE);
                addToFriend.setVisibility(View.GONE);
                invited.setVisibility(View.VISIBLE);
            } else {
                inFriends.setVisibility(View.GONE);
                invited.setVisibility(View.GONE);
                addToFriend.setVisibility(View.VISIBLE);
            }
            Picasso.with(itemView.getContext())
                    .load(user.getAvatar())
                    .transform(new CircleTransformation())
                    .into(avatar);
            String name = user.getName();
            username.setText(name.length()<40?name:name.substring(0, 37)+"...");
        }
    }

    class InvitationHolder extends BaseHolder {

        public InvitationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.add_to_friends)
        protected void addToFriend() {
      //      acceptFriendInviteListener.onRequestAccepted(invitationList.get(getAdapterPosition() - 1).getId());

        }

        @OnClick(R.id.delete)
        protected void onDeleteRequest() {
        //    acceptFriendInviteListener.onRequestRemoved(invitationList.get(getAdapterPosition() - 1).getId());
        }
    }

    class HeaderHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.header)
        protected TextView header;

        public HeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}