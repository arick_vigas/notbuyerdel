package com.notibuyer.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.notibuyer.app.model.Circle;
import com.notibuyer.app.ui.fragments.TaskListFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskPagerAdapter extends FragmentPagerAdapter {

    private List<Circle> circleList = new ArrayList<>();
    private String current;
    public TaskPagerAdapter(FragmentManager fm) {
        super(fm);
        Circle all = new Circle();
        all.setName("All");
        all.setIndex(-1L);
        all.setFbKey("All");
        circleList.add(all);
    }

    public void setCurrent(String id){
        this.current = id;
    }

    @Override
    public Fragment getItem(int position) {
        return TaskListFragment.getInstance(circleList.get(position).getFbKey(), position);
    }

    @Override
    public int getCount() {
        return circleList.size();
    }

    public void addItem(Circle item){
        for(Circle curr:circleList){
            if(curr.getFbKey().equals(item.getFbKey()))
                return;
        }
        circleList.add(item);
        notifyDataSetChanged();
       // Collections.sort(circleList, (lhs, rhs)-> lhs.getIndex().compareTo(rhs.getIndex()));
       // notifyDataSetChanged();
    }

    public void removeItem(String id){
        Circle toRemove = null;
        for(Circle currentCircle:circleList){
            if(currentCircle.getFbKey().equals(id)){
                toRemove =currentCircle;
                break;
            }
        }
        if(toRemove!=null){
            circleList.remove(toRemove);
            notifyDataSetChanged();

        }
    }

    public void onIndexChanged(int fromPosition, int toPosition) {
        Collections.swap(circleList, fromPosition, toPosition);
        notifyDataSetChanged();
    }

}
