package com.notibuyer.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.helper.TagCircleFactory;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.model.Task;
import com.notibuyer.app.network.firebase.TaskService;
import com.notibuyer.app.ui.activity.MainActivity;
import com.notibuyer.app.ui.activity.SearchActivity;
import com.notibuyer.app.ui.activity.TaskViewActivity;
import com.notibuyer.app.ui.fragments.TaskListFragment;
import com.notibuyer.app.ui.view.CircleListLayout;
import com.notibuyer.app.ui.view.TagCircleView;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.NetworkUtils;
import com.notibuyer.app.utils.TaskDiskCache;
import com.notibuyer.app.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by alexander on 11.08.2016.
 */
public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.TaskHolder> {
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private Context context;
    private List<Task> tasks = new ArrayList<>();
    private List<Task> filteredTasks = new ArrayList<>();
    private List<Task> savedTasks = new ArrayList<>();
    private String circleId;
    private TaskService taskService;
    private EventListener mEventListener;
    private TaskDiskCache taskDiskCache;

    public boolean isPressed = false;
    private Timer timer;

    public TasksAdapter(Context context, String circleId, TaskService taskService, TaskDiskCache taskDiskCache) {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        this.context = context;
        this.circleId = circleId;
        this.taskService = taskService;
        this.taskDiskCache = taskDiskCache;
    }

    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TaskHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.task_layout, parent, false));
    }


    @Override
    public void onBindViewHolder(TaskHolder holder, int position) {

        Task task = tasks.get(position);
        holder.taskImage.setImageBitmap(null);
        if (!NetworkUtils.isOn(context)) {


            if (taskDiskCache.getPictureForTask(task.getFbKey()).exists()) {
                System.out.println("cached " + taskDiskCache.getPictureForTask(task.getFbKey()).exists());
                Bitmap bitmap = BitmapFactory.decodeFile(taskDiskCache.getPictureForTask(task.getFbKey()).getAbsolutePath());
                holder.taskImage.setImageBitmap(bitmap);
                System.out.println("loaded from cache");
                holder.taskImage.setVisibility(View.VISIBLE);
            }
        }

        holder.completed.setChecked(false);
        String parent = "";
        if(task.isCompleted()){
            parent = "completed_tasks";
        } else {
            parent = "current_tasks";
        }

        holder.completedBy.setVisibility(View.GONE);
        holder.resolverName.setVisibility(View.GONE);

        databaseReference.child("circles").child(circleId).child(parent).child(task.getFbKey()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("index")) {
                    task.setIndex((Long) dataSnapshot.getValue());
                    sortTasks();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("index")) {
                    task.setIndex((Long) dataSnapshot.getValue());
                    sortTasks();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("TasksAdapter", databaseError.getMessage());
            }
        });
        holder.completed.setChecked(false);


        //holder.date.setText("Yesterday, 23.22");
        holder.circleListLayout.removeAllViews();
        holder.circleListLayout.setVisibility(View.GONE);
        holder.search.setOnClickListener((v) ->{
           /* if (!TextUtils.isEmpty(task.getImageUrl())) {
                DialogFactory.getSomeDialog(context, R.string.choose_action, R.string.text, R.string.image,
                        (dialog, which) -> {
                            SearchActivity.searchText((MainActivity)context, task.getText());
                        }, (dialog, which) -> {
                            SearchActivity.searchImage((MainActivity)context, task.getImageUrl());
                        }).show();
            } else {*/
                SearchActivity.searchText((MainActivity)context, task.getText());
           // }
        });
        holder.title.setOnClickListener((v) -> TaskViewActivity.start(context, task.getFbKey()));
        holder.mContainer.setOnClickListener((v) -> TaskViewActivity.start(context, task.getFbKey()));
        holder.itemView.setOnClickListener((v) -> TaskViewActivity.start(context, task.getFbKey()));
        holder.completed.setOnCheckedChangeListener((buttonView, isChecked)->getEventListener().onCheckBoxClicked(buttonView, position, isChecked, task.getFbKey(), task.isCompleted()));

        holder.title.setText(task.getText());
        if (task.getOwnerName() == null) {
            databaseReference.child("users").child(task.getOwnerId()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot data) {
                    task.setOwnerName((String) data.getValue());
                    holder.name.setText(task.getOwnerName());
                    databaseReference.child("tasks").child(task.getFbKey()).child("ownerName").setValue(task.getOwnerName());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e("TasksAdapter", databaseError.getMessage());
                }
            });
        } else {
            holder.name.setText(task.getOwnerName());
        }
        holder.date.setText(Utils.getFormattedDate(holder.itemView.getContext(), new Date(task.getCreated())));
        if (task.getImageUrl() != null) {
            holder.taskImage.setVisibility(View.VISIBLE);
            holder.search.setVisibility(View.VISIBLE);
            if (NetworkUtils.isOn(context)) {
                Picasso.with(holder.itemView.getContext())
                        .load(task.getImageUrl())
                        .placeholder(R.drawable.progress_loading_animation)
                        .fit().centerCrop()
                        .into(holder.taskImage);
            } else {
                new Picasso.Builder(context)
                        .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                        .build().with(holder.itemView.getContext())
                        .load(task.getImageUrl()).networkPolicy(NetworkPolicy.OFFLINE)
                        .placeholder(R.drawable.progress_loading_animation)
                        .fit()
                        .centerCrop().into(holder.taskImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d("Picasso", "Loaded from CACHE");
                    }

                    @Override
                    public void onError() {

                    }
                });
            }
        }

        if (task.isCompleted()) {
            holder.checkedOverlay.setVisibility(View.VISIBLE);
            holder.title.setPaintFlags(holder.title.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.mContainer.setOnLongClickListener(v -> {
                DialogFactory.getSomeDialog(context, R.string.choose_action, R.string.remove, R.string.restore,
                        (dialog, which) -> {
                            databaseReference.child("circles").child(circleId).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    // if user is owner of task or user is owner of circle
                                    if (dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())) {
                                        taskService.removeTask(task);
                                    } else {
                                        //NO PERMISSION TO REMOVE (USER IS NOT OWNER!!!)
                                        //notifyItemChanged(position);
                                        Toast.makeText(context, context.getString(R.string.no_permissions_for_action), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e("TaskAdapter", databaseError.getMessage());
                                }
                            });
                        }, (dialog, which) -> {
                            databaseReference.child("circles").child(circleId).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    // if user is owner of task or user is owner of circle
                                    if (dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())) {
                                        taskService.resolveTask(task);
                                    } else {
                                        //NO PERMISSION TO REMOVE (USER IS NOT OWNER!!!)
                                        //notifyItemChanged(position);
                                        Toast.makeText(context, context.getString(R.string.no_permissions_for_action), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e("TaskAdapter", databaseError.getMessage());
                                }
                            });
                        }).show();
                return true;
            });
        } else {
            holder.checkedOverlay.setVisibility(View.INVISIBLE);
            holder.title.setPaintFlags(holder.title.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            holder.mContainer.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    isPressed = true;
                    return false;
                }
            });



            //task is current, only remove
        }

        if (task.getCompletedBy() != null) {
            if (task.isCompleted()) {
                holder.completedBy.setVisibility(View.VISIBLE);
                holder.resolverName.setVisibility(View.VISIBLE);
                if (task.getCompletedBy().equals(mFirebaseUser.getUid()))
                    holder.resolverName.setText("You");
                else {
                    if (task.getCompleterName() == null) {
                        databaseReference.child("users").child(task.getCompletedBy()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                task.setCompleterName((String) dataSnapshot.getValue());
                                holder.resolverName.setText(task.getCompleterName());
                                databaseReference.child("tasks").child(task.getFbKey()).child("completerName").setValue(task.getCompleterName());
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e("TasksAdapter", databaseError.getMessage());
                            }
                        });
                    } else {
                        holder.resolverName.setText(task.getCompleterName());
                    }
                }
            }
        }

        if (task.getResolvedBy() != null) {
            if (!task.isCompleted()) {
                holder.completedBy.setVisibility(View.VISIBLE);
                holder.completedBy.setText("Restored by");
                holder.resolverName.setVisibility(View.VISIBLE);
                if (task.getResolvedBy().equals(mFirebaseUser.getUid()))
                    holder.resolverName.setText("You");
                else {
                    if (task.getResolverName() == null) {
                        databaseReference.child("users").child(task.getResolvedBy()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                task.setResolverName((String) dataSnapshot.getValue());
                                holder.resolverName.setText(task.getResolverName());
                                databaseReference.child("tasks").child(task.getFbKey()).child("resolverName").setValue(task.getCompleterName());
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e("TasksAdapter", databaseError.getMessage());
                            }
                        });
                    } else {
                        holder.resolverName.setText(task.getResolverName());
                    }
                }
            }
        }

        if (task.getCircleId() != null) {
            if (circleId.equalsIgnoreCase("all")) {
                List<String> circles = new ArrayList<String>();
                List<Circle> taskCircles = new ArrayList();
                circles.add(task.getCircleId());
                databaseReference.child("circles").child(task.getCircleId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot data) {
                        holder.circleListLayout.removeAllViews();
                        Circle circle = new Circle();
                        circle.setFbKey(data.getKey());
                        circle.setName((String) data.child("name").getValue());
                        circle.setColor((String) data.child("color").getValue());
                        if (!circle.getName().equals("Private")) {
                            taskCircles.add(circle);
                            TagCircleFactory circleViewFactory = new TagCircleFactory();
                            List<TagCircleView> circleViews = circleViewFactory.createCircles(holder.circleListLayout.getContext(), taskCircles);
                            holder.circleListLayout.addCircles(circleViews);
                            holder.circleListLayout.setUp();
                            holder.circleListLayout.setVisibility(View.VISIBLE);

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e("TasksAdapter", databaseError.getMessage());
                    }
                });
            }
        }

        databaseReference.child("tasks").child(task.getFbKey()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (task.getText() == null && dataSnapshot.getKey().equals("text")) {
                    holder.title.setText((String) dataSnapshot.getValue());
                    task.setText((String) dataSnapshot.getValue());
                }
                if (task.getImageUrl() == null && dataSnapshot.getKey().equals("imageUrl")) {
                    task.setImageUrl((String) dataSnapshot.getValue());
                    holder.taskImage.setVisibility(View.VISIBLE);
                    holder.search.setVisibility(View.VISIBLE);
                    if (NetworkUtils.isOn(context)) {
                        Picasso.with(holder.itemView.getContext())
                                .load((String) dataSnapshot.getValue())
                                .placeholder(R.drawable.progress_loading_animation)
                                .fit().centerCrop()
                                .into(holder.taskImage);
                    } else {
                        new Picasso.Builder(context)
                                .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                                .build().with(holder.itemView.getContext())
                                .load((String) dataSnapshot.getValue()).networkPolicy(NetworkPolicy.OFFLINE)
                                .placeholder(R.drawable.progress_loading_animation)
                                .fit()
                                .centerCrop().into(holder.taskImage, new Callback() {
                            @Override
                            public void onSuccess() {
                                Log.d("Picasso", "Loaded from CACHE");
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }
                }
            /*    switch(dataSnapshot.getKey()){
                    case "text":
                        holder.title.setText((String)dataSnapshot.getValue());
                        task.setText((String)dataSnapshot.getValue());
                        break;
                    case "ownerId":
                        task.setOwnerId((String)dataSnapshot.getValue());
                        databaseReference.child("users").child((String)dataSnapshot.getValue()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot data) {
                                holder.name.setText((String)data.getValue());
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e("TasksAdapter", databaseError.getMessage());
                            }
                        });
                        break;
                    case "created":
                        task.setCreated((long)dataSnapshot.getValue());
                        holder.date.setText(Utils.getFormattedDate(holder.itemView.getContext(), new Date(task.getCreated())));
                        break;

                    case "circles":



                        break;
                    case "imageUrl":
                        task.setImageUrl((String)dataSnapshot.getValue());
                        holder.taskImage.setVisibility(View.VISIBLE);
                        holder.search.setVisibility(View.VISIBLE);
                        if(NetworkUtils.isOn(context)){
                            Picasso.with(holder.itemView.getContext())
                                    .load((String)dataSnapshot.getValue())
                                    .placeholder(R.drawable.progress_loading_animation)
                                    .fit().centerCrop()
                                    .into(holder.taskImage);
                        } else {
                            new Picasso.Builder(context)
                                    .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                                    .build().with(holder.itemView.getContext())
                                    .load((String) dataSnapshot.getValue()).networkPolicy(NetworkPolicy.OFFLINE)
                                    .placeholder(R.drawable.progress_loading_animation)
                                    .fit()
                                    .centerCrop().into(holder.taskImage, new Callback() {
                                @Override
                                public void onSuccess() {
                                    Log.d("Picasso", "Loaded from CACHE");
                                }

                                @Override
                                public void onError() {

                                }
                            });
                        }
                        break;
                    case "completed":
                        task.setCompleted((boolean)dataSnapshot.getValue());
                        if((boolean)dataSnapshot.getValue()) {
                            holder.checkedOverlay.setVisibility(View.VISIBLE);
                            holder.title.setPaintFlags(holder.title.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            holder.mContainer.setOnLongClickListener(v -> {
                                DialogFactory.getSomeDialog(context, R.string.choose_action, R.string.remove, R.string.restore,
                                        (dialog, which) -> {
                                            databaseReference.child("circles").child(circleId).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    // if user is owner of task or user is owner of circle
                                                    if (dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())) {
                                                        taskService.removeTask(task);
                                                    } else {
                                                        //NO PERMISSION TO REMOVE (USER IS NOT OWNER!!!)
                                                        //notifyItemChanged(position);
                                                        Toast.makeText(context, context.getString(R.string.no_permissions_for_action), Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                    Log.e("TaskAdapter", databaseError.getMessage());
                                                }
                                            });
                                        }, (dialog, which) -> {
                                            databaseReference.child("circles").child(circleId).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    // if user is owner of task or user is owner of circle
                                                    if (dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())) {
                                                        taskService.resolveTask(task);
                                                    } else {
                                                        //NO PERMISSION TO REMOVE (USER IS NOT OWNER!!!)
                                                        //notifyItemChanged(position);
                                                        Toast.makeText(context, context.getString(R.string.no_permissions_for_action), Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                    Log.e("TaskAdapter", databaseError.getMessage());
                                                }
                                            });
                                        }).show();
                                return true;
                            });
                        } else {
                            holder.checkedOverlay.setVisibility(View.INVISIBLE);
                            holder.title.setPaintFlags(holder.title.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                            //task is current, only remove

                        }
                        break;
                    case "completedBy":
                        if(task.isCompleted()) {
                            task.setCompletedBy((String) dataSnapshot.getValue());
                            holder.completedBy.setVisibility(View.VISIBLE);
                            holder.resolverName.setVisibility(View.VISIBLE);
                            if (task.getCompletedBy().equals(mFirebaseUser.getUid()))
                                holder.resolverName.setText("You");
                            else {
                                databaseReference.child("users").child(task.getCompletedBy()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        holder.resolverName.setText((String) dataSnapshot.getValue());
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e("TasksAdapter", databaseError.getMessage());
                                    }
                                });

                            }
                        }
                        break;
                    case "resolvedBy":
                        if(!task.isCompleted()) {
                            task.setResolvedBy((String) dataSnapshot.getValue());
                            holder.completedBy.setVisibility(View.VISIBLE);
                            holder.completedBy.setText("Restored by");
                            holder.resolverName.setVisibility(View.VISIBLE);
                            if (task.getResolvedBy().equals(mFirebaseUser.getUid()))
                                holder.resolverName.setText("You");
                            else {
                                databaseReference.child("users").child(task.getResolvedBy()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        holder.resolverName.setText((String) dataSnapshot.getValue());
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e("TasksAdapter", databaseError.getMessage());
                                    }
                                });

                            }
                        }
                        break;
                    case "circleId":
                        task.setCircleId((String)dataSnapshot.getValue());
                        if(circleId.equalsIgnoreCase("all")){
                            List<String> circles = new ArrayList<String>();
                            List<Circle> taskCircles = new ArrayList();
                                circles.add(task.getCircleId());
                                databaseReference.child("circles").child(task.getCircleId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot data) {
                                        holder.circleListLayout.removeAllViews();
                                        Circle circle = new Circle();
                                        circle.setFbKey(data.getKey());
                                        circle.setName((String)data.child("name").getValue());
                                        circle.setColor((String)data.child("color").getValue());
                                        if (!circle.getName().equals("Private")){
                                            taskCircles.add(circle);
                                            TagCircleFactory circleViewFactory = new TagCircleFactory();
                                            List<TagCircleView> circleViews = circleViewFactory.createCircles(holder.circleListLayout.getContext(), taskCircles);
                                            holder.circleListLayout.addCircles(circleViews);
                                            holder.circleListLayout.setUp();
                                            holder.circleListLayout.setVisibility(View.VISIBLE);

                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e("TasksAdapter", databaseError.getMessage());
                                    }
                                });
                            }
                        break;
                }
*/
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                switch(dataSnapshot.getKey()){
                    case "created":
                        task.setCreated((long)dataSnapshot.getValue());
                        holder.date.setText(new Date(task.getCreated()).toString());
                        break;
                    case "text":
                        task.setText((String)dataSnapshot.getValue());
                        holder.title.setText((String)dataSnapshot.getValue());
                        break;
                    case "ownerId":
                        break;
                    case "imageUrl":
                        task.setImageUrl((String)dataSnapshot.getValue());
                        holder.taskImage.setVisibility(View.VISIBLE);
                        if (NetworkUtils.isOn(context)) {
                            Picasso.with(holder.itemView.getContext())
                                    .load((String) dataSnapshot.getValue())
                                    .placeholder(R.drawable.progress_loading_animation)
                                    .fit().centerCrop()
                                    .into(holder.taskImage);
                        } else {
                            new Picasso.Builder(context)
                                    .downloader(new OkHttpDownloader(context, Integer.MAX_VALUE))
                                    .build().with(holder.itemView.getContext())
                                    .load((String) dataSnapshot.getValue()).networkPolicy(NetworkPolicy.OFFLINE)
                                    .placeholder(R.drawable.progress_loading_animation)
                                    .fit()
                                    .centerCrop().into(holder.taskImage, new Callback() {
                                @Override
                                public void onSuccess() {
                                    Log.d("Picasso", "Loaded from CACHE");
                                }

                                @Override
                                public void onError() {

                                }
                            });
                        }

                        break;
                    case "circles":
                        List<String> circles = new ArrayList<>();
                        List<Circle> taskCircles = new ArrayList();
                        for(DataSnapshot circleId:dataSnapshot.getChildren()){
                            circles.add(circleId.getKey());
                            databaseReference.child("circles").child(circleId.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot data) {
                                    Circle circle = new Circle();
                                    circle.setFbKey(data.getKey());
                                    circle.setName((String) data.child("name").getValue());
                                    circle.setColor((String) data.child("color").getValue());
                                    if (!circle.getName().equals("Private")){
                                        taskCircles.add(circle);
                                        Collections.sort(taskCircles, ((lhs, rhs) -> lhs.getName().compareTo(rhs.getName())));
                                        TagCircleFactory circleViewFactory = new TagCircleFactory();
                                        List<TagCircleView> circleViews = circleViewFactory.createCircles(holder.circleListLayout.getContext(), taskCircles);
                                        holder.circleListLayout.addCircles(circleViews);
                                        holder.circleListLayout.setUp();
                                        holder.circleListLayout.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e("TasksAdapter", databaseError.getMessage());
                                }
                            });
                        }


                        break;
                    case "completedBy":
                        task.setCompletedBy((String)dataSnapshot.getValue());
                        holder.completedBy.setVisibility(View.VISIBLE);
                        holder.resolverName.setVisibility(View.VISIBLE);
                        if(task.getCompletedBy().equals(mFirebaseUser.getUid()))
                            holder.resolverName.setText("You");
                        else
                            holder.resolverName.setText(task.getCompletedBy());
                        break;
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getKey().equals("imageUrl")){
                    holder.taskImage.setVisibility(View.GONE);
                    holder.taskImage.setImageBitmap(null);
                    task.setImageUrl(null);
                }

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("TasksAdapter", databaseError.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public void addItem(Task task){
        for(Task curr:tasks){
            if(curr.getFbKey().equals(task.getFbKey())){
                int pos = tasks.indexOf(curr);
                tasks.set(pos, task);
                notifyItemChanged(pos, task);
                return;
            }
        }

        tasks.add(task);
        notifyItemInserted(tasks.size());

        sortTasks();
    }

    public void removeItem(String id){
        int pos = -1;
        Task removeItem = null;
        for(Task item:tasks){
            if(item.getFbKey().equals(id)){
                pos = tasks.indexOf(item);
                removeItem = item;
                break;
            }
        }
        if(removeItem!=null){
            tasks.remove(removeItem);
            notifyItemRemoved(pos);
        }
    }

    public void onIndexChanged(int fromPosition, int toPosition, boolean completed) {
        String parent = "";
        if(completed)
            parent = "completed_tasks";
        else
            parent = "current_tasks";
        try {
            String fromId = tasks.get(fromPosition).getFbKey();
            String toId = tasks.get(toPosition).getFbKey();
            databaseReference.child("circles").child(circleId).child(parent).child(fromId).child("index").setValue(tasks.get(toPosition).getIndex());
            databaseReference.child("circles").child(circleId).child(parent).child(toId).child("index").setValue(tasks.get(fromPosition).getIndex());
            Collections.swap(tasks, fromPosition, toPosition);
            notifyItemMoved(fromPosition, toPosition);
        }
        catch(Exception e){
            System.err.println(e.toString());
        }
    }

    public void solveTask(int position){
        Task task = tasks.get(position);
        taskService.completeTask(task);
    }

    public void solveTask(String taskId){
        for(Task task: tasks){
            if(task.getFbKey().equals(taskId)) {
                taskService.completeTask(task);
                break;
            }
        }
    }

    public void resolveTask(String taskId){
        for(Task task: tasks){
            if(task.getFbKey().equals(taskId)) {
                databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //if user owner of the circle or author of the task
                        if(mFirebaseUser.getUid().equals(dataSnapshot.getValue()) || task.getOwnerId().equals(mFirebaseUser.getUid())) {
                            taskService.resolveTask(task);
                        } else {
                            //no permissions
                            Toast.makeText(context, context.getString(R.string.no_permissions_for_action), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("TasksAdapter", databaseError.getMessage());
                    }
                });
                break;
            }
        }
    }

    public void resolveTask(int position){
        Task task = tasks.get(position);
        databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //if user owner of the circle or author of the task
                if(mFirebaseUser.getUid().equals(dataSnapshot.getValue()) || task.getOwnerId().equals(mFirebaseUser.getUid())) {
                    taskService.resolveTask(task);
                } else {
                    //no permissions
                    Toast.makeText(context, context.getString(R.string.no_permissions_for_action), Toast.LENGTH_SHORT).show();
                    //release after swipe
                    notifyItemChanged(position);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("TasksAdapter", databaseError.getMessage());
            }
        });
    }

    public void removeItemByPos(int position){
        Task task = tasks.get(position);
        databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //if user owner of the circle or author of the task
                if(mFirebaseUser.getUid().equals(dataSnapshot.getValue()) || task.getOwnerId().equals(mFirebaseUser.getUid())) {
                    taskService.removeTask(task);
                } else {
                    //no permissions
                    Toast.makeText(context, context.getString(R.string.no_permissions_for_action), Toast.LENGTH_SHORT).show();
                    //release after swipe
                    notifyItemChanged(position);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("TasksAdapter", databaseError.getMessage());
            }
        });
    }

    public void removeTask(String taskId){
        for(Task task: tasks){
            if(task.getFbKey().equals(taskId)) {
                databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //if user owner of the circle or author of the task
                        if(mFirebaseUser.getUid().equals(dataSnapshot.getValue()) || task.getOwnerId().equals(mFirebaseUser.getUid())) {
                            taskService.removeTask(task);
                        } else {
                            //no permissions
                            Toast.makeText(context, context.getString(R.string.no_permissions_for_action), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("TasksAdapter", databaseError.getMessage());
                    }
                });
                break;
            }
        }
    }

    public String getTaskId(int position){
        return tasks.get(position).getFbKey();
    }

    public void filter(String query){
        filteredTasks.clear();
        if(savedTasks.size()==0) {
            savedTasks.addAll(tasks);
        } else {
            tasks.clear();
            tasks.addAll(savedTasks);
        }
        for(Task item:tasks){
            if(item.getText()!=null && item.getText().toLowerCase().contains(query.toLowerCase())){
                filteredTasks.add(item);
            }
        }
        tasks.clear();
        tasks.addAll(filteredTasks);
        notifyDataSetChanged();
    }

    public EventListener getEventListener() {
        return mEventListener;
    }

    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    public void onTaskChanged(Task task) {
        for (Task current : tasks) {
            if (current.getFbKey().equals(task)) {
                int pos = tasks.indexOf(current);
                tasks.set(pos, task);
                notifyItemChanged(pos);
                break;
            }
        }

    }

    public void sortTasks() {
        Log.d("TASKS", "SORTINGG");
        for(int i = 0; i <= tasks.size()-1; i++) {
            Task task = tasks.get(i);
            if(task.getIndex() == null) continue;

            for (int j = i + 1; j <= tasks.size() - 1; j++) {
                Task nextTask = tasks.get(j);
                if(nextTask.getIndex() == null) continue;

                if(task.getIndex() < nextTask.getIndex()) {
                    Collections.swap(tasks, i, j);
                    notifyItemChanged(i, nextTask);
                    notifyItemChanged(j, task);
                }
            }
        }
    }

    public interface EventListener {
        void onCheckBoxClicked(CompoundButton button, int childPosition, boolean isChecked, String id, boolean completed);
    }

    public class TaskHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.container)
        protected RelativeLayout mContainer;
        @Bind(R.id.picture)
        protected ImageView taskImage;
        @Bind(R.id.title)
        protected TextView title;
        @Bind(R.id.name)
        protected TextView name;
        @Bind(R.id.resolver_name)
        protected TextView resolverName;
        @Bind(R.id.completed_by)
        protected TextView completedBy;
        @Bind(R.id.date)
        protected TextView date;
        @Bind(R.id.checkbox)
        protected CheckBox completed;
        @Bind(R.id.checked_overlay)
        protected View checkedOverlay;
        @Bind(R.id.circles_list_layout)
        protected CircleListLayout circleListLayout;
        @Bind(R.id.search)
        protected ImageView search;
        @Bind(R.id.complete_layout)
        protected LinearLayout completeButton;
        @Bind(R.id.add_to_circle_layout)
        protected LinearLayout addToCircleButton;

        public TaskHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            completed.setChecked(false);
        }

        public View getContainer() {
            return mContainer;
        }
    }
}
