package com.notibuyer.app.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.notibuyer.app.Prefs;
import com.notibuyer.app.service.ShakeService;

public class BootReceiver extends BroadcastReceiver {
    
    @Override
    public void onReceive(Context context, Intent intent) {
        Prefs prefs = new Prefs(context);
        if (prefs.isShakeEnabled()) ShakeService.startService(context);
    }
}
