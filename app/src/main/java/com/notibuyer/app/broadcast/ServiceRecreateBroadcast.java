package com.notibuyer.app.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.notibuyer.app.App;
import com.notibuyer.app.service.ShakeService;

public class ServiceRecreateBroadcast extends BroadcastReceiver {

    public static final String ACTION_RECREATE_SERVICE = "com.notibuyer.ACTION_RECREATE_SERVICE";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (ACTION_RECREATE_SERVICE.equals(intent.getAction())) {
            if (!ShakeService.isServiceRunning(context)) {
                context.startService(new Intent(context, ShakeService.class));
            }
        }
    }
}
