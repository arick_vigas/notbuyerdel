package com.notibuyer.app.dagger;

import com.notibuyer.app.App;
import com.notibuyer.app.broadcast.BootReceiver;
import com.notibuyer.app.network.firebase.CirclesService;
import com.notibuyer.app.network.firebase.TaskService;
import com.notibuyer.app.service.ShakeService;
import com.notibuyer.app.ui.activity.AddCircleActivity;
import com.notibuyer.app.ui.activity.BaseActivity;
import com.notibuyer.app.ui.activity.BaseAuthActivity;
import com.notibuyer.app.ui.activity.ChangePasswordActivity;
import com.notibuyer.app.ui.activity.ChooseCircleActivity;
import com.notibuyer.app.ui.activity.EditCircleActivity;
import com.notibuyer.app.ui.activity.EditProfileActivity;
import com.notibuyer.app.ui.activity.MainActivity;
import com.notibuyer.app.ui.activity.NewTaskActivity;
import com.notibuyer.app.ui.activity.SearchFriendsActivity;
import com.notibuyer.app.ui.activity.TaskViewActivity;
import com.notibuyer.app.ui.activity.TermsActivity;
import com.notibuyer.app.ui.activity.UserProfileActivity;
import com.notibuyer.app.ui.fragments.AllNotesFragment;
import com.notibuyer.app.ui.fragments.BaseFragment;
import com.notibuyer.app.ui.fragments.CirclesFragment;
import com.notibuyer.app.ui.fragments.FriendsFragment;
import com.notibuyer.app.ui.fragments.NotificationsFragment;
import com.notibuyer.app.ui.fragments.SettingsFragment;
import com.notibuyer.app.ui.fragments.TaskListFragment;
import com.notibuyer.app.utils.TaskDiskCache;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by alexander on 25.08.2016.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    // application
    void inject(App application);

    // activities
    void inject(BaseActivity baseActivity);
    void inject(MainActivity mainActivity);
    void inject(BaseAuthActivity baseAuthActivity);
    void inject(EditCircleActivity editCircleActivity);
    void inject(AddCircleActivity addCircleActivity);
    void inject(ChangePasswordActivity changePasswordActivity);
    void inject(ChooseCircleActivity chooseCircleActivity);
    void inject(EditProfileActivity editProfileActivity);
    void inject(NewTaskActivity taskActivity);
    void inject(TaskViewActivity taskViewActivity);
    void inject(TermsActivity termsActivity);
    void inject(UserProfileActivity userProfileActivity);
    void inject(SearchFriendsActivity searchFriendsActivity);



    // fragments
    void inject(BaseFragment fragment);
    void inject(SettingsFragment settingsFragment);
    void inject(CirclesFragment circlesFragment);
    void inject(AllNotesFragment allNotesFragment);
    void inject(FriendsFragment friendsFragment);
    void inject(NotificationsFragment notificationsFragment);
    void inject(TaskListFragment taskListFragment);

    // service
    void inject(ShakeService shakeService);
    //broadcast
    void inject(BootReceiver bootReceiver);

    void inject(TaskDiskCache taskDiskCache);

}