package com.notibuyer.app.dagger;

import android.app.Application;
import android.content.Context;

import com.notibuyer.app.Prefs;
import com.notibuyer.app.network.firebase.CirclesService;
import com.notibuyer.app.network.firebase.CommentService;
import com.notibuyer.app.network.firebase.TaskService;
import com.notibuyer.app.network.firebase.UserService;
import com.notibuyer.app.utils.TaskDiskCache;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by alexander on 25.08.2016.
 */
@Module
public class AppModule {

    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    Prefs providePrefs(Context context) {
        return new Prefs(context);
    }

    @Provides
    @Singleton
    TaskDiskCache provideTaskDiskCache(Context context){
        return new TaskDiskCache(context);
    }

    @Provides
    @Singleton
    TaskService provideTaskService(){
        return new TaskService();
    }

    @Provides
    @Singleton
    UserService provideUserService(){
        return new UserService();
    }

    @Provides
    @Singleton
    CirclesService provideCirclesService() { return new CirclesService();}

    @Provides
    @Singleton
    CommentService provideCommentService() { return  new CommentService();}
}
