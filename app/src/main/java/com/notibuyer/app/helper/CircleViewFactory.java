package com.notibuyer.app.helper;

import android.content.Context;
import android.view.View;

import com.notibuyer.app.model.Circle;

import java.util.List;

public abstract class CircleViewFactory {
    public abstract List<? extends View> createCircles(Context context, List<Circle> circleList);
}
