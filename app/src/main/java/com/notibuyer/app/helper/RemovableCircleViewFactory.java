package com.notibuyer.app.helper;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.notibuyer.app.R;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.ui.activity.MainActivity;
import com.notibuyer.app.ui.fragments.AllNotesFragment;
import com.notibuyer.app.ui.view.RemovableCircleView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RemovableCircleViewFactory extends CircleViewFactory {

    @Override
    public List<RemovableCircleView> createCircles(Context context, List<Circle> circleList) {
        if (circleList.isEmpty()) return Collections.emptyList();
        List<RemovableCircleView> viewList = new ArrayList<>(circleList.size());

        for (Circle circle: circleList) {
            RemovableCircleView removableCircleView = new RemovableCircleView(context);
            removableCircleView.setOnClickListener(v->{

                MainActivity.start(context, MainActivity.ALL, circle.getId());

                /*System.out.println(circle.getName());
                FragmentManager fragmentManager = ((MainActivity)context).getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                Fragment notes = new AllNotesFragment();
                Bundle args = new Bundle();
                args.putString("circle_id", circle.getId());
                notes.setArguments(args);
                transaction.replace(R.id.content, notes);
                transaction.addToBackStack(null);
                transaction.commit();
                ((MainActivity)context).selectDrawerItem(MainActivity.ALL);*/
            });
            removableCircleView.setUp(circle);
            viewList.add(removableCircleView);
        }
        return viewList;
    }
}
