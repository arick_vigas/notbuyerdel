package com.notibuyer.app.helper;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.notibuyer.app.R;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.ui.activity.MainActivity;
import com.notibuyer.app.ui.fragments.AllNotesFragment;
import com.notibuyer.app.ui.view.RemovableCircleView;
import com.notibuyer.app.ui.view.TagCircleView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TagCircleFactory extends CircleViewFactory {
    @Override
    public List<TagCircleView> createCircles(Context context, List<Circle> circleList) {
        if (circleList.isEmpty()) return Collections.emptyList();
        List<TagCircleView> viewList = new ArrayList<>(circleList.size());
        for (Circle circle: circleList) {
            TagCircleView tagCircleView = new TagCircleView(context);
            tagCircleView.setOnClickListener(v->{
                MainActivity.start(context, MainActivity.ALL, circle.getFbKey());
            });
            tagCircleView.setUp(circle);
            viewList.add(tagCircleView);
        }
        return viewList;
    }

}
