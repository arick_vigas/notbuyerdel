package com.notibuyer.app.interfaces;

public interface AcceptFriendInviteListener {
    void onAddToFriendClick(String userId);
    void onRequestAccepted(String userId);
    void onRequestRemoved(String userId);
    void onInviteRemoved(String userId);
}
