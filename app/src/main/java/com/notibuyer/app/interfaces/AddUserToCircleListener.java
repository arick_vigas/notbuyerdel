package com.notibuyer.app.interfaces;

public interface AddUserToCircleListener {
    void addCircleClick(String circleId, boolean add);
}
