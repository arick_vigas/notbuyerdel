package com.notibuyer.app.interfaces;

public interface AddUserToCircleListener2 {
    void addCircleClick(String circleId, boolean add, String userId, int pos);
}
