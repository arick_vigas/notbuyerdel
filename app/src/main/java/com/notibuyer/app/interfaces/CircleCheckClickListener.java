package com.notibuyer.app.interfaces;

import com.notibuyer.app.model.Circle;

public interface CircleCheckClickListener {
    void addCircleClick(String circleId);
}
