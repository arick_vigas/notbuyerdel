package com.notibuyer.app.interfaces;

public interface CreateTaskListener {
    void textChanged(String text, boolean quick);
}
