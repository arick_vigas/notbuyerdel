package com.notibuyer.app.interfaces;

/**
 * Created by alexander on 01.09.2016.
 */
public interface DrawableClickListener {

    public static enum DrawablePosition { TOP, BOTTOM, LEFT, RIGHT };
    public void onClick(DrawablePosition target);
}
