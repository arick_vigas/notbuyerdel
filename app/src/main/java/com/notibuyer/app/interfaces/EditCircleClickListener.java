package com.notibuyer.app.interfaces;

import com.notibuyer.app.model.Circle;

public interface EditCircleClickListener {
    void editCircleClick(String circleId);
    void outOfCircleClick(String circleId);
    void toCircleTasksClick(String circleId);
}
