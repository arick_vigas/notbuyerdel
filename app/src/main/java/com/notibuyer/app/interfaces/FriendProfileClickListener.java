package com.notibuyer.app.interfaces;

public interface FriendProfileClickListener {
    void onProfileClick(String id);
}
