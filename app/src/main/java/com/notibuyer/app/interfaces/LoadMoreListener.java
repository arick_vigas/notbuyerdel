package com.notibuyer.app.interfaces;

public interface LoadMoreListener {
    void loadMore(int page);
}
