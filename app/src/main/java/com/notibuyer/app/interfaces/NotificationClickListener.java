package com.notibuyer.app.interfaces;

import com.notibuyer.app.model.Notification;

public interface NotificationClickListener {
    void onNotificationClick(Notification notification);
}
