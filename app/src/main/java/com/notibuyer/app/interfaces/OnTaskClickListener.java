package com.notibuyer.app.interfaces;

public interface OnTaskClickListener {
    void onClick(int position);
}
