package com.notibuyer.app.interfaces;

public interface RestorePasswordListener {
    void onEmailReceived(String email);
}
