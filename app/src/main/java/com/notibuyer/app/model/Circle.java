package com.notibuyer.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.Map;


public class Circle {


    private String id;
    private String name;
    private String color = "#33b5e5";
    private String fbKey;
    private Long index;
    private long countTasks;

    @SerializedName("owner_id")
    private String ownerId;



    private Map<String, Boolean> members;


    @SerializedName("created")
    private Date created;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public Map<String, Boolean> getMembers() {
        return members;
    }

    public void setMembers(Map<String, Boolean> members) {
        this.members = members;
    }

    public String getFbKey(){
        return this.fbKey;
    }

    public void setFbKey(String fbKey) {
        this.fbKey = fbKey;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    public long getCountTasks() {
        return countTasks;
    }

    public void setCountTasks(long countTasks) {
        this.countTasks = countTasks;
    }
}