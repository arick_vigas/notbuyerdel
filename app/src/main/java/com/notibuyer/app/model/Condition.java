package com.notibuyer.app.model;

/**
 * Created by alexander on 26.08.2016.
 */
public class Condition {

    private String title;
    private String description;
    private String icon;
    public Condition(){}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
