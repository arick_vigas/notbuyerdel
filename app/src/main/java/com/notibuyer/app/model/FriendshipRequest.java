package com.notibuyer.app.model;

/**
 * Created by alexander on 16.08.2016.
 */
public class FriendshipRequest {
    private String userId;
    private String circle;
    private String provider;

    public FriendshipRequest(){

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
