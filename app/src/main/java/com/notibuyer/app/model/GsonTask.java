package com.notibuyer.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;
import lombok.Getter;

@Getter
public class GsonTask {
    @SerializedName("id")
    private String id;
    @SerializedName("desc")
    private String text;
    @SerializedName("owner_id")
    private String ownerId;
    @SerializedName("owner_name")
    private String ownerName;
    @SerializedName("media")
    private String audioUrl;
    @SerializedName("photo")
    private String imageUrl;
    @SerializedName("created")
    private Date created;
    @SerializedName("updated")
    private Date updated;
    @SerializedName("done")
    private String completed;
    @SerializedName("done_by_name")
    private String completedBy;
    @SerializedName("circles")
    private List<String> circles;
}
