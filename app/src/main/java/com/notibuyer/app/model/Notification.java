package com.notibuyer.app.model;

import com.google.gson.annotations.SerializedName;

public class Notification {

    public enum TYPE {
        CIRCLE(0), TASK(1), COMMENT(2), FRIENDSHIP(3), SUBSCRIBE(4);
        private long numVal;

        TYPE(long numVal){
            this.numVal = numVal;
        }

        public long getNumVal() {
            return numVal;
        }
        public static TYPE getById(long id) {
            for(TYPE e : values()) {
                if(e.numVal==id) return e;
            }
            return null;
        }
    }

    public enum ACTION {
        SHARE(0), CREATE(1), EDIT(2), COMPLETE(3), RESTORE(4), REMOVE(5), INVITE(6), ACCEPT(7), QUIT(8), BUY(9);
        private long numVal;

        ACTION(long numVal){
            this.numVal = numVal;
        }

        public long getNumVal(){
            return numVal;
        }

        public static ACTION getById(long id){
            for(ACTION e : values()) {
                if(e.numVal==id) return e;
            }
            return null;
        }
    }


    @SerializedName("user_id")
    protected String userId;
    @SerializedName("circle_id")
    protected String circleId;
    @SerializedName("task_id")
    protected String taskId;
    @SerializedName("date")
    protected Long date;
    @SerializedName("type")
    protected Long type;
    protected Long action;
    @SerializedName("comment_id")
    protected String commentId;

    protected boolean read;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCircleId() {
        return circleId;
    }

    public void setCircleId(String circleId) {
        this.circleId = circleId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getAction() {
        return action;
    }

    public void setAction(Long action) {
        this.action = action;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }
}
