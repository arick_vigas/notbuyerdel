package com.notibuyer.app.model;

import java.util.List;
import java.util.Set;

/**
 * Created by alexander on 26.08.2016.
 */
public class Subscribe {

    private String id;
    private String title;
    private String subTitle;

    private String color;

    public Subscribe(){}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
