package com.notibuyer.app.model;

/**
 * Created by alexander on 28.08.2016.
 */
public class SubscribeOption {
    private String title;
    private String brief;
    private String sku;

    public SubscribeOption(){}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }
}
