package com.notibuyer.app.model;

public class Task implements Comparable<Task>{

    private String id;
    private String text;
    private String ownerId;
    private String audioUrl;
    private String imageUrl;
    private long created;
    private Long updated;
    private boolean isCompleted;
    private boolean archived;
    private String completedBy;
    private Long index;
    private String resolvedBy;
    private String fbKey;
    private String circleId;
    private String completerName;
    private String ownerName;
    private String resolverName;

    public String getCompleterName() {
        return completerName;
    }

    public void setCompleterName(String completerName) {
        this.completerName = completerName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getResolverName() {
        return resolverName;
    }

    public void setResolverName(String resolverName) {
        this.resolverName = resolverName;
    }

    public String getResolvedBy() {
        return resolvedBy;
    }

    public void setResolvedBy(String resolvedBy) {
        this.resolvedBy = resolvedBy;
    }


    public String getCircleId() {
        return circleId;
    }

    public void setCircleId(String circleId) {
        this.circleId = circleId;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        this.isCompleted = completed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }






    public String getCompletedBy() {
        return completedBy;
    }

    public void setCompletedBy(String completedBy) {
        this.completedBy = completedBy;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public String getFbKey() {
        return fbKey;
    }

    public void setFbKey(String fbKey) {
        this.fbKey = fbKey;
    }


    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    @Override
    public int compareTo(Task o) {
        if(getIndex() == null && o.getIndex() == null) return 0;
        if(getIndex() != null && o.getIndex() == null) return 1;
        if(getIndex() == null && o.getIndex() != null) return -1;

        if(getIndex() > o.getIndex()) return -1;
        else if(getIndex() < o.getIndex()) return 1;
        else return 0;
    }
}
