package com.notibuyer.app.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;


public class User  {

    private String facebookId;
    private String gPlusId;
    private String name;
    private String email;
    private String phone;
    private String avatar;
    private String provider;
    private String id;

    @SerializedName("is_friend")
    private int isFriend;
    @SerializedName("invited")
    private int invited;
    private Integer owner;



    private String fbKey;

    public User(){}
    public String getFbKey() {
        return fbKey;
    }

    public void setFbKey(String fbKey) {
        this.fbKey = fbKey;
    }
    public void setFacebookId(String id) {
        this.facebookId = facebookId;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public String getAvatar() {
        return TextUtils.isEmpty(avatar) ? "https://cdn3.iconfinder.com/data/icons/rcons-user-action/32/boy-512.png" : avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }



    public int getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(int isFriend) {
        this.isFriend = isFriend;
    }

    public Integer getOwner() {
        return owner;
    }

    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    public int getInvited() {
        return invited;
    }

    public void setInvited(int invited) {
        this.invited = invited;
    }

    public String getGPlusId() {
        return gPlusId;
    }

    public void setGPlusId(String gPlusId) {
        this.gPlusId = gPlusId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
