package com.notibuyer.app.network;


import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by alexander on 21.08.2016.
 */
public class FirebaseInstanceIdService extends com.google.firebase.iid.FirebaseInstanceIdService{
    private static final String TAG = "FirebaseInstanceService";

    @Override
    public void onTokenRefresh() {
        String refreshToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "refresh token: " + refreshToken);


        saveToken(refreshToken);
    }

    private void saveToken(String refreshToken){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(mFirebaseUser!=null)
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("fcm_token").setValue(refreshToken);
    }
}
