package com.notibuyer.app.network;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.notibuyer.app.R;
import com.notibuyer.app.model.Notification;
import com.notibuyer.app.ui.activity.MainActivity;
import com.notibuyer.app.ui.activity.TaskViewActivity;
import com.notibuyer.app.ui.activity.UserProfileActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by alexander on 21.08.2016.
 */

public class FirebasePushService extends FirebaseMessagingService {

    public static final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    private static final String TITLE = "Notibuyer";
    static OkHttpClient mClient = new OkHttpClient();
    private static String TAG = "FBPush";

    public static void sendMessage(final JSONArray recipients, final long type, final long action, String userId, String circleId, String taskId, String commentId) {

        long cacheExpiration = 3600;
        final FirebaseRemoteConfig config = FirebaseRemoteConfig.getInstance();
        if (config.getInfo().getConfigSettings()
                .isDeveloperModeEnabled())
            cacheExpiration = 0;
        config.fetch(cacheExpiration)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // Make the fetched config available via
                        // FirebaseRemoteConfig get<type> calls.
                        config.activateFetched();
                        try {
                            JSONObject root = new JSONObject();
                            JSONObject data = new JSONObject();
                            data.put("type", type);
                            data.put("action", action);
                            data.put("user_id", userId);
                            if (circleId != null)
                                data.put("circle_id", circleId);
                            if (taskId != null)
                                data.put("task_id", taskId);
                            if (commentId != null)
                                data.put("comment_id", commentId);
                            root.put("data", data);
                            root.put("registration_ids", recipients);
                            upstream(root.toString(), config.getString("server"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("PUSH SERVICE", (e != null) ? e.getMessage() : "message");
                    }
                });


    }

    static String postToFCM(String bodyString, String server) throws IOException {
        RequestBody body = RequestBody.create(JSON, bodyString);
        Request request = new Request.Builder()
                .url(FCM_MESSAGE_URL)
                .post(body)
                .addHeader("Authorization", "key=" + server)
                .build();
        Response response = mClient.newCall(request).execute();
        return response.body().string();


    }

    private static void upstream(String root, String server) {
        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    String result = postToFCM(root, server);
                    Log.d(TAG, "Result: " + result);
                    return result;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject resultJson = new JSONObject(result);
                    int success, failure;
                    success = resultJson.getInt("success");
                    failure = resultJson.getInt("failure");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Map<String, String> data = remoteMessage.getData();
        Notification.TYPE type = Notification.TYPE.getById(Long.parseLong(data.get("type")));
        Notification.ACTION action = Notification.ACTION.getById(Long.parseLong(data.get("action")));
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        StringBuilder messageBuilder = new StringBuilder();

        switch (type){
            case FRIENDSHIP:
                if(action.equals(Notification.ACTION.INVITE)){
                    databaseReference.child("users").child(data.get("user_id")).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            messageBuilder.append("<b>");
                            messageBuilder.append(dataSnapshot.child("name").getValue());
                            messageBuilder.append("</b> has sent a <b>friendship request</b> to you");
                            Intent intent1 = new Intent(FirebasePushService.this, MainActivity.class);
                            intent1.putExtra(MainActivity.SELECTED_ITEM, MainActivity.FRIENDS);
                            intent1.putExtra("from_push", true);
                            intent1.setAction(Long.toString(System.currentTimeMillis()));
                            PendingIntent resultPendingIntent = PendingIntent.getActivity(FirebasePushService.this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                            showNotification(messageBuilder.toString(), resultPendingIntent);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                } else if(action.equals(Notification.ACTION.ACCEPT)){
                    databaseReference.child("users").child(data.get("user_id")).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            messageBuilder.append("<b>")
                                    .append(dataSnapshot.child("name").getValue())
                                    .append("</b> accepted your <b>friendship request</b>");
                            Intent intent1 = new Intent(FirebasePushService.this, UserProfileActivity.class);
                            intent1.putExtra(UserProfileActivity.USER_ID, data.get("user_id"));
                            intent1.putExtra("from_push", true);
                            intent1.setAction(Long.toString(System.currentTimeMillis()));
                            PendingIntent resultPendingIntent = PendingIntent.getActivity(FirebasePushService.this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                            showNotification(messageBuilder.toString(), resultPendingIntent);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }
                break;
            case TASK:
                switch (action){
                    case CREATE:
                        showTaskNoti("has created a task ", data);
                        break;
                    case COMPLETE:
                        showTaskNoti("has completed a task ", data);
                        break;
                    case RESTORE:
                        showTaskNoti("has restored a task ", data);
                        break;
                    case EDIT:
                        showTaskNoti("has edited a task ", data);
                        break;
                }
                break;
            case COMMENT:
                if(action.equals(Notification.ACTION.CREATE)){
                    databaseReference.child("users").child(data.get("user_id")).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            messageBuilder.append("<b>");
                            messageBuilder.append(dataSnapshot.child("name").getValue());
                            messageBuilder.append("</b> has left a comment in task ");
                            databaseReference.child("tasks").child(data.get("task_id")).child("text").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot snapshot) {
                                    if(snapshot.exists() && !snapshot.getValue().equals("")){
                                        messageBuilder.append("<b>\"")
                                                .append(snapshot.getValue())
                                                .append("\"</b>" );
                                        Intent intent1 = new Intent(FirebasePushService.this, TaskViewActivity.class);
                                        intent1.putExtra(TaskViewActivity.TASK_ID, data.get("task_id"));
                                        intent1.putExtra("from_push", true);
                                        intent1.setAction(Long.toString(System.currentTimeMillis()));
                                        PendingIntent resultPendingIntent = PendingIntent.getActivity(FirebasePushService.this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                                        showNotification(messageBuilder.toString(), resultPendingIntent);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e(TAG, databaseError.getMessage());
                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }
                break;
            case CIRCLE:
                if(action.equals(Notification.ACTION.SHARE)){
                    databaseReference.child("users").child(data.get("user_id")).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            messageBuilder.append("<b>");
                            messageBuilder.append(dataSnapshot.child("name").getValue());
                            messageBuilder.append("</b> has shared a circle <b>");
                            databaseReference.child("circles").child(data.get("circle_id")).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    messageBuilder.append("\"")
                                            .append(dataSnapshot.getValue())
                                            .append("\"</b> with you");
                                    Intent intent1 = new Intent(FirebasePushService.this, MainActivity.class);
                                    intent1.putExtra(MainActivity.SELECTED_ITEM, MainActivity.ALL);
                                    intent1.putExtra("circle_id", data.get("circle_id"));
                                    intent1.putExtra("from_push", true);
                                    intent1.setAction(Long.toString(System.currentTimeMillis()));
                                    PendingIntent resultPendingIntent = PendingIntent.getActivity(FirebasePushService.this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                                    showNotification(messageBuilder.toString(), resultPendingIntent);

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e(TAG, databaseError.getMessage());
                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                } else if(action.equals(Notification.ACTION.QUIT)) {
                    databaseReference.child("users").child(data.get("user_id")).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            messageBuilder.append("<b>");
                            messageBuilder.append(dataSnapshot.child("name").getValue());
                            messageBuilder.append("</b> has left your circle <b>");
                            databaseReference.child("circles").child(data.get("circle_id")).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    messageBuilder.append("\"")
                                            .append(dataSnapshot.getValue())
                                            .append("\"</b>");
                                    Intent intent1 = new Intent(FirebasePushService.this, UserProfileActivity.class);
                                    intent1.putExtra(UserProfileActivity.USER_ID, data.get("task_id"));
                                    intent1.putExtra("from_push", true);
                                    intent1.setAction(Long.toString(System.currentTimeMillis()));
                                    PendingIntent resultPendingIntent = PendingIntent.getActivity(FirebasePushService.this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                                    showNotification(messageBuilder.toString(), resultPendingIntent);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e(TAG, databaseError.getMessage());
                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }
                break;

        }

    }

    private void showNotification(String text, PendingIntent intent) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setAutoCancel(true)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setSound(alarmSound)
                        .setVibrate(new long[] {500, 100, 500, 100})
                        .setLights(Color.RED, 3000, 3000)
                        .setContentIntent(intent)
                        .setContentTitle(TITLE)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(Html.fromHtml(text)))
                        .setContentText(Html.fromHtml(text));
        //


        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(m, mBuilder.build());
    }

    private void showTaskNoti(String action, Map<String, String>data){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        StringBuilder messageBuilder = new StringBuilder();
        databaseReference.child("users").child(data.get("user_id")).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                messageBuilder.append("<b>")
                        .append(dataSnapshot.child("name").getValue())
                        .append("</b> ")
                        .append(action);
                databaseReference.child("tasks").child(data.get("task_id")).child("text").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot taskData) {
                        messageBuilder
                                .append("<b>\"")
                                .append(taskData.getValue())
                                .append("\"</b ");
                                Intent intent1 = new Intent(FirebasePushService.this, TaskViewActivity.class);
                                intent1.putExtra(TaskViewActivity.TASK_ID, data.get("task_id"));
                        intent1.putExtra("from_push", true);
                                intent1.setAction(Long.toString(System.currentTimeMillis()));
                                PendingIntent resultPendingIntent = PendingIntent.getActivity(FirebasePushService.this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
                                showNotification(messageBuilder.toString(), resultPendingIntent);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage());
            }
        });
    }
}
