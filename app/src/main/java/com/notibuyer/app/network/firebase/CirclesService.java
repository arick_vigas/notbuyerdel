package com.notibuyer.app.network.firebase;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.model.Task;
import com.notibuyer.app.ui.activity.AddCircleActivity;
import com.notibuyer.app.ui.activity.EditCircleActivity;

/**
 * Created by alexander on 30.08.2016.
 */



public class CirclesService extends FirebaseService {

    public CirclesService(){}

    public void removeCircle(String id){
        //get members of circle
        circlesRef.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot usrSnapshot: dataSnapshot.child("members").getChildren()){
                    //remove from member circles
                    databaseReference.child("users").child(usrSnapshot.getKey()).child("circles").child(id).removeValue();
                }

                for(DataSnapshot currentTasksSnapshot: dataSnapshot.child("current_tasks").getChildren()){
                    //remove current tasks from circle
                    tasksRef.child(currentTasksSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Task task = dataSnapshot.getValue(Task.class);
                            task.setFbKey(dataSnapshot.getKey());
                            //remove from user task list
                            usersRef.child(task.getOwnerId()).child("tasks").child(task.getFbKey()).removeValue();
                            //remove task ref
                            tasksRef.child(currentTasksSnapshot.getKey()).removeValue();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });

                }
                //remove completed tasks from circle
                for(DataSnapshot completedTaskSnapshot: dataSnapshot.child("completed_tasks").getChildren()){

                    tasksRef.child(completedTaskSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Task task = dataSnapshot.getValue(Task.class);
                            task.setFbKey(dataSnapshot.getKey());
                            //remove from user task list
                            usersRef.child(task.getOwnerId()).child("tasks").child(task.getFbKey()).removeValue();
                            //remove task ref
                            tasksRef.child(completedTaskSnapshot.getKey()).removeValue();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });

                }

                //remove circle ref
                circlesRef.child(id).removeValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    public void createCircle(AddCircleActivity activity, Circle newCircle) {
        //add circle to circles list
        DatabaseReference newChildRef = databaseReference.child("circles").push();
        newChildRef.setValue(newCircle);
        newChildRef.child("members").child(mFirebaseUser.getUid()).setValue(true);
        //add circle to user circlesList
        usersRef.child(mFirebaseUser.getUid()).child("circles").orderByChild("index").limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //get max index
                long maxIndex = -1;
                if (snapshot.exists()) {
                    for (DataSnapshot t : snapshot.getChildren()) {
                        if (t.hasChild("index"))
                            maxIndex = (long) t.child("index").getValue();
                        break;
                    }
                }
                // index = countCircles
                usersRef.child(mFirebaseUser.getUid()).child("circles").child(newChildRef.getKey()).child("index").setValue(maxIndex+1);
                EditCircleActivity.startForResult(activity, newChildRef.getKey(), 202);
                activity.finish();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("TAG", databaseError.getMessage());
            }
        });
    }

    public void addCircle(String circleId, String userId){
        circlesRef.child(circleId).child("members").child(userId).setValue(true);
        //add to user circles
        usersRef.child(userId).child("circles").orderByChild("index").limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot data) {
                //get max index
                long maxIndex = -1;
                if (data.exists()) {
                    for (DataSnapshot t : data.getChildren()) {
                        if (t.hasChild("index"))
                            maxIndex = (long) t.child("index").getValue();
                        break;
                    }
                }
                usersRef.child(userId).child("circles").child(circleId).child("index").setValue(maxIndex+1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }
}
