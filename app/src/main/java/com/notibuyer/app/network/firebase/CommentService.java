package com.notibuyer.app.network.firebase;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.model.Comment;
import com.notibuyer.app.model.Notification;
import com.notibuyer.app.network.FirebasePushService;
import com.notibuyer.app.utils.NotificationFactory;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by alexander on 31.08.2016.
 */
public class CommentService extends FirebaseService {
    public CommentService(){

    }

    public void addComment(Comment comment){
        DatabaseReference commentRef = commentsRef.push();
        commentRef.setValue(comment);
        comment.setId(commentRef.getKey());
        tasksRef.child(comment.getTaskId()).child("comments").child(commentRef.getKey()).setValue(true);
        notifyUsers(comment);
    }

    public void notifyUsers(Comment comment){
        //get task circle
        tasksRef.child(comment.getTaskId()).child("circleId").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //get circle members
                circlesRef.child((String)dataSnapshot.getValue()).child("members").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot membersDataSnapshot) {
                        //foreach member
                        for(DataSnapshot memberSnapshot:membersDataSnapshot.getChildren()){
                            //if member is not curr_user-> notify all circle members
                            if(!memberSnapshot.getKey().equals(mFirebaseUser.getUid())){
                                //create notification
                                Notification notification = NotificationFactory.addComment(comment.getTaskId(), comment.getId());
                                notification.setRead(false);
                                DatabaseReference notificationRef = databaseReference.child("notifications").push();
                                notificationRef.setValue(notification);
                                databaseReference.child("users").child(memberSnapshot.getKey()).child("notifications").child(notificationRef.getKey()).setValue(true);

                                databaseReference.child("users").child(memberSnapshot.getKey()).child("notifications_unread").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        long current = 0;
                                        if (dataSnapshot.exists()) {
                                            current = (long) dataSnapshot.getValue();
                                        }
                                        databaseReference.child("users").child(memberSnapshot.getKey()).child("notifications_unread").setValue(current + 1);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.d(TAG, databaseError.getMessage());
                                    }
                                });

                                //send push notificaion

                                databaseReference.child("users").child(memberSnapshot.getKey()).child("fcm_token").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @TargetApi(Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        String[]recepients = {(String)dataSnapshot.getValue()};
                                        try {
                                            JSONArray recepientsJs = new JSONArray(recepients);
                                            FirebasePushService.sendMessage(recepientsJs, Notification.TYPE.COMMENT.getNumVal(), Notification.ACTION.CREATE.getNumVal(), mFirebaseUser.getUid(), null, comment.getTaskId(), comment.getId());
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e(TAG, databaseError.getMessage());
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }
}
