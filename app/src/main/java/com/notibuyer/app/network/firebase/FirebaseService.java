package com.notibuyer.app.network.firebase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.notibuyer.app.Prefs;

import javax.inject.Inject;

/**
 * Created by alexander on 25.08.2016.
 */
public abstract class FirebaseService {
    protected DatabaseReference databaseReference;
    protected FirebaseUser mFirebaseUser;
    protected DatabaseReference tasksRef;
    protected DatabaseReference circlesRef;
    protected DatabaseReference usersRef;
    protected DatabaseReference commentsRef;
    protected final String TAG = this.getClass().getSimpleName();
    @Inject
    protected Prefs prefs;

    FirebaseService(){
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        tasksRef = databaseReference.child("tasks");
        circlesRef = databaseReference.child("circles");
        usersRef = databaseReference.child("users");
        commentsRef = databaseReference.child("comments");
    }
}
