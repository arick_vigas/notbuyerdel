package com.notibuyer.app.network.firebase;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.notibuyer.app.model.Notification;
import com.notibuyer.app.model.Task;
import com.notibuyer.app.network.FirebasePushService;
import com.notibuyer.app.utils.NotificationFactory;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by alexander on 25.08.2016.
 */
public class TaskService extends FirebaseService {


    StorageReference storageRef;
    public TaskService(){
        storageRef = FirebaseStorage.getInstance().getReference();
    }

    public void createTask(Task task, String circleId, File audio, File image) {
        DatabaseReference newTaskRef = tasksRef.child(task.getFbKey());
        newTaskRef.setValue(task);

        usersRef.child(mFirebaseUser.getUid()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                newTaskRef.child("ownerName").setValue(dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage());
            }
        });

        usersRef.child(mFirebaseUser.getUid()).child("tasks").child(newTaskRef.getKey()).setValue(true);
        //place in current / private cirlce
        if(circleId.equals("All")){
            usersRef.child(mFirebaseUser.getUid()).child("private_circle").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot data) {
                    circlesRef.child((String)data.getValue()).child("current_tasks").orderByChild("index").limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            //get count tasks
                            long maxIndex = -1;
                            if(snapshot.exists()){
                                for(DataSnapshot t:snapshot.getChildren()){
                                    if(t.hasChild("index"))
                                        maxIndex = (long)t.child("index").getValue();
                                    break;
                                }
                            }
                            //set index = maxIndex +1
                            circlesRef.child((String)data.getValue()).child("current_tasks").child(newTaskRef.getKey()).child("index").setValue(maxIndex+1);
                            //put in self circles_list
                            newTaskRef.child("circleId").setValue(data.getValue());
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });
        } else {
            //put in self circles_list
            newTaskRef.child("circleId").setValue(circleId);
            //put in current circle tasks_list
            circlesRef.child(circleId).child("current_tasks").orderByChild("index").limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    long maxIndex = -1;
                    if(dataSnapshot.exists()){
                        for(DataSnapshot t:dataSnapshot.getChildren()){
                            if(t.hasChild("index"))
                                maxIndex = (long) t.child("index").getValue();
                                break;
                        }
                    }
                    circlesRef.child(circleId).child("current_tasks").child(newTaskRef.getKey()).child("index").setValue(maxIndex + 1);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });

            //add notification to user
            //notify user
            notifyUsers(newTaskRef.getKey(), circleId, Notification.ACTION.CREATE.getNumVal());
        }
        System.out.println(audio==null);
        System.out.println(image==null);
        if(audio!=null && audio.exists()) {
            Uri file = Uri.fromFile(audio);
            StorageReference updateRecord = storageRef.child("voice_records").child(newTaskRef.getKey()).child(file.getLastPathSegment());
            UploadTask uploadTask = updateRecord.putFile(file);
            // Register observers to listen for when the upload is done or if it fails
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Log.e(TAG, exception.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and upload URL.
                    Uri uploadUrl = taskSnapshot.getDownloadUrl();
                    newTaskRef.child("audioUrl").setValue(uploadUrl.toString());
                }
            });

        }
        if(image!=null && image.exists()){
            Uri file = Uri.fromFile(image);
            StorageReference updateImage = storageRef.child("images").child(newTaskRef.getKey()).child(file.getLastPathSegment());
            UploadTask uploadTask = updateImage.putFile(file);
            // Register observers to listen for when the upload is done or if it fails
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Log.e(TAG, exception.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and upload URL.
                    Uri uploadUrl = taskSnapshot.getDownloadUrl();
                    newTaskRef.child("imageUrl").setValue(uploadUrl.toString());
                }
            });
        }
    }

    public void copyTaskToCircle(String taskId, String circleId, Context context){
        tasksRef.child(taskId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //get task from db
                Task task = dataSnapshot.getValue(Task.class);
                if(!task.getCircleId().equals(circleId)) {
                    //set new circleID to task
                    task.setCircleId(circleId);
                    //generate new ID
                    DatabaseReference clonedTaskRef = tasksRef.push();
                    //write task to DB
                    clonedTaskRef.setValue(task);
                    if(task.getImageUrl()!=null){
                        StorageReference imgRef = FirebaseStorage.getInstance().getReferenceFromUrl(task.getImageUrl());
                        try {
                            File localFile = File.createTempFile("images", "jpg");
                            imgRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                    // Local temp file has been created
                                    Uri file = Uri.fromFile(localFile);
                                    String path = "images/"+ clonedTaskRef.getKey() + "/" +file.getLastPathSegment();
                                    StorageReference updateImage = storageRef.child(path);
                                    UploadTask uploadTask = updateImage.putFile(file);
                                    uploadTask.addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception exception) {
                                            // Handle unsuccessful uploads
                                            Log.e(TAG, exception.getMessage());
                                        }
                                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and upload URL.
                                            Uri uploadUrl = taskSnapshot.getDownloadUrl();
                                            clonedTaskRef.child("imageUrl").setValue(uploadUrl.toString());
                                            clonedTaskRef.child("updated").setValue(new java.util.Date().getTime());
                                        }
                                    });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle any errors
                                    Log.e(TAG, exception.getMessage());
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    if(task.getAudioUrl()!=null){
                        StorageReference audioRef = FirebaseStorage.getInstance().getReferenceFromUrl(task.getAudioUrl());
                        try {
                            File localFile = File.createTempFile("audio", "amr", context.getCacheDir());
                            audioRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                    // Local temp file has been created
                                    Uri file = Uri.fromFile(localFile);
                                    String path = "voice_records/"+ clonedTaskRef.getKey() + "/" +file.getLastPathSegment();
                                    StorageReference updateAudio = storageRef.child(path);
                                    UploadTask uploadTask = updateAudio.putFile(file);
                                    uploadTask.addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception exception) {
                                            // Handle unsuccessful uploads
                                            Log.e(TAG, exception.getMessage());
                                        }
                                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and upload URL.
                                            Uri uploadUrl = taskSnapshot.getDownloadUrl();
                                            clonedTaskRef.child("audioUrl").setValue(uploadUrl.toString());
                                        }
                                    });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle any errors
                                    Log.e(TAG, exception.getMessage());
                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    //write task to circle tasks list
                    circlesRef.child(circleId).child(task.isCompleted() ? "completed_tasks" : "current_tasks").orderByChild("index").limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            //get max index
                            long maxIndex = -1;
                            if (snapshot.exists()) {
                                System.out.println("exists");
                                for (DataSnapshot t : snapshot.getChildren()) {
                                    if (t.hasChild("index"))
                                        maxIndex = (long) t.child("index").getValue();
                                    break;

                                }
                            }
                            circlesRef.child(circleId).child(task.isCompleted() ? "completed_tasks" : "current_tasks").child(clonedTaskRef.getKey()).child("index").setValue(maxIndex+1);


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d(TAG, databaseError.getMessage());
                        }
                    });
                    //write to user' tasks list
                    usersRef.child(mFirebaseUser.getUid()).child("tasks").child(clonedTaskRef.getKey()).setValue(true);
                    //notify users
                    notifyUsers(clonedTaskRef.getKey(), circleId, Notification.ACTION.CREATE.getNumVal());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    public void completeTask(Task task) {
        //task set updated date
        tasksRef.child(task.getFbKey()).child("updated").setValue(new Date().getTime());
        //set task completed
        tasksRef.child(task.getFbKey()).child("completed").setValue(true);
        //set completed BY
        tasksRef.child(task.getFbKey()).child("completedBy").setValue(mFirebaseUser.getUid());

        usersRef.child(mFirebaseUser.getUid()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tasksRef.child(task.getFbKey()).child("completerName").setValue(dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage());
            }
        });

        //remove from current tasks in task circle
        circlesRef.child(task.getCircleId()).child("current_tasks").child(task.getFbKey()).removeValue();
        //place task in completed in task circle
        circlesRef.child(task.getCircleId()).child("completed_tasks").orderByChild("index").limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //get max index
                long maxIndex = -1;
                if (snapshot.exists()) {
                    System.out.println("exists");
                    for (DataSnapshot t : snapshot.getChildren()) {
                        if (t.hasChild("index"))
                            maxIndex = (long) t.child("index").getValue();
                        break;

                    }
                }
                circlesRef.child(task.getCircleId()).child("completed_tasks").child(task.getFbKey()).child("index").setValue(maxIndex + 1);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
        //notify users
        notifyUsers(task.getFbKey(), task.getCircleId(), Notification.ACTION.COMPLETE.getNumVal());
    }

    public void resolveTask(Task task){
        //set task completed
        tasksRef.child(task.getFbKey()).child("completed").setValue(false);
        //set task resolver
        tasksRef.child(task.getFbKey()).child("resolvedBy").setValue(mFirebaseUser.getUid());

        usersRef.child(mFirebaseUser.getUid()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                tasksRef.child(task.getFbKey()).child("resolverName").setValue(dataSnapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage());
            }
        });

        //task set updated date
        tasksRef.child(task.getFbKey()).child("updated").setValue(new Date().getTime());
        //remove from completed tasks in task circle
        circlesRef.child(task.getCircleId()).child("completed_tasks").child(task.getFbKey()).removeValue();
        //place task in current in task circle
        circlesRef.child(task.getCircleId()).child("current_tasks").orderByChild("index").limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //get max index
                long maxIndex = -1;
                if (snapshot.exists()) {
                    System.out.println("exists");
                    for (DataSnapshot t : snapshot.getChildren()) {
                        if (t.hasChild("index"))
                            maxIndex = (long) t.child("index").getValue();
                            break;

                    }
                }
                circlesRef.child(task.getCircleId()).child("current_tasks").child(task.getFbKey()).child("index").setValue(maxIndex + 1);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
        //notify users
        notifyUsers(task.getFbKey(), task.getCircleId(), Notification.ACTION.RESTORE.getNumVal());
    }

    public void removeTask(Task task){
        //remove from circle
        circlesRef.child(task.getCircleId()).child(task.isCompleted()?"completed_tasks":"current_tasks").child(task.getFbKey()).removeValue();
        //setUpdated
        tasksRef.child(task.getFbKey()).child("updated").setValue(new Date().getTime());
        //set archived
        tasksRef.child(task.getFbKey()).child("archived").setValue(true);
    }

    public void removePicture(Task task){
        //remove from file storage
        FirebaseStorage.getInstance().getReferenceFromUrl(task.getImageUrl()).delete();
        //remove from db
        tasksRef.child(task.getFbKey()).child("imageUrl").removeValue();
        tasksRef.child(task.getFbKey()).child("updated").setValue(new java.util.Date().getTime());

        notifyUsers(task.getId(), task.getCircleId(), Notification.ACTION.EDIT.getNumVal());
    }

    public void notifyUsers(String taskId, String circleId, long action){
        circlesRef.child(circleId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //add notification to user
                Set<String> recipients = new HashSet<>();
                for(DataSnapshot snap:dataSnapshot.child("members").getChildren()) {
                    if (!snap.getKey().equals(mFirebaseUser.getUid()) && !circleId.equals("All") && !recipients.contains(snap.getKey())) {
                        recipients.add(snap.getKey());
                        Notification notification = NotificationFactory.createTaskNotification(taskId, action);
                        notification.setRead(false);
                        DatabaseReference notiRef = databaseReference.child("notifications").push();
                        notiRef.setValue(notification);
                        databaseReference.child("users").child(snap.getKey()).child("notifications").child(notiRef.getKey()).setValue(true);

                        databaseReference.child("users").child(snap.getKey()).child("notifications_unread").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                long current = 0;
                                if (dataSnapshot.exists()) {
                                    current = (long) dataSnapshot.getValue();
                                }
                                databaseReference.child("users").child(snap.getKey()).child("notifications_unread").setValue(current + 1);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.d(TAG, databaseError.getMessage());
                            }
                        });

                        databaseReference.child("users").child(snap.getKey()).child("fcm_token").addListenerForSingleValueEvent(new ValueEventListener() {
                            @TargetApi(Build.VERSION_CODES.KITKAT)
                            @Override
                            public void onDataChange(DataSnapshot data) {
                                String[] recepients = {(String) data.getValue()};
                                try {
                                    JSONArray recepientsJson = new JSONArray(recepients);
                                    FirebasePushService.sendMessage(recepientsJson, Notification.TYPE.TASK.getNumVal(), action, mFirebaseUser.getUid(), circleId, taskId, null);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e(TAG, databaseError.getMessage());
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG,databaseError.getMessage());
            }
        });
    }
}
