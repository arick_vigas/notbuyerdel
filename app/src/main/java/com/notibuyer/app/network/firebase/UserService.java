package com.notibuyer.app.network.firebase;

/**
 * Created by alexander on 28.08.2016.
 */
public class UserService extends FirebaseService {
    public void removeUserpic(String userId){
        usersRef.child(userId).child("avatar").setValue("https://cdn3.iconfinder.com/data/icons/rcons-user-action/32/boy-512.png");
    }
}
