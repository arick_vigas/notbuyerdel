package com.notibuyer.app.network.response

import com.notibuyer.app.R

open class BaseResponse {
    var code: Int? = null
    var message: String? = null

    open fun isError(): Boolean = code != 0

    fun getErrorMessageForCode(): Int {
        when(message) {
            "Bad id" -> return R.string.bad_id_error
            "Bad token" -> return R.string.bad_token_error
            "Database error" -> return R.string.database_error
            "Email already in use" -> return R.string.email_in_use_error
            "Unauthorized" -> return R.string.unauthorized_error
            "Circle does not exist" -> return R.string.no_such_circle_error
            "User is not owner of this circle" -> return R.string.no_rules_for_circle_error
            "Already in this circle" -> return R.string.already_in_circle_error
            "User does not exist" -> return R.string.no_such_user_error
            "This user is not a member" -> return R.string.user_is_not_member_error
            "User is neither member nor owner of this circle" -> return R.string.neither_member_nor_owner_error
            "Permission denied" -> return R.string.no_permissions_for_action
            else -> {   println(code)
                        return R.string.database_error}

        }
    }


}