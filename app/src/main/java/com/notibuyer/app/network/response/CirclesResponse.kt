package com.notibuyer.app.network.response

import com.notibuyer.app.model.Circle

class CirclesResponse: BaseResponse() {
    var list: Array<Circle>? = null
}