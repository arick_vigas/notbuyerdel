package com.notibuyer.app.network.response

class ErrorCodeThrowable(val errorStringRes: Int) : Error()