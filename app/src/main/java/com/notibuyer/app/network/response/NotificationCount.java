package com.notibuyer.app.network.response;

import lombok.Getter;

@Getter
public class NotificationCount extends BaseResponse {
    private Integer count;
}
