package com.notibuyer.app.network.response;

import com.notibuyer.app.model.Notification;

import java.util.List;

import lombok.Getter;

@Getter
public class NotificationResponse extends BaseResponse {
    private List<Notification> notifications;
}
