package com.notibuyer.app.network.response

import com.notibuyer.app.model.Task

class SingleTaskResponse : BaseResponse() {
    var task: Task? = null
}
