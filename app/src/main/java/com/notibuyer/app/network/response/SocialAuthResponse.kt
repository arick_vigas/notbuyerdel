package com.notibuyer.app.network.response

class SocialAuthResponse() : BaseResponse() {
    var token: String? = null;

    override fun isError(): Boolean = code != 0 && code != 1
}