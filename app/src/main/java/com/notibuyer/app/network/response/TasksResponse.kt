package com.notibuyer.app.network.response

import com.notibuyer.app.model.Task

class TasksResponse : BaseResponse() {
    var tasks: List<Task>? = null
}
