package com.notibuyer.app.network.response;

import com.notibuyer.app.model.User;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class UserListResponse extends BaseResponse {
    private List<User> users = new ArrayList<>();
}
