package com.notibuyer.app.network.response

import com.notibuyer.app.model.User

class UserResponse: BaseResponse() {
    var profile: User? = null
}