package com.notibuyer.app.service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.notibuyer.app.Prefs;
import com.notibuyer.app.broadcast.ServiceRecreateBroadcast;
import com.notibuyer.app.ui.activity.MainActivity;
import com.squareup.seismic.ShakeDetector;

import javax.inject.Inject;

public class ShakeService extends Service implements ShakeDetector.Listener {

    private ShakeDetector sd;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sd = new ShakeDetector(this);
        sd.setSensitivity(ShakeDetector.SENSITIVITY_HARD);
        sd.start(sensorManager);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void hearShake() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public static boolean isServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (ShakeService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void stopService(Context context) {
        Log.d("Shake", "stop");
        context.stopService(new Intent(context, ShakeService.class));
    }

    public static void startService(Context context) {
        if (!ShakeService.isServiceRunning(context)) {
            Log.d("Shake", "start");
            context.startService(new Intent(context, ShakeService.class));
        }
    }

    @Override
    public void onDestroy() {
        sd.stop();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}