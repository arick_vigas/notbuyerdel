package com.notibuyer.app.service;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.annimon.stream.Stream;
import com.notibuyer.app.R;

import java.util.ArrayList;
import java.util.Collection;

import root.gast.speech.activation.SpeechActivationListener;
import root.gast.speech.activation.SpeechActivator;
import root.gast.speech.activation.SpeechActivatorFactory;

public class SpeechActivationService extends Service implements SpeechActivationListener {
    private static final String TAG = "SpeechActivationService";
    public static final String ACTIVATION_TYPE_INTENT_KEY =
            "ACTIVATION_TYPE_INTENT_KEY";

    private boolean isStarted;

//    private SpeechActivator activator;
    private SpeechRecognizerManager speechRecognizerManager;

    @Override
    public void onCreate() {
        super.onCreate();
        isStarted = false;
        speechRecognizerManager = new SpeechRecognizerManager(this, new SpeechRecognizerManager.onResultsReady() {
            @Override
            public void onResults(ArrayList<String> results) {
                if (results != null && !results.isEmpty()) {
                    Stream.of(results).forEach(s-> Log.d(TAG, s));
                }
            }
        });
    }

    /**
     * stop or start an activator based on the activator type and if an
     * activator is currently running
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        speechRecognizerManager.startListening();
//        if (intent != null) {
//            if (isStarted) {
//                stopActivator();
//                startDetecting(intent);
//            } else {
//                // activator not started, start it
//                startDetecting(intent);
//            }
//        }
        return START_REDELIVER_INTENT;
    }

//    private void startDetecting(Intent intent) {
//        Log.d(TAG, "extras: " + intent.getExtras().toString());
//        if (activator == null) {
//            Log.d(TAG, "null activator");
//        }
//
//        activator = getRequestedActivator(intent);
//        Log.d(TAG, "started: " + activator.getClass().getSimpleName());
//        isStarted = true;
//        activator.detectActivation();
//    }

    private SpeechActivator getRequestedActivator(Intent intent) {
        String type = intent.getStringExtra(ACTIVATION_TYPE_INTENT_KEY);
        return SpeechActivatorFactory.createSpeechActivator(this, this, type);
    }

    /**
     * determine if the intent contains an activator type
     * that is different than the currently running type
     */
//    private boolean isDifferentType(Intent intent) {
//        boolean different;
//        if (activator == null) {
//            return true;
//        } else {
//            SpeechActivator possibleOther = getRequestedActivator(intent);
//            different = !(possibleOther.getClass().getName().
//                    equals(activator.getClass().getName()));
//        }
//        return different;
//    }

    @Override
    public void activated(boolean success) {
        Log.d(TAG, "activated: " + success);
//        stopActivator();
//        activator.detectActivation();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "On destroy");
//        stopActivator();
    }

//    private void stopActivator() {
//        if (activator != null) {
//            Log.d(TAG, "stopped: " + activator.getClass().getSimpleName());
//            activator.stop();
//            isStarted = false;
//        }
//    }

    public static void stopService(Context context) {
        if (SpeechActivationService.isServiceRunning(context)) {
            Log.d("SpeechActivation", "stop");
            context.stopService(new Intent(context, SpeechActivationService.class));
        }
    }

    public static void startService(Context context, String activationType) {
        if (!SpeechActivationService.isServiceRunning(context)) {
            Log.d("SpeechActivation", "start");
            Intent i = new Intent(context, SpeechActivationService.class);
            i.putExtra(ACTIVATION_TYPE_INTENT_KEY, activationType);
            context.startService(i);
        }
    }

    public static boolean isServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (SpeechActivationService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
