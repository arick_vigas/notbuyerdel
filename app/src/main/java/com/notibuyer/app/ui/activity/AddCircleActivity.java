package com.notibuyer.app.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.notibuyer.app.R;
import com.notibuyer.app.dagger.AppComponent;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.network.firebase.CirclesService;
import com.notibuyer.app.ui.view.colorpicker.ColorPickerDialog;
import com.notibuyer.app.ui.view.colorpicker.ColorPickerSwatch;
import com.notibuyer.app.utils.UIUtils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

public class AddCircleActivity extends BaseActivity implements ColorPickerSwatch.OnColorSelectedListener {

    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.circle_name) protected EditText circleName;
    @Bind(R.id.circle_color) protected TextView circleColor;
    @Inject
    CirclesService circlesService;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private Circle newCircle;

    public static void startForResult(Activity context, int requestCode) {
        Intent intent = new Intent(context, AddCircleActivity.class);
        context.startActivityForResult(intent, requestCode);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.add_circles_layout;
    }

    public void setUpComponent(@NonNull AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.create_circle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        newCircle = new Circle();
        newCircle.setColor("#33b5e5");
        circleColor.getBackground().setColorFilter(Color.parseColor("#33b5e5"), PorterDuff.Mode.MULTIPLY);
        newCircle.setOwnerId(mFirebaseUser.getUid());


    }

    @OnClick(R.id.circle_color)
    protected void changeColor() {
        ColorPickerDialog colorPicker = ColorPickerDialog.newInstance(
                R.string.color_picker_default_title,
                UIUtils.colorChoice(this), 0, 4,
                UIUtils.isTablet(this)? ColorPickerDialog.SIZE_LARGE : ColorPickerDialog.SIZE_SMALL);
        colorPicker.setOnColorSelectedListener(this);
        colorPicker.show(getFragmentManager(), "color");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            case R.id.save:
               // if (!NetworkUtils.isOn(this)) {
                 //   showError(getString(R.string.no_internet_connection));
                //    return true;
                //}
                if (TextUtils.isEmpty(circleName.getText())) {
                    toast(getString(R.string.empty_name));
                    return true;
                }
                newCircle.setName(circleName.getText().toString().trim());
                hideKeyboard();
                showLoader();

                circlesService.createCircle(this, newCircle);
                // back to circles list
                //finish();
                return true;
        }
        return false;
    }

    @Override
    public void onColorSelected(int color) {
        String hexColor = String.format("#%06X", (0xFFFFFF & color));
        newCircle.setColor(hexColor);
        Drawable drawable = circleColor.getBackground();
        drawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.MULTIPLY));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_changes_menu, menu);
        return true;
    }
}
