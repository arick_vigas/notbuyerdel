package com.notibuyer.app.ui.activity;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.notibuyer.app.App;
import com.notibuyer.app.Prefs;
import com.notibuyer.app.R;
import com.notibuyer.app.dagger.AppComponent;
import com.notibuyer.app.network.response.ErrorCodeThrowable;
import com.notibuyer.app.utils.DialogFactory;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.ButterKnife;


public abstract class BaseActivity extends AppCompatActivity {

    protected final String TAG = this.getClass().getSimpleName();
    @Inject
    protected Prefs prefs;


    private boolean isStarted = false;
    private boolean isRestarted = false;
    private AlertDialog loader;

    private ArrayList<View> editTexts;
    private FrameLayout keyboardCloseResolver;

    protected abstract int getContentViewId();

    public void setUpComponent(@NonNull AppComponent appComponent) {
        appComponent.inject(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewId());
        ButterKnife.bind(this);
        setUpComponent(App.getAppComponent(this));
    }

    protected void showKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, 0);
    }

    protected void hideKeyboard() {
        hideKeyboard(getCurrentFocus());
    }

    protected void hideKeyboard(View view) {
        if (view == null) {
            view = new View(this);
        }
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected boolean isLive() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return !isDestroyed() && !isFinishing();
        } else {
            return !isFinishing();
        }
    }

    protected void showLoader() {
        if (loader == null && this.isLive()) {
            try {
                loader = DialogFactory.getLoadingDialog(this).create();
                loader.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    protected void hideLoader() {
        if (loader != null) {
            loader.hide();
        }
    }

    protected void showError() {
        showError(getString(R.string.default_server_error));
    }

    protected void showError(Throwable t) {
        if (t == null || !(t instanceof ErrorCodeThrowable)) {
            showError();
            return;
        }
        ErrorCodeThrowable error = ((ErrorCodeThrowable) t);
        showError(getString(error.getErrorStringRes()));
    }

    protected void showError(String msg) {
        DialogFactory.getInfoDialog(this, msg).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isRestarted = true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        isStarted = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        addKeyboardCloser();
    }

    @Override
    protected void onPause() {
        removeKeyboardCloser();
        super.onPause();
    }

    @Override
    protected void onStop() {
        isStarted = false;
        isRestarted = false;
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    protected void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    protected void toast(@StringRes int msg) {
        toast(getString(msg));
    }

    public boolean isStarted() {
        return isStarted;
    }

    private void addKeyboardCloser() {
        FrameLayout rootLayout = (FrameLayout) findViewById(android.R.id.content);
        editTexts = new ArrayList<>();
        findEditTexts(rootLayout);
        keyboardCloseResolver = new FrameLayout(this);
        ViewGroup.LayoutParams lp = rootLayout.getLayoutParams();
        rootLayout.addView(keyboardCloseResolver, lp);
        keyboardCloseResolver.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                int x = (int) event.getX();
                int y = (int) event.getY();
                if (!isEditTextTouch(x, y) && (getCurrentFocus() instanceof EditText)) {
                    hideKeyboard();
                }
            }
            return false;
        });
    }

    /**
     * Recursively find and store all EditTexts within given view
     * @param view within which all target EditTexts situated
     */
    private void findEditTexts(View view) {
        if ((view instanceof EditText)) {
            editTexts.add(view);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                findEditTexts(innerView);
            }
        }
    }

    public void removeKeyboardCloser() {
        ViewGroup rootLayout = (ViewGroup) findViewById(android.R.id.content);
        rootLayout.removeView(keyboardCloseResolver);
        editTexts.clear();
    }

    /**
     * Get rectangle that represents view's area on screen
     * @param view to get area from
     * @return rectangle representing view's area
     */
    protected Rect getLocationOnScreen(View view) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        view.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + view.getWidth();
        mRect.bottom = location[1] + view.getHeight();

        return mRect;
    }

    /**
     * Detect if point represented by given coordinates match any stored view
     * @param x - x-coordinate of touch point
     * @param y - y-coordinate of touch point
     * @return if point match any stored EditText
     */
    public boolean isEditTextTouch(int x, int y) {
        for (View view : editTexts) {
            if (getLocationOnScreen(view).contains(x, y)) {
                return true;
            }
        }
        return false;
    }
}
