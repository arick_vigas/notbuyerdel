package com.notibuyer.app.ui.activity;


import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.notibuyer.app.R;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.model.User;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.NetworkUtils;
import com.notibuyer.app.utils.Utils;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import rx.android.schedulers.AndroidSchedulers;

public abstract class BaseAuthActivity extends BaseActivity implements FacebookCallback<LoginResult> {

    private static final int GOOGLE_SIGN_IN = 200;
    //private static final String SCOPE = "oauth2:https://www.googleapis.com/auth/userinfo.profile";
    private static GoogleApiClient googleApiClient;
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.title_toolbar) protected TextView titleToolbar;
    @Bind(R.id.main_layout) protected RelativeLayout mainLayout;
    private CallbackManager callbackManager;
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference databaseReference;

    public static GoogleApiClient  getGClient(){
        return googleApiClient;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        LoginManager.getInstance().registerCallback(callbackManager, this);
        mFirebaseAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(new Scope(Scopes.PROFILE), new Scope(Scopes.EMAIL), new Scope(Scopes.PLUS_LOGIN))
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, connectionResult -> {
                   hideLoader();
                   DialogFactory.getInfoDialog(this, R.string.cant_connect_to_google).show();
               })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                //.addApi(Plus.API)
                .build();

    }

   // static final int REQUEST_CODE_PICK_ACCOUNT = 1000;


    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
               // handleSignInResult(result);
            } else {
                // Google Sign In failed
                Log.e("AUTH", "GOOGLE auth failed!!!");

            }
          //  handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(BaseAuthActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            databaseReference.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    showLoader();
                                    User user;
                                    FirebaseUser fbUser = mFirebaseAuth.getCurrentUser();
                                    if(!dataSnapshot.hasChild(fbUser.getUid())){
                                        user = new User();
                                        user.setEmail(acct.getEmail());
                                        user.setName(acct.getDisplayName());
                                        user.setGPlusId(acct.getId());
                                        if(acct.getPhotoUrl()!=null)
                                            user.setAvatar(acct.getPhotoUrl().toString());
                                        databaseReference.child("users").child(fbUser.getUid()).setValue(user);
                                        String fcmToken = FirebaseInstanceId.getInstance().getToken();
                                        databaseReference.child("users").child(fbUser.getUid()).child("fcm_token").setValue(fcmToken);
                                        setUpCircles();
                                        RxPermissions.getInstance(BaseAuthActivity.this)
                                                .request(Manifest.permission.READ_PHONE_STATE)
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(granted -> {
                                                    if (granted) {
                                                        String mPhoneNumber = Utils.formatPhoneNumber(BaseAuthActivity.this, Utils.getPhoneNumber(BaseAuthActivity.this));
                                                        databaseReference.child("users").child(fbUser.getUid()).child("phone").setValue(mPhoneNumber);
                                                    } else {
                                                        DialogFactory.getInfoDialog(BaseAuthActivity.this, getString(R.string.no_permissions_for_action)).show();
                                                        hideLoader();
                                                    }
                                                }, throwable -> {
                                                    Log.d(TAG, throwable.getMessage());
                                                    hideLoader();
                                                });

                                    } else {
                                        user = dataSnapshot.child(fbUser.getUid()).getValue(User.class);
                                        String fcmToken = FirebaseInstanceId.getInstance().getToken();
                                        databaseReference.child("users").child(fbUser.getUid()).child("fcm_token").setValue(fcmToken);
                                    }

                                    prefs.setLoginWithGoogle();
                                    showLoader();
                                    Intent intent = new Intent(BaseAuthActivity.this, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                    // MainActivity.start(BaseAuthActivity.this);
                                    BaseAuthActivity.this.finish();
                                    hideLoader();
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e("Auth", databaseError.getMessage());
                                    hideLoader();
                                }
                            });
                        }
                    }
                });

    }

    @Override
    public void onStop() {
        super.onStop();
        if (googleApiClient.isConnected()) {

            googleApiClient.stopAutoManage(this);
            googleApiClient.disconnect();
        }
    }

    // Facebook login


    public void facebookAuth() {
        if (!NetworkUtils.isOn(this)) {
            Snackbar.make(mainLayout, R.string.no_internet_connection, Snackbar.LENGTH_SHORT).show();
            return;
        }
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));
    }

    public void emailLogin(String email, String password){
        mFirebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail:failed", task.getException());
                            Toast.makeText(BaseAuthActivity.this, R.string.cannot_login_with_email,
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            String fcmToken = FirebaseInstanceId.getInstance().getToken();
                            databaseReference.child("users").child(mFirebaseAuth.getCurrentUser().getUid()).child("fcm_token").setValue(fcmToken);

                            Intent intent = new Intent(BaseAuthActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                            //MainActivity.start(BaseAuthActivity.this);
                            //BaseAuthActivity.this.finish();
                            hideLoader();
                        }

                        // ...
                    }
                });
    }

    //passord Auth
    public void emailRegister(String email, String password, String userName){
        mFirebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            getString(R.string.cannot_login_with_email);
                        } else {
                            String id = task.getResult().getUser().getUid();
                            DatabaseReference newUserRef = databaseReference.child("users").child(id);
                            User user = new User();
                            user.setName(userName);
                            user.setEmail(email);
                            newUserRef.setValue(user);
                            newUserRef.child("fcm_token").setValue(FirebaseInstanceId.getInstance().getToken());

                            setUpCircles();

                            RxPermissions.getInstance(BaseAuthActivity.this)
                                    .request(Manifest.permission.READ_PHONE_STATE)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(granted -> {
                                        if (granted) {
                                            String mPhoneNumber = Utils.formatPhoneNumber(BaseAuthActivity.this, Utils.getPhoneNumber(BaseAuthActivity.this));
                                            newUserRef.child("phone").setValue(mPhoneNumber);
                                        } else {
                                            DialogFactory.getInfoDialog(BaseAuthActivity.this, getString(R.string.no_permissions_for_action)).show();
                                        }
                                    }, throwable -> {
                                        Log.d(TAG, throwable.getMessage());
                                    });
                        }
                        //MainActivity.start(BaseAuthActivity.this);
                        //BaseAuthActivity.this.finish();

                        Intent intent = new Intent(BaseAuthActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                        hideLoader();
                        // ...
                    }
                });
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        String token = loginResult.getAccessToken().getToken();
        showLoader();
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token);
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(BaseAuthActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            FirebaseUser fbUser = mFirebaseAuth.getCurrentUser();
                            databaseReference.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    showLoader();
                                    User user;
                                    if(!dataSnapshot.hasChild(fbUser.getUid())){
                                        user = new User();
                                        user.setFacebookId(loginResult.getAccessToken().getUserId());
                                        user.setEmail(fbUser.getEmail());
                                        user.setName(fbUser.getDisplayName());
                                        if(fbUser.getPhotoUrl()!=null)
                                            user.setAvatar("http://graph.facebook.com/" + loginResult.getAccessToken().getUserId() + "/picture?type=large");
                                        databaseReference.child("users").child(fbUser.getUid()).setValue(user);
                                        databaseReference.child("users").child(fbUser.getUid()).child("email").setValue(fbUser.getProviderData().get(1).getEmail());
                                        databaseReference.child("users").child(fbUser.getUid()).child("facebookId").setValue(loginResult.getAccessToken().getUserId());
                                        String fcmToken = FirebaseInstanceId.getInstance().getToken();
                                        databaseReference.child("users").child(fbUser.getUid()).child("fcm_token").setValue(fcmToken);
                                        setUpCircles();
                                        RxPermissions.getInstance(BaseAuthActivity.this)
                                                .request(Manifest.permission.READ_PHONE_STATE)
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(granted -> {
                                                    if (granted) {
                                                        String mPhoneNumber = Utils.formatPhoneNumber(BaseAuthActivity.this, Utils.getPhoneNumber(BaseAuthActivity.this));
                                                        databaseReference.child("users").child(fbUser.getUid()).child("phone").setValue(mPhoneNumber);
                                                    } else {
                                                        DialogFactory.getInfoDialog(BaseAuthActivity.this, getString(R.string.no_permissions_for_action)).show();
                                                    }
                                                }, throwable -> {
                                                    Log.d(TAG, throwable.getMessage());
                                                });

                                    } else {
                                        user = dataSnapshot.child(fbUser.getUid()).getValue(User.class);
                                        String fcmToken = FirebaseInstanceId.getInstance().getToken();
                                        databaseReference.child("users").child(fbUser.getUid()).child("fcm_token").setValue(fcmToken);
                                    }
                                    prefs.setLoginWithFb();

                                    Intent intent = new Intent(BaseAuthActivity.this, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);

                                    //MainActivity.start(BaseAuthActivity.this);
                                    BaseAuthActivity.this.finish();
                                    hideLoader();

                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e("Auth", databaseError.getMessage());
                                }
                            });

                        }

                        // [START_EXCLUDE]
                       hideLoader();
                        // [END_EXCLUDE]
                    }
                });

/*
        RxPermissions.getInstance(this).request(Manifest.permission.READ_PHONE_STATE)
                .flatMap(granted -> {
                    String phone = null;
                    if (granted) {
                        phone = Utils.formatPhoneNumber(BaseAuthActivity.this, Utils.getPhoneNumber(BaseAuthActivity.this));
                        prefs.setTempPhone(phone);
                    }
                    return authService.loginWithFacebook(token)
                            .subscribeOn(Schedulers.io());
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .finallyDo(this::hideLoader)
                .subscribe(userResponse -> {
                    if (userResponse.getCode() != null && 0 == userResponse.getCode()) {
                        facebookLoginSuccess();
                    } else {
                        facebookLoginFail(new Throwable());
                    }
                }, this::facebookLoginFail);*/
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {
        facebookLoginFail(error);
    }

    // Google login

    public void googleAuth() {
        if (!NetworkUtils.isOn(this)) {
            Snackbar.make(mainLayout, R.string.no_internet_connection, Snackbar.LENGTH_SHORT).show();
            return;
        }
        RxPermissions.getInstance(this).request(Manifest.permission.GET_ACCOUNTS).subscribe(aBoolean -> {
            if (aBoolean) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
            } else {
                googleLoginFail(new Throwable());
            }
        });
    }
/*
    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            showLoader();
            RxPermissions.getInstance(this).request(Manifest.permission.READ_PHONE_STATE)
                    .flatMap(granted -> {
                        String phone = null;
                        if (granted) {
                            phone = Utils.formatPhoneNumber(BaseAuthActivity.this, Utils.getPhoneNumber(BaseAuthActivity.this));
                            prefs.setTempPhone(phone);
                        }
                        return authService.loginWithGoogle(result.getSignInAccount().getIdToken())
                                .subscribeOn(Schedulers.io());
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .finallyDo(this::hideLoader)
                    .subscribe(userResponse -> {
                        if (userResponse.getCode() != null && 0 == userResponse.getCode()) {
                            googleLoginSuccess();
                        } else {
                            googleLoginFail(new Throwable());
                        }
                    }, this::googleLoginFail);
        } else {
            googleLoginFail(new Throwable(result.getStatus().getStatusMessage()));
        }
    }

    private String fetchToken(String email) {
        try {
            return GoogleAuthUtil.getToken(this, email, SCOPE);
        } catch (UserRecoverableAuthException e) {
            startActivityForResult(e.getIntent(), GOOGLE_SIGN_IN);
        } catch (GoogleAuthException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void googleLoginSuccess() {
        prefs.setLoginWithGoogle();
        MainActivity.start(this);
        finish();
    }

    protected void facebookLoginSuccess() {
        prefs.setLoginWithFb();
        MainActivity.start(this);
        finish();
    }
*/
    protected void facebookLoginFail(Throwable t) {
        System.out.println(t.getMessage());
        showError(getString(R.string.cannot_login_with_facebook));
    }

    protected void googleLoginFail(Throwable t) {
        Log.e(TAG, "googleLoginFail: " + t.getMessage());
        showError(getString(R.string.cannot_login_with_google));
    }
    private void setUpCircles(){

        FirebaseUser fbUser = mFirebaseAuth.getCurrentUser();
        Circle privateCircle = new Circle();
        privateCircle.setName("Private");
        privateCircle.setCreated(new Date());
        privateCircle.setOwnerId(fbUser.getUid());
        Map<String, Boolean> membersPrivate = new HashMap<String, Boolean>();
        membersPrivate.put(fbUser.getUid(), true);
        DatabaseReference privateRef =databaseReference.child("circles").push();
        privateRef.setValue(privateCircle);
        privateRef.child("id").setValue(privateRef.getKey());
        privateRef.child("members").setValue(membersPrivate);
        privateRef.child("color").setValue("#888888");
        databaseReference.child("users").child(fbUser.getUid()).child("circles").child(privateRef.getKey()).child("index").setValue(0);
        databaseReference.child("users").child(fbUser.getUid()).child("private_circle").setValue(privateRef.getKey());

    }
}
