package com.notibuyer.app.ui.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.notibuyer.app.R;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.NetworkUtils;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ChangePasswordActivity extends BaseActivity {

    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.old_password) protected EditText oldPassword;
    @Bind(R.id.new_password) protected EditText newPassword;
    @Bind(R.id.main_layout) protected RelativeLayout mainLayout;


    @Override
    protected int getContentViewId() {
        return R.layout.change_password_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.change_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.confirm)
    public void confirmNewPassword() {
        if (!NetworkUtils.isOn(this)) {
            Snackbar.make(mainLayout, R.string.no_internet_connection, Snackbar.LENGTH_SHORT).show();
            return;
        }

        String msg = null;

        if (TextUtils.isEmpty(oldPassword.getText().toString())) {
            msg = "Old password is empty";
        } else if (TextUtils.isEmpty(newPassword.getText().toString())) {
            msg = "New password is empty";
        } else if (oldPassword.getText().toString().equals(newPassword.getText().toString())) {
            msg = "Passwords are equal";
        }

        if (msg != null) {
            Snackbar.make(mainLayout, msg, Snackbar.LENGTH_SHORT).show();
            return;
        }

        hideKeyboard();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
        }
        return false;
    }



}
