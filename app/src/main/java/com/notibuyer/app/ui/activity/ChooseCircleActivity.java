package com.notibuyer.app.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.dagger.AppComponent;
import com.notibuyer.app.interfaces.CircleCheckClickListener;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.network.firebase.TaskService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseCircleActivity extends BaseActivity implements CircleCheckClickListener {

    public static final String CIRCLES_KEY = "circles";
    public static final String TASKS_LIST_KEY = "tasks_list";
    private static final int ADD_CIRCLE_CODE = 202;
    @Bind(R.id.toolbar)
    protected Toolbar toolbar;
    @Bind(R.id.circles_list)
    protected RecyclerView recyclerView;
    @Bind(R.id.add_new_circle_container)
    protected LinearLayout newCircleContainer;
    @Inject
    protected TaskService taskService;
    CirclesAdapter adapter;
    private String privateCircle;
    private String currentTaskId=null;
    private DatabaseReference databaseReference;
    private DatabaseReference userCirclesRef;
    private ChildEventListener userCirclesListener;
    private FirebaseUser mFirebaseUser;

   // private RealmResults<Circle> circlesList;

    //private List<String> tasksIdsList = new ArrayList<>();
    private FirebaseAuth mFirebaseAuth;
    private List<String> chosenCirclesIds = new ArrayList<>();
    private List<String> chosenTasksIds = new ArrayList<>();

    public static void startForTask(Activity activity, String taskId, int requestCode) {
        Intent intent = new Intent(activity, ChooseCircleActivity.class);
        intent.putExtra("currentTaskId", taskId);
        System.out.println("for task");
        activity.startActivityForResult(intent, requestCode);
    }

    public static void startForNewTask(Activity activity, List<String> chosenCircles, int requestCode) {
        Intent intent = new Intent(activity, ChooseCircleActivity.class);
        intent.putStringArrayListExtra(CIRCLES_KEY, (ArrayList<String>) chosenCircles);
        activity.startActivityForResult(intent, requestCode);
        System.out.println("for new");
    }

    public static void startForTaskList(Activity activity, List<String> chosenTasks, int requestCode) {
        Intent intent = new Intent(activity, ChooseCircleActivity.class);
        intent.putStringArrayListExtra(TASKS_LIST_KEY, (ArrayList<String>) chosenTasks);
        activity.startActivityForResult(intent, requestCode);
        System.out.println("for task list");
    }

    public void setUpComponent(@NonNull AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.act_circles;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);


        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        userCirclesRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.copy_to_circle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (getIntent().getExtras().getString("currentTaskId")!=null){
            currentTaskId = getIntent().getExtras().getString("currentTaskId");
        } else currentTaskId = null;
        if (getIntent().getExtras().getStringArrayList(CIRCLES_KEY) != null
                && !getIntent().getExtras().getStringArrayList(CIRCLES_KEY).isEmpty()) {
            chosenCirclesIds = getIntent().getExtras().getStringArrayList(CIRCLES_KEY);
        }
        if (getIntent().getExtras().getStringArrayList(TASKS_LIST_KEY) !=null
                && !getIntent().getExtras().getStringArrayList(TASKS_LIST_KEY).isEmpty()) {
            chosenTasksIds = getIntent().getExtras().getStringArrayList(TASKS_LIST_KEY);
        }




    }

    @Override
    protected void onStart() {
        super.onStart();

        adapter = new CirclesAdapter(this, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        // sync user circles
        userCirclesListener =userCirclesRef.orderByChild("index").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Circle circle = dataSnapshot.getValue(Circle.class);
                circle.setFbKey(dataSnapshot.getKey());
                if(currentTaskId!=null) {
                    databaseReference.child("tasks").child(currentTaskId).child("circleId").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            System.out.println("tasks/" + currentTaskId  + "/" + "circleId/");
                            if(!circle.getFbKey().equals(dataSnapshot.getValue())){
                                adapter.addItem(circle);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d(TAG, databaseError.getMessage());
                        }
                    });
                } else if(chosenTasksIds!=null && !chosenTasksIds.isEmpty()){
                    for(String item:chosenTasksIds){
                        databaseReference.child("tasks").child(item).child("circleId").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(!dataSnapshot.getValue().equals(circle.getFbKey())){
                                    adapter.addItem(circle);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.d(TAG, databaseError.getMessage());
                            }
                        });
                    }
                } else {
                    adapter.addItem(circle);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                adapter.removeItem(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

        databaseReference.child("users").child(mFirebaseUser.getUid()).child("private_circle").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                privateCircle =(String)dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        userCirclesRef.removeEventListener(userCirclesListener);
    }

    @OnClick(R.id.add_new_circle_container)
    protected void addNewCircle() {
        //  if (adapter.getItemCount() <= 10) {
        AddCircleActivity.startForResult(this, ADD_CIRCLE_CODE);
        // } else {
        //  if(prefs.getCurrentSubscribe()!=null && prefs.getCurrentSubscribe().equals("noti_month_test")) {
        //      AddCircleActivity.startForResult(this, ADD_CIRCLE_CODE);
        //   } else {
        //new UnleashDialog(this).show();
        //      Toast.makeText(this, getResources().getString(R.string.circles_limit), Toast.LENGTH_SHORT).show();
        //  //  }
        //}
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ADD_CIRCLE_CODE) {
              //  circlesList = realm.where(Circle.class).notEqualTo("name", "Private").findAll();
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        }
    }

    @Override
    public void addCircleClick(String circleId) {
        if (chosenCirclesIds.contains(circleId)) {
            chosenCirclesIds.remove(circleId);

            if(chosenCirclesIds.size()==0){
                chosenCirclesIds.add(privateCircle);
                recyclerView.getAdapter().notifyDataSetChanged();
                if(circleId.equals(privateCircle))
                    Toast.makeText(this, "Task must have at least one circle", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "Task moved to Private circle", Toast.LENGTH_SHORT).show();
            }
        } else {
            chosenCirclesIds.add(circleId);
        }

    }

    @Override
    public void onBackPressed() {
        if(currentTaskId!=null){
            for(String id:chosenCirclesIds){
                taskService.copyTaskToCircle(currentTaskId, id, this);
            }
            setResult(RESULT_OK);
            finish();
            /*databaseReference.child("tasks").child(currentTaskId).child("circles").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot snap:dataSnapshot.getChildren()){
                        removeTaskFromCircle(snap.getKey(), currentTaskId);
                    }
                    for(String id:chosenCirclesIds){
                        addTaskToCircle(id, currentTaskId);
                    }
                    setResult(RESULT_OK);
                    finish();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });*/
        } else if(chosenTasksIds!=null && !chosenTasksIds.isEmpty()){
            System.out.println("on back");
            for(String taskId:chosenTasksIds){
                for(String circleId:chosenCirclesIds){
                    taskService.copyTaskToCircle(taskId, circleId, this);
                }
                /*databaseReference.child("tasks").child(taskId).child("circles").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for(DataSnapshot snap:dataSnapshot.getChildren()){
                            removeTaskFromCircle(snap.getKey(), taskId);
                        }
                        for(String id:chosenCirclesIds){
                            addTaskToCircle(id, taskId);
                        }
                        setResult(RESULT_OK);
                        finish();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });*/
                setResult(RESULT_OK);
                finish();
            }
        } else {
            Intent intent = new Intent();
            intent.putStringArrayListExtra("data", (ArrayList<String>) chosenCirclesIds);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void addTaskToCircle(String circleId, String taskId){

        //add circles to task
        databaseReference.child("tasks").child(taskId).child("circles").child(circleId).setValue(true);
        //add task to circles
        databaseReference.child("tasks").child(taskId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String parent = "";
                if((boolean)dataSnapshot.child("completed").getValue()){
                    parent = "completed_tasks";
                } else {
                    parent = "current_tasks";
                }

                //get circles count
                final String finalParent = parent;
                databaseReference.child("circles").child(circleId).child(parent).orderByChild("index").limitToLast(1).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        long maxIndex = -1;
                        if(snapshot.exists()){
                            for(DataSnapshot t:snapshot.getChildren()){
                                if(t.hasChild("index"))
                                    maxIndex = (long)t.child("index").getValue();
                                break;
                            }
                        }

                        //add to circle
                        databaseReference.child("circles").child(circleId).child(finalParent).child(taskId).child("index").setValue(maxIndex +1);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    public void removeTaskFromCircle(String circleId, String taskId){
        //remove from task circles
        databaseReference.child("tasks").child(taskId).child("circles").child(circleId).removeValue();
        // remove from circle tasks
        databaseReference.child("tasks").child(taskId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String parent = "";
                if ((boolean) dataSnapshot.child("completed").getValue()) {
                    parent = "completed_tasks";
                } else {
                    parent = "current_tasks";
                }
                databaseReference.child("circles").child(circleId).child(parent).child(taskId).removeValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
            case R.id.save:
                this.onBackPressed();
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.save_changes_menu, menu);
            return true;
    }



    class CirclesAdapter extends RecyclerView.Adapter<CirclesAdapter.CircleHolder> {
        private List<Circle> circlesList = new ArrayList<>();
        private CircleCheckClickListener clickListener;
        private Context context;

        public CirclesAdapter(CircleCheckClickListener clickListener, Context context) {
            this.clickListener = clickListener;
            this.context = context;
        }

        public void addItem(Circle circle){
            circlesList.add(circle);
            notifyItemInserted(circlesList.size());
        }

        public void removeItem(String circleId){
            Circle remove = null;
            int pos = -1;
            for(Circle circ:circlesList){
                if(circ.getFbKey().equals(circleId)){
                    pos = circlesList.indexOf(circ);
                    remove = circ;
                    break;
                }
            }
            if(remove!=null){
                circlesList.remove(remove);
                notifyItemRemoved(pos);
            }
        }

        @Override
        public CircleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new CircleHolder(LayoutInflater.from(context).inflate(R.layout.choose_circle_list_item, parent, false));
        }

        @Override
        public int getItemCount() {
          return circlesList.size();
        }

        @Override
        public void onBindViewHolder(CircleHolder holder, int position) {
            holder.includeTask.setChecked(false);
            holder.includeTask.setChecked(chosenCirclesIds.contains(circlesList.get(position).getFbKey()));
            holder.itemView.setOnClickListener((v) -> {
                holder.includeTask.setChecked(!holder.includeTask.isChecked());
                clickListener.addCircleClick(circlesList.get(position).getFbKey());
            });


            //get task circle
            /*
            if (currentTaskId!=null && !TextUtils.isEmpty(currentTaskId)){
                databaseReference.child("tasks").child(currentTaskId).child("circles").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.hasChild(circlesList.get(position).getFbKey())){
                            holder.includeTask.setChecked(true);
                            chosenCirclesIds.add(circlesList.get(position).getFbKey());
                        } else {
                            holder.includeTask.setChecked(false);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            }*/

            databaseReference.child("circles").child(circlesList.get(position).getFbKey()).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if(dataSnapshot.getKey().equals("name")){
                        holder.circle.setText((String)dataSnapshot.getValue());
                    }
                    if(dataSnapshot.getKey().equals("color")){
                        GradientDrawable gd = (GradientDrawable) holder.circle.getBackground().getCurrent();
                        gd.setColor(Color.parseColor((String)dataSnapshot.getValue()));
                        gd.setStroke(0, 0);
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    if(dataSnapshot.getKey().equals("name")){
                        holder.circle.setText((String)dataSnapshot.getValue());
                    }
                    if(dataSnapshot.getKey().equals("color")){
                        GradientDrawable gd = (GradientDrawable) holder.circle.getBackground().getCurrent();
                        gd.setColor(Color.parseColor((String)dataSnapshot.getValue()));
                        gd.setStroke(0, 0);
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });

               /*


                includeTask.setChecked(chosenCirclesIds.contains(circle.getId()));*/

        }

        class CircleHolder extends RecyclerView.ViewHolder {
            @Bind(R.id.circle)
            TextView circle;
            @Bind(R.id.include_task)
            CheckBox includeTask;
            public CircleHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
