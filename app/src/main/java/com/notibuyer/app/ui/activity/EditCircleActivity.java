package com.notibuyer.app.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.EditCirclesPagerAdapter;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.ui.view.colorpicker.ColorPickerDialog;
import com.notibuyer.app.ui.view.colorpicker.ColorPickerSwatch;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.UIUtils;

import butterknife.Bind;
import butterknife.OnClick;


public class EditCircleActivity extends BaseActivity implements ColorPickerSwatch.OnColorSelectedListener {

    private static final String CIRCLE_ID = "circle_id";

    @Bind(R.id.circle_name) protected EditText circleName;
    @Bind(R.id.edit_circles_pager) protected ViewPager pager;
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.circle_color) protected ImageView circleColor;
    @Bind(R.id.search_friends) protected Button searchFriends;
    @Bind(R.id.my_friends) protected Button myFriends;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private Circle updatedCircle;
    private EditCirclesPagerAdapter editCirclesPagerAdapter;
    private String circleId;

    public static void startForResult(Activity context, String circleId, int requestCode) {
        Intent intent = new Intent(context, EditCircleActivity.class);
        intent.putExtra(CIRCLE_ID, circleId);
        context.startActivityForResult(intent, requestCode);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.edit_circles_layout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.edit_circle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        circleId = getIntent().getExtras().getString(CIRCLE_ID);
        editCirclesPagerAdapter = new EditCirclesPagerAdapter(getSupportFragmentManager(), circleId);
        pager.setAdapter(editCirclesPagerAdapter);
        pager.setCurrentItem(0);
        pager.getAdapter().notifyDataSetChanged();
        searchFriends.setOnClickListener(v->pager.setCurrentItem(1));
        myFriends.setOnClickListener(v->pager.setCurrentItem(0));

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        if (TextUtils.isEmpty(circleId)) throw new IllegalArgumentException("circle id can not be empty");
        databaseReference.child("circles").child(circleId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                updatedCircle = dataSnapshot.getValue(Circle.class);
                if (updatedCircle == null)
                    throw new IllegalStateException("cant find circle with id [" + circleId + "]");
                circleName.setText(updatedCircle.getName());


                circleColor.getBackground().setColorFilter(new PorterDuffColorFilter(Color.parseColor(updatedCircle.getColor()), PorterDuff.Mode.MULTIPLY));
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("EDIT_CIRCLE", databaseError.getMessage());
            }
        });
    }

    @OnClick(R.id.circle_color)
    protected void changeColor() {
        ColorPickerDialog colorPicker = ColorPickerDialog.newInstance(
                R.string.color_picker_default_title,
                UIUtils.colorChoice(this), 0, 4,
                UIUtils.isTablet(this)? ColorPickerDialog.SIZE_LARGE : ColorPickerDialog.SIZE_SMALL);
        colorPicker.setOnColorSelectedListener(this);
        colorPicker.show(getFragmentManager(), "color");
    }

    // TODO: 20.08.2016 place in service
    @OnClick(R.id.remove_circle)
    protected void removeCircle() {
        /*if (!NetworkUtils.isOn(this)) {
            showError(getString(R.string.no_internet_connection));
            return;
        }*/
        // TODO: 20.08.2016 remove from task circles!!!
        DialogFactory.getConfirmationDialog(this, R.string.confirm_circle_remove, (dialog, which) ->{
            databaseReference.child("circles").child(circleId).child("members").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //get Member userID
                    for(DataSnapshot member:dataSnapshot.getChildren()){
                        //remove from user circles
                        databaseReference.child("users").child(member.getKey()).child("circles").child(circleId).removeValue();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });
            //remove from circles
            databaseReference.child("circles").child(circleId).removeValue();
            //back to circlesFragment
            finish();
        }).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            case R.id.save:
              /*  if (!NetworkUtils.isOn(this)) {
                    showError(getString(R.string.no_internet_connection));
                    return true;
                }*/
                if (TextUtils.isEmpty(circleName.getText())) {
                    toast(getString(R.string.empty_name));
                    return true;
                }

                hideKeyboard();
                showLoader();
                DatabaseReference circleReference = databaseReference.child("circles").child(circleId);
                circleReference.child("name").setValue(circleName.getText().toString());
                circleReference.child("color").setValue(updatedCircle.getColor());
                hideLoader();
                this.onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void onColorSelected(int color) {
        String hexColor = String.format("#%06X", (0xFFFFFF & color));
        updatedCircle.setColor(hexColor);
        circleColor.setBackground(getResources().getDrawable(R.drawable.palette));
        circleColor.getBackground().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.MULTIPLY));
    }
}
