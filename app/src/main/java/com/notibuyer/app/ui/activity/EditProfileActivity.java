package com.notibuyer.app.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.notibuyer.app.R;
import com.notibuyer.app.dagger.AppComponent;
import com.notibuyer.app.model.User;
import com.notibuyer.app.network.firebase.UserService;
import com.notibuyer.app.ui.view.CircleTransformation;
import com.notibuyer.app.ui.view.StrokeTransform;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.NetworkUtils;
import com.notibuyer.app.utils.Utils;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

public class EditProfileActivity extends BaseActivity implements FacebookCallback<LoginResult> {

    private static final int REQUEST_IMAGE_PICK = 200;
    private static final int REQUEST_IMAGE_CAPTURE = 201;
    private static final int LINK_GOOGLE =10500;
    private static GoogleApiClient googleApiClient;
    @Bind(R.id.name) protected EditText name;
    @Bind(R.id.email) protected EditText email;
    @Bind(R.id.phone) protected EditText phone;
    @Bind(R.id.main_layout) protected LinearLayout mainLayout;
    @Bind(R.id.change_avatar) protected TextView changeAvatar;
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.profile_icon) protected ImageView profileIcon;
    @Bind(R.id.link_facebook) protected ImageView fbLink;
    @Bind(R.id.link_gplus) protected ImageView googleLink;
    @Bind(R.id.link_social_title) protected TextView linkSocialTitle;
    @Bind(R.id.facebook) protected ImageView fbIcon;
    @Bind(R.id.gplus) protected ImageView gplusIcon;
    @Bind(R.id.social_title) protected TextView socialTitle;
    @Inject
    UserService userService;
    private Uri cropped;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;
    private DatabaseReference userRef;
    private ChildEventListener userDataListener;
    private User user;
    private CallbackManager callbackManager;

    @Override
    protected int getContentViewId() {
        return R.layout.edit_profile;
    }

    public void setUpComponent(@NonNull AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.edit_profile);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(new Scope(Scopes.PROFILE), new Scope(Scopes.EMAIL), new Scope(Scopes.PLUS_LOGIN))
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, connectionResult -> {
                    hideLoader();
                    DialogFactory.getInfoDialog(this, R.string.cant_connect_to_google).show();
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                //.addApi(Plus.API)
                .build();


        registerForContextMenu(profileIcon);
        cropped = Uri.fromFile(new File(getCacheDir(), "avatar.png"));
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        storageReference = FirebaseStorage.getInstance().getReferenceFromUrl( "gs://notibuyer-b084a.appspot.com");
        databaseReference = FirebaseDatabase.getInstance().getReference();
        userRef = databaseReference.child("users").child(mFirebaseUser.getUid());

    }

    @Override
    protected void onStart() {
        super.onStart();


        for(UserInfo userInfo:mFirebaseUser.getProviderData()){
            if(userInfo.getProviderId().equals("facebook.com")){
                fbLink.setVisibility(View.GONE);
                fbIcon.setVisibility(View.VISIBLE);
            } else if(userInfo.getProviderId().equals("google.com")){
                googleLink.setVisibility(View.GONE);
                gplusIcon.setVisibility(View.VISIBLE);
            }
        }
        if(fbLink.getVisibility()==View.GONE && googleLink.getVisibility()==View.GONE)
            linkSocialTitle.setVisibility(View.GONE);
        if(fbIcon.getVisibility()==View.GONE && gplusIcon.getVisibility()==View.GONE)
            socialTitle.setVisibility(View.GONE);


        user = new User();
        userDataListener = userRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "name":
                        user.setName((String)dataSnapshot.getValue());
                        name.setText(user.getName());
                        break;
                    case "avatar":
                        Picasso.with(EditProfileActivity.this)
                                .load((String)dataSnapshot.getValue())
                                .error(R.drawable.profile_icon_error)
                                .placeholder(R.drawable.profile_icon_error)
                                .transform(new CircleTransformation())
                                .transform(new StrokeTransform())
                                .into(profileIcon);
                        break;
                    case "email":
                        user.setEmail((String)dataSnapshot.getValue());
                        email.setText(user.getEmail());
                        break;
                    case "phone":
                        user.setPhone((String)dataSnapshot.getValue());
                        phone.setText(user.getPhone());
                        break;
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "name":
                        user.setName((String)dataSnapshot.getValue());
                        name.setText(user.getName());
                        break;
                    case "avatar":
                        Picasso.with(EditProfileActivity.this)
                                .load((String)dataSnapshot.getValue())
                                .error(R.drawable.profile_icon_error)
                                .placeholder(R.drawable.profile_icon_error)
                                .transform(new CircleTransformation())
                                .transform(new StrokeTransform())
                                .into(profileIcon);
                        break;
                    case "email":
                        user.setEmail((String)dataSnapshot.getValue());
                        email.setText(user.getEmail());
                        break;
                    case "phone":
                        user.setPhone((String)dataSnapshot.getValue());
                        phone.setText(user.getPhone());
                        break;
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }


    @OnClick(R.id.profile_icon)
    protected void previewImage() {
        databaseReference.child("users").child(mFirebaseUser.getUid()).child("avatar").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ImagePreviewActivity.start(EditProfileActivity.this, (String) dataSnapshot.getValue());
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        userRef.removeEventListener(userDataListener);
        if (googleApiClient.isConnected()) {
            googleApiClient.stopAutoManage(this);
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        menu.setHeaderTitle(R.string.profile_picture);
        inflater.inflate(R.menu.profile_change_photo_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(!NetworkUtils.isOn(this)) {
            DialogFactory.getInfoDialog(this, getString(R.string.no_internet_connection));
            return false;
        }
        switch (item.getItemId()) {
            case R.id.take_photo:
                dispatchTakePictureIntent();
                break;
            case R.id.gallery_photo:
                pickPictureFromGallery();
                break;
            case R.id.remove_photo:
                userService.removeUserpic(mFirebaseUser.getUid());
                break;
        }
        return true;
    }

    private void dispatchTakePictureIntent() {
        RxPermissions.getInstance(this).request(Manifest.permission.CAMERA).subscribe(granted -> {
            if (granted) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } else {
                DialogFactory.getInfoDialog(EditProfileActivity.this, R.string.camera_permissions_not_granted).show();
            }
        });
    }

    private void pickPictureFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, getString(R.string.select_picture)),
                REQUEST_IMAGE_PICK);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_changes_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_OK);
                saveUserData();
                this.onBackPressed();
                return true;
            case R.id.save:
                saveUserData();
                return true;
        }
        return false;
    }

    private void saveUserData() {
        if (!NetworkUtils.isOn(this)) {
            Snackbar.make(mainLayout, R.string.no_internet_connection, Snackbar.LENGTH_SHORT).show();
            return;
        }

        String msg = null;


        if (TextUtils.isEmpty(name.getText().toString())) {
            msg = getString(R.string.empty_name);
        } else if (TextUtils.isEmpty(phone.getText().toString())) {
            msg = getString(R.string.wrong_phone);
        } else if (name.getText().toString().equals(user.getName()) &&
                email.getText().toString().equals(user.getEmail()) &&
                phone.getText().toString().equals(user.getPhone())) {
            msg = getString(R.string.no_changes_error);
        } else {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            try {
                Phonenumber.PhoneNumber number = phoneUtil.parse(phone.getText().toString(), Utils.getCountryIso(EditProfileActivity.this).toUpperCase());
                boolean isValid = phoneUtil.isValidNumber(number);
                if (isValid) {
                    phone.setText(phoneUtil.format(number, PhoneNumberUtil.PhoneNumberFormat.E164));
                } else {
                    msg = getString(R.string.invalid_phone);
                }
            } catch (NumberParseException e) {
                msg = getString(R.string.invalid_phone);
            }
        }

        if (msg != null) {
            Snackbar.make(mainLayout, msg, Snackbar.LENGTH_SHORT).show();
            return;
        }



        hideKeyboard();
        showLoader();

        userRef.child("name").setValue(name.getText().toString());
        userRef.child("email").setValue(email.getText().toString());
        userRef.child("phone").setValue(phone.getText().toString());


        hideLoader();
        Snackbar.make(mainLayout, getResources().getString(R.string.changes_saved), Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.change_avatar)
    protected void changeAvatar() {
        this.registerForContextMenu(changeAvatar);
        this.openContextMenu(changeAvatar);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_OK);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                Bundle extras = data.getExtras();
                Bitmap bitmap = (Bitmap) extras.get("data");
                startCropping(Uri.fromFile(Utils.getFile(this, bitmap)));
            } else if (requestCode == REQUEST_IMAGE_PICK) {
                Uri imageUri = data.getData();
                startCropping(imageUri);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                final Uri resultUri = UCrop.getOutput(data);
                if (resultUri != null) {
                    try {
                        uploadProfileImage(MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (requestCode == LINK_GOOGLE){
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                if (result.isSuccess()) {
                    GoogleSignInAccount account = result.getSignInAccount();
                    AuthCredential googleCredential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
                    mFirebaseAuth.getCurrentUser().linkWithCredential(googleCredential)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Log.d(TAG, "linkWithCredential:onComplete:" + task.isSuccessful());

                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        Toast.makeText(EditProfileActivity.this, "Authentication failed.",
                                                Toast.LENGTH_SHORT).show();

                                    } else {
                                        databaseReference.child("users").child(mFirebaseUser.getUid()).child("gplusId").setValue(account.getId());
                                        Toast.makeText(EditProfileActivity.this, "Account linked",
                                                Toast.LENGTH_SHORT).show();
                                        googleLink.setVisibility(View.GONE);
                                        gplusIcon.setVisibility(View.VISIBLE);
                                        socialTitle.setVisibility(View.VISIBLE);
                                        if(fbLink.getVisibility() == View.GONE)
                                            linkSocialTitle.setVisibility(View.GONE);
                                    }
                                    // ...
                                }
                            });
                }
            }
        }
    }

    @OnClick(R.id.link_gplus)
    protected void linkWithGoogle(){
        linkGoogle();
    }

    @OnClick(R.id.link_facebook)
    protected void linkWithFacebook(){
        linkFb();
    }

    private void linkFb(){
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));
    }

    private void startCropping(Uri source) {
        UCrop.of(source, cropped)
                .withAspectRatio(1, 1)
                .withMaxResultSize(300, 300)
                .start(this);
    }

    public void uploadProfileImage(Bitmap bitmap) {
        RxPermissions.getInstance(this)
                .request(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) {
                        showLoader();
                        File imageFile = Utils.getFile(this, bitmap);
                        //RequestBody requestBody =
                                //RequestBody.create(MediaType.parse("image/*"), file);
                        showLoader();
                        Uri file = Uri.fromFile(imageFile);
                        String path = "userpic/" + mFirebaseUser.getUid() + "/" +file.getLastPathSegment();
                        StorageReference updateImage = storageReference.child(path);
                        UploadTask uploadTask = updateImage.putFile(file);
                        // Register observers to listen for when the upload is done or if it fails
                        uploadTask.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle unsuccessful uploads
                                Log.e(TAG, exception.getMessage());
                                hideLoader();
                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and upload URL.
                                Uri uploadUrl = taskSnapshot.getDownloadUrl();
                                userRef.child("avatar").setValue(uploadUrl.toString());
                                hideLoader();
                            }
                        });

                    } else {
                        DialogFactory.getInfoDialog(this, R.string.cant_update_avatar).show();
                    }
                });

    }

    private void linkGoogle(){
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, LINK_GOOGLE);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        AuthCredential credential = FacebookAuthProvider.getCredential(loginResult.getAccessToken().getToken());
        mFirebaseAuth.getCurrentUser().linkWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "linkWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(EditProfileActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        } else {
                            databaseReference.child("users").child(mFirebaseUser.getUid()).child("facebookId").setValue(loginResult.getAccessToken().getUserId());
                            Toast.makeText(EditProfileActivity.this, "Account linked",
                                    Toast.LENGTH_SHORT).show();
                            fbLink.setVisibility(View.GONE);
                            fbIcon.setVisibility(View.VISIBLE);
                            socialTitle.setVisibility(View.VISIBLE);
                            if(googleLink.getVisibility() == View.GONE)
                                linkSocialTitle.setVisibility(View.GONE);
                        }
                        // ...
                    }
                });
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {

    }
}
