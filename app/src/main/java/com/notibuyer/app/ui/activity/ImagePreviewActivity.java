package com.notibuyer.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.notibuyer.app.R;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.Bind;

public class ImagePreviewActivity extends BaseActivity {

    private static final String IMAGE_URL = "image";
    private static final String IMAGE_PATH = "image_path";

    @Bind(R.id.image)
    protected ImageView image;

    @Override
    protected int getContentViewId() {
        return R.layout.act_image_preview;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().hasExtra(IMAGE_PATH)) {
            Picasso.with(this).load(new File(getIntent().getExtras().getString(IMAGE_PATH))).into(image);
        } else if (getIntent().hasExtra(IMAGE_URL)) {
            Picasso.with(this).load(getIntent().getExtras().getString(IMAGE_URL)).into(image);
        }
    }

    public static void start(Context context, String image) {
        Intent intent = new Intent(context, ImagePreviewActivity.class);
        intent.putExtra(IMAGE_URL, image);
        context.startActivity(intent);
    }

    public static void startWithFile(Context context, String image) {
        Intent intent = new Intent(context, ImagePreviewActivity.class);
        intent.putExtra(IMAGE_PATH, image);
        context.startActivity(intent);
    }
}