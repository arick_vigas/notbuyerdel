package com.notibuyer.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.notibuyer.app.R;
import com.notibuyer.app.ui.fragments.AllNotesFragment;
import com.notibuyer.app.ui.fragments.ArchiveFragment;
import com.notibuyer.app.ui.fragments.CirclesFragment;
import com.notibuyer.app.ui.fragments.FeedbackFragment;
import com.notibuyer.app.ui.fragments.FriendsFragment;
import com.notibuyer.app.ui.fragments.NotificationsFragment;
import com.notibuyer.app.ui.fragments.SettingsFragment;
import com.notibuyer.app.ui.view.CircleTransformation;
import com.notibuyer.app.ui.view.NavDrawerItem;
import com.notibuyer.app.ui.view.NavDrawerListAdapter;
import com.notibuyer.app.ui.view.StrokeTransform;
import com.notibuyer.app.utils.DialogFactory;
import com.squareup.picasso.Picasso;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;

public class MainActivity extends BaseActivity implements AdapterView.OnItemClickListener, DrawerLayout.DrawerListener {

    public static final String SELECTED_ITEM = "selected_item";

    public static final int ALL = 1;
    public static final int NOTIFICATIONS = 2;
    public static final int CIRCLES = 3;
    public static final int FRIENDS = 4;
    public static final int SPREAD = 5;
    public static final int FEEDBACK = 6;
    public static final int ARCHIVE = 7;
    public static final int SETTINGS = 8;
    public static final int PREMIUM = 9;
    public static final int LOGOUT = 10;
    public static final int CODE_EDIT_PROFILE = 1111;
    @Bind(R.id.toolbar)
    protected Toolbar toolbar;
    @Bind(R.id.drawer_layout)
    protected DrawerLayout drawer;
    @Bind(R.id.nav_view)
    protected ListView drawerList;
    FirebaseRemoteConfig mFirebaseRemoteConfig;
    DatabaseReference notiCountRef;
    TextView countNotifications;
    private boolean showNotiCount = false;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private ChildEventListener profileListener;
    private DatabaseReference userRef;
    private GoogleApiClient googleApiClient;
    private ViewGroup drawerHeader;
    private int currentFragment = -1;
    private boolean fromPush = false;
    private ArrayList<NavDrawerItem> navDrawerItems = new ArrayList<>();

    public static void start(Context context, @WhichSelection int menuItem, String circleId) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("circle_id", circleId);
        intent.putExtra(SELECTED_ITEM, menuItem);
        context.startActivity(intent);

    }

    public static void start(Context context, @WhichSelection int menuItem) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(SELECTED_ITEM, menuItem);
        context.startActivity(intent);


    }

    public static void start(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));

    }

    @Override
    protected void onResume() {
        super.onResume();
        //Appodeal.onResume(this, Appodeal.BANNER_BOTTOM);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // notiCountRef.removeEventListener(notiCountListener);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (!hasFocus && prefs.isAutoRecordEnabled())
            prefs.setOnLaunch(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.noti, menu);
        final MenuItem noti = menu.findItem(R.id.action_notifications);
        MenuItemCompat.setActionView(noti, R.layout.feed_update_count);
        noti.getActionView().setOnClickListener(v -> showFragment(NOTIFICATIONS));
        if (showNotiCount)
            noti.setVisible(true);
        else
            noti.setVisible(false);
        countNotifications = (TextView) noti.getActionView().findViewById(R.id.hotlist_hot);
        countNotifications.setVisibility(View.GONE);
        notiCountRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    long count = (long) dataSnapshot.getValue();
                    if (count > 0) {
                        countNotifications.setText(String.valueOf(count));
                        countNotifications.setVisibility(View.VISIBLE);
                    } else {
                        countNotifications.setText(String.valueOf(count));
                        countNotifications.setVisibility(View.GONE);
                    }
                } else {
                    countNotifications.setVisibility(View.GONE);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage());
            }
        });
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Appodeal.show(this, Appodeal.BANNER_BOTTOM);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        notiCountRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread");


        setSupportActionBar(toolbar);
        if (mFirebaseUser == null) {

            SignInActivity.start(this);
            finish();
            return;
        } else {
            userRef = databaseReference.child("users").child(mFirebaseUser.getUid());
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        FirebaseRemoteConfigSettings firebaseRemoteConfigSettings =
                new FirebaseRemoteConfigSettings.Builder()
                        .setDeveloperModeEnabled(true)
                        .build();

        Map<String, Object> defaultConfigMap = new HashMap<>();
        defaultConfigMap.put("show_premium", true);

        mFirebaseRemoteConfig.setConfigSettings(firebaseRemoteConfigSettings);
        mFirebaseRemoteConfig.setDefaults(defaultConfigMap);





        if (drawerHeader == null) {
            drawerHeader = (ViewGroup) getLayoutInflater().inflate(R.layout.nav_header_main, drawerList, false);
            drawerList.addHeaderView(drawerHeader, "header", false);
            drawerHeader.findViewById(R.id.edit_profile).setOnClickListener(v -> startActivityForResult(new Intent(this, EditProfileActivity.class), CODE_EDIT_PROFILE));
            drawerHeader.findViewById(R.id.profile_icon).setOnClickListener(v -> startActivityForResult(new Intent(this, EditProfileActivity.class), CODE_EDIT_PROFILE));
            drawerHeader.findViewById(R.id.profile_name).setOnClickListener(v -> startActivityForResult(new Intent(this, EditProfileActivity.class), CODE_EDIT_PROFILE));
        }


        drawerList.setOnItemClickListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.addDrawerListener(this);
        toggle.syncState();

        int selectedId = ALL;
        if (getIntent().getExtras() != null) {
            selectedId = getIntent().getExtras().getInt(SELECTED_ITEM);
        }
        showFragment(selectedId);
        if (getIntent().getExtras() != null) {
            fromPush = getIntent().getExtras().getBoolean("from_push");
        }


        if (prefs.isLoginWithGoogle()) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestEmail()
                    .requestScopes(new Scope(Scopes.PROFILE), new Scope(Scopes.EMAIL), new Scope(Scopes.PLUS_LOGIN))
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .build();

            googleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, connectionResult -> {
                        hideLoader();
                        DialogFactory.getInfoDialog(this, R.string.cant_connect_to_google).show();
                    })
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    //.addApi(Plus.API)
                    .build();
            googleApiClient.connect();
        }
        setMenu();
    }


    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (fromPush)
                showFragment(MainActivity.ALL);
            else
                super.onBackPressed();
        }
    }

    public void deselectDrawerItems() {
        drawerList.clearChoices();
        for (int i = 0; i < drawerList.getChildCount(); i++) {
            drawerList.getChildAt(i).setSelected(false);
        }
    }

    public void selectDrawerItem(int pos) {
        deselectDrawerItems();
        drawerList.setItemChecked(pos, true);
        drawerList.setSelection(pos);
        currentFragment = pos;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SettingsFragment.CODE_EDIT_PROFILE && resultCode == RESULT_OK) {

        }
        System.out.println(resultCode + " " + requestCode);
        try {
            if (getSupportFragmentManager().getFragments() != null)
                Stream.of(getSupportFragmentManager().getFragments())
                        .filter(f -> f != null)
                        .forEach(f -> f.onActivityResult(requestCode, resultCode, data));
        } catch (Exception e) {
            e.printStackTrace();
            MainActivity.start(this, MainActivity.ALL);
            this.finish();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        System.out.println(position);
        showFragment(position);
    }

    private void showFragment(int which) {
        if (which == SPREAD) {
            drawerList.setItemChecked(currentFragment, true);
            drawerList.setSelection(currentFragment);
            drawer.closeDrawer(drawerList);
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.notibuyer.app");
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, "Share"));
            return;
        }
        if (currentFragment == which) {
            drawer.closeDrawer(drawerList);
            return;
        }
        currentFragment = which;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        switch (which) {
            case 0:
                //UserProfileActivity.start(this, "", );
                return;
            case ALL:
                showNotiCount = true;
                if (prefs.isAutoRecordEnabled())
                    prefs.setOnLaunch(true);
                AllNotesFragment allNotes = new AllNotesFragment();
                if (getIntent().getExtras() != null && !TextUtils.isEmpty(getIntent().getStringExtra("circle_id"))) {
                    Bundle bundle = new Bundle();
                    bundle.putString("circle_id", getIntent().getStringExtra("circle_id"));
                    allNotes.setArguments(bundle);
                }
                transaction.replace(R.id.content, allNotes);
                break;
            case NOTIFICATIONS:
                transaction.replace(R.id.content, new NotificationsFragment());
                showNotiCount = false;
                break;
            case CIRCLES:
                showNotiCount = true;
                transaction.replace(R.id.content, new CirclesFragment());
                break;
            case FRIENDS:
                showNotiCount = true;
                transaction.replace(R.id.content, new FriendsFragment());
                break;
            case FEEDBACK:
                showNotiCount = true;
                transaction.replace(R.id.content, new FeedbackFragment());
                break;
            case ARCHIVE:
                showNotiCount = true;
                transaction.replace(R.id.content, new ArchiveFragment());
                break;
            case SETTINGS:
                showNotiCount = true;
                System.out.println("settings");
                transaction.replace(R.id.content, new SettingsFragment());
                break;
            case PREMIUM:
                startActivity(new Intent(this, PremiumActivity.class));
                finish();
                break;
            case LOGOUT:
                System.out.println("logout");

                if (prefs.isLoginWithGoogle()) {
                    Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    // ...
                                    System.out.println(status.getStatusMessage());
                                }
                            });
                }
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(this, StartupActivity.class));
                finish();


                break;
            default:
                break;
        }
        transaction.commit();
        drawerList.setItemChecked(which, true);
        drawerList.setSelection(which);
        drawer.closeDrawer(drawerList);
    }

    @Override
    protected void onStop() {
        super.onStop();
        userRef.removeEventListener(profileListener);

        getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
                .putBoolean("isFirstRun", true).apply();
    }

    @Override
    protected void onStart() {
        super.onStart();
        /*Picasso.with(MainActivity.this)
                .load(R.drawable.profile_icon_error)
                .transform(new CircleTransformation())
                .transform(new StrokeTransform())
                .placeholder( R.drawable.progress_loading_animation)
                .into((ImageView) drawerHeader.findViewById(R.id.profile_icon));
*/

        profileListener = userRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getKey().equals("name"))
                    ((TextView) drawerHeader.findViewById(R.id.profile_name)).setText((String) dataSnapshot.getValue());
                if (dataSnapshot.getKey().equals("avatar"))
                    Picasso.with(MainActivity.this)
                            .load((String) dataSnapshot.getValue())
                            .transform(new CircleTransformation())
                            .transform(new StrokeTransform())
                            .placeholder(R.drawable.progress_loading_animation)
                            .into((ImageView) drawerHeader.findViewById(R.id.profile_icon));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.getKey().equals("name"))
                    ((TextView) drawerHeader.findViewById(R.id.profile_name)).setText((String) dataSnapshot.getValue());
                if (dataSnapshot.getKey().equals("avatar"))
                    Picasso.with(MainActivity.this)
                            .load((String) dataSnapshot.getValue())
                            .transform(new CircleTransformation())
                            .transform(new StrokeTransform())
                            .placeholder(R.drawable.progress_loading_animation)
                            .into((ImageView) drawerHeader.findViewById(R.id.profile_icon));
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

    }
    @SuppressWarnings("ResourceType")
    public void setMenu() {
        String[] navMenuTitles = MainActivity.this.getResources().getStringArray(R.array.nav_drawer_items);
        TypedArray navMenuIcons = MainActivity.this.getResources().obtainTypedArray(R.array.nav_drawer_icons);

        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1), false, "3"));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1), true, ""));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1), true, ""));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1), true, ""));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1), false, "3"));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1), false, "3"));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1), false, "3"));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1), false, "3"));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[9], navMenuIcons.getResourceId(9, -1), false, "3"));


        //  databaseReference.child("show_premium").addListenerForSingleValueEvent(new ValueEventListener() {
        //     @Override
        //     public void onDataChange(DataSnapshot dataSnapshot) {
        // if((Boolean)dataSnapshot.getValue()){
        //navDrawerItems.add(8, new NavDrawerItem(navMenuTitles[8], navMenuIcons.getResourceId(8, -1), false, "3"));
        // }
        NavDrawerListAdapter navDrawerListAdapter = new NavDrawerListAdapter(MainActivity.this, navDrawerItems);
        navMenuIcons.recycle();
        navDrawerListAdapter.notifyDataSetChanged();
        drawerList.setAdapter(navDrawerListAdapter);
        // }

        //     @Override
        //    public void onCancelled(DatabaseError databaseError) {
        //       Log.e(TAG, databaseError.getMessage());
        //  }
        // });


    }


    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }



    @IntDef({ALL, NOTIFICATIONS, CIRCLES, FRIENDS, SPREAD, FEEDBACK, ARCHIVE, SETTINGS, PREMIUM, LOGOUT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface WhichSelection {
    }
}