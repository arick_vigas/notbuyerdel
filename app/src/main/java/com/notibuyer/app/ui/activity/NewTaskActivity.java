package com.notibuyer.app.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.annimon.stream.Stream;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.notibuyer.app.R;
import com.notibuyer.app.dagger.AppComponent;
import com.notibuyer.app.helper.RemovableCircleViewFactory;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.model.Task;
import com.notibuyer.app.network.firebase.TaskService;
import com.notibuyer.app.ui.view.AudioPlayerView;
import com.notibuyer.app.ui.view.CustomEditText;
import com.notibuyer.app.ui.view.EditableCircleListLayout;
import com.notibuyer.app.ui.view.RemovableCircleView;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.NetworkUtils;
import com.notibuyer.app.utils.TaskDiskCache;
import com.notibuyer.app.utils.Utils;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;

public class NewTaskActivity extends BaseActivity implements AudioPlayerView.AudioPlaybackPress, RemovableCircleView.RemoveClickListener{

    private static final int REQUEST_IMAGE_PICK = 200;
    private static final int REQUEST_IMAGE_CAPTURE = 201;
    private static final int CHOOSE_CIRCLES_CODE = 202;
    private static final int RECORD_CODE = 205;

    private static final String NEW_TASK_TEXT = "task_text";
    private static final String NEW_TASK_PHOTO = "task_id";
    private static final String NEW_TASK_AUDIO = "task_audio";

    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.task_text) protected CustomEditText taskText;
    @Bind(R.id.date) protected TextView taskCreateDate;
    @Bind(R.id.add_to_circle) protected TextView addToCircle;
    @Bind(R.id.player) protected AudioPlayerView audioPlayerView;
    @Bind(R.id.task_image) protected ImageView taskImage;
    @Bind(R.id.picture_container) protected RelativeLayout pictureContainer;
    @Bind(R.id.create_task) protected Button createTask;
    @Bind(R.id.circles_list_layout) protected EditableCircleListLayout circleListLayout;
    @Inject
    protected TaskDiskCache taskDiskCache;
    @Inject
    protected TaskService taskService;
    // variables for audio player
    private MediaPlayer mp;
    private ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
    private Runnable updateAudioTask = () -> runOnUiThread(() -> audioPlayerView.updateProgress(mp.getCurrentPosition()));
    private Future updateFuture;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference databaseReference;
    private StorageReference storageRef;
    private boolean completed = false;
    // like ID for new task
    private String taskTextHash;
    //private String audioKey = String.valueOf(System.currentTimeMillis());
    // id for current task
    private Task task;
    private File tmpFile;
    private List<String> chosenCirclesIds = new ArrayList<String>();

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void startNew(Activity activity, String taskText, int requestCode) {
        Intent intent = new Intent(activity, NewTaskActivity.class);
        intent.putExtra(NEW_TASK_TEXT, taskText);
        activity.startActivityForResult(intent, requestCode);
    }

    public static void startWithAudio(Activity activity, String taskText, String audioKey, int requestCode) {
        Intent intent = new Intent(activity, NewTaskActivity.class);
        intent.putExtra(NEW_TASK_TEXT, taskText);
        intent.putExtra(NEW_TASK_AUDIO, audioKey);
        activity.startActivityForResult(intent, requestCode);
    }

    public static void startWithPhoto(Activity activity, File file, int requestCode) {
        Intent intent = new Intent(activity, NewTaskActivity.class);
        //ByteArrayOutputStream bs = new ByteArrayOutputStream();
        //bitmap.compress(Bitmap.CompressFormat.PNG, 50, bs);

        intent.putExtra(NEW_TASK_PHOTO, file);

        activity.startActivityForResult(intent, requestCode);
    }

   /* private List<Circle> getChosenCircleList(){
        List<Circle> circleList = new ArrayList<Circle>();
        for(String circleId:chosenCirclesIds){
           // Circle circle = realm.where(Circle.class).contains("id", circleId).notEqualTo("name", "Private").findFirst();
         //   if(circle!=null)
             //   circleList.add(circle);
        }
        System.out.println(circleList);
        return circleList;
    }*/

    @Override
    protected int getContentViewId() {
        return R.layout.act_new_task;
    }

    public void setUpComponent(@NonNull AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.new_item);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        storageRef = FirebaseStorage.getInstance().getReferenceFromUrl( "gs://notibuyer-b084a.appspot.com");




        registerForContextMenu(createTask);

        task = new Task();
        task.setFbKey(databaseReference.child("tasks").push().getKey());
        task.setOwnerId(mFirebaseUser.getUid());
        task.setCreated(new Date().getTime());
        task.setCompleted(false);
        task.setArchived(false);

        taskText.setMovementMethod(new ScrollingMovementMethod());
        taskText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                task.setText(taskText.getText().toString());
                hideSoftKeyboard(this);
            }
            return true;
        });
        audioPlayerView.setVisibility(View.GONE);
        if (getIntent().hasExtra(NEW_TASK_PHOTO)) {
            File img = (File)getIntent().getExtras().get(NEW_TASK_PHOTO);
            //task.setId(String.valueOf(System.currentTimeMillis()));
            //Bitmap b = BitmapFactory.decodeByteArray(
             //       getIntent().getByteArrayExtra(NEW_TASK_PHOTO), 0, getIntent().getByteArrayExtra(NEW_TASK_PHOTO).length);

            pictureContainer.setVisibility(View.VISIBLE);
            taskDiskCache.savePicture(task.getFbKey(), img);
            Bitmap rotated = Utils.compressAndRotate(taskDiskCache.getPictureForTask(task.getFbKey()).getAbsolutePath());
            taskImage.setImageBitmap(rotated);
            taskDiskCache.savePicture(task.getFbKey(), Utils.getFile(this, rotated));
            img.delete();
        } else if (getIntent().hasExtra(NEW_TASK_AUDIO)) {
            //taskTextHash = String.valueOf(getIntent().getExtras().getString(NEW_TASK_TEXT).hashCode());
            task.setFbKey(getIntent().getExtras().getString(NEW_TASK_AUDIO));
            task.setText(getIntent().getExtras().getString(NEW_TASK_TEXT));
            taskText.setText(task.getText());
            audioPlayerView.setAudioPlaybackPress(this);
            audioPlayerView.setVisibility(View.VISIBLE);
        } else {
            taskTextHash = String.valueOf(getIntent().getExtras().getString(NEW_TASK_TEXT).hashCode());
            task.setId(taskTextHash);
            task.setText(getIntent().getExtras().getString(NEW_TASK_TEXT));
            taskText.setText(task.getText());
        }

        taskCreateDate.setText(Utils.getFormattedDate(this, new Date(System.currentTimeMillis())));


        if (!TextUtils.isEmpty(prefs.getLastVisitedCircle()) && !chosenCirclesIds.contains(prefs.getLastVisitedCircle())) {
            chosenCirclesIds.add(prefs.getLastVisitedCircle());
        }
    }

    private void setUpCircles() {
        List<Circle> cList = new ArrayList<>();
        for(String item:chosenCirclesIds){
            databaseReference.child("circles").child(item).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Circle circle = new Circle();
                    circle.setFbKey(dataSnapshot.getKey());
                    circle.setName((String)dataSnapshot.child("name").getValue());
                    circle.setColor((String)dataSnapshot.child("color").getValue());
                    if(!circle.getName().equals("Private")) {
                        cList.add(circle);
                        Collections.sort(cList, ((lhs, rhs) -> lhs.getName().compareTo(rhs.getName())));
                        RemovableCircleViewFactory circleViewFactory = new RemovableCircleViewFactory();
                        List<RemovableCircleView> circleViews = circleViewFactory.createCircles(NewTaskActivity.this, cList);
                        Stream.of(circleViews).forEach(c -> c.setRemoveClickListener(NewTaskActivity.this));
                        circleListLayout.setTags(circleViews);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });
        }
    }

    @Override
    protected void onStop() {
        stopAudio();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (!taskDiskCache.getPictureForTask(task.getFbKey()).exists() && !taskDiskCache.getAudioForTask(task.getFbKey()).exists()) {
            inflater.inflate(R.menu.record_and_choose_photo, menu);
        } else if (!taskDiskCache.getPictureForTask(task.getFbKey()).exists()) {
            inflater.inflate(R.menu.choose_photo, menu);
        } else if (!taskDiskCache.getAudioForTask(task.getFbKey()).exists()) {
            inflater.inflate(R.menu.record_audio, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
            case R.id.take_photo:
                openContextMenu(createTask);
                return true;
            case R.id.record:
                createTaskWithRecord();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        menu.setHeaderTitle(R.string.choose_picture_title);
        inflater.inflate(R.menu.choose_picture_task_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(!NetworkUtils.isOn(this)) {
            DialogFactory.getInfoDialog(this, getString(R.string.no_internet_connection));
            return false;
        }
        switch (item.getItemId()) {
            case R.id.take_photo:
                dispatchTakePictureIntent();
                break;
            case R.id.gallery_photo:
                pickPictureFromGallery();
                break;
        }
        return true;
    }

    public void createTaskWithRecord() {
        RxPermissions.getInstance(this)
                .request(Manifest.permission.RECORD_AUDIO)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(granted -> {
                    if (granted) {
                        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                        intent.putExtra("android.speech.extra.GET_AUDIO_FORMAT", "audio/AMR");
                        intent.putExtra("android.speech.extra.GET_AUDIO", true);
                        startActivityForResult(intent, RECORD_CODE);
                    } else {
                        DialogFactory.getInfoDialog(this, R.string.record_permissions_not_granted).show();
                    }
                });
    }

    private void dispatchTakePictureIntent() {
        RxPermissions.getInstance(this).request(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(granted -> {
            if (granted) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    try {
                        tmpFile = Utils.createTemporaryFile(task.getFbKey(), ".png");
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tmpFile));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } else {
                DialogFactory.getInfoDialog(NewTaskActivity.this, R.string.camera_permissions_not_granted).show();
            }
        });
    }

    private void pickPictureFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, getString(R.string.select_picture)),
                REQUEST_IMAGE_PICK);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                Bitmap bitmap = null;
                bitmap = Utils.compressAndRotate(tmpFile.getAbsolutePath());
                taskImage.setImageBitmap(bitmap);
                taskImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                taskDiskCache.savePicture(task.getFbKey(), Utils.getFile(this, bitmap));
                pictureContainer.setVisibility(View.VISIBLE);
                tmpFile.delete();
                tmpFile = null;
            } else if (requestCode == REQUEST_IMAGE_PICK) {
                Uri imageUri = data.getData();
                try {
                    Bitmap bitmap = null;
                    bitmap = Utils.compressAndRotate(Utils.getFile(this, BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri))).getAbsolutePath());
                    taskImage.setImageBitmap(bitmap);
                    taskImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    taskDiskCache.savePicture(task.getFbKey(), Utils.getFile(this, bitmap));
                    pictureContainer.setVisibility(View.VISIBLE);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == CHOOSE_CIRCLES_CODE) {
                chosenCirclesIds = data.getStringArrayListExtra("data");
                setUpCircles();
            } else if (requestCode == RECORD_CODE) {
                if (data != null && data.getExtras() != null) {
                    Bundle bundle = data.getExtras();
                    ArrayList<String> matches = bundle.getStringArrayList(RecognizerIntent.EXTRA_RESULTS);
                    if (matches == null || matches.isEmpty()) return;
                    DialogFactory.getEditTaskDialog(this, matches.get(0), (dialog, which) -> {
                        dialog.dismiss();
                        createTaskWithRecord();
                    }, (text, quick) -> {
                        task.setText(text);
                        taskText.setText(text);
                        Uri audioUri = data.getData();
                        ContentResolver contentResolver = getContentResolver();
                        try {
                            InputStream filestream = contentResolver.openInputStream(audioUri);
                            taskDiskCache.saveAudio(task.getFbKey(), filestream);
                            audioPlayerView.setAudioPlaybackPress(this);
                            audioPlayerView.setVisibility(View.VISIBLE);
                            invalidateOptionsMenu();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }).show();
                }
            }
        }
    }

    @OnClick(R.id.task_image)
    protected void previewImage() {
        ImagePreviewActivity.startWithFile(this, taskDiskCache.getPictureForTask(task.getFbKey()).getPath());
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @OnClick(R.id.remove_picture)
    protected void removePicture() {
        taskImage.setImageBitmap(null);
        pictureContainer.setVisibility(View.GONE);
        taskDiskCache.removeTaskPicture(task.getFbKey());
        invalidateOptionsMenu();
    }

    @OnClick(R.id.add_to_circle)
    protected void addToCircle() {
        System.out.println(chosenCirclesIds.size());
        ChooseCircleActivity.startForNewTask(this, chosenCirclesIds, CHOOSE_CIRCLES_CODE);
    }

    @OnClick(R.id.create_task)
    protected void createTask() {
        showLoader();
        task.setText(taskText.getText().toString());
        if(validation()) {
            for (String circleId : chosenCirclesIds) {
                taskService.createTask(task, circleId, taskDiskCache.getAudioForTask(task.getFbKey()), taskDiskCache.getPictureForTask(task.getFbKey()));
            }
            hideLoader();
            setResult(RESULT_OK);
            finish();
        } else {
            Toast.makeText(this, getResources().getString(R.string.error_empty_task), Toast.LENGTH_SHORT).show();
            hideLoader();
        }
    }

    private boolean validation(){

        return (taskText.getText() != null && !taskText.getText().toString().isEmpty() && !taskText.getText().equals(""))
                || taskDiskCache.getAudioForTask(task.getFbKey()).exists() || taskDiskCache.getPictureForTask(task.getFbKey()).exists();
    }

    @Override
    public void playerStateChanged(boolean isPlaying) {
        if (isPlaying) {
            File file = taskDiskCache.getAudioForTask(task.getFbKey());
            playAudio(file);
        } else {
            stopAudio();
        }
    }

    @Override
    public void onAudioRemove() {
        stopAudio();
        taskDiskCache.removeTaskAudio(task.getFbKey());
        audioPlayerView.setVisibility(View.GONE);
        invalidateOptionsMenu();
    }

    public void playAudio(File file) {
        mp = new MediaPlayer();
        try {
            mp.setDataSource(file.getAbsolutePath());
            mp.prepareAsync();
            mp.setOnCompletionListener(mp1 -> {
                stopAudio();
            });
            mp.setOnPreparedListener(mp1 -> {
                audioPlayerView.setMaxValue(mp.getDuration());
                audioPlayerView.start();
                mp.start();
                updateFuture = service.scheduleWithFixedDelay(updateAudioTask, 0, 1, TimeUnit.SECONDS);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopAudio() {
        if (updateFuture != null) {
            updateFuture.cancel(true);
        }
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
        audioPlayerView.stop();
    }

    @Override
    public void onBackPressed() {


        super.onBackPressed();
    }

    @Override
    public void onRemoveCircle(Circle circle) {
        chosenCirclesIds.remove(circle.getId());
        setUpCircles();
    }


}