package com.notibuyer.app.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.vending.billing.IInAppBillingService;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.SubscribePagerAdapter;
import com.notibuyer.app.model.Subscribe;
import com.notibuyer.app.ui.view.CustomViewPager;
import com.notibuyer.app.utils.IabHelper;
import com.notibuyer.app.utils.IabResult;
import com.notibuyer.app.utils.Inventory;
import com.notibuyer.app.utils.Purchase;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

public class PremiumActivity extends BaseActivity {

    public static final int BILLING_RESPONSE_RESULT_OK = 0;
    static final String ITEM_SKU = "noti_month_test";
    private static final int CLEAN = 0;
    private static final int FULL = 1;
    @Bind(R.id.premium_toolbar) protected Toolbar toolbar;
    @Bind(R.id.premium_pager) protected CustomViewPager pager;
    @Bind(R.id.back) protected ImageView backButton;
    //  @Bind(R.id.basic) protected ImageView basicButton;
    @Bind(R.id.clean) protected ImageView cleanButton;
    @Bind(R.id.full) protected ImageView fullButton;
    //@Bind(R.id.left_line) protected ImageView leftLine;
    @Bind(R.id.center_line) protected ImageView centerLine;
    @Bind(R.id.right_line) protected  ImageView rightLine;
    IabHelper mHelper;
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;
    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // Handle failure
            } else {
                try {
                    mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                            mConsumeFinishedListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    private SubscribePagerAdapter adapter;
    private DatabaseReference databaseReference;
    private DatabaseReference subscribeRef;
    private FirebaseUser mFirebaseUser;
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {
                        //buyButton.setText("bought");
                        databaseReference.child("users").child(mFirebaseUser.getUid()).child("subscribe").setValue(purchase.getSku());
                    } else {
                        // handle error
                    }
                }
            };
    private List<Subscribe> subscribes= new ArrayList<>();
    private IInAppBillingService mService;
    private IabHelper.QueryInventoryFinishedListener mQueryFinishedListener;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_premium;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
           // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        //pager.setSwipeable(false);
        // basicButton.setImageDrawable(getResources().getDrawable(R.drawable.dot_white));
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmSxE9mfS5jL+E8m/ubNKIiStf80XmYyTjcbz75Up2A3376rQSXef21ms+nKFlfUmBfj/kBK07loDMLEuXDxbbMWjrBmZiOEXH1b+DtUcDSlz2T9rBwgefntimLUC98Ly71pdJzi62C3Kz0OMV7w7utcLJAvnphWgzJiuGO5cQdIVTEC6V3Q2UIgGtmbxA9Y60fMSfsQHbgRWCWGlPk0ejJtAsXa9jGiil0RXSxa+Gj00HxxZSnapwh6yUlX6EPToc+xbfsZkTfhNHStPVa5FEcCwTCnETu6G+R4qJynMj9VNhMP86OIZpFycypbrA2FKtpaIC1iEwEyS1zKDTOMCpQIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.startSetup(new
                                   IabHelper.OnIabSetupFinishedListener() {
                                       public void onIabSetupFinished(IabResult result)
                                       {
                                           if (!result.isSuccess()) {
                                               Log.d(TAG, "In-app Billing setup failed: " +
                                                       result);
                                           } else {
                                               Log.d(TAG, "In-app Billing is set up OK");
                                           }
                                       }
                                   });

        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        mPurchaseFinishedListener
                = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result,
                                              Purchase purchase)
            {
                if (result.isFailure()) {
                    // Handle error
                    return;
                }
                else if (purchase.getSku().equals(ITEM_SKU)) {
                        consumeItem();
                    prefs.setCurrentSubscribe(ITEM_SKU);
                       // buyButton.setEnabled(false);
                }

            }
        };

        mHelper.enableDebugLogging(true);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        subscribeRef = databaseReference.child("subscribes");
        centerLine.setVisibility(View.VISIBLE);
        cleanButton.setImageDrawable(getResources().getDrawable(R.drawable.dot_white));
        toolbar.setBackgroundColor(Color.parseColor("#68b8fd"));

    }

    public void subscribe(String sku){
        try {
            mHelper.launchPurchaseFlow(this, sku, 10001,
                    mPurchaseFinishedListener, "mypurchasetoken");
            pager.getAdapter().notifyDataSetChanged();
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
    }

    /*    @OnClick(R.id.basic)
        protected  void selectBasic(){
            if(pager.getCurrentItem()!=BASIC) {
                clearButtonsView();
                pager.setCurrentItem(BASIC);
                leftLine.setVisibility(View.VISIBLE);
                basicButton.setImageDrawable(getResources().getDrawable(R.drawable.dot_white));
            }
        }
    */
    @OnClick(R.id.clean)
    protected  void selectClean(){
        if(pager.getCurrentItem()!=CLEAN) {
            pager.setCurrentItem(CLEAN);
            clearButtonsView();
            centerLine.setVisibility(View.VISIBLE);
            cleanButton.setImageDrawable(getResources().getDrawable(R.drawable.dot_white));
        }
    }

    @OnClick(R.id.full)
    protected  void selectFull(){
        if(pager.getCurrentItem()!=FULL) {
            pager.setCurrentItem(FULL);
            clearButtonsView();
            rightLine.setVisibility(View.VISIBLE);
            fullButton.setImageDrawable(getResources().getDrawable(R.drawable.dot_white));
        }
    }

    private void clearButtonsView(){
        //leftLine.setVisibility(View.INVISIBLE);
        centerLine.setVisibility(View.INVISIBLE);
        rightLine.setVisibility(View.INVISIBLE);
        //basicButton.setImageDrawable(null);
        cleanButton.setImageDrawable(null);
        fullButton.setImageDrawable(null);
    }

    @OnClick(R.id.back)
    public void back(){
        onBackPressed();
    }

    public void consumeItem() {
        try {
            mHelper.queryInventoryAsync(mReceivedInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //leftLine.setVisibility(View.VISIBLE);
        adapter = new SubscribePagerAdapter(getSupportFragmentManager(), toolbar, this);
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                toolbar.setBackgroundColor(Color.parseColor(subscribes.get(position).getColor()));
                if (position == 0) {
                    clearButtonsView();
                    centerLine.setVisibility(View.VISIBLE);
                    cleanButton.setImageDrawable(getResources().getDrawable(R.drawable.dot_white));
                } else if (position == 1) {
                    clearButtonsView();
                    rightLine.setVisibility(View.VISIBLE);
                    fullButton.setImageDrawable(getResources().getDrawable(R.drawable.dot_white));
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        pager.setCurrentItem(0);

        subscribeRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Subscribe subscribe = dataSnapshot.getValue(Subscribe.class);
                subscribe.setId(dataSnapshot.getKey());
                System.out.println(subscribe.getColor() + " activity");
                adapter.addItem(subscribe);
                subscribes.add(subscribe);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                adapter.removeItem(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (mHelper != null) try {
            mHelper.dispose();
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
        mHelper = null;
        MainActivity.start(this, MainActivity.ALL);
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) try {
            mHelper.dispose();
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
        mHelper = null;
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data)
    {
        if (!mHelper.handleActivityResult(requestCode,
                resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
