package com.notibuyer.app.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.loopj.android.http.AsyncHttpClient;
import com.mopub.common.util.Strings;
import com.notibuyer.app.R;

import org.apache.commons.io.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Hashtable;

import butterknife.Bind;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.mime.HttpMultipartMode;
import cz.msebera.android.httpclient.entity.mime.MultipartEntityBuilder;
import cz.msebera.android.httpclient.entity.mime.content.FileBody;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EncodingUtils;

public class SearchActivity extends BaseActivity {

    public static final int REQ_CODE_SEARCH = 98;

    public static final String TEXT = "text";
    public static final String IMAGE = "image";
    public static final String GOOGLE_TEXT_SEARCH = "https://www.google.com/search?q=";
    public static final String GOOGLE_IMAGE_SEARCH = "https://www.google.com/searchbyimage?&image_url=";
    //public static final String GOOGLE_IMAGE_SEARCH = "https://www.google.com/searchbyimage/upload";


    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.web_view) protected WebView webView;

    @Override
    protected int getContentViewId() {
        return R.layout.search_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();
        showLoader();
        Intent intent = getIntent();
        String textQuery = intent.getStringExtra(TEXT);
        String imageQuery = intent.getStringExtra(IMAGE);
        String url="";

        if (TextUtils.isEmpty(imageQuery)) {
            url = GOOGLE_TEXT_SEARCH + textQuery;
        } else {
                url = GOOGLE_IMAGE_SEARCH + imageQuery;
                System.out.println(url);
        }

        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // do your handling codes here, which url is the requested url
                // probably you need to open that url rather than redirect:
                view.loadUrl(url);
                return false; // then it is not handled by default action
            }});

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    hideLoader();
                }
            }
        });

        webView.loadUrl(url);
    }


    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.search));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    public static void searchText(Activity context, String text) {
        Intent intent = new Intent(context, SearchActivity.class);
        intent.putExtra(TEXT, text);
        context.startActivityForResult(intent, REQ_CODE_SEARCH);
    }

    public static void searchImage(Activity context, String image) {
        Intent intent = new Intent(context, SearchActivity.class);
        intent.putExtra(IMAGE, image);
        context.startActivityForResult(intent, REQ_CODE_SEARCH);
    }
}
