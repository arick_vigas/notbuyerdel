package com.notibuyer.app.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;

public class SearchFriendsActivity extends BaseActivity {

    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;

    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.users) protected RecyclerView usersList;
    @Bind(R.id.empty_container) protected TextView emptyText;



    @Override
    protected int getContentViewId() {
        return R.layout.act_search_friends;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.search_friends);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        SearchFriendsAdapter adapter = new SearchFriendsAdapter();
        usersList.setLayoutManager(new LinearLayoutManager(this));
        usersList.setAdapter(adapter);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.child("users").child(mFirebaseUser.getUid()).child("invites").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                FriendItem item = dataSnapshot.getValue(FriendItem.class);
                item.setFbKey(dataSnapshot.getKey());
                databaseReference.child("users").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot userData) {
                        item.setName((String)userData.child("name").getValue());
                        item.setAvatar((String)userData.child("avatar").getValue());
                        adapter.addItem(item);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                adapter.removeItem(dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }


    public class SearchFriendsAdapter extends RecyclerView.Adapter {

        private List<FriendItem> usersList = new ArrayList();

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FriendViewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_suggested, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return usersList.size();
        }

        public void addItem(FriendItem item){
            usersList.add(item);
            Collections.sort(usersList, (lhs, rhs)->lhs.getName().compareTo(rhs.getName()));
            notifyItemInserted(usersList.indexOf(item));
        }

        public void removeItem(String key){
            FriendItem removeFriend = null;
            int pos =-1;
            for(FriendItem friend:usersList){
                if(friend.getFbKey().equals(key)) {
                    removeFriend =friend;
                    pos = usersList.indexOf(friend);
                    break;
                }
            }
            if(removeFriend!=null){
                usersList.remove(removeFriend);
                notifyItemRemoved(pos);
            }
        }



    }

    public class FriendViewholder extends RecyclerView.ViewHolder {

        public FriendViewholder(View itemView) {
            super(itemView);
        }
    }

    public class FriendItem extends User{
        private String type;
        private String inviteCircleId;
        private String provider;

        public FriendItem(String type, String inviteCircleId, String provider){
            this.type = type;
            this.inviteCircleId = inviteCircleId;
            this.provider = provider;
        }

        public FriendItem(){
            super();
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getInviteCircleId() {
            return inviteCircleId;
        }

        public void setInviteCircleId(String inviteCircleId) {
            this.inviteCircleId = inviteCircleId;
        }

        public String getProvider() {
            return provider;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }
    }

}