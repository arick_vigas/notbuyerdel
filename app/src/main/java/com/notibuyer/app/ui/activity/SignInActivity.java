package com.notibuyer.app.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.notibuyer.app.R;
import com.notibuyer.app.interfaces.RestorePasswordListener;
import com.notibuyer.app.network.response.BaseResponse;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.NetworkUtils;
import com.notibuyer.app.utils.Utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.Bind;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class SignInActivity extends BaseAuthActivity {

    @Bind(R.id.email) protected EditText email;
    @Bind(R.id.password) protected EditText password;
    @Bind(R.id.sign_in) protected Button signIn;
    @Bind(R.id.forgot_password) protected TextView forgotPassword;
    @Bind(R.id.create_account) protected TextView createAccount;

    @Override
    protected int getContentViewId() {
        return R.layout.sing_in_activity;
    }

    public static void start(Context context) {
        context.startActivity(new Intent(context, SignInActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        titleToolbar.setText(getString(R.string.sign_in));
       /* try {
            PackageInfo info = getPackageManager().getPackageInfo("com.notibuyer.app", PackageManager.GET_SIGNATURES);
            for(Signature signature:info.signatures){
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                email.setText(Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }*/

    }

    @OnClick(R.id.sign_in)
    public void singIn() {
        String msg = null;
        if (!Utils.isValidEmail(email.getText().toString())) {
            msg = getString(R.string.wrong_email);
        } else if (!Utils.isValidPassword(password.getText().toString())) {
            msg = getString(R.string.wrong_password);
        } else if(!NetworkUtils.isOn(this)) {
            msg = getString(R.string.no_internet_connection);
        }

        if (msg != null) {
            Snackbar.make(mainLayout, msg, Snackbar.LENGTH_SHORT).show();
            return;
        }

        hideKeyboard();
        emailLogin(email.getText().toString(), password.getText().toString());
    }

    @OnClick(R.id.forgot_password)
    public void forgotPassword() {
        DialogFactory.getResporePasswordDialog(this, (email1) -> {
            FirebaseAuth auth = FirebaseAuth.getInstance();


            auth.sendPasswordResetEmail(email1)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "Email sent.");
                            }
                        }
                    });
        }).show();
    }

    @OnClick(R.id.create_account)
    public void createAccount() {
        startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
        finish();
    }

    @OnClick(R.id.facebook)
    public void facebook() {
        facebookAuth();
    }

    @OnClick(R.id.google)
    public void google() {
        googleAuth();
    }
}
