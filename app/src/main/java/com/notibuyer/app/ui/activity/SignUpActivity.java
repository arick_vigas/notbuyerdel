package com.notibuyer.app.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.widget.EditText;

import com.notibuyer.app.R;
import com.notibuyer.app.network.response.UserResponse;
import com.notibuyer.app.utils.Utils;
import com.tbruyelle.rxpermissions.RxPermissions;

import butterknife.Bind;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class SignUpActivity extends BaseAuthActivity {

    @Bind(R.id.name) protected     EditText name;
    @Bind(R.id.email) protected    EditText email;
    @Bind(R.id.password) protected EditText password;

    public static void start(Context context) {
        context.startActivity(new Intent(context, SignUpActivity.class));
    }

    @Override
    protected int getContentViewId() {
        return R.layout.sign_up_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        titleToolbar.setText(getString(R.string.sign_up));


    }

    @OnClick(R.id.create_account)
    public void createAccount() {
        String msg = null;

        if (TextUtils.isEmpty(name.getText().toString())) {
            msg = getString(R.string.empty_name);
        } else if (!Utils.isValidEmail(email.getText().toString())) {
            msg = getString(R.string.wrong_email);
        } else if (!Utils.isValidPassword(password.getText().toString())) {
            msg = getString(R.string.wrong_password);
        }

        if (msg != null) {
            Snackbar.make(mainLayout, msg, Snackbar.LENGTH_SHORT).show();
            return;
        }

        hideKeyboard();
        emailRegister(email.getText().toString(), password.getText().toString(), name.getText().toString());

    /*    RxPermissions.getInstance(this).request(Manifest.permission.READ_PHONE_STATE)
                .flatMap(granted -> {
                    String phone = null;
                    if (granted) {
                        phone = Utils.formatPhoneNumber(SignUpActivity.this, Utils.getPhoneNumber(SignUpActivity.this));
                    }
                    return authService.registerAndLogin(email.getText().toString(), password.getText().toString(), name.getText().toString(), phone)
                            .subscribeOn(Schedulers.io());
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .finallyDo(this::hideLoader)
                .subscribe(userResponse -> {
                    if (userResponse.getCode() != null && 0 == userResponse.getCode()) {
                        persistencePrefs.setLastEmail(email.getText().toString());
                        MainActivity.start(this);
                        finish();
                    } else {
                        showError(getString(R.string.registration_error));
                    }
                }, this::showError);*/
    }

    @OnClick(R.id.sign_in)
    public void singIn() {
        startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
        finish();
    }

    @OnClick(R.id.facebook)
    public void facebook() {
        facebookAuth();
    }

    @OnClick(R.id.google)
    public void google() {
        googleAuth();
    }
}