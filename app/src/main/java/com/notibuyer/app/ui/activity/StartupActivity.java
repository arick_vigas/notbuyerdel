package com.notibuyer.app.ui.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Toast;

import com.appodeal.ads.Appodeal;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.notibuyer.app.R;
import com.notibuyer.app.ui.fragments.startup.StartUpFragment1;
import com.notibuyer.app.ui.fragments.startup.StartUpFragment2;
import com.notibuyer.app.ui.fragments.startup.StartUpFragment3;
import com.notibuyer.app.ui.fragments.startup.StartUpFragment4;
import com.notibuyer.app.utils.IabException;
import com.notibuyer.app.utils.IabHelper;
import com.notibuyer.app.utils.IabResult;
import com.notibuyer.app.utils.Inventory;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

public class StartupActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static boolean persistence_enabled = false;
    @Bind(R.id.pager) protected ViewPager pager;
    IabHelper mHelper;
    private IabHelper.QueryInventoryFinishedListener mQueryFinishedListener;

    @Override
    protected int getContentViewId() {
        return R.layout.start_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefs.setOnLaunch(true);

        AppEventsLogger.activateApp(this);
        String appKey = "778474ddeaccbcc4a493c5b42b7733e9c7f99c48ce0b45b2";
        Appodeal.setBannerViewId(R.id.appodealBannerView);
        Appodeal.disableLocationPermissionCheck();
        Appodeal.initialize(this, appKey, Appodeal.BANNER_BOTTOM);
        Appodeal.setLogging(true);
        //Appodeal.setTesting(true);
        Uri data = this.getIntent().getData();
        if (data != null && data.isHierarchical()) {
            String uri = this.getIntent().getDataString();
            Log.i("MyApp", "Deep link clicked " + uri);
        }

        pager.setAdapter(new StartupPagerAdapter(getSupportFragmentManager()));
        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        if(!persistence_enabled) {
            try {
                FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                persistence_enabled = true;
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }

        }
        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser mFirebaseUser = mFirebaseAuth.getCurrentUser();

        String base64EncodedPublicKey = getResources().getString(R.string.billing_key);
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.startSetup(new
                                   IabHelper.OnIabSetupFinishedListener() {
                                       public void onIabSetupFinished(IabResult result) {
                                           if (!result.isSuccess()) {
                                               Log.d(TAG, "In-app Billing setup failed: " +
                                                       result);
                                           } else {
                                               Log.d(TAG, "In-app Billing is set up OK");
                                               try {
                                                   Inventory inventory = mHelper.queryInventory();
                                                   if (inventory.hasPurchase("noti_month_test"))
                                                       prefs.setCurrentSubscribe("noti_month_test");
                                                   else
                                                       prefs.setCurrentSubscribe(null);
                                               } catch (IabException e) {
                                                   e.printStackTrace();
                                               }
                                           }
                                       }
                                   });


        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();


        if (mFirebaseUser != null){
            MainActivity.start(this);
            finish();
            return;
        }

        if (prefs.isFirstTimeLaunch()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.voice_recognize_dialog_title))
                    .setMessage(getResources().getString(R.string.voice_recognize_message))
                    .setPositiveButton("OK", null);
            builder.create().show();

        }
        prefs.setFirstTimeLaunch(false);

    }

    @OnClick(R.id.sign_in)
    public void singIn() {
        startActivity(new Intent(StartupActivity.this, SignInActivity.class));
        finish();
    }

    @OnClick(R.id.create_account)
    public void createAccount() {
        startActivity(new Intent(StartupActivity.this, SignUpActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mHelper != null) {
            try {
                try {
                    mHelper.dispose();
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            } finally {
            }
        }
        mHelper = null;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    private static class StartupPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;

        public StartupPagerAdapter(FragmentManager fm) {
            super(fm);
            if (fragments == null) fragments = new ArrayList<>();
            fragments.add(new StartUpFragment1());
            fragments.add(new StartUpFragment2());
            fragments.add(new StartUpFragment3());
            fragments.add(new StartUpFragment4());
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }
    }
}