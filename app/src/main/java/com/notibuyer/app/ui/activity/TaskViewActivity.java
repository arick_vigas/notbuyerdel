package com.notibuyer.app.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.CommentsAdapter;
import com.notibuyer.app.dagger.AppComponent;
import com.notibuyer.app.helper.TagCircleFactory;
import com.notibuyer.app.interfaces.DrawableClickListener;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.model.Comment;
import com.notibuyer.app.model.Notification;
import com.notibuyer.app.model.Task;
import com.notibuyer.app.network.firebase.CommentService;
import com.notibuyer.app.network.firebase.TaskService;
import com.notibuyer.app.ui.view.AudioPlayerView;
import com.notibuyer.app.ui.view.CustomEditText;
import com.notibuyer.app.ui.view.EditableCircleListLayout;
import com.notibuyer.app.ui.view.TagCircleView;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.NetworkUtils;
import com.notibuyer.app.utils.NotificationFactory;
import com.notibuyer.app.utils.TaskDiskCache;
import com.notibuyer.app.utils.Utils;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;

public class TaskViewActivity extends BaseActivity implements AudioPlayerView.AudioPlaybackPress {

    public static final String TASK_ID = "task_id";
    private static final int REQUEST_IMAGE_PICK = 220;
    private static final int REQUEST_IMAGE_CAPTURE = 221;
    private static final int CHOOSE_CIRCLES_CODE = 222;
    private static final int RECORD_CODE = 223;
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.task_text) protected CustomEditText taskText;
    @Bind(R.id.name) protected TextView name;
    @Bind(R.id.date) protected TextView taskCreateDate;
    @Bind(R.id.add_to_circle) protected TextView addToCircle;
    @Bind(R.id.player) protected AudioPlayerView audioPlayerView;
    @Bind(R.id.task_image) protected ImageView taskImage;
    @Bind(R.id.picture_container) protected RelativeLayout pictureContainer;
    @Bind(R.id.remove_picture) protected ImageView removePicture;
    @Bind(R.id.circles_list_layout) protected EditableCircleListLayout circleListLayout;
    @Bind(R.id.comments_view) protected RecyclerView commentsView;
    @Bind(R.id.comment) protected CustomEditText addComment;
    @Bind(R.id.comments_title) protected TextView commentsTitle;
    @Bind(R.id.no_comments) protected TextView noCommentsMessage;
    @Bind(R.id.play) protected ImageView playButton;
    @Bind(R.id.resolver) protected LinearLayout resolverContainer;
    @Bind(R.id.resolver_name) protected TextView resolverName;
    @Bind(R.id.resolver_by) protected AppCompatTextView resolverBy;
    // @Bind(R.id.ic)
   // ImageView sendCommentIc;
    @Inject
    protected TaskDiskCache taskDiskCache;
    @Inject
    protected TaskService taskService;
    @Inject
    protected CommentService commentService;
    @Bind(R.id.add_text)
    ImageView addTextButton;
    @Bind(R.id.add_photo)
    ImageView addPhotoButton;
    @Bind(R.id.add_record)
    ImageView addAudioButton;
    @Bind(R.id.task_scroll)
    ScrollView taskScroll;
    private StorageReference storageReference;
    // variables for audio player
    private MediaPlayer mp;
    private ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
    private Runnable updateAudioTask = () -> runOnUiThread(() -> {
        if (mp != null)
            audioPlayerView.updateProgress(mp.getCurrentPosition());
    });
    private Future updateFuture;

    private Task task;
    private List<String> chosenCirclesIds = new ArrayList<>();
    private File tmpFile;

    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private ChildEventListener listener;
    private DatabaseReference taskRef;
    private CommentsAdapter commentsAdapter;
    private DatabaseReference commentsRef;
    private ChildEventListener commentsListener;
    private boolean sendCommentClicked = false;
    private Notification notification;
    private String fileName;
    private boolean fromPush = false;

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void start(Context context, String taskId) {
        Intent intent = new Intent(context, TaskViewActivity.class);
        intent.putExtra(TASK_ID, taskId);
        context.startActivity(intent);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.act_task_view;
    }

    public void setUpComponent(@NonNull AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        bundle.putString("fileName", fileName);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        taskText.setOnTouchListener((v, event) -> {

            v.getParent().requestDisallowInterceptTouchEvent(true);
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_UP:
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
            }
            return false;
        });
        if (savedInstanceState != null)
            fileName = savedInstanceState.getString("fileName");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (getIntent().getExtras() != null) {
            fromPush = getIntent().getExtras().getBoolean("from_push");
        }


        registerForContextMenu(addToCircle);
        if (!getIntent().hasExtra(TASK_ID)) return;

        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        storageReference = FirebaseStorage.getInstance().getReferenceFromUrl( "gs://notibuyer-b084a.appspot.com");
        taskText.setMovementMethod(new ScrollingMovementMethod());

        task = new Task();
        task.setFbKey(getIntent().getExtras().getString(TASK_ID));
        taskRef = databaseReference.child("tasks").child(task.getFbKey());
        System.out.println(taskDiskCache.getPictureForTask(task.getFbKey()).exists());
        audioPlayerView.setVisibility(View.GONE);


        commentsRef = databaseReference.child("tasks").child(task.getFbKey()).child("comments");


    }

    @Override
    protected void onStop() {
        stopAudio();
        taskRef.removeEventListener(listener);
        commentsRef.removeEventListener(commentsListener);

        super.onStop();
    }

    @Override

    protected void onStart() {
        super.onStart();

        if (!NetworkUtils.isOn(TaskViewActivity.this)) {
            if (taskDiskCache.getPictureForTask(task.getFbKey()).exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(taskDiskCache.getPictureForTask(task.getFbKey()).getAbsolutePath());
                taskImage.setImageBitmap(bitmap);
                pictureContainer.setVisibility(View.VISIBLE);
            } else if (taskDiskCache.getAudioForTask(task.getFbKey()).exists()) {
                audioPlayerView.setAudioPlaybackPress(this);
                audioPlayerView.setVisibility(View.VISIBLE);
            }

        }
        System.out.println("cached " + taskDiskCache.getPictureForTask(task.getFbKey()).exists());

        commentsView.setLayoutManager(new LinearLayoutManager(this));
        commentsAdapter = new CommentsAdapter(task.getFbKey(), this);
        commentsView.setAdapter(commentsAdapter);
        addComment.setDrawableClickListener(new DrawableClickListener() {
            @Override
            public void onClick(DrawablePosition target) {
                switch (target) {
                    case RIGHT:

                        if(!sendCommentClicked ) {
                            addComment.requestFocus();
                            showKeyboard(addComment);
                            sendCommentClicked = true;

                        } else {
                           /* System.out.println(addComment.getText());
                            Comment comment = new Comment();
                            comment.setText(addComment.getText().toString());
                            comment.setCreated(new Date().getTime());
                            comment.setAuthorId(mFirebaseUser.getUid());
                            comment.setTaskId(task.getFbKey());
                            commentService.addComment(comment);
                            addComment.setText("");
                            hideSoftKeyboard(TaskViewActivity.this);
                            sendCommentClicked = false;*/
                            addComment.onEditorAction(EditorInfo.IME_ACTION_DONE);
                            sendCommentClicked = false;
                        }
                        break;
                    default:
                        break;
                }
            }
        });
        /*sendCommentIc.setOnClickListener(v1 -> {
            if(!sendCommentClicked ) {
                addComment.requestFocus();
                showKeyboard(addComment);
                sendCommentClicked = true;
            } else {

                Comment comment = new Comment();
                comment.setText(addComment.getText().toString());
                comment.setCreated(new Date().getTime());
                comment.setAuthorId(mFirebaseUser.getUid());
                comment.setTaskId(task.getFbKey());
                commentService.addComment(comment);
                addComment.setText("");
                hideSoftKeyboard(this);
                sendCommentClicked = false;

            }
        });*/
        addComment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                //sendCommentIc.callOnClick();
                sendCommentClicked = hasFocus;
            }
        });
        addComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                sendCommentClicked = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
                sendCommentClicked = true;
            }
        });
        addComment.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if(v.getText().toString().equals("")) {
                    Toast.makeText(this, "Comment text cannot be empty", Toast.LENGTH_SHORT).show();
                    hideSoftKeyboard(this);
                    sendCommentClicked = false;
                } else {



                    Comment comment = new Comment();
                    comment.setText(v.getText().toString());
                    comment.setAuthorId(mFirebaseUser.getUid());
                    comment.setTaskId(task.getFbKey());
                    comment.setCreated(new Date().getTime());

                    commentService.addComment(comment);
                    hideSoftKeyboard(TaskViewActivity.this);
                    sendCommentClicked = false;
                    v.setText("");

                    taskScroll.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            taskScroll.fullScroll(View.FOCUS_DOWN);
                        }
                    }, 1000);
                }
            }
            return true;
        });

        listener = taskRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "text":
                        taskText.setText((String)dataSnapshot.getValue());
                        task.setText((String)dataSnapshot.getValue());
                        addTextButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_text_grey));
                        addTextButton.setClickable(false);
                        addTextButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_grey));
                        break;
                    case "ownerId":
                        task.setOwnerId((String)dataSnapshot.getValue());
                        if(!task.getOwnerId().equals(mFirebaseUser.getUid())){
                            taskText.setFocusable(false);
                            taskText.setFocusableInTouchMode(false);
                            taskText.setClickable(false);
                            addToCircle.setBackgroundResource(R.drawable.add_to_circle_btn_inactive);
                            addToCircle.setTextColor(Color.LTGRAY);

                            addTextButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_text_grey));
                            addTextButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_grey));
                            addTextButton.setClickable(false);

                            addPhotoButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_photo_grey));
                            addPhotoButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_grey));
                            addPhotoButton.setClickable(false);

                            addAudioButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_record_grey));
                            addAudioButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_grey));
                            addAudioButton.setClickable(false);


                            databaseReference.child("users").child(task.getOwnerId()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    name.setText((String) dataSnapshot.getValue());
                                    name.setOnClickListener(v -> UserProfileActivity.start(TaskViewActivity.this, task.getOwnerId(), ""));
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e(TAG, databaseError.getMessage());
                                }
                            });


                        } else {
                            name.setText("You");
                            taskText.setOnEditorActionListener((v, actionId, event) -> {
                                if (actionId == EditorInfo.IME_ACTION_DONE) {
                                    if(!task.isCompleted()) {
                                        if (v.getText().toString().equals(""))
                                            databaseReference.child("tasks").child(task.getFbKey()).child("text").removeValue();
                                        else {
                                            databaseReference.child("tasks").child(task.getFbKey()).child("text").setValue(v.getText().toString());
                                            if (!task.getOwnerId().equals(mFirebaseUser.getUid())) {
                                                taskService.notifyUsers(task.getFbKey(), task.getCircleId(), Notification.ACTION.EDIT.getNumVal());
                                            }
                                        }
                                    }
                                    hideSoftKeyboard(TaskViewActivity.this);
                                }
                                return true;
                            });
                        }
                        break;
                    case "created":
                        task.setCreated((Long)dataSnapshot.getValue());
                        taskCreateDate.setText(Utils.getFormattedDate(TaskViewActivity.this, new Date(task.getCreated())));
                        break;
                    case "imageUrl":
                        System.out.println("test test test");
                        showLoader();
                        task.setImageUrl((String)dataSnapshot.getValue());
                        pictureContainer.setVisibility(View.VISIBLE);

                        if (!NetworkUtils.isOn(TaskViewActivity.this)) {
                            new Picasso.Builder(TaskViewActivity.this)
                                    .downloader(new OkHttpDownloader(TaskViewActivity.this)).build()
                                    .with(TaskViewActivity.this)
                                    .load(task.getImageUrl()).networkPolicy(NetworkPolicy.OFFLINE)
                                    .placeholder(R.drawable.progress_loading_animation).fit().centerCrop()
                                    .centerCrop().into(taskImage);
                        } else {
                            Picasso.with(TaskViewActivity.this)
                                    .load(task.getImageUrl())
                                    .placeholder(R.drawable.progress_loading_animation)
                                    .fit().centerCrop()
                                    .into(taskImage);
                            Log.d("Picasso", "Loaded from Server");
                        }


                        addPhotoButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_photo_grey));
                        addPhotoButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_grey));
                        addPhotoButton.setClickable(false);
                        hideLoader();
                        break;
                    case "audioUrl":
                        task.setAudioUrl((String)dataSnapshot.getValue());
                        audioPlayerView.setAudioPlaybackPress(TaskViewActivity.this);
                        audioPlayerView.setVisibility(View.VISIBLE);
                        addAudioButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_record_grey));
                        addAudioButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this,  R.drawable.button_stroke_grey));
                        addAudioButton.setClickable(false);
                        break;
                    case "completed":
                        task.setCompleted((boolean)dataSnapshot.getValue());
                        if(task.isCompleted()) {
                            getSupportActionBar().setTitle(R.string.completed_task);
                            taskText.setPaintFlags(taskText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            taskText.setTextColor(Color.LTGRAY);
                            taskText.setClickable(false);
                            taskText.setFocusable(false);
                            addToCircle.setBackgroundResource(R.drawable.add_to_circle_btn_inactive);
                            addToCircle.setTextColor(Color.LTGRAY);
                            addToCircle.setClickable(false);
                            addTextButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_photo_grey));
                            addTextButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_grey));
                            addTextButton.setClickable(false);

                        }  else {
                            getSupportActionBar().setTitle(R.string.task);
                            taskText.setPaintFlags(taskText.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                            addToCircle.setOnClickListener(v -> ChooseCircleActivity.startForTask(TaskViewActivity.this, task.getFbKey(), CHOOSE_CIRCLES_CODE));
                            taskText.setClickable(true);
                            taskText.setFocusable(true);
                        }
                        invalidateOptionsMenu();
                        break;
                    case "circleId":
                        task.setCircleId((String)dataSnapshot.getValue());

                        databaseReference.child("circles").child(task.getCircleId()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                List<Circle> cList= new ArrayList<Circle>();
                                Circle circle = new Circle();
                                circle.setFbKey(dataSnapshot.getKey());
                                circle.setName((String) dataSnapshot.child("name").getValue());
                                circle.setColor((String) dataSnapshot.child("color").getValue());
                                if (!circle.getName().equals("Private")) {
                                    cList.add(circle);
                                }
                                TagCircleFactory circleViewFactory = new TagCircleFactory();
                                List<TagCircleView> circleViews = circleViewFactory.createCircles(TaskViewActivity.this, cList);
                                if(circleListLayout!=null)
                                    circleListLayout.setTags(circleViews);

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e(TAG, databaseError.getMessage());
                            }
                        });
                        break;
                    case "completedBy":
                        resolverContainer.setVisibility(View.VISIBLE);
                        if(dataSnapshot.getValue().equals(mFirebaseUser.getUid())){
                            resolverName.setText("You");
                        } else {
                            databaseReference.child("users").child(mFirebaseUser.getUid()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    resolverName.setText((String)dataSnapshot.getValue());
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e(TAG,databaseError.getMessage());
                                }
                            });
                        }
                        break;

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()) {
                    case "text":
                        taskText.setText((String) dataSnapshot.getValue());
                        task.setText((String) dataSnapshot.getValue());
                        break;
                    case "completed":
                        task.setCompleted((boolean)dataSnapshot.getValue());
                        if(task.isCompleted()) {
                            getSupportActionBar().setTitle(R.string.completed_task);
                            taskText.setPaintFlags(taskText.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            taskText.setTextColor(Color.LTGRAY);
                            taskText.setClickable(false);
                            taskText.setFocusable(false);
                            addToCircle.setBackgroundResource(R.drawable.add_to_circle_btn_inactive);
                            addToCircle.setTextColor(Color.LTGRAY);
                            addToCircle.setClickable(false);

                            addTextButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_text_grey));
                            addTextButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_grey));
                            addTextButton.setClickable(false);

                            addAudioButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_record_grey));
                            addAudioButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_grey));
                            addAudioButton.setClickable(false);

                            addPhotoButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_photo_grey));
                            addPhotoButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_grey));
                            addPhotoButton.setClickable(false);

                        }  else {
                            taskText.setClickable(true);
                            taskText.setFocusable(true);
                            getSupportActionBar().setTitle(R.string.task);
                            taskText.setPaintFlags(taskText.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                            taskText.setTextColor(ContextCompat.getColor(TaskViewActivity.this, R.color.task_title));
                            addToCircle.setBackgroundResource(R.drawable.add_to_circle_btn);
                            addToCircle.setTextColor(ContextCompat.getColor(TaskViewActivity.this, R.color.blue));
                            addToCircle.setClickable(true);

                            databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())) {
                                        taskText.setFocusable(true);
                                        taskText.setFocusableInTouchMode(true);
                                        taskText.setClickable(true);

                                        addTextButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_text_blue));
                                        addTextButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_blue));
                                        addTextButton.setClickable(true);

                                        addAudioButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_record_blue));
                                        addAudioButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_blue));
                                        addAudioButton.setClickable(true);

                                        addPhotoButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_photo_blue));
                                        addPhotoButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_blue));
                                        addPhotoButton.setClickable(true);

                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e(TAG, databaseError.getMessage());
                                }
                            });
                        }
                        invalidateOptionsMenu();
                        break;
                    case "imageUrl":
                        showLoader();
                        task.setImageUrl((String)dataSnapshot.getValue());
                        pictureContainer.setVisibility(View.VISIBLE);
                        if (NetworkUtils.isOn(TaskViewActivity.this)) {
                            new Picasso.Builder(TaskViewActivity.this)
                                    .downloader(new OkHttpDownloader(TaskViewActivity.this)).build()
                                    .with(TaskViewActivity.this)
                                    .load(task.getImageUrl()).networkPolicy(NetworkPolicy.OFFLINE)
                                    .placeholder(R.drawable.progress_loading_animation).fit().centerCrop()
                                    .centerCrop().into(taskImage);
                        } else {
                            Picasso.with(TaskViewActivity.this)
                                    .load(task.getImageUrl())
                                    .placeholder(R.drawable.progress_loading_animation)
                                    .fit().centerCrop()
                                    .into(taskImage);
                            Log.d("Picasso", "Loaded from Server");
                        }

                        addPhotoButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_photo_grey));
                        addPhotoButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_grey));
                        addPhotoButton.setClickable(false);
                        hideLoader();
                        break;
                    case "audioUrl":
                        task.setAudioUrl((String)dataSnapshot.getValue());
                        audioPlayerView.setAudioPlaybackPress(TaskViewActivity.this);
                        audioPlayerView.setVisibility(View.VISIBLE);
                        addAudioButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_record_grey));
                        addAudioButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this,  R.drawable.button_stroke_grey));
                        addAudioButton.setClickable(false);
                        break;
                    case "completedBy":
                        resolverContainer.setVisibility(View.VISIBLE);
                        if(dataSnapshot.getValue().equals(mFirebaseUser.getUid())){
                            resolverName.setText("You");
                        } else {
                            databaseReference.child("users").child(mFirebaseUser.getUid()).child("name").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    resolverName.setText((String)dataSnapshot.getValue());
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e(TAG,databaseError.getMessage());
                                }
                            });
                        }
                        break;
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                switch (dataSnapshot.getKey()) {
                    case "audioUrl":
                        audioPlayerView.setVisibility(View.GONE);
                        task.setAudioUrl(null);
                        //todo place in variable isCircleAdmin!!!
                        databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())){
                                    if(!task.isCompleted()){
                                        addAudioButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_record_blue));
                                        addAudioButton.setClickable(true);
                                        addAudioButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_blue));
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e(TAG, databaseError.getMessage());
                            }
                        });
                        break;
                    case "imageUrl":
                        showLoader();
                        taskImage.setImageBitmap(null);
                        pictureContainer.setVisibility(View.GONE);
                        task.setImageUrl(null);
                        //todo place in variable isCircleAdmin!!!
                        databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())){
                                    if(!task.isCompleted()){
                                        addPhotoButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_photo_blue));
                                        addPhotoButton.setClickable(true);
                                        addPhotoButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_blue));
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e(TAG, databaseError.getMessage());
                            }
                        });
                        hideLoader();
                        break;
                    case "text":
                        //todo place in variable isCircleAdmin!!!
                        databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if(dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())){
                                    if(!task.isCompleted()){
                                        addTextButton.setImageDrawable(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.add_text_blue));
                                        addTextButton.setClickable(true);
                                        addTextButton.setBackground(ContextCompat.getDrawable(TaskViewActivity.this, R.drawable.button_stroke_blue));
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e(TAG, databaseError.getMessage());
                            }
                        });
                        break;
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
        commentsListener = commentsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                noCommentsMessage.setVisibility(View.GONE);
                commentsAdapter.addItem(dataSnapshot.getKey());

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                commentsAdapter.removeItem(dataSnapshot.getKey());
                if(commentsAdapter.getItemCount()==0)
                    noCommentsMessage.setVisibility(View.VISIBLE);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (task.isCompleted())
            inflater.inflate(R.menu.restore_task, menu);
        else
            inflater.inflate(R.menu.complete_task, menu);

        inflater.inflate(R.menu.remove_task, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.complete:
                if (!NetworkUtils.isOn(TaskViewActivity.this) && !task.getOwnerId().equals(mFirebaseUser.getUid())) {
                    showError(getResources().getString(R.string.task_edit_offline));
                } else {
                    taskService.completeTask(task);
                }
                return true;
            case R.id.restore:
                databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if((dataSnapshot.getValue()).equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())){
                            if (!NetworkUtils.isOn(TaskViewActivity.this) && !task.getOwnerId().equals(mFirebaseUser.getUid())) {
                                showError(getResources().getString(R.string.task_edit_offline));
                            } else {
                                taskService.resolveTask(task);
                            }
                        } else {
                            showError(getString(R.string.no_permissions_for_action));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });

                return true;
            case R.id.remove:
                databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if((dataSnapshot.getValue()).equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())){
                            if (!NetworkUtils.isOn(TaskViewActivity.this) && !task.getOwnerId().equals(mFirebaseUser.getUid())) {
                                showError(getResources().getString(R.string.task_edit_offline));
                            } else {
                                taskService.removeTask(task);
                            }
                        } else {
                            showError(getString(R.string.no_permissions_for_action));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
                finish();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:return false;
        }

    }

    @Override
    public void onBackPressed() {
        if (fromPush)
            MainActivity.start(this, MainActivity.ALL);
        else
            super.onBackPressed();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        menu.setHeaderTitle(R.string.choose_picture_title);
        inflater.inflate(R.menu.choose_picture_task_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(!NetworkUtils.isOn(this)) {
            DialogFactory.getInfoDialog(this, getString(R.string.no_internet_connection));
            return false;
        }
        switch (item.getItemId()) {
            case R.id.take_photo:
                dispatchTakePictureIntent();
                break;
            case R.id.gallery_photo:
                pickPictureFromGallery();
                break;
        }
        return true;
    }

    private void dispatchTakePictureIntent() {
        RxPermissions.getInstance(this).request(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(granted -> {
            if (granted) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    try {
                        if (tmpFile!=null) {
                            tmpFile.delete();
                            tmpFile=null;
                        }
                        tmpFile = Utils.createTemporaryFile(String.valueOf(new java.util.Date().toString().hashCode()), ".png");
                        fileName = tmpFile.getAbsolutePath();
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tmpFile));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } else {
                DialogFactory.getInfoDialog(TaskViewActivity.this, R.string.camera_permissions_not_granted).show();
            }
        });
    }

    private void pickPictureFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, getString(R.string.select_picture)),
                REQUEST_IMAGE_PICK);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CHOOSE_CIRCLES_CODE) {
                //chosenCirclesIds = data.getStringArrayListExtra("data");
                //System.out.println(chosenCirclesIds);
               // if (task.getCircles().isEmpty() && chosenCirclesIds.isEmpty()) return;
            } else if (requestCode == REQUEST_IMAGE_CAPTURE) {
                if(tmpFile == null && data!=null){
                    tmpFile = new File(data.getData().getPath());
                    if(!tmpFile.exists())
                        return;
                }
                Bitmap bitmap = null;
                System.out.println("TMP"+tmpFile.exists());
                bitmap = Utils.compressAndRotate(tmpFile.getAbsolutePath());
                taskDiskCache.savePicture(task.getFbKey(), Utils.getFile(this, bitmap));
                if(bitmap!=null){
                    bitmap.recycle();
                    bitmap=null;
                }
                uploadPicture();
            } else if (requestCode == REQUEST_IMAGE_PICK) {
                Uri imageUri = data.getData();
                try {
                    taskDiskCache.savePicture(task.getFbKey(), Utils.getFile(this, Utils.compressAndRotate(Utils.getFile(this, BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri))).getAbsolutePath())));
                    uploadPicture();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == RECORD_CODE) {
                if (data != null && data.getExtras() != null) {
                    Bundle bundle = data.getExtras();
                    ArrayList<String> matches = bundle.getStringArrayList(RecognizerIntent.EXTRA_RESULTS);
                    if (matches == null || matches.isEmpty()) return;
                    DialogFactory.getEditTaskDialog(this, matches.get(0), (dialog, which) -> {
                        dialog.dismiss();
                        createTaskWithRecord();
                    }, (text, quick) -> {
                        Uri audioUri = data.getData();
                        ContentResolver contentResolver = getContentResolver();
                        try {
                            InputStream filestream = contentResolver.openInputStream(audioUri);
                            taskDiskCache.saveAudio(task.getFbKey(), filestream);
                            addAudio(text);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }).show();
                }
            }
        }
    }

    public void createTaskWithRecord() {
        RxPermissions.getInstance(this)
                .request(Manifest.permission.RECORD_AUDIO)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(granted -> {
                    if (granted) {
                        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                        intent.putExtra("android.speech.extra.GET_AUDIO_FORMAT", "audio/AMR");
                        intent.putExtra("android.speech.extra.GET_AUDIO", true);
                        startActivityForResult(intent, RECORD_CODE);
                    } else {
                        DialogFactory.getInfoDialog(this, R.string.record_permissions_not_granted).show();
                    }
                });
    }

    @OnClick(R.id.task_image)
    protected void previewImage() {
        if (NetworkUtils.isOn(this)) {
            ImagePreviewActivity.start(this, task.getImageUrl());
        } else {
            ImagePreviewActivity.startWithFile(this, taskDiskCache.getPictureForTask(task.getFbKey()).getAbsolutePath());
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    @OnClick(R.id.add_text)
    protected void addText(){
        databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //if circle admin or task owner
                if(dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())){
                    if(!task.isCompleted()){
                        if (!NetworkUtils.isOn(TaskViewActivity.this) && !task.getOwnerId().equals(mFirebaseUser.getUid())) {
                            showError(getResources().getString(R.string.task_edit_offline));
                        } else {
                            taskText.requestFocus();
                            showKeyboard(taskText);
                        }
                    }
                } else {
                    showError(getString(R.string.no_permissions_for_action));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @OnClick(R.id.remove_picture)
    protected void removePicture() {
        showLoader();
        databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //if circle admin or task owner
                if(dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())){
                    if(!task.isCompleted()){
                        if (!NetworkUtils.isOn(TaskViewActivity.this) && !task.getOwnerId().equals(mFirebaseUser.getUid())) {
                            showError(getResources().getString(R.string.task_edit_offline));
                            hideLoader();
                        } else {
                            taskService.removePicture(task);
                            taskDiskCache.removeTaskPicture(task.getFbKey());
                            pictureContainer.setVisibility(View.GONE);
                            hideLoader();
                        }
                    }
                    hideLoader();
                } else {
                    hideLoader();
                    showError(getString(R.string.no_permissions_for_action));
                }
                hideLoader();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
                hideLoader();
            }
        });
    }

    @OnClick(R.id.add_photo)
    protected void addPhoto() {
        databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //if circle admin or task owner
                if(dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())){
                    if(!task.isCompleted()){
                        if (!NetworkUtils.isOn(TaskViewActivity.this) && !task.getOwnerId().equals(mFirebaseUser.getUid())) {
                            showError(getResources().getString(R.string.task_edit_offline));
                        } else {
                            openContextMenu(addToCircle);
                        }
                    }
                } else {
                    showError(getString(R.string.no_permissions_for_action));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @OnClick(R.id.add_record)
    protected void addRecord() {
        databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //if circle admin or task owner
                if(dataSnapshot.getValue().equals(mFirebaseUser.getUid()) || task.getOwnerId().equals(mFirebaseUser.getUid())){
                    if(!task.isCompleted()){
                        if (!NetworkUtils.isOn(TaskViewActivity.this) && !task.getOwnerId().equals(mFirebaseUser.getUid())) {
                            showError(getResources().getString(R.string.task_edit_offline));
                        } else {
                            createTaskWithRecord();
                        }
                    }
                } else {
                    showError(getString(R.string.no_permissions_for_action));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
                hideLoader();
            }
        });
    }

//just copy??
    @OnClick(R.id.add_to_circle)
    protected  void setAddToCircle(){
        if (!task.isCompleted()) {
            ChooseCircleActivity.startForTask(this, task.getFbKey(), CHOOSE_CIRCLES_CODE);
        }
    }

    private void uploadPicture() {
        //upload picture
        if(taskDiskCache.getPictureForTask(task.getFbKey())!=null){
            showLoader();
            Uri file = Uri.fromFile(taskDiskCache.getPictureForTask(task.getFbKey()));
            String path = "images/"+ taskRef.getKey() + "/" +file.getLastPathSegment();
            StorageReference updateImage = storageReference.child(path);
            UploadTask uploadTask = updateImage.putFile(file);
            // Register observers to listen for when the upload is done or if it fails
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Log.e(TAG, exception.getMessage());
                    hideLoader();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and upload URL.
                    Uri uploadUrl = taskSnapshot.getDownloadUrl();
                    taskRef.child("imageUrl").setValue(uploadUrl.toString());
                    taskRef.child("updated").setValue(new java.util.Date().getTime());

                    if(!task.getOwnerId().equals(mFirebaseUser.getUid())) {
                        Notification notification = NotificationFactory.createTaskNotification(task.getFbKey(), Notification.ACTION.EDIT.getNumVal());
                        notification.setRead(false);
                        DatabaseReference notiRef = databaseReference.child("notifications").push();
                        notiRef.setValue(notification);
                        databaseReference.child("users").child(task.getOwnerId()).child("notifications").child(notiRef.getKey()).setValue(true);
                    }
                    taskService.notifyUsers(task.getFbKey(), task.getCircleId(), Notification.ACTION.EDIT.getNumVal());
                    hideLoader();
                }
            });
            if (!NetworkUtils.isOn(this)) {
                Bitmap bitmap = Utils.compressAndRotate(taskDiskCache.getPictureForTask(task.getFbKey()).getAbsolutePath());
                taskImage.setImageBitmap(bitmap);
                pictureContainer.setVisibility(View.VISIBLE);
            }
        }
    }

    private void addAudio(String text) {
        showLoader();
        if(taskDiskCache.getAudioForTask(task.getFbKey())!=null){
            Uri file = Uri.fromFile(taskDiskCache.getAudioForTask(task.getFbKey()));
            String path = "voice_records/"+ taskRef.getKey() + "/" +file.getLastPathSegment();
            StorageReference updateRecord = storageReference.child(path);
            UploadTask uploadTask = updateRecord.putFile(file);
            // Register observers to listen for when the upload is done or if it fails
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                    Log.e(TAG, exception.getMessage());
                    hideLoader();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and upload URL.
                    Uri uploadUrl = taskSnapshot.getDownloadUrl();
                    taskRef.child("audioUrl").setValue(uploadUrl.toString());
                    taskRef.child("updated").setValue(new java.util.Date().getTime());
                    if(!task.getOwnerId().equals(mFirebaseUser.getUid())) {
                        Notification notification = NotificationFactory.createTaskNotification(task.getFbKey(), Notification.ACTION.EDIT.getNumVal());
                        notification.setRead(false);
                        DatabaseReference notiRef = databaseReference.child("notifications").push();
                        notiRef.setValue(notification);
                        databaseReference.child("users").child(task.getOwnerId()).child("notifications").child(notiRef.getKey()).setValue(true);
                    }
                    taskService.notifyUsers(task.getFbKey(), task.getCircleId(), Notification.ACTION.EDIT.getNumVal());
                    hideLoader();
                }
            });
            if (!NetworkUtils.isOn(this)) {
                audioPlayerView.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void playerStateChanged(boolean isPlaying) {
        if (isPlaying) {
            playAudio(task.getAudioUrl());
        } else {
            stopAudio();
            System.out.println("ON AUDIO STOP");
        }
    }

    @Override
    public void onAudioRemove() {
        stopAudio();
        databaseReference.child("circles").child(task.getCircleId()).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(task.getOwnerId().equals(mFirebaseUser.getUid()) || dataSnapshot.getValue().equals(mFirebaseUser.getUid())){
                    if(!task.isCompleted()){
                        if (!NetworkUtils.isOn(TaskViewActivity.this) && !task.getOwnerId().equals(mFirebaseUser.getUid())) {
                            showError(getResources().getString(R.string.task_edit_offline));
                        } else {
                            audioPlayerView.setVisibility(View.GONE);
                            storageReference.child("voice_records").child(task.getFbKey()).child("audio.amr").delete();
                            taskRef.child("audioUrl").removeValue();
                            addAudioButton.setClickable(true);
                        }

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

    }

    public void playAudio(String path) {
        mp = new MediaPlayer();
        try {
            if (!NetworkUtils.isOn(this))
                path = taskDiskCache.getAudioForTask(task.getFbKey()).getAbsolutePath();
            mp.setDataSource(path);
            mp.prepareAsync();
            mp.setOnCompletionListener(mp1 -> {
                stopAudio();
            });
            mp.setOnPreparedListener(mp1 -> {
                audioPlayerView.setMaxValue(mp.getDuration());
                audioPlayerView.start();
                mp.start();
                updateFuture = service.scheduleWithFixedDelay(updateAudioTask, 0, 1, TimeUnit.SECONDS);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopAudio() {
        if (updateFuture != null) {
            updateFuture.cancel(true);
        }
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
        audioPlayerView.stop();

    }

}
