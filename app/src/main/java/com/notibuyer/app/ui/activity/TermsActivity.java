package com.notibuyer.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;

import butterknife.Bind;
import rx.Observable;

public class TermsActivity extends BaseActivity {

    private static final String MODE = "mode";
    public static final String MODE_TERMS = "terms";
    public static final String MODE_PRIVACY = "privacy";
    private DatabaseReference databaseReference;
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.terms_text) protected TextView termsText;


    @Override
    protected int getContentViewId() {
        return R.layout.act_terms;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
           // getSupportActionBar().setTitle(R.string.task);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (getIntent().getExtras().getString(MODE) == null) finish();

        databaseReference = FirebaseDatabase.getInstance().getReference();
        String source = "";
        switch (getIntent().getExtras().getString(MODE)) {
            case MODE_TERMS:
                getSupportActionBar().setTitle("Terms of use");
                source = "terms";
                break;
            case MODE_PRIVACY:
                getSupportActionBar().setTitle("Privacy Policy");
                source = "policy";
                break;
            default:
                finish();

        }
        if (source == "") {
            finish();
            return;
        }
        showLoader();
        databaseReference.child("terms_policy").child(source).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                termsText.setText(Html.fromHtml((String)dataSnapshot.getValue()));
                hideLoader();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
                hideLoader();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    public static void start(Context context, String mode) {
        Intent intent = new Intent(context, TermsActivity.class);
        intent.putExtra(MODE, mode);
        context.startActivity(intent);
    }
}