package com.notibuyer.app.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.interfaces.AddUserToCircleListener;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.ui.view.CircleTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserProfileActivity extends BaseActivity implements AddUserToCircleListener {

    public static final String USER_ID = "user_id";
    private static final String PROVIDER = "provider";
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.picture) protected ImageView picture;
    @Bind(R.id.name) protected TextView name;
    @Bind(R.id.email) protected TextView email;
    @Bind(R.id.phone) protected TextView phone;
    @Bind(R.id.send_request) protected Button sendRequest;
    @Bind(R.id.remove_friend) protected Button removeFriend;
    @Bind(R.id.circles_list) protected RecyclerView recyclerView;
    @Bind(R.id.user_profile_block) protected LinearLayout userProfileBlock;
    @Bind(R.id.friend_management) protected LinearLayout friendManagement;



    private String userId;

    private boolean fromPush = false;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference userRef;
    private ChildEventListener userDataListener;
    private String provider;
    private CirclesAdapter adapter;

    // pass empty string for current user
    public static void start(Context context, String userId, String provider) {
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra(USER_ID, userId);
        intent.putExtra(PROVIDER, provider);
        context.startActivity(intent);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.user_profile;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.user_profile);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (getIntent().getExtras() != null) {
            fromPush = getIntent().getExtras().getBoolean("from_push");
        }

        sendRequest.setVisibility(View.VISIBLE);
        userId = getIntent().getExtras().getString(USER_ID);
        provider = getIntent().getExtras().getString(PROVIDER);
        userProfileBlock.setVisibility(View.GONE);

        userRef = databaseReference.child("users").child(userId);
        removeFriend.setOnClickListener(v->{
            //remove user from friend's friendlist
            databaseReference.child("users").child(userId).child("friends").child(mFirebaseUser.getUid()).removeValue();
            //remove friend from user's friendList
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("friends").child(userId).removeValue();
            //remove friend from users's circles
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //for each circe id in user circles
                    for(DataSnapshot circleData:dataSnapshot.getChildren()){
                        // select * from "circles" where circle id ='id'
                        databaseReference.child("circles").child(circleData.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot data) {
                                //if user is circle owner and friend is just a member
                                if(data.child("ownerId").getValue().equals(mFirebaseUser.getUid()) && data.child("members").hasChild(userId)){
                                    //remove friend from circle members
                                    databaseReference.child("circles").child(circleData.getKey()).child("members").child(userId).removeValue();
                                    //remove circle from friend circles
                                    databaseReference.child("users").child(userId).child("circles").child(circleData.getKey()).removeValue();
                                    //if friend is circle owner and user is just a member
                                } else if(data.child("ownerId").getValue().equals(userId) && data.child("members").hasChild(mFirebaseUser.getUid())){
                                    //remove user from circle members
                                    databaseReference.child("circles").child(circleData.getKey()).child("members").child(mFirebaseUser.getUid()).removeValue();
                                    //remove circle from user circles
                                    databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles").child(circleData.getKey()).removeValue();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e("FriendsAdapter", databaseError.getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e("FriendsAdapter", databaseError.getMessage());
                }
            });

        });
        sendRequest.setOnClickListener(v -> {
            databaseReference.child("users").child(userId).child("friendship_requests").child(mFirebaseUser.getUid()).child("provider").setValue(provider);
        });

    }

    @OnClick(R.id.picture)
    protected void previewImage() {
        databaseReference.child("users").child(userId).child("avatar").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ImagePreviewActivity.start(UserProfileActivity.this, (String) dataSnapshot.getValue());
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        userRef.removeEventListener(userDataListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter = new CirclesAdapter(this, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        userDataListener = userRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "name":
                        name.setText((String)dataSnapshot.getValue());
                        break;
                    case "avatar":
                        Picasso.with(UserProfileActivity.this)
                                .load((String)dataSnapshot.getValue())
                                .error(R.drawable.profile_icon_error)
                                .transform(new CircleTransformation())
                                .into(picture);
                        break;
                    case "email":
                        email.setText((String)dataSnapshot.getValue());
                        break;
                    case "phone":
                        phone.setText((String)dataSnapshot.getValue());
                        break;
                    case "friends":
                        if(dataSnapshot.hasChild(mFirebaseUser.getUid())){
                            removeFriend.setVisibility(View.VISIBLE);
                            sendRequest.setVisibility(View.GONE);
                            friendManagement.setVisibility(View.VISIBLE);

                        } else {
                            sendRequest.setVisibility(View.VISIBLE);
                            removeFriend.setVisibility(View.GONE);
                            friendManagement.setVisibility(View.INVISIBLE);
                        }
                        break;
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "name":
                        name.setText((String)dataSnapshot.getValue());
                        break;
                    case "avatar":
                        Picasso.with(UserProfileActivity.this)
                                .load((String)dataSnapshot.getValue())
                                .error(R.drawable.profile_icon_error)
                                .transform(new CircleTransformation())
                                .into(picture);
                        break;
                    case "email":
                        email.setText((String)dataSnapshot.getValue());
                        break;
                    case "phone":
                        phone.setText((String)dataSnapshot.getValue());
                        break;
                    case "friends":
                        if(dataSnapshot.hasChild(mFirebaseUser.getUid())){
                            removeFriend.setVisibility(View.VISIBLE);
                            sendRequest.setVisibility(View.GONE);
                            friendManagement.setVisibility(View.VISIBLE);

                        } else {
                            sendRequest.setVisibility(View.VISIBLE);
                            removeFriend.setVisibility(View.GONE);
                            friendManagement.setVisibility(View.GONE);
                        }
                        break;
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
               if(dataSnapshot.getKey().equals("friends")) {
                   sendRequest.setVisibility(View.VISIBLE);
                   removeFriend.setVisibility(View.GONE);
                   friendManagement.setVisibility(View.GONE);
               }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

        databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println(dataSnapshot.getChildrenCount());
                for(DataSnapshot circleId:dataSnapshot.getChildren()){
                    adapter.addItem(circleId.getKey());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @OnClick(R.id.settings)
    protected void settings() {
        MainActivity.start(this, MainActivity.SETTINGS);
    }

    @OnClick(R.id.friends)
    protected void friends() {
        MainActivity.start(this, MainActivity.FRIENDS);
    }

    @OnClick(R.id.add_friend)
    protected void addFriends() {
        startActivity(new Intent(this, SearchFriendsActivity.class));
    }

    @OnClick(R.id.circles)
    protected void circles() {
        MainActivity.start(this, MainActivity.CIRCLES);
    }

    @Override
    public void addCircleClick(String circleId, boolean add) {
        showLoader();
        // TODO: 17.08.2016 refactor - put this code in service
        if (add) {
            databaseReference.child("circles").child(circleId).child("members").child(userId).setValue(true);
            databaseReference.child("users").child(userId).child("circles").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    long count = dataSnapshot.getChildrenCount();
                    databaseReference.child("users").child(userId).child("circles").child(circleId).child("index").setValue(count);
                    databaseReference.child("circles").child(circleId).child("members").child(userId).setValue(true);
                    hideLoader();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                    hideLoader();
                }
            });

        } else {
            // TODO: 17.08.2016 refactor - put this code in service
            //remove from circle members
            databaseReference.child("circles").child(circleId).child("members").child(userId).removeValue();
            //remove from user circles
            databaseReference.child("users").child(userId).child("circles").child(circleId).removeValue();
            //rebuild indexes
            databaseReference.child("users").child(userId).child("circles").orderByChild("index").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot userCirclesDataSnapshot) {
                    long prevIndex = -1;
                    for (DataSnapshot circle : userCirclesDataSnapshot.getChildren()) {
                        if (prevIndex == -1) {
                            prevIndex = (long) circle.child("index").getValue();
                            if (prevIndex > 0)
                                databaseReference.child("users").child(userId).child("circles").child(circle.getKey()).child("index").setValue(0);
                            prevIndex = 0;
                            continue;
                        }
                        long currentIndex = (long) circle.child("index").getValue();
                        if ((currentIndex - prevIndex) > 1) {
                            currentIndex = prevIndex + 1;
                            //set in firebase
                            databaseReference.child("users").child(userId).child("circles").child(circle.getKey()).child("index").setValue(currentIndex);
                        }

                        //set prev = current
                        prevIndex = currentIndex;
                    }
                    hideLoader();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    hideLoader();
                    Log.e("CircleMembersAdapter", databaseError.getMessage());
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (fromPush)
            MainActivity.start(this, MainActivity.ALL);
        else
            super.onBackPressed();
    }

    class CirclesAdapter extends RecyclerView.Adapter<CirclesAdapter.CircleHolder> {

        private AddUserToCircleListener clickListener;
        private Context context;
        private List<Circle> ownCircleList = new ArrayList<>();
        public CirclesAdapter(AddUserToCircleListener clickListener, Context context) {
            this.clickListener = clickListener;
            this.context = context;
        }

        public void addItem(String id){
            databaseReference.child("circles").child(id).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    System.out.println(dataSnapshot.getValue());
                    Circle circle = dataSnapshot.getValue(Circle.class);
                    circle.setFbKey(dataSnapshot.getKey());
                    if (circle.getOwnerId() != null && circle.getOwnerId().equals(mFirebaseUser.getUid()) && circle.getName() != null && !circle.getName().equals("Private")) {
                        System.out.println("added");
                        ownCircleList.add(circle);
                        notifyItemInserted(ownCircleList.size());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });


        }

        @Override
        public CircleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new CircleHolder(LayoutInflater.from(context).inflate(R.layout.choose_circle_list_item, parent, false));
        }

        @Override
        public int getItemCount() {
            return ownCircleList.size();
        }

        @Override
        public void onBindViewHolder(CircleHolder holder, int position) {

            holder.setCircle(ownCircleList.get(position));

        }

        class CircleHolder extends RecyclerView.ViewHolder {
            @Bind(R.id.circle)
            TextView circle;
            @Bind(R.id.include_task)
            CheckBox include;
            public CircleHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            public void setCircle(Circle circle) {

                itemView.setOnClickListener((v) -> {
                    include.setChecked(!include.isChecked());
                    clickListener.addCircleClick(circle.getFbKey(), include.isChecked());
                });
                include.setChecked(false);
                databaseReference.child("circles").child(circle.getFbKey()).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        if(dataSnapshot.getKey().equals("name"))
                            CircleHolder.this.circle.setText(circle.getName());
                        if(dataSnapshot.getKey().equals("color")){
                            GradientDrawable gd = (GradientDrawable) CircleHolder.this.circle.getBackground().getCurrent();
                            gd.setColor(Color.parseColor(circle.getColor()));
                            gd.setStroke(0, 0);
                        }
                        if(dataSnapshot.getKey().equals("members")){
                            if(dataSnapshot.hasChild(userId))
                                include.setChecked(true);
                            else
                                include.setChecked(false);
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        if(dataSnapshot.getKey().equals("name"))
                            CircleHolder.this.circle.setText(circle.getName());
                        if(dataSnapshot.getKey().equals("color")){
                            GradientDrawable gd = (GradientDrawable) CircleHolder.this.circle.getBackground().getCurrent();
                            gd.setColor(Color.parseColor(circle.getColor()));
                            gd.setStroke(0, 0);
                        }
                        if(dataSnapshot.getKey().equals("members")){
                            if(dataSnapshot.hasChild(userId))
                                include.setChecked(true);
                            else
                                include.setChecked(false);
                        }
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getKey().equals("members"))
                            include.setChecked(false);
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            }
        }
    }
}