package com.notibuyer.app.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.appodeal.ads.Appodeal;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.CirclesTabsAdapter;
import com.notibuyer.app.adapter.TaskPagerAdapter;
import com.notibuyer.app.dagger.AppComponent;
import com.notibuyer.app.model.Circle;
import com.notibuyer.app.model.Task;
import com.notibuyer.app.network.firebase.TaskService;
import com.notibuyer.app.ui.activity.MainActivity;
import com.notibuyer.app.ui.activity.NewTaskActivity;
import com.notibuyer.app.ui.activity.SearchActivity;
import com.notibuyer.app.ui.view.AddTaskButtons;
import com.notibuyer.app.ui.view.CustomViewPager;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.Utils;
import com.tbruyelle.rxpermissions.RxPermissions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import butterknife.Bind;
import rx.android.schedulers.AndroidSchedulers;

import static android.content.Context.MODE_PRIVATE;

public class AllNotesFragment extends BaseFragment implements CirclesTabsAdapter.CircleSelectListener,
        AddTaskButtons.CreateTaskListener {

    private static final int REQUEST_IMAGE_PICK = 200;
    private static final int REQUEST_IMAGE_CAPTURE = 111;
    private static final int RECORD_CODE = 300;
    private static final int CREATE_TASK_CODE = 301;
    private static final int JUST_LAUNCH = 117;
    @Bind(R.id.circles_view)
    protected RecyclerView circlesView;
    @Bind(R.id.create_task_buttons)
    protected AddTaskButtons addTaskButtons;
    @Bind(R.id.pager)
    protected CustomViewPager viewPager;
    @Inject
    protected TaskService taskService;
    DatabaseReference notificationsRef;
    private String fileName;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private CirclesTabsAdapter circleTabsAdapter;
    private TaskPagerAdapter pagerAdapter;
    private DatabaseReference circlesRef;
    private ChildEventListener circlesListener;
    private String circleId = null;
    private File tmpFile;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        notificationsRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications_unread");

        getActivity().setTitle(R.string.all_notes);
        Log.d(TAG, "ON CREATE: ");
        //setHasOptionsMenu(true);
        if (prefs.getLastVisitedCircle() == null || TextUtils.isEmpty(prefs.getLastVisitedCircle()) || prefs.getLastVisitedCircle().equals("")) {
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("private_circle").addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            prefs.setLastVisitedCircle((String) dataSnapshot.getValue());
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.d(TAG, databaseError.getMessage());
                        }
                    }
            );
        }

    }

    @Override
    protected int getContentViewId() {
        return R.layout.all_notes_fragment;
    }

    public void setUpComponent(AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        pagerAdapter = new TaskPagerAdapter(getChildFragmentManager());
        if (getArguments() != null && getArguments().containsKey("circle_id")) {
            circleId = getArguments().getString("circle_id");
            pagerAdapter.setCurrent(circleId);
            circleTabsAdapter = new CirclesTabsAdapter(getActivity(), this, viewPager, circlesView, circleId);
            circleTabsAdapter.selectCircle(circleId);
        } else {
            circleTabsAdapter = new CirclesTabsAdapter(getActivity(), AllNotesFragment.this, viewPager, circlesView);
            pagerAdapter.setCurrent(circleId);
        }

        addTaskButtons.setListener(this);
        if (prefs.getCurrentSubscribe() == null || !prefs.getCurrentSubscribe().equals("noti_month_test"))
            Appodeal.show(getActivity(), Appodeal.BANNER_VIEW);
        else
            Appodeal.hide(getActivity(), Appodeal.BANNER_VIEW);

    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "on RESUME");
        System.out.println("TEST* " + prefs.isAutoRecordEnabled());
        System.out.println("ONLAUNCH*" + prefs.isOnLaunch());
//        if (prefs.isAutoRecordEnabled() && prefs.isOnLaunch()) {
//            prefs.setOnLaunch(false);
//            createTaskWithRecord();
//        }

        Boolean isFirstRun = getContext().getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("isFirstRun", true);

        if (prefs.isAutoRecordEnabled() && prefs.isOnLaunch() && isFirstRun) {
            prefs.setOnLaunch(false);
            prefs.setWasLaunched(true);
            createTaskWithRecord();
        }

        getContext().getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
                .putBoolean("isFirstRun", false).apply();

        Appodeal.onResume(getActivity(), Appodeal.BANNER_VIEW);
        circlesView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        circlesView.setAdapter(circleTabsAdapter);

        circlesView.scrollToPosition(circleTabsAdapter.getSelectedCircleIndex());
        viewPager.setCurrentItem(circleTabsAdapter.getSelectedCircleIndex());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setSwipeable(false);

        circlesRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles");


        circlesListener = circlesRef.orderByChild("index").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                databaseReference.child("circles").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        Circle circle = snapshot.getValue(Circle.class);
                        circle.setFbKey(dataSnapshot.getKey());
                        pagerAdapter.addItem(circle);
                        circleTabsAdapter.addItem(circle);
                        if (circle.getFbKey().equals(circleId)) {
                            circlesView.smoothScrollToPosition(circleTabsAdapter.getItemCount() - 1);
                            circleId = null;
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                circleTabsAdapter.removeItem(dataSnapshot.getKey());
                pagerAdapter.removeItem(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
        ItemTouchHelper.Callback ithCallback = new ItemTouchHelper.Callback() {

            int dragFrom = -1;
            int dragTo = -1;

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.START | ItemTouchHelper.END);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int fromPosition = viewHolder.getAdapterPosition();
                int toPosition = target.getAdapterPosition();
                if(dragFrom == -1) {
                    dragFrom =  fromPosition;
                }
                if(fromPosition==0||toPosition==0)
                    return false;
                dragTo = toPosition;
                circleTabsAdapter.onIndexChanged(fromPosition, toPosition);
                pagerAdapter.onIndexChanged(fromPosition,toPosition);


                return true;

            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(ithCallback);
        itemTouchHelper.attachToRecyclerView(circlesView);
    }



    @Override
    public void onStop() {
        super.onStop();
        circlesRef.removeEventListener(circlesListener);
    }

    @Override
    public void onCircleSelected(int selectedPosition, String circleId) {
        viewPager.setCurrentItem(selectedPosition);
        pagerAdapter.setCurrent(circleId);
        prefs.setLastVisitedCircle(circleId);
        System.out.println("on circle selected " + selectedPosition);
    }

    @Override
    public void createTaskWithText() {
        DialogFactory.getNewTaskDialog(getContext(), "", (dialog, which) -> dialog.dismiss(), (text, quick) -> {
            if (!text.isEmpty()) {
                if (quick) {
                    String newTaskId = databaseReference.child("tasks").push().getKey();
                    quickCreateTask(newTaskId, text, null);
                    circleTabsAdapter.notifyDataSetChanged();
                } else {
                    NewTaskActivity.startNew(getActivity(), text, CREATE_TASK_CODE);
                }
            }
        }).show();
    }

    @Override
    public void createTaskWithRecord() {
        RxPermissions.getInstance(getActivity())
                .request(Manifest.permission.RECORD_AUDIO)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(granted -> {
                    if (granted) {
                        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                        //intent.putExtra("android.speech.extra.PREFER_OFFLINE", true);
                        intent.putExtra("android.speech.extra.GET_AUDIO_FORMAT", "audio/AMR");
                        intent.putExtra("android.speech.extra.GET_AUDIO", true);
                        getActivity().startActivityForResult(intent, RECORD_CODE);
                    } else {
                        DialogFactory.getInfoDialog(getActivity(), R.string.record_permissions_not_granted).show();
                    }
                });
    }

    @Override
    public void createTaskWithPhoto(View v) {

        RxPermissions.getInstance(getActivity())
                .request(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) {
                        registerForContextMenu(v);
                        getActivity().openContextMenu(v);
                    } else {
                        DialogFactory.getInfoDialog(getActivity(), R.string.camera_permissions_not_granted).show();
                    }
                });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        prefs.setOnLaunch(false);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RECORD_CODE) {

                if (data != null && data.getExtras() != null) {
                    Bundle bundle = data.getExtras();
                    ArrayList<String> matches = bundle.getStringArrayList(RecognizerIntent.EXTRA_RESULTS);
                    if (matches == null || matches.isEmpty()) return;
                    DialogFactory.getNewTaskSpeechDialog(getActivity(), matches.get(0), (dialog, which) -> {
                        dialog.dismiss();
                        createTaskWithRecord();
                    }, (text, quick) -> {
                        Uri audioUri = data.getData();
                        ContentResolver contentResolver = getActivity().getContentResolver();

                        String newTaskId = databaseReference.child("tasks").push().getKey();
                        try {
                            InputStream filestream = contentResolver.openInputStream(audioUri);
                            taskDiskCache.saveAudio(newTaskId, filestream);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        if (quick) {

                            quickCreateTask(newTaskId, text, taskDiskCache.getAudioForTask(newTaskId));
                        } else {
                            NewTaskActivity.startWithAudio(getActivity(), text, newTaskId, CREATE_TASK_CODE);
                        }
                    }).show();
                }
            } else if (requestCode == REQUEST_IMAGE_CAPTURE) {

                if(tmpFile == null && data!=null){
                    tmpFile = new File(data.getData().getPath());
                    if(!tmpFile.exists())
                        return;
                }
                Bitmap bitmap = null;
                bitmap = Utils.compressAndRotate(tmpFile.getAbsolutePath());
                //taskDiskCache.savePicture(task.getFbKey(), Utils.getFile(getActivity(), bitmap));
                uploadTaskImage(bitmap);

                if(bitmap!=null){
                    bitmap.recycle();
                    bitmap=null;
                }

            } else if (requestCode == REQUEST_IMAGE_PICK) {
                Uri imageUri = data.getData();
                try {
                    Bitmap bitmap = Utils.decodeUri(getActivity(), imageUri, 200);
                    uploadTaskImage(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if(requestCode == SearchActivity.REQ_CODE_SEARCH) {

            } else if (requestCode == JUST_LAUNCH) {
            }
        }
    }

    public void uploadTaskImage(Bitmap bitmap) {
        File file = Utils.getFile(getActivity(), bitmap);
        NewTaskActivity.startWithPhoto(getActivity(), file, CREATE_TASK_CODE);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        menu.setHeaderTitle(R.string.choose_photo_task);
        inflater.inflate(R.menu.choose_photo_task_menu, menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
    /*    if (!NetworkUtils.isOn(getContext())) {
            DialogFactory.getInfoDialog(getContext(), getString(R.string.no_internet_connection));
            return false;
        }*/

        switch (item.getItemId()) {
            case R.id.take_photo:
                dispatchTakePictureIntent();
                break;
            case R.id.gallery_photo:
                pickPictureFromGallery();
                break;
        }
        return true;
    }


    private void dispatchTakePictureIntent() {
        RxPermissions.getInstance(getActivity()).request(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(granted -> {
            if (granted) {
                if (tmpFile!=null) {
                    tmpFile.delete();
                    tmpFile=null;
                }
                try {
                    tmpFile = Utils.createTemporaryFile(String.valueOf(new Date().toString().hashCode()), ".png");
                    fileName = tmpFile.getAbsolutePath();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tmpFile));
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    getActivity().startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } else {
                DialogFactory.getInfoDialog(getActivity(), R.string.camera_permissions_not_granted).show();
            }
        });
    }

    private void pickPictureFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        getActivity().startActivityForResult(
                Intent.createChooser(intent, getString(R.string.select_picture)),
                REQUEST_IMAGE_PICK);
    }

    public void hideButtons() {
        addTaskButtons.hide();
    }

    public void showButtons() {
        addTaskButtons.show();
    }

    private void quickCreateTask(String newTaskId, String text, File audio) {
        showLoader();
        System.out.println("last = " + prefs.getLastVisitedCircle());
        Task newTask = new Task();
        newTask.setFbKey(newTaskId);
        newTask.setCompleted(false);
        newTask.setCreated(new Date().getTime());
        newTask.setOwnerId(mFirebaseUser.getUid());
        newTask.setArchived(false);
        newTask.setText(text);
        newTask.setUpdated(newTask.getCreated());
        //generate ID in firebase
        taskService.createTask(newTask, prefs.getLastVisitedCircle(), audio, null);


        hideLoader();
    }


}