package com.notibuyer.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.appodeal.ads.Appodeal;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.ArchiveTaskAdapter;
import com.notibuyer.app.ui.view.CustomScrollView;

import butterknife.Bind;

public class ArchiveFragment extends BaseFragment implements RecyclerViewExpandableItemManager.OnGroupExpandListener,
        RecyclerViewExpandableItemManager.OnGroupCollapseListener, SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    @Bind(R.id.current_tasks_view)
    protected RecyclerView curreentTasksView;
    @Bind(R.id.completed_tasks_view)
    protected RecyclerView completedTasksView;
    @Bind(R.id.showCompleted)
    protected Button expandCollapseCompleted;
    @Bind(R.id.scrollList)
    protected CustomScrollView scroll;


    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private ChildEventListener archiveTasksListener;
    private DatabaseReference archiveTasksRef;
    private ArchiveTaskAdapter currentAdapter;
    private ArchiveTaskAdapter compleedAdapter;

    //TextView countNotifications;
    //ChildEventListener notificationsCountListener;
    // DatabaseReference notificationsRef;
    //private List<String> notifications = new ArrayList<>();

    public static TaskListFragment getInstance(String circleId) {
        return new TaskListFragment();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.frag_task_list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.archive);
        setHasOptionsMenu(true);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        archiveTasksRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("tasks");
        //notificationsRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications");

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_simple_search, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        /*final MenuItem noti = menu.findItem(R.id.action_notifications);
        MenuItemCompat.setActionView(noti, R.layout.feed_update_count);
        noti.getActionView().setOnClickListener(v -> MainActivity.start(getActivity(), MainActivity.NOTIFICATIONS));

        countNotifications = (TextView) noti.getActionView().findViewById(R.id.hotlist_hot);
        if(notifications.size()>0) {
            countNotifications.setText(String.valueOf(notifications.size()));
            countNotifications.setVisibility(View.VISIBLE);
        } else {
            countNotifications.setVisibility(View.INVISIBLE);
        }*/
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
    }
/*
    public void listenNotifications(){
        notificationsCountListener = notificationsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                databaseReference.child("notifications").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Notification notification = dataSnapshot.getValue(Notification.class);
                        if (!notification.isRead() && !notifications.contains(dataSnapshot.getKey())) {
                            notifications.add(dataSnapshot.getKey());
                            if(getActivity()!=null)
                                getActivity().invalidateOptionsMenu();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }*/

    @Override
    public void onStart() {
        super.onStart();
        //  listenNotifications();
        curreentTasksView.setLayoutManager(new LinearLayoutManager(getActivity()));
        completedTasksView.setLayoutManager(new LinearLayoutManager(getActivity()));

        currentAdapter = new ArchiveTaskAdapter(getActivity(), false);
        compleedAdapter = new ArchiveTaskAdapter(getActivity(), true);

        curreentTasksView.setAdapter(currentAdapter);
        completedTasksView.setAdapter(compleedAdapter);

        expandCollapseCompleted.setOnClickListener(v -> {
            if (completedTasksView.getVisibility() == View.VISIBLE) {
                expandCollapseCompleted.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.arrow_up_float, 0, 0, 0);
                completedTasksView.setVisibility(View.GONE);
            } else {
                expandCollapseCompleted.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.arrow_down_float, 0, 0, 0);
                completedTasksView.setVisibility(View.VISIBLE);
            }
        });
        syncTasks();
        //  listenNotifications();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (prefs.getCurrentSubscribe() == null || !prefs.getCurrentSubscribe().equals("noti_month_test"))
            Appodeal.show(getActivity(), Appodeal.BANNER_VIEW);
        else
            Appodeal.hide(getActivity(), Appodeal.BANNER_VIEW);
    }

    @Override
    public void onResume() {
        super.onResume();
        Appodeal.onResume(getActivity(), Appodeal.BANNER_VIEW);
        // notifications.clear();


/*
        databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snap:dataSnapshot.getChildren()){
                    databaseReference.child("notifications").child(snap.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnap) {
                            Notification notification = dataSnap.getValue(Notification.class);
                            if(!notification.isRead() && !notifications.contains(dataSnap.getKey())){
                                notifications.add(dataSnap.getKey());
                                getActivity().invalidateOptionsMenu();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

        System.out.println("ON RES");*/
    }

    private void syncTasks(){
        archiveTasksListener = archiveTasksRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                currentAdapter.addItem(dataSnapshot.getKey());
                compleedAdapter.addItem(dataSnapshot.getKey());
                if(compleedAdapter.getItemCount()>0)
                    expandCollapseCompleted.setVisibility(View.VISIBLE);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                currentAdapter.removeItem(dataSnapshot.getKey());
                compleedAdapter.removeItem(dataSnapshot.getKey());
                if(compleedAdapter.getItemCount()==0)
                    expandCollapseCompleted.setVisibility(View.GONE);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        archiveTasksRef.removeEventListener(archiveTasksListener);
    }

    @Override
    public void onGroupExpand(int groupPosition, boolean fromUser) {

    }

    @Override
    public void onGroupCollapse(int groupPosition, boolean fromUser) {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        showLoader();
        currentAdapter.filter(query);
        compleedAdapter.filter(query);
        hideLoader();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            currentAdapter.filter("");
            compleedAdapter.filter("");
        }
        return true;
    }

    @Override
    public boolean onClose() {
        System.out.println("onClose");
        currentAdapter = null;
        compleedAdapter = null;
        currentAdapter  = new ArchiveTaskAdapter(getActivity(),false);
        compleedAdapter = new ArchiveTaskAdapter(getActivity(), true);
        curreentTasksView.setAdapter(currentAdapter);
        completedTasksView.setAdapter(compleedAdapter);
            syncTasks();
        return false;
    }


}