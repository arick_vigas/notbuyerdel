package com.notibuyer.app.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.notibuyer.app.App;
import com.notibuyer.app.Prefs;
import com.notibuyer.app.R;
import com.notibuyer.app.dagger.AppComponent;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.TaskDiskCache;

import javax.inject.Inject;

import butterknife.ButterKnife;


public abstract class BaseFragment extends Fragment {

    protected final String TAG = this.getClass().getSimpleName();

    @Inject
    protected Prefs prefs;

    @Inject
    protected TaskDiskCache taskDiskCache;

    private AlertDialog loader;

    protected abstract int getContentViewId();

    public void setUpComponent(AppComponent appComponent) {
        appComponent.inject(this);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpComponent(App.getAppComponent(getActivity()));

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getContentViewId(), container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        super.onDestroyView();
    }

    protected void hideKeyboard(View view) {
        if (view == null) {
            view = new View(getActivity());
        }
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        view.clearFocus();
    }

    protected void hideKeyboard() {
        hideKeyboard(getActivity().getCurrentFocus());
    }

    protected void showKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        view.requestFocus();
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    protected void showLoader() {
        if (loader == null) {
            loader = DialogFactory.getLoadingDialog(getActivity()).create();
            loader.show();
        }

    }

    protected void hideLoader() {
        if (loader != null) {
            loader.hide();
        }
    }

    protected void toast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    protected void toast(@StringRes int msg) {
        toast(getString(msg));
    }

    protected void showError() {
        showError(getString(R.string.default_server_error));
    }

    protected void showError(String msg) {
        DialogFactory.getInfoDialog(getActivity(), msg).show();
    }

    protected void showError(@StringRes int msg) {
        showError(getString(msg));
    }
}
