package com.notibuyer.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.CircleMembersAdapter;
import com.notibuyer.app.model.Notification;
import com.notibuyer.app.ui.activity.MainActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class CircleMembersFragment extends BaseFragment implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {
    @Bind(R.id.friends)
    RecyclerView friendsList;
    CircleMembersAdapter editCirclesFriendAdapter;
    TextView countNotifications;
    ChildEventListener notificationsCountListener;
    DatabaseReference notificationsRef;
    private String circleId;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private List<String> notifications = new ArrayList<>();

    public CircleMembersFragment() {
        // Required empty public constructor
    }

    public static CircleMembersFragment newInstance(String circleId) {
        CircleMembersFragment fragment = new CircleMembersFragment();
        Bundle args = new Bundle();
        args.putString("circleId", circleId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_cirle_members;
    }

    @Override
    public void onStart() {
        super.onStart();
        listenNotifications();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            circleId = getArguments().getString("circleId");

            mFirebaseAuth = FirebaseAuth.getInstance();
            mFirebaseUser = mFirebaseAuth.getCurrentUser();
            databaseReference = FirebaseDatabase.getInstance().getReference();

            notificationsRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        notifications.clear();

        databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    databaseReference.child("notifications").child(snap.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnap) {
                            Notification notification = dataSnap.getValue(Notification.class);
                            if (!notification.isRead() && !notifications.contains(dataSnap.getKey())) {
                                notifications.add(dataSnap.getKey());
                                getActivity().invalidateOptionsMenu();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

        System.out.println("ON RES");
    }

    public void listenNotifications() {
        notificationsCountListener = notificationsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                databaseReference.child("notifications").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Notification notification = dataSnapshot.getValue(Notification.class);
                        if (!notification.isRead() && !notifications.contains(dataSnapshot.getKey())) {
                            notifications.add(dataSnapshot.getKey());
                            if (getActivity() != null)
                                getActivity().invalidateOptionsMenu();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        notificationsRef.removeEventListener(notificationsCountListener);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        System.out.println("on view created");
        friendsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        editCirclesFriendAdapter = new CircleMembersAdapter(getActivity(), circleId);
        friendsList.setAdapter(editCirclesFriendAdapter);
        addFriends();
        System.out.println(friendsList.getAdapter().getItemCount());
    }

    //search listener overriden methods
    @Override
    public boolean onClose() {
        System.out.println("onClose");
        editCirclesFriendAdapter = null;
        editCirclesFriendAdapter = new CircleMembersAdapter(getActivity(), circleId);
        friendsList.setAdapter(editCirclesFriendAdapter);
        addFriends();

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        System.out.println("filter");
        showLoader();
        editCirclesFriendAdapter.filter(query);
        hideLoader();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            editCirclesFriendAdapter.filter("");
        }
        return true;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // What i have added is this
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);
        inflater.inflate(R.menu.save_changes_menu, menu);
        final MenuItem item = menu.findItem(R.id.action_search);

        final MenuItem noti = menu.findItem(R.id.action_notifications);
        MenuItemCompat.setActionView(noti, R.layout.feed_update_count);
        noti.getActionView().setOnClickListener(v -> MainActivity.start(getActivity(), MainActivity.NOTIFICATIONS));

        countNotifications = (TextView) noti.getActionView().findViewById(R.id.hotlist_hot);
        if (notifications.size() > 0) {
            countNotifications.setText(String.valueOf(notifications.size()));
            countNotifications.setVisibility(View.VISIBLE);
        } else {
            countNotifications.setVisibility(View.INVISIBLE);
        }

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
    }

    private void addFriends(){
        //add or remove friends in list on change(!!!and fill in a list on start)
        databaseReference.child("users").child(mFirebaseUser.getUid()).child("friends").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                editCirclesFriendAdapter.addItem(dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                // editCirclesFriendAdapter.removeItem(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }
}
