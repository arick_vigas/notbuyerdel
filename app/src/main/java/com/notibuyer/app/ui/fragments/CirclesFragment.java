package com.notibuyer.app.ui.fragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appodeal.ads.Appodeal;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.dagger.AppComponent;
import com.notibuyer.app.interfaces.EditCircleClickListener;
import com.notibuyer.app.model.Notification;
import com.notibuyer.app.network.FirebasePushService;
import com.notibuyer.app.network.firebase.CirclesService;
import com.notibuyer.app.ui.activity.AddCircleActivity;
import com.notibuyer.app.ui.activity.EditCircleActivity;
import com.notibuyer.app.ui.activity.MainActivity;
import com.notibuyer.app.utils.NotificationFactory;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CirclesFragment extends BaseFragment implements EditCircleClickListener {

    public static final int EDIT_CIRCLE_CODE = 201;
    public static final int ADD_CIRCLE_CODE = 202;
    @Bind(R.id.circles_list)
    protected RecyclerView recyclerView;
    @Inject
    protected CirclesService circlesService;
    TextView countNotifications;
    ChildEventListener notificationsCountListener;
    DatabaseReference notificationsRef;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private CircleListAdapter mCircleListAdapter;
    private List<String> notifications = new ArrayList<>();
    private Set<String> ownedCircles = new HashSet<>();


    @Override
    protected int getContentViewId() {
        return R.layout.circles_fragment;
    }

    public void setUpComponent(@NonNull AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        //setHasOptionsMenu(true);

        notificationsRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications");

    }
/*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.noti, menu);
        final MenuItem noti = menu.findItem(R.id.action_notifications);
        MenuItemCompat.setActionView(noti, R.layout.feed_update_count);
        noti.getActionView().setOnClickListener(v -> MainActivity.start(getActivity(), MainActivity.NOTIFICATIONS));
        countNotifications = (TextView) noti.getActionView().findViewById(R.id.hotlist_hot);
        if(notifications.size()>0) {
            countNotifications.setText(String.valueOf(notifications.size()));
            countNotifications.setVisibility(View.VISIBLE);
        } else {
            countNotifications.setVisibility(View.INVISIBLE);
        }
    }*/

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.circles);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (prefs.getCurrentSubscribe() == null || !prefs.getCurrentSubscribe().equals("noti_month_test"))
            Appodeal.show(getActivity(), Appodeal.BANNER_VIEW);
        else
            Appodeal.hide(getActivity(), Appodeal.BANNER_VIEW);
        mCircleListAdapter = new CircleListAdapter();
        recyclerView.setAdapter(mCircleListAdapter);
        ItemTouchHelper.Callback ithCallback = new ItemTouchHelper.Callback() {

            int dragFrom = -1;
            int dragTo = -1;

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG, ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.START | ItemTouchHelper.END);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int fromPosition = viewHolder.getAdapterPosition();
                int toPosition = target.getAdapterPosition();
                if(dragFrom == -1) {
                    dragFrom =  fromPosition;
                }
                dragTo = toPosition;
                mCircleListAdapter.onIndexChanged(fromPosition, toPosition);
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }
        };

         ItemTouchHelper itemTouchHelper = new ItemTouchHelper(ithCallback);
         itemTouchHelper.attachToRecyclerView(recyclerView);
        databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles").orderByChild("index").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                mCircleListAdapter.addCircle(dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                mCircleListAdapter.removeCircle(dataSnapshot.getKey());


            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        listenNotifications();
        Appodeal.onResume(getActivity(), Appodeal.BANNER_VIEW);
    }

    @Override
    public void onStop() {
        super.onStop();
        notificationsRef.removeEventListener(notificationsCountListener);
    }

    public void listenNotifications(){
        notificationsCountListener = notificationsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                databaseReference.child("notifications").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Notification notification = dataSnapshot.getValue(Notification.class);
                        if (!notification.isRead() && !notifications.contains(dataSnapshot.getKey())) {
                            notifications.add(dataSnapshot.getKey());
                            if (getActivity() != null)
                                getActivity().invalidateOptionsMenu();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @OnClick(R.id.add_new_circle_container)
    protected void addNewCircle() {
        //  if (ownedCircles.size() <= 10) {
            AddCircleActivity.startForResult(getActivity(), ADD_CIRCLE_CODE);
        /*} else {
            if(prefs.getCurrentSubscribe()!=null && prefs.getCurrentSubscribe().equals("noti_month_test")) {
                AddCircleActivity.startForResult(getActivity(), ADD_CIRCLE_CODE);
            } else {
                new UnleashDialog(getActivity()).show();
                Toast.makeText(getActivity(), getResources().getString(R.string.circles_limit), Toast.LENGTH_SHORT).show();
            }
        }*/
    }

    @Override
    public void editCircleClick(String circleId) {
        EditCircleActivity.startForResult(getActivity(), circleId, EDIT_CIRCLE_CODE);
    }

    @Override
    public void outOfCircleClick(String circleId) {
        mCircleListAdapter.removeFromCircle(circleId);
    }

    @Override
    public void toCircleTasksClick(String circleId) {

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment notes = new AllNotesFragment();
        Bundle args = new Bundle();
        args.putString("circle_id", circleId);
        notes.setArguments(args);
        transaction.replace(R.id.content, notes);
        transaction.addToBackStack(null);
        transaction.commit();
        ((MainActivity)getActivity()).selectDrawerItem(MainActivity.ALL);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == EDIT_CIRCLE_CODE || requestCode == ADD_CIRCLE_CODE) {
                // circlesList = realm.where(Circle.class).notEqualTo("name", "Private").findAll();
                //recyclerView.getAdapter().notifyDataSetChanged();
            }
        }
    }

    static class CircleListHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.remove)
        ImageView removeCircle;
        @Bind(R.id.circle)
        TextView circle;
        @Bind(R.id.edit)
        ImageView edit;
        @Bind(R.id.by)
        TextView by;
        @Bind(R.id.name)
        TextView owner;

        public CircleListHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class CircleListAdapter extends RecyclerView.Adapter {
        private List<String> circlesList = new ArrayList<>();

        public CircleListAdapter(){

        }

        public void addCircle(String id){
            if (!circlesList.contains(id)) {
                circlesList.add(id);
                notifyItemInserted(circlesList.size());
            }
        }

        public void removeCircle(String id){
            int pos = -1;
            String circleToRemove = null;
            for (String circleId : circlesList) {
                if (circleId.equals(id)) {
                    pos = circlesList.indexOf(circleId);
                    circleToRemove = circleId;
                    break;
                }
            }
            if (circleToRemove != null) {

                System.out.println(circlesList.remove(circleToRemove));
                notifyDataSetChanged();
                System.out.println(circlesList.size());
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            String key = circlesList.get(position);
            CircleListHolder viewHolder = (CircleListHolder)holder;
            viewHolder.circle.setOnClickListener(view ->CirclesFragment.this.toCircleTasksClick(key));
            viewHolder.edit.setImageDrawable(null);
            viewHolder.removeCircle.setImageDrawable(null);
            //get circles data from firebase by key + onChange listener
            databaseReference.child("circles").child(key).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                    switch (dataSnapshot.getKey()) {
                        case "name":
                            viewHolder.circle.setText((String) dataSnapshot.getValue());
                            if (dataSnapshot.getValue().equals("Private")) {
                                viewHolder.edit.setVisibility(View.INVISIBLE);
                                viewHolder.removeCircle.setVisibility(View.INVISIBLE);
                            }
                            break;
                        case "color":
                            GradientDrawable gd = (GradientDrawable) viewHolder.circle.getBackground().getCurrent();
                            gd.setColor(Color.parseColor((String) dataSnapshot.getValue()));
                            gd.setStroke(0, 0);
                            break;
                        case "ownerId":
                            if (dataSnapshot.getValue().equals(mFirebaseUser.getUid())) {
                                ownedCircles.add(key);
                                viewHolder.edit.setOnClickListener(view -> {CirclesFragment.this.editCircleClick(key);});
                                viewHolder.edit.setImageResource(R.drawable.ic_create);
                                viewHolder.removeCircle.setImageResource(R.drawable.ic_trash);
                                if(!viewHolder.circle.getText().equals("Private")) {
                                    viewHolder.edit.setVisibility(View.VISIBLE);
                                    viewHolder.removeCircle.setVisibility(View.VISIBLE);
                                    viewHolder.removeCircle.setOnClickListener(v -> {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                        builder.setTitle(getResources().getString(R.string.remove_circle_title));
                                        builder.setMessage(getResources().getString(R.string.realy_delete));
                                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                circlesService.removeCircle(circlesList.get(position));
                                            }
                                        });
                                        builder.setNegativeButton("No", null);
                                        builder.create().show();
                                    });
                                }
                                viewHolder.by.setVisibility(View.GONE);
                                viewHolder.owner.setVisibility(View.GONE);
                            } else {
                                viewHolder.edit.setOnClickListener(view -> {CirclesFragment.this.outOfCircleClick(key);});
                                viewHolder.edit.setImageResource(R.drawable.but_close);
                                viewHolder.edit.setVisibility(View.VISIBLE);
                                viewHolder.by.setVisibility(View.VISIBLE);
                                databaseReference.child("users").child((String)dataSnapshot.getValue()).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot ownerDataSnapshot) {
                                        System.out.println((String)ownerDataSnapshot.child("name").getValue());
                                        viewHolder.owner.setText((String)ownerDataSnapshot.child("name").getValue());
                                        viewHolder.owner.setVisibility(View.VISIBLE);
                                        viewHolder.by.setVisibility(View.VISIBLE);

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e(TAG, databaseError.getMessage());
                                    }
                                });
                            }
                            break;
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    switch (dataSnapshot.getKey()) {
                        case "name":
                            viewHolder.circle.setText((String) dataSnapshot.getValue());
                            break;

                        case "color":
                            GradientDrawable gd = (GradientDrawable) viewHolder.circle.getBackground().getCurrent();
                            gd.setColor(Color.parseColor((String) dataSnapshot.getValue()));
                            gd.setStroke(0, 0);
                            break;
                    }
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {}

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });

        }

        @Override
        public int getItemCount() {
            return circlesList.size();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new CircleListHolder(LayoutInflater.from(CirclesFragment.this.getContext()).inflate(R.layout.edit_circle_list_item, parent, false));
        }

        public void onIndexChanged(int fromPosition, int toPosition) {

            String fromId = circlesList.get(fromPosition);
            String toId = circlesList.get(toPosition);
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles").child(fromId).child("index").setValue(toPosition);
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles").child(toId).child("index").setValue(fromPosition);
             Collections.swap(circlesList, fromPosition, toPosition);
            notifyItemMoved(fromPosition, toPosition);
            notifyItemChanged(fromPosition);
            notifyItemChanged(toPosition);
        }

        public void removeFromCircle(String circleId){
            //remove from users.circlesList
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles").child(circleId).removeValue();
            //remove from circles.membersList
            databaseReference.child("circles").child(circleId).child("members").child(mFirebaseUser.getUid()).removeValue();
            //rebuild list
            notifyDataSetChanged();
            //send notification
            Notification noti = NotificationFactory.outOfCircle(circleId);
            noti.setRead(false);
            DatabaseReference notiRef = databaseReference.child("notifications").push();
            notiRef.setValue(noti);
            databaseReference.child("circles").child(circleId).child("ownerId").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    databaseReference.child("users").child((String)dataSnapshot.getValue()).child("fcm_token").addListenerForSingleValueEvent(new ValueEventListener() {
                        @TargetApi(Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            String[] recepients = {(String)snapshot.getValue()};
                            try {
                                JSONArray recepientsJson = new JSONArray(recepients);
                                FirebasePushService.sendMessage(recepientsJson, Notification.TYPE.CIRCLE.getNumVal(), Notification.ACTION.QUIT.getNumVal(), mFirebaseUser.getUid(), circleId, null,null);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.e(TAG, databaseError.getMessage());
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });
        }
    }
}
