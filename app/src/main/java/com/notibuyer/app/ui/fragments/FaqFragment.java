package com.notibuyer.app.ui.fragments;
import android.os.Bundle;
import android.view.View;

import com.notibuyer.app.R;
public class FaqFragment extends BaseFragment {
    @Override
    protected int getContentViewId() {
        return R.layout.faq_fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.faq);
    }
}
