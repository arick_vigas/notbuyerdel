package com.notibuyer.app.ui.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.appodeal.ads.Appodeal;
import com.notibuyer.app.R;

import butterknife.OnClick;

public class FeedbackFragment extends BaseFragment {

    @Override
    protected int getContentViewId() {
        return R.layout.feedback_fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.feedback);
        if (prefs.getCurrentSubscribe() == null || !prefs.getCurrentSubscribe().equals("noti_month_test"))
            Appodeal.show(getActivity(), Appodeal.BANNER_VIEW);
        else
            Appodeal.hide(getActivity(), Appodeal.BANNER_VIEW);
    }

    @OnClick(R.id.send_email)
    protected void sendEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto","notibuyerapp@gmail.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));

    }

    @OnClick(R.id.facebook)
    protected void facebook() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://www.facebook.com/Notibuyer-1110182299023856"));
        startActivity(i);
    }

    @OnClick(R.id.instagram)
    protected void google() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://www.instagram.com/notibuyer"));
        startActivity(i);
    }

    @OnClick(R.id.rate_app)
    protected void rateApp() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.notibuyer.app"));
        startActivity(i);
    }

    @OnClick(R.id.faq)
    protected void faq() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("http://notibuyer.com/faq"));
        startActivity(i);
    }

    @Override
    public void onResume() {
        super.onResume();
        Appodeal.onResume(getActivity(), Appodeal.BANNER_VIEW);
    }
}