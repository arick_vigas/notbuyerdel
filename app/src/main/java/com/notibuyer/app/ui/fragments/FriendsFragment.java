package com.notibuyer.app.ui.fragments;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.appodeal.ads.Appodeal;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.FriendsPagerAdapter;
import com.notibuyer.app.adapter.RequestAdapter;
import com.notibuyer.app.model.FriendshipRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class FriendsFragment extends BaseFragment {



    @Bind(R.id.friends_pager)
    protected ViewPager pager;
    @Bind(R.id.my_friends)
    protected Button myFriends;
    @Bind(R.id.search_friends)
    protected Button searchFriends;
    @Bind(R.id.requests_header)
    protected TextView requestsHeader;
    @Bind(R.id.requests)
    protected RecyclerView friendRequests;
    FriendsPagerAdapter pagerAdapter;
    TextView countNotifications;
    ChildEventListener notificationsCountListener;
    DatabaseReference notificationsRef;
    private RequestAdapter adapter;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference databaseReference;
    private DatabaseReference userRef;
    private ChildEventListener requestListener;
    private ChildEventListener countRequestListener;
    private DatabaseReference userFrRequestsRef;
    private List<String> notifications = new ArrayList<>();

    @Override
    protected int getContentViewId() {
        return R.layout.friends_fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        userRef = databaseReference.child("users").child(mFirebaseUser.getUid());
        userFrRequestsRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("friendship_requests");
        notificationsRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications");

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.friends);
        //setHasOptionsMenu(true);
        if (prefs.getCurrentSubscribe() == null || !prefs.getCurrentSubscribe().equals("noti_month_test"))
            Appodeal.show(getActivity(), Appodeal.BANNER_VIEW);
        else
            Appodeal.hide(getActivity(), Appodeal.BANNER_VIEW);
        pagerAdapter = new FriendsPagerAdapter(getChildFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.setCurrentItem(0);
        pager.getAdapter().notifyDataSetChanged();
        searchFriends.setOnClickListener(v->{
            pager.setCurrentItem(1);
            pager.getAdapter().notifyDataSetChanged();
        });
        myFriends.setOnClickListener(v->{
            pager.setCurrentItem(0);

        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Appodeal.onResume(getActivity(), Appodeal.BANNER_VIEW);
    }

    @Override
    public void onStart() {
        super.onStart();
        RequestAdapter adapter = new RequestAdapter(getActivity());
        requestsHeader.setText(getString(R.string.requests_header, 0));
        friendRequests.setLayoutManager(new LinearLayoutManager(getActivity()));
        friendRequests.setAdapter(adapter);

        countRequestListener = userRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("friendship_requests"))
                    requestsHeader.setText(getString(R.string.requests_header, dataSnapshot.getChildrenCount()));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("friendship_requests"))
                    requestsHeader.setText(getString(R.string.requests_header, dataSnapshot.getChildrenCount()));
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getKey().equals("friendship_requests"))
                    requestsHeader.setText(getString(R.string.requests_header, 0));
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
        requestListener = userFrRequestsRef.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                FriendshipRequest request = dataSnapshot.getValue(FriendshipRequest.class);
                request.setUserId(dataSnapshot.getKey());
                adapter.addItem(request);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                adapter.removeItem(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

    }

    @Override
    public void onStop() {
        super.onStop();
        userFrRequestsRef.removeEventListener(requestListener);
        userRef.removeEventListener(countRequestListener);
    }
}
