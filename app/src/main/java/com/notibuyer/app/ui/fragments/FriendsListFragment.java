package com.notibuyer.app.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.FriendsAdapter;

import butterknife.Bind;


public class FriendsListFragment extends BaseFragment implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {
    @Bind(R.id.friends)
    protected RecyclerView friendsView;
    private FriendsAdapter adapter;

    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private DatabaseReference friendsRef;
    private ChildEventListener friendsListener;

    // TextView countNotifications;
    //ChildEventListener notificationsCountListener;
    //DatabaseReference notificationsRef;
    //private List<String> notifications = new ArrayList<>();

    public FriendsListFragment() {

    }

    public static FriendsListFragment newInstance() {
        FriendsListFragment fragment = new FriendsListFragment();
        return fragment;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_friends_list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        friendsRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("friends");
        // notificationsRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications");


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter = new FriendsAdapter(getActivity());
        friendsView.setLayoutManager(new LinearLayoutManager(getContext()));
        friendsView.setAdapter(adapter);
        addFriends();
        //listenNotifications();
    }

   /* public void listenNotifications(){
        notificationsCountListener = notificationsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                databaseReference.child("notifications").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Notification notification = dataSnapshot.getValue(Notification.class);
                        if (!notification.isRead() && !notifications.contains(dataSnapshot.getKey())) {
                            notifications.add(dataSnapshot.getKey());
                            if(getActivity()!=null)
                                getActivity().invalidateOptionsMenu();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }*/

    @Override
    public void onStop() {
        super.onStop();
        friendsRef.removeEventListener(friendsListener);
        //notificationsRef.removeEventListener(notificationsCountListener);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(friendsListener!=null){
            friendsRef.removeEventListener(friendsListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(friendsListener!=null){
            friendsRef.removeEventListener(friendsListener);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(friendsListener!=null){
            friendsRef.removeEventListener(friendsListener);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    public void addFriends(){
        friendsListener = friendsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {adapter.addItem(dataSnapshot.getKey());}

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {adapter.removeItem(dataSnapshot.getKey());}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {Log.e(TAG, databaseError.getMessage());}
        });
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_simple_search, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
       /* final MenuItem noti = menu.findItem(R.id.action_notifications);
        MenuItemCompat.setActionView(noti, R.layout.feed_update_count);
        noti.getActionView().setOnClickListener(v -> MainActivity.start(getActivity(), MainActivity.NOTIFICATIONS));

        countNotifications = (TextView) noti.getActionView().findViewById(R.id.hotlist_hot);
        if(notifications.size()>0) {
            countNotifications.setText(String.valueOf(notifications.size()));
            countNotifications.setVisibility(View.VISIBLE);
        } else {
            countNotifications.setVisibility(View.INVISIBLE);
        }*/
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);


    }

    @Override
    public void onResume() {
        super.onResume();
        //notifications.clear();
/*
        databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snap:dataSnapshot.getChildren()){
                    databaseReference.child("notifications").child(snap.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnap) {
                            Notification notification = dataSnap.getValue(Notification.class);
                            if(!notification.isRead() && !notifications.contains(dataSnap.getKey())){
                                notifications.add(dataSnap.getKey());
                                if(getActivity()!=null)
                                    getActivity().invalidateOptionsMenu();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

        System.out.println("ON RES");*/
    }




    //search listener overriden methods
    @Override
    public boolean onClose() {
        System.out.println("onClose");
        adapter = null;
        adapter = new FriendsAdapter(getActivity());
        friendsView.setAdapter(adapter);
        addFriends();

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        System.out.println("filter");
        showLoader();
        adapter.filter(query);
        hideLoader();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            adapter.filter("");
        }
        return true;
    }
}
