package com.notibuyer.app.ui.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.appodeal.ads.Appodeal;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.NotificationsAdapter;

import butterknife.Bind;

public class NotificationsFragment extends BaseFragment {

    private static final String LIST_STATE_KEY = "lsk";
    @Bind(R.id.notifications)
    protected RecyclerView notifications;
    @Bind(R.id.empty_container)
    protected TextView empty;
    Parcelable mListState;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private int pageSize = 10;
    private NotificationsAdapter adapter;
    private DatabaseReference notificationRef;
    private ChildEventListener notiListener;

    @Override
    protected int getContentViewId() {
        return R.layout.notifications_fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        notificationRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications");
        setHasOptionsMenu(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save list state
        mListState = notifications.getLayoutManager().onSaveInstanceState();
        outState.putParcelable(LIST_STATE_KEY, mListState);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(LIST_STATE_KEY)) {
            mListState = savedInstanceState.getParcelable(LIST_STATE_KEY);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter = new NotificationsAdapter(getActivity());
        notifications.setLayoutManager(new LinearLayoutManager(getActivity()));
        notifications.setAdapter(adapter);
        if (mListState != null)
            notifications.getLayoutManager().onRestoreInstanceState(mListState);
        notiListener = notificationRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                adapter.addItem(dataSnapshot.getKey());
                empty.setVisibility(View.GONE);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                adapter.removeItem(dataSnapshot.getKey());
                if(adapter.getItemCount()==0)
                    empty.setVisibility(View.VISIBLE);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        notificationRef.removeEventListener(notiListener);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.notifications);
        //notifications.addOnScrollListener(new ScrollListener(this));
        if (prefs.getCurrentSubscribe() == null || !prefs.getCurrentSubscribe().equals("noti_month_test"))
            Appodeal.show(getActivity(), Appodeal.BANNER_VIEW);
        else
            Appodeal.hide(getActivity(), Appodeal.BANNER_VIEW);
    }

    @Override
    public void onResume() {
        super.onResume();
        Appodeal.onResume(getActivity(), Appodeal.BANNER_VIEW);
    }
}
