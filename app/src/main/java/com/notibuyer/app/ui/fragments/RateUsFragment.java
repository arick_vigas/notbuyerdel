package com.notibuyer.app.ui.fragments;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.notibuyer.app.R;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

public class RateUsFragment extends BaseFragment {

    @Bind(R.id.container) protected RelativeLayout container;
    @Bind(R.id.header) protected TextView header;
    @Bind(R.id.description) protected TextView description;
    @Bind(R.id.cancel) protected Button cancel;
    @Bind(R.id.buttons_container) protected LinearLayout buttonsContainer;
    @Bind({R.id.star1, R.id.star2, R.id.star3, R.id.star4, R.id.star5}) List<ImageView> starList;

    @Override
    protected int getContentViewId() {
        return R.layout.rate_us_fragment;
    }

    @OnClick({R.id.star1, R.id.star2, R.id.star3, R.id.star4, R.id.star5})
    protected void starChanged(View v) {
        int position = starList.lastIndexOf(v);
        for (int i = 0; i < starList.size(); i++) {
            if (i <= position) {
                starList.get(i).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star1));
            } else {
                starList.get(i).setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star0));
            }
        }
        changedRatingBar();
    }

    private void changedRatingBar() {
        header.setText(R.string.thanks_we_like_you_too);
        description.setText(R.string.appreciate_your_effort_give_nice_review);
        cancel.setVisibility(View.GONE);
        buttonsContainer.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.cancel)
    public void cancel() {

    }

    @OnClick(R.id.not_now)
    public void notNow() {

    }

    @OnClick(R.id.review)
    public void review() {

    }
}
