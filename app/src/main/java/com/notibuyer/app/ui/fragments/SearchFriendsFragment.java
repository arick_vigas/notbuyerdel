package com.notibuyer.app.ui.fragments;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.SearchFriendsAdapter;
import com.notibuyer.app.model.Notification;
import com.notibuyer.app.model.User;
import com.notibuyer.app.network.FirebasePushService;
import com.notibuyer.app.utils.DialogFactory;
import com.notibuyer.app.utils.NetworkUtils;
import com.notibuyer.app.utils.NotificationFactory;
import com.notibuyer.app.utils.Utils;
import com.tbruyelle.rxpermissions.RxPermissions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import rx.android.schedulers.AndroidSchedulers;


public class SearchFriendsFragment extends BaseFragment implements SearchView.OnQueryTextListener, SearchView.OnCloseListener, FacebookCallback<LoginResult>, SearchFriendsAdapter.UserInvitedListener {

    DatabaseReference usersRef;
    DatabaseReference currentUserRef;
    ChildEventListener currentUserListener;
    ChildEventListener usersListener;
    @Bind(R.id.friends_search)
    RecyclerView usersList;
    TextView countNotifications;
    ChildEventListener notificationsCountListener;
    DatabaseReference notificationsRef;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private CallbackManager callbackManager;
    private SearchFriendsAdapter adapter;
    private String circleId;
    private List<String> notifications = new ArrayList<>();

    public SearchFriendsFragment() {
    }

    public static SearchFriendsFragment newInstance(String circleId) {
        SearchFriendsFragment fragment = new SearchFriendsFragment();
        Bundle args = new Bundle();
        args.putString("circleId", circleId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (usersListener != null)
        usersRef.removeEventListener(usersListener);
        if (currentUserListener != null)
        currentUserRef.removeEventListener(currentUserListener);
        //if(notificationsCountListener!=null)
        // notificationsRef.removeEventListener(notificationsCountListener);
    }
/*
    public void listenNotifications(){
        notificationsCountListener = notificationsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                databaseReference.child("notifications").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Notification notification = dataSnapshot.getValue(Notification.class);
                        if (!notification.isRead() && !notifications.contains(dataSnapshot.getKey())) {
                            notifications.add(dataSnapshot.getKey());
                            if(getActivity()!=null)
                                getActivity().invalidateOptionsMenu();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }*/

    @Override
    public void onStart() {
        super.onStart();
        if (getArguments() != null) {
            circleId = getArguments().getString("circleId");
            adapter = new SearchFriendsAdapter(getActivity(), circleId, this);
        } else {
            adapter = new SearchFriendsAdapter(getActivity(), "", this);
        }

        usersList.setLayoutManager(new LinearLayoutManager(getContext()));
        usersList.setHasFixedSize(true);
        usersList.setAdapter(null);
        usersList.setAdapter(adapter);
        addUsers();
        syncList();
        // listenNotifications();
    }

    private void syncList(){
        currentUserListener = currentUserRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("friendship_requests")){
                    for(DataSnapshot item:dataSnapshot.getChildren()){
                        adapter.removeItem(item.getKey());
                    }
                } else if(dataSnapshot.getKey().equals("friends")){
                    for(DataSnapshot item:dataSnapshot.getChildren()){
                        adapter.removeItem(item.getKey());
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("friendship_requests")){
                    for(DataSnapshot item:dataSnapshot.getChildren()){
                        adapter.removeItem(item.getKey());
                    }
                } else if(dataSnapshot.getKey().equals("friends")){
                    for(DataSnapshot item:dataSnapshot.getChildren()){
                        adapter.removeItem(item.getKey());
                    }
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_search_friends;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            circleId = getArguments().getString("circleId");
        } else {
            circleId = "";
        }

        setHasOptionsMenu(true);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        usersRef = databaseReference.child("users");
        currentUserRef = databaseReference.child("users").child(mFirebaseUser.getUid());
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, this);

        //notificationsRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications");


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    public void addUsers(){
        RxPermissions.getInstance(getActivity())
                .request(Manifest.permission.READ_CONTACTS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(granted -> {
                    if (granted) {
                        showLoader();
                        List<String> contacts = Utils.getContacts(getActivity().getContentResolver());
                        usersListener = usersRef.addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                if (contacts.contains(dataSnapshot.child("phone").getValue())) {

                                    User fromContactsUser = dataSnapshot.getValue(User.class);
                                    fromContactsUser.setFbKey(dataSnapshot.getKey());
                                    fromContactsUser.setProvider("Contacts");
                                    if(!fromContactsUser.getFbKey().equals(mFirebaseUser.getUid()))
                                        adapter.addItem(fromContactsUser);
                                }
                                hideLoader();
                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {
                                adapter.removeItem(dataSnapshot.getKey());
                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.e(TAG, databaseError.getMessage());
                                hideLoader();
                            }
                        });
                    } else {
                        DialogFactory.getInfoDialog(getActivity(), getString(R.string.contacts_permissions_not_granted)).show();
                        hideLoader();
                    }
                }, throwable -> {
                    Log.d(TAG, throwable.getMessage());
                    hideLoader();
                });

        //facebook

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            for (UserInfo profile : user.getProviderData()) {
                // Id of the provider (ex: google.com)
                String providerId = profile.getProviderId();
                System.out.println(providerId);
                // UID specific to the provider
                String uid = profile.getUid();
                if(providerId.equals("facebook.com")){
                    addFbFriends(uid);
                }
            }
        }
    }

    //search listener overriden methods
    @Override
    public boolean onClose() {
        System.out.println("onClose");
        adapter = null;
        adapter = new SearchFriendsAdapter(getContext(), circleId, this);
        usersList.setAdapter(adapter);
        addUsers();

        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        System.out.println("filter");
        showLoader();
        adapter.filter(query);
        hideLoader();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            adapter.filter("");
        }
        return true;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_simple_search, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        /*final MenuItem noti = menu.findItem(R.id.action_notifications);
        MenuItemCompat.setActionView(noti, R.layout.feed_update_count);
        noti.getActionView().setOnClickListener(v -> MainActivity.start(getActivity(), MainActivity.NOTIFICATIONS));

        countNotifications = (TextView) noti.getActionView().findViewById(R.id.hotlist_hot);
        if(notifications.size()>0) {
            countNotifications.setText(String.valueOf(notifications.size()));
            countNotifications.setVisibility(View.VISIBLE);
        } else {
            countNotifications.setVisibility(View.INVISIBLE);
        }*/
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {

    }

    @Override
    public void onCancel() {
        Log.d(TAG, "onCancel: ");
    }

    @Override
    public void onResume() {
        super.onResume();
        /*notifications.clear();

        databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snap:dataSnapshot.getChildren()){
                    databaseReference.child("notifications").child(snap.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnap) {
                            Notification notification = dataSnap.getValue(Notification.class);
                            if(!notification.isRead() && !notifications.contains(dataSnap.getKey())){
                                notifications.add(dataSnap.getKey());
                                getActivity().invalidateOptionsMenu();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });*/

        System.out.println("ON RES");
    }

    @Override
    public void onError(FacebookException error) {
        Log.e(TAG, error.getMessage());
    }

    private void addFbFriends(String uid){
        if (!NetworkUtils.isOn(getActivity())) {
            DialogFactory.getInfoDialog(getActivity(), R.string.no_internet_connection).show();
            return;
        }
        //if (isFbLoaded) return;
        if (AccessToken.getCurrentAccessToken() != null && AccessToken.getCurrentAccessToken().getToken() != null) {


            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/" + uid +"/friends",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {

                            try {
                                showLoader();
                                JSONArray friends = response.getJSONObject().getJSONArray("data");
                                for(int i=0;i<friends.length();i++){
                                    JSONObject ob = (JSONObject)friends.get(i);

                                    databaseReference.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            for(DataSnapshot data:dataSnapshot.getChildren()){
                                                try {
                                                    if(data.hasChild("facebookId") && data.child("facebookId").getValue().equals(ob.getString("id")) && !data.child("friends").hasChild(mFirebaseUser.getUid())){
                                                        databaseReference.child("users").child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(DataSnapshot snapshot) {
                                                                try {
                                                                    if(!snapshot.child("friendship_requests").hasChild(ob.getString("id"))){
                                                                        User user = data.getValue(User.class);
                                                                        user.setFbKey(data.getKey());
                                                                        user.setProvider("Facebook");
                                                                        adapter.addItem(user);
                                                                        hideLoader();
                                                                    }

                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                    hideLoader();
                                                                }
                                                            }

                                                            @Override
                                                            public void onCancelled(DatabaseError databaseError) {
                                                                Log.e(TAG, databaseError.getMessage());
                                                                hideLoader();
                                                            }
                                                        });
                                                        break;
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                    hideLoader();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            Log.e(TAG, databaseError.getMessage());
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ).executeAsync();
            hideLoader();
        } else {
            LoginManager.getInstance().logInWithReadPermissions(this, Collections.singletonList("email, user_friends, public_profile"));
        }
    }

    @Override
    public void onUserInvited(String userId, String circleId, String provider) {
        databaseReference.child("users").child(userId).child("friendship_requests").child(mFirebaseUser.getUid()).child("provider").setValue(provider);
        if (!circleId.equals(""))
            databaseReference.child("users").child(userId).child("friendship_requests").child(mFirebaseUser.getUid()).child("circle").setValue(circleId);

        Notification notification = NotificationFactory.sendFriendshipRequest();
        notification.setRead(false);
        DatabaseReference notiRef = databaseReference.child("notifications").push();
        notiRef.setValue(notification);
        databaseReference.child("users").child(userId).child("notifications").child(notiRef.getKey()).setValue(true);

        databaseReference.child("users").child(userId).child("fcm_token").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String[] tokens = {(String) dataSnapshot.getValue()};

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    try {
                        JSONArray recepients = new JSONArray(tokens);
                        FirebasePushService.sendMessage(recepients, Notification.TYPE.FRIENDSHIP.getNumVal(), Notification.ACTION.INVITE.getNumVal(), mFirebaseUser.getUid(), null, null, null);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    public void onUserInviteRemoved(String userId) {
        databaseReference.child("users").child(userId).child("friendship_requests").child(mFirebaseUser.getUid()).removeValue();
    }
}
