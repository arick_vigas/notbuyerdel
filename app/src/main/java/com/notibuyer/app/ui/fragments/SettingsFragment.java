package com.notibuyer.app.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.appodeal.ads.Appodeal;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kyleduo.switchbutton.SwitchButton;
import com.notibuyer.app.BuildConfig;
import com.notibuyer.app.R;
import com.notibuyer.app.service.ShakeService;
import com.notibuyer.app.ui.activity.ChangePasswordActivity;
import com.notibuyer.app.ui.activity.EditProfileActivity;
import com.notibuyer.app.ui.activity.TermsActivity;
import com.notibuyer.app.ui.view.CircleTransformation;
import com.notibuyer.app.ui.view.StrokeTransform;
import com.notibuyer.app.utils.NetworkUtils;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class SettingsFragment extends BaseFragment {

    public static final int CODE_EDIT_PROFILE = 1111;
    @Bind(R.id.profile_icon)
    protected ImageView profileIcon;
    @Bind(R.id.profile_name)
    protected TextView profileName;
    //    @Bind(R.id.voice_switch) protected SwitchButton voiceSwitch;
    @Bind(R.id.shake_switch)
    protected SwitchButton shakeSwitch;
    @Bind(R.id.record_on_start_switch)
    protected SwitchButton recordSwitch;
    @Bind(R.id.version)
    protected TextView appVer;
    private DatabaseReference userReference;
    private FirebaseUser mFirebaseUser;

    @Override
    protected int getContentViewId() {
        return R.layout.settings_fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.settings);
        //  if(prefs.getCurrentSubscribe()==null || !prefs.getCurrentSubscribe().equals("noti_month_test")) {
        Appodeal.show(getActivity(), Appodeal.BANNER_VIEW);
            /*shakeSwitch.setEnabled(false);
            shakeSwitch.setClickable(false);
            shakeSwitch.setFocusable(false);
            recordSwitch.setEnabled(false);
            recordSwitch.setClickable(false);
            recordSwitch.setFocusable(false);
            shakeSwitch.setOnCheckedChangeListener(null);
            recordSwitch.setOnCheckedChangeListener(null);
        } else {
           Appodeal.hide(getActivity(), Appodeal.BANNER_VIEW);
        }*/
        shakeSwitch.setChecked(prefs.isShakeEnabled());
        recordSwitch.setChecked(prefs.isAutoRecordEnabled());


        PackageInfo pInfo = null;
        int versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;
        appVer.setText("Application v" + versionName);

        if (prefs.isLoginWithFb() || prefs.isLoginWithGoogle())
            view.findViewById(R.id.change_password).setVisibility(View.GONE);
//        voiceSwitch.setChecked(false); // don't support this function yet :(
    }

    @Override
    public void onResume() {
        super.onResume();
        Appodeal.onResume(getActivity(), Appodeal.BANNER_VIEW);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        userReference = FirebaseDatabase.getInstance().getReference().child("users").child(mFirebaseUser.getUid());
    }

    @Override
    public void onStart() {
        super.onStart();
        userReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (profileName != null)
                    profileName.setText((String) dataSnapshot.child("name").getValue());
                    /*Picasso.with(getContext())
                            .load((String)dataSnapshot.child("avatar").getValue())
                            .transform(new CircleTransformation())
                            .error(R.drawable.profile_icon_error).placeholder(R.drawable.progress_loading_animation)
                            .into(profileIcon);*/
                if (getActivity() != null && !NetworkUtils.isOn(getActivity())) {
                    new Picasso.Builder(getActivity())
                            .downloader(new OkHttpDownloader(getActivity())).build()
                            .with(getActivity())
                            .load((String) dataSnapshot.child("avatar").getValue()).networkPolicy(NetworkPolicy.OFFLINE)
                            .transform(new CircleTransformation())
                            .transform(new StrokeTransform())
                            .placeholder(R.drawable.progress_loading_animation).fit().centerCrop()
                            .centerCrop().into(profileIcon);
                } else {
                    Picasso.with(getActivity())
                            .load((String) dataSnapshot.child("avatar").getValue())
                            .transform(new CircleTransformation())
                            .transform(new StrokeTransform())
                            .placeholder(R.drawable.progress_loading_animation)
                            .fit().centerCrop()
                            .into(profileIcon);
                    Log.d("Picasso", "Loaded from Server");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @OnClick(R.id.edit_profile)
    public void editProfile() {
        getActivity().startActivityForResult(new Intent(getContext(), EditProfileActivity.class), CODE_EDIT_PROFILE);
    }

    @OnClick(R.id.change_password)
    protected void changePassword() {
        startActivity(new Intent(getActivity(), ChangePasswordActivity.class));
    }

    @OnClick(R.id.privacy_settings)
    protected void privacy() {
        TermsActivity.start(getContext(), TermsActivity.MODE_PRIVACY);
    }

    @OnClick(R.id.service_terms)
    protected void serviceTerms() {
        TermsActivity.start(getContext(), TermsActivity.MODE_TERMS);
    }
/*
    @OnClick(R.id.log_out)
    protected void logout() {

        if (!NetworkUtils.isOn(getContext())) {
            showError(getString(R.string.no_internet_connection));
            return;
        }
        showLoader();

        NotificationManager notifManager= (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancelAll();
        authService.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .finallyDo(this::hideLoader)
                .subscribe(userResponse -> {
                    handleLogout();
                }, e -> handleLogout());
        }

*/
//    @OnClick(R.id.voice_start)
//    public void voiceStart() {
//        if (!voiceSwitch.isChecked()) {
//            voiceSwitch.setChecked(true);
//            prefs.setVoiceEnabled(true);
//        } else {
//            voiceSwitch.setChecked(false);
//            prefs.setVoiceEnabled(false);
//        }
//    }

    @OnClick(R.id.shake_start)
    public void shakeStart() {
        //Toast.makeText(getActivity(), R.string.feature_unavailable, Toast.LENGTH_SHORT).show();
        //if(prefs.getCurrentSubscribe()!=null && prefs.getCurrentSubscribe().equals("noti_month_test"))
        handleShakeState(!shakeSwitch.isChecked());
        //else
        //new UnleashDialog(getActivity()).show();
    }

    @OnCheckedChanged(R.id.shake_switch)
    protected void onShakeStateChanged(boolean isChecked) {
        handleShakeState(isChecked);
    }

    @OnClick(R.id.record_on_start)
    public void recordOnStart() {
        //Toast.makeText(getActivity(), R.string.feature_unavailable, Toast.LENGTH_SHORT).show();
        // if(prefs.getCurrentSubscribe()!=null && prefs.getCurrentSubscribe().equals("noti_month_test"))
        handleRecordState(!recordSwitch.isChecked());
        //  else
        //    new UnleashDialog(getActivity()).show();
    }

    @OnCheckedChanged(R.id.record_on_start_switch)
    protected void onRecordStateChanged(boolean isChecked) {
        handleRecordState(isChecked);
    }

    private void handleRecordState(boolean isChecked) {
        if (isChecked) {
            recordSwitch.setChecked(true);
            prefs.setAutoRecordEnabled(true);
        } else {
            recordSwitch.setChecked(false);
            prefs.setAutoRecordEnabled(false);
        }
    }

    private void handleShakeState(boolean isChecked) {
        if (isChecked) {
            shakeSwitch.setChecked(true);
            prefs.setShakeEnabled(true);
            ShakeService.startService(getActivity());
        } else {
            shakeSwitch.setChecked(false);
            prefs.setShakeEnabled(false);
            ShakeService.stopService(getActivity());
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_EDIT_PROFILE && resultCode == Activity.RESULT_OK) {
            //initProfile(prefs.getUser());
        }
    }


}
