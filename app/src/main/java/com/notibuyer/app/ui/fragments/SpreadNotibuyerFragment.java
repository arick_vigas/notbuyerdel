package com.notibuyer.app.ui.fragments;
import android.os.Bundle;
import android.view.View;

import com.notibuyer.app.R;
public class SpreadNotibuyerFragment extends BaseFragment {
    @Override
    protected int getContentViewId() {
        return R.layout.spread_notibuyer_fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.spread_notibuyer);
    }
}
