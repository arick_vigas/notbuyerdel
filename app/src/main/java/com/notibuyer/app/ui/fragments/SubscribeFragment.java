package com.notibuyer.app.ui.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.ConditionsAdapter;
import com.notibuyer.app.model.Condition;
import com.notibuyer.app.model.SubscribeOption;
import com.notibuyer.app.ui.activity.PremiumActivity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;


public class SubscribeFragment extends BaseFragment{

    @Bind(R.id.conditions)
    RecyclerView conditions;
    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.sub_title)
    TextView subTitle;
    @Bind(R.id.subscribe_button)
    RelativeLayout subscribeButton;
    @Bind(R.id.button_title)
    TextView buttonTitle;
    @Bind(R.id.button_desc)
    TextView buttonDescription;
    private DatabaseReference databaseReference;
    private DatabaseReference subscribeRef;
    private ChildEventListener listener;
    private ConditionsAdapter adapter;
    private String color;

    private List<SubscribeOption> subscribeOptionsList;
    private FirebaseUser mFirebaseUser;
    public SubscribeFragment() {
        // Required empty public constructor
    }


    public static SubscribeFragment newInstance(String id, String color) {
        SubscribeFragment fragment = new SubscribeFragment();
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("color", color);
        System.out.println(color + " onInstantinate");
        fragment.setArguments(args);
        return fragment;


    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_subscribe;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        if(getArguments()!=null && getArguments().containsKey("id")){
            subscribeRef = databaseReference.child("subscribes").child(getArguments().getString("id"));
            if(getArguments().containsKey("color")) {
                this.color = getArguments().getString("color");
                System.out.println(color + " onCreate");
            }
            mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            subscribeOptionsList = new ArrayList<>();
        }



    }


    @OnClick(R.id.subscribe_button)
    protected void subscribeClick() {
        if (subscribeOptionsList.size() > 1) {
        } else if (subscribeOptionsList.size() == 1) {
            String sku = subscribeOptionsList.get(0).getSku();
            ((PremiumActivity) getActivity()).subscribe(sku);
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        subscribeButton.setBackgroundColor(Color.parseColor(this.color));
    }



    @Override
    public void onStart() {
        super.onStart();

        System.out.println(color);
        listener = subscribeRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "title":
                        title.setText((String)dataSnapshot.getValue());

                        break;
                    case "subTitle":
                        subTitle.setText((String)dataSnapshot.getValue());
                        break;
                    case "conditions":
                        adapter = new ConditionsAdapter(getActivity(), color);
                        conditions.setLayoutManager(new LinearLayoutManager(getActivity()));
                        conditions.setAdapter(adapter);
                        for(DataSnapshot item:dataSnapshot.getChildren()){
                            databaseReference.child("conditions").child(item.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    Condition condition = dataSnapshot.getValue(Condition.class);
                                    adapter.addItem(condition);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e(TAG, databaseError.getMessage());
                                }
                            });
                        }
                        break;
                    case "options":
                        Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                        Iterator<DataSnapshot> iterator = children.iterator();
                        //iterator.next();

                        while (iterator.hasNext()){
                            databaseReference.child("subscribe_options").child(iterator.next().getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    SubscribeOption option = dataSnapshot.getValue(SubscribeOption.class);
                                    subscribeOptionsList.add(option);
                                    if (prefs.getCurrentSubscribe() != null) {
                                        String titleText = title.getText().toString();
                                        if (prefs.getCurrentSubscribe().equals(option.getSku()) && !titleText.contains("current")) {
                                            title.setText(titleText + " (current)");
                                            subscribeButton.setVisibility(View.GONE);
                                        }
                                    }
                                    String text = buttonDescription.getText().toString();
                                    if(text.isEmpty() || text.equals(""))
                                        buttonDescription.setText(option.getTitle());
                                    else
                                        buttonDescription.setText(text + " or " + option.getTitle());
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e(TAG, databaseError.getMessage());
                                }
                            });

                        }
                        break;
                    case "button_title":
                        buttonTitle.setText((String)dataSnapshot.getValue());
                        break;
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                switch (dataSnapshot.getKey()){
                    case "title":
                        title.setText((String)dataSnapshot.getValue());

                        break;
                    case "subTitle":
                        subTitle.setText((String)dataSnapshot.getValue());
                        break;
                    case "conditions":
                        adapter = new ConditionsAdapter(getActivity(), color);
                        conditions.setLayoutManager(new LinearLayoutManager(getActivity()));
                        conditions.setAdapter(adapter);
                        for(DataSnapshot item:dataSnapshot.getChildren()){
                            databaseReference.child("conditions").child(item.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    Condition condition = dataSnapshot.getValue(Condition.class);
                                    adapter.addItem(condition);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    Log.e(TAG, databaseError.getMessage());
                                }
                            });
                        }
                        break;
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        subscribeRef.removeEventListener(listener);
        // userSubscribeRef.removeEventListener(subscribeListener);
    }
}
