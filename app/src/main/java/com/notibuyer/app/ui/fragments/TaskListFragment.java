package com.notibuyer.app.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.notibuyer.app.R;
import com.notibuyer.app.adapter.TasksAdapter;
import com.notibuyer.app.dagger.AppComponent;
import com.notibuyer.app.model.Task;
import com.notibuyer.app.network.firebase.TaskService;
import com.notibuyer.app.ui.activity.ChooseCircleActivity;
import com.notibuyer.app.ui.activity.MainActivity;
import com.notibuyer.app.ui.view.CustomScrollView;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.Bind;

import static com.cmcm.utils.ThreadHelper.runOnUiThread;


public class TaskListFragment extends BaseFragment implements SearchView.OnQueryTextListener, SearchView.OnCloseListener{
    private static final int CHOOSE_CIRCLES_CODE = 202;
    @Bind(R.id.current_tasks_view)
    protected RecyclerView curreentTasksView;
    @Bind(R.id.completed_tasks_view)
    protected RecyclerView completedTasksView;
    @Bind(R.id.showCompleted)
    protected Button expandCollapseCompleted;
    @Bind(R.id.scrollList)
    protected CustomScrollView scroll;
    @Inject
    protected TaskService taskService;
    TasksAdapter currentTasksAdapter;
    TasksAdapter completedTasksAdapter;
    TextView countNotifications;
    DatabaseReference completedRef;
    DatabaseReference currentRef;
    DatabaseReference allRef;
    //DatabaseReference notificationsRef;
    ChildEventListener currentListener;
    ChildEventListener completedListener;
    ChildEventListener allListener;
    ChildEventListener notificationsCountListener;
    ItemTouchHelper currentItemTouchHelper;
    ItemTouchHelper completedItemTouchHelper;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private String circleId;
    private List<String> currentSelectedIds = new ArrayList<>();
    private List<String> completedSelectedIds = new ArrayList<>();
    private List<String> notifications = new ArrayList<>();

    private final int LOAD_STEP = 5;
    private int limit = 5;
    private int previousComItemsCount = 0;
    private int previousCurItemsCount = 0;
    SharedPreferences sPref; // used for keeping completed tasks visible or gone
    Timer timer; // used for setting visibility of addTaskButtons
    public static TaskListFragment getInstance(String circleId, int circlePosition) {
        TaskListFragment fragment = new TaskListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("circleId", circleId);
        bundle.putInt("circlePosition", circlePosition);
        fragment.setArguments(bundle);

        return fragment;
    }
    /*
    Timer timer1;
    boolean startedscroll,timerstarted; // for scrolling when dragging support
    int scrollerThreshold = 30;//speed of scroller when dragging
    int x=0;//used for detecting finger position up down or none
    boolean isFirstMove = true; // detecting whether it is first touch or just moved finger
    int fragmentHeight;// used for detecting bottom dragging zone */


    @Override
    protected int getContentViewId() {
        return R.layout.frag_task_list;
    }

    public void setUpComponent(AppComponent appComponent) {
        appComponent.inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        /*notifications.clear();
        if(getActivity()!=null) {
            databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot snap : dataSnapshot.getChildren()) {
                        databaseReference.child("notifications").child(snap.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnap) {
                                Notification notification = dataSnap.getValue(Notification.class);
                                if (!notification.isRead() && !notifications.contains(dataSnap.getKey())) {
                                    notifications.add(dataSnap.getKey());
                                    if(getActivity()!=null)
                                        getActivity().invalidateOptionsMenu();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });
        }*/
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (currentSelectedIds.isEmpty() && completedSelectedIds.isEmpty()) {
            inflater.inflate(R.menu.menu_simple_search, menu);
            final MenuItem item = menu.findItem(R.id.action_search);
            /*final MenuItem noti = menu.findItem(R.id.action_notifications);
            MenuItemCompat.setActionView(noti, R.layout.feed_update_count);
            noti.getActionView().setOnClickListener(v -> MainActivity.start(getActivity(), MainActivity.NOTIFICATIONS));

            countNotifications = (TextView) noti.getActionView().findViewById(R.id.hotlist_hot);
            if(notifications.size()>0) {
                countNotifications.setText(String.valueOf(notifications.size()));
                countNotifications.setVisibility(View.VISIBLE);
            } else {
                countNotifications.setVisibility(View.INVISIBLE);
            }
            */
            final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
            searchView.setOnQueryTextListener(this);
            searchView.setOnCloseListener(this);

        } else {
            System.out.println(currentSelectedIds.size());
            System.out.println(completedSelectedIds.size());
            if(!currentSelectedIds.isEmpty())
                inflater.inflate(R.menu.search_extended, menu);
            if(!completedSelectedIds.isEmpty())
                inflater.inflate(R.menu.restore_task, menu);
            inflater.inflate(R.menu.remove_task, menu);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        System.out.println(item.getItemId() + " " );
        switch(item.getItemId()){


            case R.id.add_to_circle:
                ChooseCircleActivity.startForTaskList(getActivity(), currentSelectedIds, CHOOSE_CIRCLES_CODE);
                getActivity().invalidateOptionsMenu();
                return true;
            case R.id.complete:
                for(String id:currentSelectedIds){
                    currentTasksAdapter.solveTask(id);
                }
                currentSelectedIds.clear();
                getActivity().invalidateOptionsMenu();
                return true;
            case R.id.restore:
                for(String id:completedSelectedIds){
                    completedTasksAdapter.resolveTask(id);
                }
                completedSelectedIds.clear();
                getActivity().invalidateOptionsMenu();
                return true;
            case R.id.remove:
                for(String id:currentSelectedIds){
                    currentTasksAdapter.removeTask(id);
                }
                for(String id:completedSelectedIds){
                    completedTasksAdapter.removeTask(id);
                }
                currentSelectedIds.clear();
                completedSelectedIds.clear();
                getActivity().invalidateOptionsMenu();
                return true;
        }
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        setHasOptionsMenu(true);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        circleId = getArguments().getString("circleId");
        Log.d(TAG, circleId);
        completedRef = databaseReference.child("circles").child(circleId).child("completed_tasks");
        currentRef = databaseReference.child("circles").child(circleId).child("current_tasks");
        allRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("circles");
        // notificationsRef = databaseReference.child("users").child(mFirebaseUser.getUid()).child("notifications");
        currentTasksAdapter = new TasksAdapter(getActivity(), circleId, taskService, taskDiskCache);
        completedTasksAdapter = new TasksAdapter(getActivity(), circleId, taskService, taskDiskCache);
        currentTasksAdapter.setEventListener(new MyEventLisener());
        completedTasksAdapter.setEventListener(new MyEventLisener());
    }

    @Override
    public void onStart() {
        super.onStart();
        curreentTasksView.setLayoutManager(new LinearLayoutManager(getActivity()));
        curreentTasksView.setAdapter(currentTasksAdapter);
        curreentTasksView.setNestedScrollingEnabled(false);
        completedTasksView.setNestedScrollingEnabled(false);
        if (sPref.getBoolean("isVisibleCompeted",false)) {
            expandCollapseCompleted.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.arrow_down_float, 0, 0, 0);
            completedTasksView.setVisibility(View.VISIBLE);
        }
        else {
            expandCollapseCompleted.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.arrow_up_float, 0, 0, 0);
            completedTasksView.setVisibility(View.GONE);
        }

        //timer1 = new Timer();


        ItemTouchHelper.Callback currentIthCallback = new ItemTouchHelper.Callback() {

            Drawable xMark;
            int xMarkMargin;
            int dragFrom = -1;
            int dragTo = -1;

            private void init() {

                xMark = ContextCompat.getDrawable(getActivity(), R.drawable.ic_add_circle);
               // xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                xMarkMargin = (int) getActivity().getResources().getDimension(R.dimen.fab_margin);
            }

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if(!circleId.equals("All")) {
                    int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                    int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                    return makeMovementFlags(dragFlags, swipeFlags);
                } else {
                    return makeFlag(ItemTouchHelper.ACTION_STATE_SWIPE, ItemTouchHelper.START | ItemTouchHelper.END);
                }
            }


            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int fromPosition = viewHolder.getAdapterPosition();
                int toPosition = target.getAdapterPosition();
                if(dragFrom == -1) {
                    dragFrom =  fromPosition;
                }
                dragTo = toPosition;
                currentTasksAdapter.onIndexChanged(fromPosition, toPosition, false);
                return true;
            }


            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

                super.clearView(recyclerView, viewHolder);
                TasksAdapter.TaskHolder holder = (TasksAdapter.TaskHolder)viewHolder;
                holder.getContainer().setBackground(getActivity().getResources().getDrawable(R.drawable.task_card_bg));
            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                    TasksAdapter.TaskHolder holder = (TasksAdapter.TaskHolder)viewHolder;
                    holder.getContainer().setBackground(getActivity().getResources().getDrawable(R.drawable.task_card_bg_grey));
                } else {/*
                    TasksAdapter.TaskHolder holder = (TasksAdapter.TaskHolder)viewHolder;
                    holder.getContainer().setBackground(getActivity().getResources().getDrawable(R.drawable.task_card_bg));*/
                }

                super.onSelectedChanged(viewHolder, actionState);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                if(direction == ItemTouchHelper.START) {
                    ChooseCircleActivity.startForTask(getActivity(), currentTasksAdapter.getTaskId(viewHolder.getAdapterPosition()), CHOOSE_CIRCLES_CODE);
                } else if(direction == ItemTouchHelper.END){
                    currentTasksAdapter.solveTask(viewHolder.getAdapterPosition());
                }

            }



            @Override
            public boolean isLongPressDragEnabled() {
                return true;
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }



            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){
                    TasksAdapter.TaskHolder holder = (TasksAdapter.TaskHolder)viewHolder;
                    View itemView = holder.itemView;
                    // not sure why, but this method get's called for viewholder that are already swiped away
                 if (viewHolder.getAdapterPosition() == -1) {
                     // not interested in those
                    return;
                }

                if(dX<0 && dY==0) {
                    System.out.println(dY + " posY");
                    xMark = ContextCompat.getDrawable(getActivity(), R.drawable.menu_ic_circles);
                    xMarkMargin = (int) getActivity().getResources().getDimension(R.dimen.fab_margin);
                    // draw x mark
                    int itemHeight = itemView.getBottom() - itemView.getTop();
                    int intrinsicWidth = (int)getActivity().getResources().getDimension(R.dimen.swipe_ic_height_small);
                    int intrinsicHeight = (int)getActivity().getResources().getDimension(R.dimen.swipe_ic_height_small);

                    Paint textPaint = new Paint();
                    float textMargin = getActivity().getResources().getDimension(R.dimen.swipe_text_margin);
                    float textWidth = textPaint.measureText(getActivity().getResources().getString(R.string.add_to_circle));


                    int xMarkLeft = (int)(itemView.getRight()-textMargin-textWidth*1.5);
                    int xMarkRight = xMarkLeft + intrinsicWidth;
                    int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2 - (int)(textMargin/2);
                    int xMarkBottom = xMarkTop + intrinsicHeight;

                    xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);
                    xMark.draw(c);


                    textPaint.setColor(Color.parseColor("#999999"));
                    textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                    textPaint.setTextAlign(Paint.Align.CENTER);
                    textPaint.setTextSize(getActivity().getResources().getDimension(R.dimen.text_size_medium));
                    c.drawText(getActivity().getResources().getString(R.string.add_to_circle), itemView.getRight()-textMargin-textWidth, xMarkBottom+textMargin/1.5f,textPaint);

                } else if (dX>0 &&dY==0){
                    System.out.println(dY + " posY");
                    xMark = ContextCompat.getDrawable(getActivity(), R.drawable.ic_action_tick);
                    xMarkMargin = (int) getActivity().getResources().getDimension(R.dimen.fab_margin);
                    // draw x mark
                    int itemHeight = itemView.getBottom() - itemView.getTop();
                    int intrinsicWidth = (int)getActivity().getResources().getDimension(R.dimen.swipe_ic_height);
                    int intrinsicHeight = (int)getActivity().getResources().getDimension(R.dimen.swipe_ic_height);

                    Paint textPaint = new Paint();
                    float textMargin = getActivity().getResources().getDimension(R.dimen.swipe_text_margin);
                    float textWidth = textPaint.measureText(getActivity().getResources().getString(R.string.complete));

                   //int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                   // int xMarkRight = itemView.getRight() - xMarkMargin;
                    int xMarkLeft = (int)(itemView.getLeft() + textMargin+textWidth - intrinsicWidth/2);
                    int xMarkRight = xMarkLeft + intrinsicWidth;
                    int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2- (int)(textMargin/2);
                    int xMarkBottom = xMarkTop + intrinsicHeight;

                    xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);
                    xMark.draw(c);

                    textPaint.setColor(Color.parseColor("#999999"));
                    textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                    textPaint.setTextAlign(Paint.Align.CENTER);
                    textPaint.setTextSize(getActivity().getResources().getDimension(R.dimen.text_size_medium));
                    c.drawText(getActivity().getResources().getString(R.string.complete), itemView.getLeft()+textMargin+textWidth, xMarkBottom+textMargin/3,textPaint);
                }

}

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        ItemTouchHelper.Callback completedIthCallback = new ItemTouchHelper.Callback() {
            Drawable xMark;
            int xMarkMargin;

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
               // int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeFlag(ItemTouchHelper.ACTION_STATE_SWIPE, swipeFlags);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
               //no drag!!
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                if(direction == ItemTouchHelper.START) {
                    completedTasksAdapter.removeItemByPos(viewHolder.getAdapterPosition());
                } else if(direction == ItemTouchHelper.END){
                    completedTasksAdapter.resolveTask(viewHolder.getAdapterPosition());
                }
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return true;
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

                super.clearView(recyclerView, viewHolder);
                TasksAdapter.TaskHolder holder = (TasksAdapter.TaskHolder)viewHolder;
                holder.getContainer().setBackground(getActivity().getResources().getDrawable(R.drawable.task_card_bg));

            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                    TasksAdapter.TaskHolder holder = (TasksAdapter.TaskHolder)viewHolder;
                    holder.getContainer().setBackground(getActivity().getResources().getDrawable(R.drawable.task_card_bg_grey));
                }
                super.onSelectedChanged(viewHolder, actionState);
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                TasksAdapter.TaskHolder holder = (TasksAdapter.TaskHolder)viewHolder;
                View itemView = holder.itemView;

                // not sure why, but this method get's called for viewholder that are already swiped away
                if (viewHolder.getAdapterPosition() == -1) {
                    // not interested in those
                    return;
                }


                if(dX<0 && dY==0) {
                    System.out.println(dY + " posY");
                    xMark = ContextCompat.getDrawable(getActivity(), R.drawable.menu_ic_remove);
                    xMarkMargin = (int) getActivity().getResources().getDimension(R.dimen.fab_margin);
                    // draw x mark
                    int itemHeight = itemView.getBottom() - itemView.getTop();
                    int intrinsicWidth = (int)(getActivity().getResources().getDimension(R.dimen.swipe_ic_height_small));
                    int intrinsicHeight = (int)(getActivity().getResources().getDimension(R.dimen.swipe_ic_height_small) );

                    Paint textPaint = new Paint();
                    float textMargin = getActivity().getResources().getDimension(R.dimen.swipe_text_margin);
                    float textWidth = textPaint.measureText(getActivity().getResources().getString(R.string.remove));


                    int xMarkLeft = (int)(itemView.getRight()-textMargin-textWidth - intrinsicWidth/2);
                    int xMarkRight = xMarkLeft + intrinsicWidth;
                    int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2 - (int)(textMargin/2);
                    int xMarkBottom = xMarkTop + intrinsicHeight;

                    xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);
                    xMark.draw(c);


                    textPaint.setColor(Color.parseColor("#999999"));
                    textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                    textPaint.setTextAlign(Paint.Align.CENTER);
                    textPaint.setTextSize(getActivity().getResources().getDimension(R.dimen.text_size_medium));
                    c.drawText(getActivity().getResources().getString(R.string.remove), itemView.getRight()-textMargin-textWidth, xMarkBottom+textMargin/2f,textPaint);
                } else if(dX>0 && dY==0){
                    System.out.println(dY + " posY");
                    xMark = ContextCompat.getDrawable(getActivity(), R.drawable.menu_ic_restore);

                    xMarkMargin = (int) getActivity().getResources().getDimension(R.dimen.fab_margin);
                    // draw x mark
                    int itemHeight = itemView.getBottom() - itemView.getTop();
                    int intrinsicWidth = (int)(getActivity().getResources().getDimension(R.dimen.swipe_ic_height_small)*1.25);
                    int intrinsicHeight = (int)(getActivity().getResources().getDimension(R.dimen.swipe_ic_height_small)*1.25);

                    Paint textPaint = new Paint();
                    float textMargin = getActivity().getResources().getDimension(R.dimen.swipe_text_margin);
                    float textWidth = textPaint.measureText(getActivity().getResources().getString(R.string.restore));

                    //int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
                    // int xMarkRight = itemView.getRight() - xMarkMargin;
                    //int xMarkLeft = itemView.getLeft() + xMarkMargin+ (int)textWidth;
                    int xMarkLeft = (int)(itemView.getLeft() + textWidth + textMargin - intrinsicWidth/2 );
                    int xMarkRight = xMarkLeft + intrinsicWidth;
                    int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight) / 2- (int)(textMargin/2);
                    int xMarkBottom = xMarkTop + intrinsicHeight;

                    xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);
                    xMark.draw(c);

                    textPaint.setColor(Color.parseColor("#999999"));
                    textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
                    textPaint.setTextAlign(Paint.Align.CENTER);
                    textPaint.setTextSize(getActivity().getResources().getDimension(R.dimen.text_size_medium));
                    c.drawText(getActivity().getResources().getString(R.string.restore), itemView.getLeft()+textMargin+textWidth, xMarkBottom+textMargin/3,textPaint);
                }



                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };




        currentItemTouchHelper = new ItemTouchHelper(currentIthCallback);
        completedItemTouchHelper = new ItemTouchHelper(completedIthCallback);
        currentItemTouchHelper.attachToRecyclerView(curreentTasksView);
        completedItemTouchHelper.attachToRecyclerView(completedTasksView);
        completedTasksView.setLayoutManager(new LinearLayoutManager(getActivity()));
        completedTasksView.setAdapter(completedTasksAdapter);
        curreentTasksView.setAdapter(currentTasksAdapter);

        curreentTasksView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.d("TaskListFragment", "onLongClick: ");
                return false;
            }
        });
        curreentTasksView.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                Log.d("TaskListFragment", "onDrag: ");
                return false;
            }
        });


       /* curreentTasksView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    ((AllNotesFragment) getParentFragment()).hideButtons();

                }
                if (event.getAction() == MotionEvent.ACTION_MOVE){
                    Log.d("TaskListFragment", "onMove: ");
                    // can get - values ; it's a touch, not an item
                    Log.d(TAG, "onTouch: event.getX = " + event.getX() + " event.getY = " + event.getY());
                    Log.d(TAG, "onTouch:  curreentTasksView.getChildCount(): " + curreentTasksView.getChildCount());//all the child
                    Log.d(TAG, "onTouch: curreentTasksView.getTop()" + curreentTasksView.getTop());//top
                    Log.d(TAG, "onTouch: curreentTasksView.getBottom()" + curreentTasksView.getBottom());//bottom

                    Log.d(TAG, "onTouch: getScrollY" +  scroll.getScrollY());
                    if (currentTasksAdapter.isPressed) { currentTasksAdapter.isPressed=false; timer1 = new Timer(); }
                    if (!(scroll.getScrollY()+350<event.getY() && !startedscroll)){
                        startedscroll=false; x=0; timerstarted=false; timer.cancel();
                    }
                    getActivity().findViewById(R.id.current_tasks_view).setOnDragListener(new View.OnDragListener() {
                        @Override
                        public boolean onDrag(View v, DragEvent event) {
                            Log.d(TAG, "onDrag: Event: " + event.toString());
                            return false;
                        }
                    });
                    Log.d(TAG, "onTouch: getActivity().findViewById(R.id.current_tasks_view).getHeight()" + getActivity().findViewById(R.id.current_tasks_view).getHeight());
                    if (!isFirstMove) {
                        if (scroll.getScrollY()+500 <event.getY() && !startedscroll) {
                            startedscroll=true; x=1;
                            Log.d(TAG, "onTouch: x=1");
                        }
                        if (scroll.getScrollY() + 220 > event.getY() && !startedscroll) {
                                   startedscroll = true;
                                   x = -1;
                                   Log.d(TAG, "onTouch: x=-1");
                        }
                        if (x != 0  && startedscroll ) {
                            if (timer1!=null) {
                                Log.d(TAG, "onTouch: timerstarted");
                                timer1.schedule(new scrollTimerTask2(),0,scrollerThreshold);
                                timerstarted=true; return false;
                            }
                        }
                        if (x==0 && startedscroll && timerstarted) {
                            startedscroll=false;
                            timerstarted = false;
                            timer1.cancel();
                            Log.d(TAG, "onTouch: timer stopped");
                            timer1 = null;
                        }
                        Log.d(TAG, "onTouch: x=0");  x=0;
                    }

                    if (isFirstMove && !(scroll.getScrollY()+350<event.getY() || scroll.getScrollY()+200 /*screen width >
                            event.getY()))
                        isFirstMove = false ;
                }
                if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL){
                    ((AllNotesFragment) getParentFragment()).showButtons();
                    completedTasksAdapter.isPressed = false;
                    isFirstMove = true;
                    if(timerstarted) { timerstarted = false; startedscroll=false; timer1.cancel(); timer1.purge(); timer1 = null; }
                }
                Log.d(TAG, "onTouch: event = " + event.toString() + " pressure: " + event.getPressure());
                return false;
        }
        });

*/

        expandCollapseCompleted.setOnClickListener(v -> {
            if(completedTasksView.getVisibility()==View.VISIBLE){
                expandCollapseCompleted.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.arrow_up_float, 0, 0, 0);
                completedTasksView.setVisibility(View.GONE);
                SharedPreferences.Editor ed = sPref.edit();
                ed.putBoolean("isVisibleCompeted",false);
                ed.commit();
            } else {
                expandCollapseCompleted.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.arrow_down_float, 0, 0, 0);
                completedTasksView.setVisibility(View.VISIBLE);
                SharedPreferences.Editor ed = sPref.edit();
                ed.putBoolean("isVisibleCompeted",true);
                ed.commit();

            }
        });

        if(circleId.equals("All")){
            getAllTasks();
        } else {
            syncTasks();
        }
        //   listenNotifications();
    }

   /* public void listenNotifications(){
        notificationsCountListener = notificationsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                databaseReference.child("notifications").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Notification notification = dataSnapshot.getValue(Notification.class);
                        if (!notification.isRead() && !notifications.contains(dataSnapshot.getKey())) {
                            notifications.add(dataSnapshot.getKey());
                            if(getActivity()!=null)
                                getActivity().invalidateOptionsMenu();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }
*/




    /*
    class scrollTimerTask extends TimerTask {

        @Override
        public void run() {

            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        ((AllNotesFragment) getParentFragment()).showButtons();
                    } catch (Exception e) {
                        timer.cancel();
                        Log.d("taskButtoons", "run: Error: method does not exist!!");
                    }
                }
            });
        }
    }*/
  /*  class scrollTimerTask2 extends TimerTask {

        @Override
        public void run() {

            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    scroll.scrollBy(0,x);
                }
            });
        }
    }*/

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        System.out.println("in view created");

        scroll.setOnScrollViewListener(new CustomScrollView.OnScrollViewListener() {
            public void onScrollChanged(CustomScrollView v, int l, int t, int oldl, int oldt) {
                infiniteScroll();
                Log.d("Scroller", "x = " + l + " y = " + t + " xold = " + oldl + " yold = " + oldt);
                if (t < oldt) {
                    ((AllNotesFragment) getParentFragment()).showButtons();
                      Log.d("Scroller", "i show");
                } else {
                    if (t> oldt+10) {
                        Log.d("Scroller", "i hide");
                        ((AllNotesFragment) getParentFragment()).hideButtons();
                    }
                }

                Log.d(TAG, "onScrollChanged: curreentTasksView.getChildCount()" + curreentTasksView.getChildCount());
                if (scroll.getChildAt(0).getHeight()-600 < t && !(curreentTasksView.getChildCount() <= 2) ) {
                    Log.d("Scroller", "i hide");
                    ((AllNotesFragment) getParentFragment()).hideButtons();
                }

            }
        });







        // scroll.setSmoothScrollingEnabled(true);
        if (scroll != null) {

                    /*
                    scroll.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY <= oldScrollY) {
                        ((AllNotesFragment) getParentFragment()).showButtons();
                    } else {
                        ((AllNotesFragment) getParentFragment()).hideButtons();
                    }
                }
            });*/
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(currentListener!=null)
            currentRef.removeEventListener(currentListener);
        if(completedListener!=null)
            completedRef.removeEventListener(completedListener);
    }

    @Override
    public void onStop() {
        super.onStop();


        if(allListener!=null)
            allRef.removeEventListener(allListener);
        completedItemTouchHelper.attachToRecyclerView(null);
        currentItemTouchHelper.attachToRecyclerView(null);
        currentSelectedIds.clear();
        completedSelectedIds.clear();
        //notificationsRef.removeEventListener(notificationsCountListener);
        getActivity().invalidateOptionsMenu();
     //   timer.cancel();
    }

    private void syncTasks(){

        //sync current tasks;
        currentListener = currentRef.orderByChild("index").limitToLast(limit).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                databaseReference.child("tasks").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            Task task = snapshot.getValue(Task.class);
                            task.setFbKey(snapshot.getKey());
                            currentTasksAdapter.addItem(task);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                currentTasksAdapter.removeItem(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
        //sync completed tasks
        completedListener = completedRef.orderByChild("index").limitToLast(limit).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                databaseReference.child("tasks").child(dataSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            Task task = snapshot.getValue(Task.class);
                            task.setFbKey(snapshot.getKey());
                            completedTasksAdapter.addItem(task);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, databaseError.getMessage());
                    }
                });

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                completedTasksAdapter.removeItem(dataSnapshot.getKey());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

    }

    public void getAllTasks(){
        System.out.println("onSyncAll");
        //get all available user circles
        allListener = allRef.orderByChild("index").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //sync all current tasks
                currentRef = databaseReference.child("circles").child(dataSnapshot.getKey()).child("current_tasks");
                currentListener = currentRef.orderByChild("index").limitToLast(limit/LOAD_STEP).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot taskData, String s) {
                        databaseReference.child("tasks").child(taskData.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                if (snapshot.exists()) {
                                    Task task = snapshot.getValue(Task.class);
                                    task.setFbKey(snapshot.getKey());
                                    currentTasksAdapter.addItem(task);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.d(TAG, databaseError.getMessage());
                            }
                        });
                    }

                    @Override
                    public void onChildChanged(DataSnapshot taskData, String s) {}

                    @Override
                    public void onChildRemoved(DataSnapshot taskData) {
                        currentTasksAdapter.removeItem(taskData.getKey());
                    }

                    @Override
                    public void onChildMoved(DataSnapshot taskData, String s) {}

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });

                //sync all completed tasks
                completedRef = databaseReference.child("circles").child(dataSnapshot.getKey()).child("completed_tasks");
                completedListener = completedRef.orderByChild("index").limitToLast(limit/LOAD_STEP).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot taskData, String s) {
                        System.out.println(taskData.getKey());
                        databaseReference.child("tasks").child(taskData.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                if (snapshot.exists()) {
                                    Task task = snapshot.getValue(Task.class);
                                    task.setFbKey(snapshot.getKey());
                                    completedTasksAdapter.addItem(task);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.d(TAG, databaseError.getMessage());
                            }
                        });
                    }

                    @Override
                    public void onChildChanged(DataSnapshot taskData, String s) {}

                    @Override
                    public void onChildRemoved(DataSnapshot taskData) {
                        completedTasksAdapter.removeItem(taskData.getKey());
                    }

                    @Override
                    public void onChildMoved(DataSnapshot taskData, String s) {}

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                //if user is not in circle anymore
                for(DataSnapshot currentTask:dataSnapshot.child("current_tasks").getChildren()){
                    currentTasksAdapter.removeItem(currentTask.getKey());
                }
                for(DataSnapshot completedTask:dataSnapshot.child("completed_tasks").getChildren()){
                    completedTasksAdapter.removeItem(completedTask.getKey());
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    //search listener overriden methods
    @Override
    public boolean onClose() {
        System.out.println("onClose");
        currentTasksAdapter = null;
        completedTasksAdapter = null;
        currentTasksAdapter = new TasksAdapter(getActivity(), circleId, taskService, taskDiskCache);
        completedTasksAdapter = new TasksAdapter(getActivity(), circleId, taskService, taskDiskCache);
        curreentTasksView.setAdapter(currentTasksAdapter);
        completedTasksView.setAdapter(completedTasksAdapter);
        if(prefs.getLastVisitedCircle().equals("All"))
            getAllTasks();
        else
            syncTasks();
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        showLoader();
        currentTasksAdapter.filter(query);
        completedTasksAdapter.filter(query);
        hideLoader();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            currentTasksAdapter.filter("");
            completedTasksAdapter.filter("");
        }
        return true;
    }

    class MyEventLisener implements TasksAdapter.EventListener{

        @Override
        public void onCheckBoxClicked(CompoundButton button, int position, boolean isChecked, String id, boolean completed) {

            if(completed) {
                System.out.println("completed");
                if (completedSelectedIds.contains(id)) {
                    completedSelectedIds.remove(id);
                    button.setChecked(false);
                    System.out.println("contains");
                } else {
                    completedSelectedIds.add(id);
                    button.setChecked(true);
                    System.out.println("does not contain");
                }
            } else {
                System.out.println("current");
                if (currentSelectedIds.contains(id)) {
                    System.out.println("contains");
                    currentSelectedIds.remove(id);
                    button.setChecked(false);
                } else {
                    currentSelectedIds.add(id);
                    button.setChecked(true);
                    System.out.println("does not contain");
                }
            }
            getActivity().invalidateOptionsMenu();
        }
    }

    private void infiniteScroll() {
        if(isScrolledToCompleted()) {
            loadMoreData();
        }
    }

    private void loadMoreData() {

        if( previousCurItemsCount < currentTasksAdapter.getItemCount() ||
                previousComItemsCount < completedTasksAdapter.getItemCount()) {

            previousCurItemsCount = currentTasksAdapter.getItemCount();
            previousComItemsCount = completedTasksAdapter.getItemCount();

            limit += LOAD_STEP;

            if(circleId.equals("All")) {
                getAllTasks();
            } else {
                syncTasks();
            }
        }
    }

    private boolean isScrolledToCompleted() {
        final int threshold = 350;//was 800

        int totalHeight = scroll.getChildAt(0).getHeight();
        int offsetY = scroll.getScrollY();
        int[] completedY = {0, 0};
        expandCollapseCompleted.getLocationOnScreen(completedY);

        int delta = completedY[1] - offsetY - threshold;
        if(delta < 0) {
            return  true;
        }
        return false;
    }
}
