package com.notibuyer.app.ui.fragments.startup;

import com.notibuyer.app.R;
import com.notibuyer.app.ui.fragments.BaseFragment;

public class StartUpFragment1 extends BaseFragment {

    @Override
    protected int getContentViewId() {
        return R.layout.frag_startup1;
    }
}
