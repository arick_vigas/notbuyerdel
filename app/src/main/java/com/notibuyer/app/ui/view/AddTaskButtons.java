package com.notibuyer.app.ui.view;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.notibuyer.app.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Getter;
import lombok.Setter;

public class AddTaskButtons extends LinearLayout implements Animator.AnimatorListener {

    @Setter
    protected CreateTaskListener listener;

    private boolean isVisible = true;

    public AddTaskButtons(Context context) {
        super(context);
        inflate(context);
    }

    public AddTaskButtons(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context);
    }

    public AddTaskButtons(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context);
    }

    @TargetApi(21)
    public AddTaskButtons(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(context);
    }

    private void inflate(Context context) {
        LayoutInflater.from(context).inflate(R.layout.add_task_buttons_layout, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void hide() {
        if (isVisible) {
            animate().alpha(0).setDuration(200).setListener(this).start();
            isVisible = false;
        }
    }

    public void show() {
        if (!isVisible) {
            animate().alpha(1).setDuration(200).start();
            isVisible = true;
        }
    }

    @OnClick(R.id.type)
    protected void typeClick() {
        if (listener != null) listener.createTaskWithText();
    }

    @OnClick(R.id.record)
    protected void recordClick() {
        if (listener != null) listener.createTaskWithRecord();
    }

    @OnClick(R.id.photo)
    protected void photoClick(View v) {
        if (listener != null) listener.createTaskWithPhoto(v);
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        setVisibility(isVisible ? VISIBLE : GONE);
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    public interface CreateTaskListener {
        void createTaskWithText();
        void createTaskWithRecord();
        void createTaskWithPhoto(View v);
    }
}
