package com.notibuyer.app.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.notibuyer.app.R;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Setter;

public class AudioPlayerView extends LinearLayout {

    @Bind(R.id.progress)
    protected ProgressBar progressBar;
    @Bind(R.id.seconds)
    protected TextView time;
    @Bind(R.id.play)
    protected ImageView playButton;
    @Bind(R.id.stop)
    protected ImageView stopButton;
    @Setter
    private AudioPlaybackPress audioPlaybackPress;
    private boolean isPlaying;

    public AudioPlayerView(Context context) {
        super(context);
        inflate(context);
    }

    public AudioPlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context);
    }

    public AudioPlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context);
    }

    @TargetApi(21)
    public AudioPlayerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(context);
    }

    private void inflate(Context context) {
        LayoutInflater.from(context).inflate(R.layout.player_view, this, true);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        progressBar.getProgressDrawable().setColorFilter(
                Color.GRAY, android.graphics.PorterDuff.Mode.SRC_IN);
        time.setText("0:00");
        playButton.setVisibility(VISIBLE);
        stopButton.setVisibility(GONE);
    }

    @OnClick(R.id.play)
    protected void play() {
        if (!isPlaying) {
            start();
            playButton.setVisibility(GONE);
            stopButton.setVisibility(VISIBLE);
        }
        if (audioPlaybackPress != null) audioPlaybackPress.playerStateChanged(isPlaying);

    }

    @OnClick(R.id.stop)
    protected void stopClick(){
        if (isPlaying) {
            stop();

        }
        if (audioPlaybackPress != null) audioPlaybackPress.playerStateChanged(isPlaying);
    }

    @OnClick(R.id.delete)
    protected void delete() {
        if (audioPlaybackPress != null) audioPlaybackPress.onAudioRemove();
    }

    public void setMaxValue(int max) {
        progressBar.setMax(max);
    }

    public void updateProgress(int progress) {
        progressBar.setProgress(progress);
        int minutes = (int) Math.ceil(progress / 1000 / 60);
        int seconds = (int) Math.ceil(progress / 1000 - minutes * 60);
        time.setText(String.format("%d:%02d", minutes, seconds));
    }

    public void start() {
        isPlaying = true;

    }

    public void stop() {
        isPlaying = false;
        progressBar.setProgress(0);
        time.setText("0:00");
        stopButton.setVisibility(GONE);
        playButton.setVisibility(VISIBLE);
    }

    public interface AudioPlaybackPress {
        void playerStateChanged(boolean isPlaying);
        void onAudioRemove();
    }
}
