package com.notibuyer.app.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.notibuyer.app.R;
import com.notibuyer.app.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

public class CircleListLayout extends LinearLayout {

    private List<? extends View> circles = new ArrayList<>();
    private int margins;
    private int w;
    private boolean configured;

    public CircleListLayout(Context context) {
        super(context);
        inflate();
    }

    public CircleListLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }

    public CircleListLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate();
    }

    @TargetApi(21)
    public CircleListLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.w = MeasureSpec.getSize(widthMeasureSpec);
        if (w > 0 && !configured && circles != null && !circles.isEmpty()) {
            setUp();
            configured = true;
        }
    }

    private void inflate() {
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(params);
        setOrientation(VERTICAL);
        margins = (int) UIUtils.pxFromDp(getContext(), 2);
    }

    public void addCircles(List<? extends View> circles) {
        this.circles = circles;
        configured = false;
        invalidate();
    }


    public void setUp() {
        removeAllViews();
        int childrenWidth = 0;
        LinearLayout layout = new LinearLayout(getContext());
        layout.setOrientation(HORIZONTAL);
        if (circles != null) {
            for (int i = 0; i < circles.size(); i++) {
                circles.get(i).measure(0, 0);
                int childWidth = circles.get(i).getMeasuredWidth();
                if (childWidth + childrenWidth + margins < w) {
                    childrenWidth += childWidth + margins;
                    layout.addView(circles.get(i));
                    continue;
                } else {
                    childrenWidth = 0;
                    addView(layout);
                    layout = new LinearLayout(getContext());
                    layout.setOrientation(HORIZONTAL);
                    layout.addView(circles.get(i));
                    childrenWidth += childWidth + margins;
                }

            }
            addView(layout);
            configured = true;

        }

    }
}