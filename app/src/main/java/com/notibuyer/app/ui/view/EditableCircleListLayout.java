package com.notibuyer.app.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.notibuyer.app.Constants;
import com.notibuyer.app.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

public class EditableCircleListLayout extends RelativeLayout {

    private List<? extends View> mTags = new ArrayList<>();
    private int mWidth;
    private boolean mInitialized = false;

    int lineMargin;
    int tagMargin;
    int textPaddingLeft;
    int textPaddingRight;
    int textPaddingTop;
    int texPaddingBottom;

    public EditableCircleListLayout(Context ctx) {
        super(ctx, null);
        initialize(ctx, null, 0);

    }

    public EditableCircleListLayout(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
        initialize(ctx, attrs, 0);
    }

    public EditableCircleListLayout(Context ctx, AttributeSet attrs, int defStyle) {
        super(ctx, attrs, defStyle);
        initialize(ctx, attrs, defStyle);
    }

    private void initialize(Context ctx, AttributeSet attrs, int defStyle) {
        ViewTreeObserver mViewTreeObserber = getViewTreeObserver();
        mViewTreeObserber.addOnGlobalLayoutListener(() -> {
            if (!mInitialized) {
                mInitialized = true;
                drawTags();
            }
        });

        // get AttributeSet
        this.lineMargin = (int) UIUtils.pxFromDp(this.getContext(), Constants.DEFAULT_LINE_MARGIN);
        this.tagMargin = (int) UIUtils.pxFromDp(this.getContext(), Constants.DEFAULT_TAG_MARGIN);
        this.textPaddingLeft = (int) UIUtils.pxFromDp(this.getContext(), Constants.DEFAULT_TAG_TEXT_PADDING_LEFT);
        this.textPaddingRight = (int) UIUtils.pxFromDp(this.getContext(), Constants.DEFAULT_TAG_TEXT_PADDING_RIGHT);
        this.textPaddingTop = (int) UIUtils.pxFromDp(this.getContext(), Constants.DEFAULT_TAG_TEXT_PADDING_TOP);
        this.texPaddingBottom = (int) UIUtils.pxFromDp(this.getContext(), Constants.DEFAULT_TAG_TEXT_PADDING_BOTTOM);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        if (width <= 0)
            return;
        mWidth = getMeasuredWidth();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawTags();
    }

    private void drawTags() {
        if (!mInitialized) {
            return;
        }
        // clear all tag
        removeAllViews();
        // layout padding left & layout padding right
        float total = getPaddingLeft() + getPaddingRight();

        int listIndex = 1;// List Index
        int indexBottom = 1;// The Tag to add below
        int indexHeader = 1;// The header tag of this line
        View tagPre = null;
        for (View item : mTags) {
            final int position = listIndex - 1;

            // calculate　of tag layout width

            item.setId(listIndex);
            item.measure(0, 0);
            float tagWidth = item.getMeasuredWidth();

            LayoutParams tagParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            //add margin of each line
            tagParams.bottomMargin = lineMargin;

            if (mWidth <= total + tagWidth + UIUtils.pxFromDp(this.getContext(), 10)) {
                //need to add in new line
                tagParams.addRule(RelativeLayout.BELOW, indexBottom);
                // initialize total param (layout padding left & layout padding right)
                total = getPaddingLeft() + getPaddingRight();
                indexBottom = listIndex;
                indexHeader = listIndex;
            } else {
                //no need to new line
                tagParams.addRule(RelativeLayout.ALIGN_TOP, indexHeader);
                //not header of the line
                if (listIndex != indexHeader) {
                    tagParams.addRule(RelativeLayout.RIGHT_OF, listIndex-1);
                    tagParams.leftMargin = tagMargin;
                    total += tagMargin;
                    if (tagPre != null && tagPre.getMeasuredWidth() < item.getMeasuredWidth()) {
                        indexBottom = listIndex;
                    }
                }
            }
            total += tagWidth;
            addView(item, tagParams);
            tagPre = item;
            listIndex++;
        }
    }

    public void setTags(List<? extends View> tags) {
        this.mTags = tags;
        drawTags();
    }

    public List<? extends View> getTags() {
        return mTags;
    }

    public void remove(int position) {
        if (position < mTags.size()) {
            mTags.remove(position);
            drawTags();
        }
    }
}
