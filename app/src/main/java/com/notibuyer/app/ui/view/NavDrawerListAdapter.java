package com.notibuyer.app.ui.view;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.notibuyer.app.R;

import java.util.ArrayList;

public class NavDrawerListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private DatabaseReference databaseReference;
    private FirebaseUser mFirebaseUser;
    public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems) {
        this.context = context;
        this.navDrawerItems = navDrawerItems;
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        this.navDrawerItems = navDrawerItems;
    }


    @Override
    public int getCount() {
        return navDrawerItems.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (getItemViewType(position) == 1) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.divider, parent, false);
        } else {
            if (position > navDrawerItems.size() - 1 - 1) position--;
            if (convertView == null) {
                LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                convertView = mInflater.inflate(R.layout.drawer_menu_item, parent, false);
            }

            ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
            TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
            TextView txtCount = (TextView) convertView.findViewById(R.id.counter);
            if(imgIcon!=null)
            imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
            if(txtTitle!=null)
            txtTitle.setText(navDrawerItems.get(position).getTitle());
            System.out.println(navDrawerItems.get(position).getTitle() + " " + position);

            final int finalPosition = position;
            databaseReference.child("users").child(mFirebaseUser.getUid()).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        if (dataSnapshot.getKey().equals("notifications_unread") && finalPosition == 1) {
                            txtCount.setText(String.valueOf(dataSnapshot.getValue()));
                        }
                        if(dataSnapshot.getKey().equals("circles") && finalPosition ==2) {
                            txtCount.setText(String.valueOf(dataSnapshot.getChildrenCount()-1));
                        } else if(dataSnapshot.getKey().equals("friends") && finalPosition ==3)
                            txtCount.setText(String.valueOf(dataSnapshot.getChildrenCount()));
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        if (dataSnapshot.getKey().equals("notifications_unread")) {
                            navDrawerItems.get(1).setCount(String.valueOf(dataSnapshot.getValue()));
                            notifyDataSetChanged();
                        }
                        if(dataSnapshot.getKey().equals("circles") && finalPosition ==2) {
                            txtCount.setText(String.valueOf(dataSnapshot.getChildrenCount()-1));
                        } else if(dataSnapshot.getKey().equals("friends") && finalPosition ==3)
                            txtCount.setText(String.valueOf(dataSnapshot.getChildrenCount()));
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getKey().equals("circles") && finalPosition ==2) {
                            txtCount.setText("");
                        } else if(dataSnapshot.getKey().equals("friends") && finalPosition ==3)
                            txtCount.setText("");
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e("DRAWER_ADAPTER", databaseError.getMessage());
                    }
                });

            if (navDrawerItems.get(position).getCounterVisibility()) {
                //txtCount.setText(navDrawerItems.get(position).getCount());
            } else {
                if(txtCount!=null)
                txtCount.setVisibility(View.GONE);
            }
        }

        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        return position == navDrawerItems.size() - 1 ? 1 : 0;
    }
}
