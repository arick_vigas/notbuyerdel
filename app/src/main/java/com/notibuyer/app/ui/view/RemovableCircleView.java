package com.notibuyer.app.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.notibuyer.app.R;
import com.notibuyer.app.model.Circle;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Setter;

public class RemovableCircleView extends RelativeLayout {

    @Setter
    private Circle circle;

    @Setter
    private RemoveClickListener removeClickListener;

    public Circle getCircle(){
        return circle;
    }
    public RemovableCircleView(Context context) {
        super(context);
        inflate(context);
    }

    public RemovableCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context);
    }

    public RemovableCircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context);
    }

    @TargetApi(21)
    public RemovableCircleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(context);
    }

    private void inflate(Context context) {
        LayoutInflater.from(context).inflate(R.layout.removable_circle, this, true);
    }

    public void setUp(Circle circle) {
        this.circle = circle;
        RelativeLayout container = (RelativeLayout) findViewById(R.id.circle_container);
        TextView tv = (TextView) findViewById(R.id.circle);
        tv.setText(circle.getName());
        GradientDrawable gd = (GradientDrawable) container.getBackground().getCurrent();
        gd.setColor(Color.parseColor(circle.getColor()));
        gd.setStroke(0, 0);
        findViewById(R.id.remove).setOnClickListener((v) -> {
            if (removeClickListener != null) {
            removeClickListener.onRemoveCircle(circle);
        }});
    }

    public interface RemoveClickListener {
        void onRemoveCircle(Circle circle);
    }
}