package com.notibuyer.app.ui.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.notibuyer.app.R;
import com.notibuyer.app.model.Circle;

import lombok.Getter;
import lombok.Setter;

public class TagCircleView extends LinearLayout {

    @Setter
    @Getter
    private Circle circle;

    public TagCircleView(Context context) {
        super(context);
        inflate(context);
    }

    public TagCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context);
    }

    public TagCircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context);
    }

    @TargetApi(21)
    public TagCircleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        inflate(context);
    }

    private void inflate(Context context) {
        LayoutInflater.from(context).inflate(R.layout.tag_circle, this, true);
    }

    public void setUp(Circle circle) {
        if (circle == null) return;
        this.circle = circle;
        RelativeLayout container = (RelativeLayout) findViewById(R.id.circle_container);
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        TextView tv = (TextView) findViewById(R.id.circle);
        databaseReference.child("circles").child(circle.getFbKey()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("name")) {
                    tv.setText((String)dataSnapshot.getValue());
                }
                if(dataSnapshot.getKey().equals("color")) {
                    GradientDrawable gd = (GradientDrawable) container.getBackground().getCurrent();
                    gd.setColor(Color.parseColor((String)dataSnapshot.getValue()));
                    gd.setStroke(0, 0);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals("name")) {
                    tv.setText((String)dataSnapshot.getValue());
                }
                if(dataSnapshot.getKey().equals("color")) {
                    GradientDrawable gd = (GradientDrawable) container.getBackground().getCurrent();
                    gd.setColor(Color.parseColor((String)dataSnapshot.getValue()));
                    gd.setStroke(0, 0);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("TAGCIRCLEVIEW", databaseError.getMessage());
            }
        });

    }
}