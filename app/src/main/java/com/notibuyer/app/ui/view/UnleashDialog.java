package com.notibuyer.app.ui.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.notibuyer.app.R;
import com.notibuyer.app.ui.activity.PremiumActivity;

/**
 * Created by alexander on 21.09.2016.
 */
public class UnleashDialog extends Dialog implements
        android.view.View.OnClickListener {
    public Button yes;
    private Activity activity;

    public UnleashDialog(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    public UnleashDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected UnleashDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.subscribe_dialog);
        yes = (Button) findViewById(R.id.btn_yes);
        yes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                activity.startActivity(new Intent(activity, PremiumActivity.class));
                activity.finish();
                break;
        }
    }
}
