package com.notibuyer.app.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.notibuyer.app.R;
import com.notibuyer.app.interfaces.CreateTaskListener;
import com.notibuyer.app.interfaces.RestorePasswordListener;

public final class DialogFactory {

    public DialogFactory() {
    }

    public static AlertDialog.Builder getInfoDialog(Context context, String msg) {
        return new AlertDialog.Builder(context)
                .setMessage(msg)
                .setPositiveButton(R.string.ok, null);
    }

    public static AlertDialog.Builder getInfoDialog(Context context, @StringRes int res) {
        return new AlertDialog.Builder(context)
                .setMessage(context.getString(res))
                .setPositiveButton(R.string.ok, null);
    }

    public static AlertDialog.Builder getInfoDialog(Context context, String msg, DialogInterface.OnClickListener listener) {
        return new AlertDialog.Builder(context)
                .setMessage(msg)
                .setPositiveButton(R.string.ok, listener);
    }

    public static AlertDialog.Builder getConfirmationDialog(Context context, @StringRes int res, DialogInterface.OnClickListener listener) {
        return new AlertDialog.Builder(context)
                .setMessage(context.getString(res))
                .setPositiveButton(R.string.ok, listener)
                .setNegativeButton(R.string.cancel, null);
    }

    public static AlertDialog.Builder getSomeDialog(Context context, @StringRes int res, @StringRes int positiveText,
                                                    @StringRes int negativeText,
                                                    DialogInterface.OnClickListener positiveListener,
                                                    DialogInterface.OnClickListener negativeListener) {
        return new AlertDialog.Builder(context)
                .setMessage(context.getString(res))
                .setPositiveButton(context.getString(positiveText), positiveListener)
                .setNegativeButton(context.getString(negativeText), negativeListener)
                .setNeutralButton(R.string.cancel, null);
    }

    public static AlertDialog.Builder getLoadingDialog(@NonNull Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_loader_layout, null, false);
        return new AlertDialog.Builder(context, R.style.TransparentAlertDialog)
                .setView(view)
                .setCancelable(false);
    }

    public static AlertDialog.Builder getNewTaskDialog(Context context, String text,
                                                       DialogInterface.OnClickListener negativeListener,
                                                       CreateTaskListener createTaskListener) {
        final EditText editText = new EditText(context);
        FrameLayout container = new FrameLayout(context);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        float margin = UIUtils.dpFromPx(context, 150);
        lp.setMargins(Math.round(margin), 0, Math.round(margin), 0);
        editText.setLayoutParams(lp);
        editText.setText(text);
        container.addView(editText);
        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.new_task))
                .setView(container)
                .setPositiveButton(R.string.create, (dialog, which) -> {
                    createTaskListener.textChanged(editText.getText().toString(), true);
                })
                .setNeutralButton(R.string.details, (dialog, which) -> {
                    createTaskListener.textChanged(editText.getText().toString(), false);
                })
                .setNegativeButton(R.string.cancel, negativeListener);
    }

    public static AlertDialog.Builder getEditTaskDialog(Context context, String text,
                                                       DialogInterface.OnClickListener negativeListener,
                                                       CreateTaskListener createTaskListener) {
        final EditText editText = new EditText(context);
        FrameLayout container = new FrameLayout(context);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        float margin = UIUtils.dpFromPx(context, 150);
        lp.setMargins(Math.round(margin), 0, Math.round(margin), 0);
        editText.setLayoutParams(lp);
        editText.setText(text);
        container.addView(editText);
        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.new_task))
                .setView(container)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    createTaskListener.textChanged(editText.getText().toString(), false);
                })
                .setNegativeButton(R.string.cancel, negativeListener);
    }

    public static AlertDialog.Builder getNewTaskSpeechDialog(Context context, String text,
                                                       DialogInterface.OnClickListener negativeListener,
                                                       CreateTaskListener createTaskListener) {
        final EditText editText = new EditText(context);
        FrameLayout container = new FrameLayout(context);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        float margin = UIUtils.dpFromPx(context, 150);
        lp.setMargins(Math.round(margin), 0, Math.round(margin), 0);
        editText.setLayoutParams(lp);
        editText.setText(text);
        container.addView(editText);
        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.new_task))
                .setView(container)
                .setPositiveButton(R.string.create, (dialog, which) -> {
                    createTaskListener.textChanged(editText.getText().toString(), true);
                })
                .setNegativeButton(R.string.repeat, negativeListener)
                .setNeutralButton(R.string.details, (dialog, which) -> {
                    createTaskListener.textChanged(editText.getText().toString(), false);
                });

    }

    public static AlertDialog.Builder getEditTaskDialog(Context context, String text,
                                                       CreateTaskListener createTaskListener) {
        final EditText editText = new EditText(context);
        FrameLayout container = new FrameLayout(context);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        float margin = UIUtils.dpFromPx(context, 150);
        lp.setMargins(Math.round(margin), 0, Math.round(margin), 0);
        editText.setLayoutParams(lp);
        editText.setText(text);
        container.addView(editText);
        return new AlertDialog.Builder(context)
                .setView(container)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    createTaskListener.textChanged(editText.getText().toString(), false);
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> {
                    dialog.dismiss();
                });
    }

    public static AlertDialog.Builder getResporePasswordDialog(Context context,
                                                       RestorePasswordListener listener) {
        final EditText editText = new EditText(context);
        FrameLayout container = new FrameLayout(context);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        float margin = UIUtils.dpFromPx(context, 150);
        lp.setMargins(Math.round(margin), 0, Math.round(margin), 0);
        editText.setLayoutParams(lp);
        container.addView(editText);
        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.enter_email))
                .setView(container)
                .setCancelable(true)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    listener.onEmailReceived(editText.getText().toString());
                });
    }
}