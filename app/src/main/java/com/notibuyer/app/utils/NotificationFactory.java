package com.notibuyer.app.utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.notibuyer.app.model.Notification;

import java.util.Date;

/**
 * Created by alexander on 20.08.2016.
 */

public class NotificationFactory {
    private static FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private static Notification notification;

    public static Notification sendFriendshipRequest(){
        notification = new Notification();
        notification.setType(Notification.TYPE.FRIENDSHIP.getNumVal());
        notification.setAction(Notification.ACTION.INVITE.getNumVal());
        notification.setUserId(mFirebaseUser.getUid());
        notification.setDate(new Date().getTime());
        return notification;
    }

    public static Notification acceptFriendshipRequest(){
        notification = new Notification();
        notification.setType(Notification.TYPE.FRIENDSHIP.getNumVal());
        notification.setAction(Notification.ACTION.ACCEPT.getNumVal());
        notification.setUserId(mFirebaseUser.getUid());
        notification.setDate(new Date().getTime());
        return notification;
    }

    public static Notification shareCircle(String circleId){
        notification = new Notification();
        notification.setType(Notification.TYPE.CIRCLE.getNumVal());
        notification.setAction(Notification.ACTION.SHARE.getNumVal());
        notification.setUserId(mFirebaseUser.getUid());
        notification.setCircleId(circleId);
        notification.setDate(new Date().getTime());
        return notification;
    }

    public static Notification outOfCircle(String circleId){
        notification = new Notification();
        notification.setType(Notification.TYPE.CIRCLE.getNumVal());
        notification.setAction(Notification.ACTION.QUIT.getNumVal());
        notification.setUserId(mFirebaseUser.getUid());
        notification.setCircleId(circleId);
        notification.setDate(new Date().getTime());
        return notification;
    }

    public static Notification createTaskNotification(String taskId, long actionCode){
        notification = new Notification();
        notification.setType(Notification.TYPE.TASK.getNumVal());
        notification.setAction(actionCode);
        notification.setUserId(mFirebaseUser.getUid());
        notification.setDate(new Date().getTime());
        notification.setTaskId(taskId);
        return notification;
    }

    public static Notification addComment(String taskId, String commentId){
        notification = new Notification();
        notification.setTaskId(taskId);
        notification.setCommentId(commentId);
        notification.setType(Notification.TYPE.COMMENT.getNumVal());
        notification.setAction(Notification.ACTION.CREATE.getNumVal());
        notification.setDate(new Date().getTime());
        notification.setUserId(mFirebaseUser.getUid());
        return notification;
    }
}
