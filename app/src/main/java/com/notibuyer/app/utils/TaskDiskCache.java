package com.notibuyer.app.utils;

import android.content.Context;
import android.os.Environment;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class TaskDiskCache {

    private File cacheDir;

    @Inject
    public TaskDiskCache(Context context) {
        this.cacheDir = new File(context.getCacheDir(), context.getPackageName());
        cacheDir.mkdir();
    }

    public void saveAudio(String id, InputStream audioStream) {
        try {
            File taskDir = new File(cacheDir, id);
            taskDir.mkdir();
            File audio = new File(taskDir, "audio.amr");
            audio.createNewFile();
            Utils.copyInputStreamToFile(audioStream, audio);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void removeTaskPicture(String id) {
        File taskDir = new File(cacheDir, id);
        new File(taskDir, "picture.png").delete();
    }

    public void removeTaskAudio(String id) {
        File taskDir = new File(cacheDir, id);
        new File(taskDir, "audio.amr").delete();
    }

    public void savePicture(String id, File picture) {
        try {
            File taskDir = new File(cacheDir, id);
            taskDir.mkdir();
            FileUtils.copyFileToDirectory(picture, taskDir);
            File file = new File(taskDir, "picture.png");
            picture.renameTo(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File getAudioForTask(String id) {
        File taskDir = new File(cacheDir, id);
        taskDir.mkdir();
        return new File(taskDir, "audio.amr");
    }

    public File getPictureForTask(String id) {
        File taskDir = new File(cacheDir, id);
        taskDir.mkdir();
        return new File(taskDir, "picture.png");
    }
}
