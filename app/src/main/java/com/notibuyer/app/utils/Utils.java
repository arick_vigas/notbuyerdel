package com.notibuyer.app.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.notibuyer.app.R;

import org.joda.time.DateTime;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public final class Utils {

    private static String countryIso;

    private Utils() {
    }

    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(String password) {
        return !TextUtils.isEmpty(password) && password.length() > 5;
    }

    public static boolean passwordsEqual(String password, String confirmPassword) {
        return !TextUtils.isEmpty(password) && !TextUtils.isEmpty(confirmPassword) && password.equals(confirmPassword);
    }

    public static String getCountryIso(Context context) {
        if (countryIso == null) {
            countryIso = ((TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE)).getNetworkCountryIso().toUpperCase();
        }
        if (TextUtils.isEmpty(countryIso)) return "US";
        return countryIso;
    }

    @Nullable
    public static String formatPhoneNumber(Context context, @Nullable String phoneNumber) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            String curLocale = getCountryIso(context);
            Phonenumber.PhoneNumber number = phoneUtil.parse(phoneNumber, curLocale);
            return phoneUtil.format(number, PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (Exception e) {
            return null;
        }
    }

    public static void copyInputStreamToFile(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap decodeUri(Context c, Uri uri, final int requiredSize) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth
                , height_tmp = o.outHeight;
        int scale = 1;

        while(true) {
            if(width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }

    @NonNull
    public static File getFile(Context context, Bitmap bitmap) {
        File file = new File(context.getCacheDir().getPath(), String.valueOf(System.currentTimeMillis()));
        FileOutputStream out;
        try {
            if (!file.exists())
                file.createNewFile();
            out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    public static String getFormattedDate(Context context, Date javaDate) {
        DateTime dateTime = new DateTime(javaDate);
        DateTime today = new DateTime();
        int minutes = dateTime.getMinuteOfHour();

        String time = String.format("%s:%s", dateTime.hourOfDay().getAsText(), minutes>9?minutes:"0"+minutes);
        if (dateTime.isEqual(today)) {
            return String.format("%s, %s", context.getString(R.string.today), time);
        } else if (dateTime.isEqual(today.minusDays(1))) {
            return String.format("%s, %s", context.getString(R.string.yesterday), time);
        }
        String date = dateTime.monthOfYear().getAsText(Locale.US).substring(0, 3) + " " + dateTime.dayOfMonth().getAsText(Locale.US) + Utils.getLastDigitSuffix(dateTime.getDayOfMonth());
        return String.format("%s, %s", date, time);
    }

    public static String getLastDigitSuffix(int number) {
        switch ((number < 20) ? number : number % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);

        return retVal;
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap compressAndRotate(String source) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(source, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, 800);

        options.inJustDecodeBounds = false;
        Bitmap decoded = BitmapFactory.decodeFile(source, options);
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(source);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            switch(orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    decoded = Utils.rotateImage(decoded, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    decoded = Utils.rotateImage(decoded, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    decoded = Utils.rotateImage(decoded, 270);
                    break;
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    decoded = flip(bitmap, true, false);
                    break;
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    decoded = flip(bitmap, false, true);
                    break;
                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    break;
                // etc.
            }
        } catch (IOException e) {
            return decoded;
        }
        return decoded;

        /*ByteArrayOutputStream out = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, out);

        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(source);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            switch(orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    decoded = Utils.rotateImage(decoded, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    decoded = Utils.rotateImage(decoded, 180);
                    break;
                // etc.
            }
        } catch (IOException e) {
            return decoded;
        }
        return decoded;*/
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int resolution) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > resolution || width > resolution) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= resolution
                    && (halfWidth / inSampleSize) >= resolution) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static boolean isCompleted(String str) {
        if ("1".equals(str)) return true;
        else if ("0".equals(str)) return false;
        return false;
    }

    public static List<String> getContacts(ContentResolver cr) {
        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        if (phones == null) return Collections.emptyList();
        List<String> contacts = new ArrayList<>();
        while (phones.moveToNext()) {
            contacts.add(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replace(" ", ""));
        }
        phones.close();
        return contacts;
    }

    public static String getPhoneNumber(Context context) {
        TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return tMgr.getLine1Number();
    }

    public static boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable(activity);
        return resultCode == ConnectionResult.SUCCESS;
    }

    public static String getDateFormatted(Long date){
        return null;
    }
}
